
/*
 * the book_manager caches name/number mapping that it finds in
 * BOOK/cache so that these mapping are available for logon/logoff even
 * the database is inaccessable
 *
 * the format if the file is simply "typenumber number name\n"
 *
 */

#include	<stdio.h>
#include	<string.h>

static void open_cachef(char *mode);

static FILE *cachef = NULL;
static int next_ent(int *type, int *num, char **name)
{
    char linebuf[1024];
    static char nm[200];
    int n, t;

    if (cachef == NULL)
	open_cachef("r");

    if (cachef == NULL)
	return 0;

    if (fgets(linebuf, sizeof(linebuf), cachef)== NULL)
    {
	fclose(cachef);
	cachef = NULL;
	return 0;
    }

    nm[0]=0;
    sscanf(linebuf, "%d %d %[^\n]\n", &t, &n, nm);
    if (nm[0])
    {
	*type = t;
	*num = n;
	*name = nm;
	return 1;
    }
    else
    {
	fclose(cachef);
	cachef = NULL;
	return 0;
    }
}

static void open_cachef(char *mode)
{
    cachef = fopen("/var/book/cache", mode);
}

void reset_cache()
{
    if (cachef)
	fclose(cachef);
    cachef = NULL;
}

int get_mappingint_cache(char *name, int type)
{
    int t, n;
    char *nm;
    
    reset_cache();
    while (next_ent(&t, &n, &nm))
    {
	if (t == type && strcmp(name, nm)==0)
	    return n;
    }
    return -1;
}

char *get_mappingchar_cache(int num, int type)
{
    int t, n;
    char *nm;
    
    reset_cache();
    while (next_ent(&t, &n, &nm))
    {
	if (t == type && num == n)
	    return strdup(nm);
    }
    return NULL;
}
