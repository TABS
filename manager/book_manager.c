 /* this program receives tokens from the network with information */
 /* about other nodes in the laboratory.  It fills in private */
 /* information  and passes the token on to the next *available* node */
 /* in the laboratory */

/* #define TESTING */

#include	<unistd.h>
#include	<stdlib.h>
#include	<time.h>
#include	<signal.h>
#include	<sys/wait.h>
#include	<sys/time.h>
#include	<sys/file.h>
#include	"helper.h"
#ifdef sun
#include	<sys/fcntl.h>
#endif


char errbuf[200];

/* static struct timeval SHORTTIMEOUT = {10, 0};*/
int sendtable; /* the last time the table was sent on to another node */

bool_t am_master;
int last_send = 0;


/* this routine is called when an interupt is received to send the */
 /* table on somewhere else */
SIGRTN set_counter()
{
	set_bookdebug_level();
	sendtable++;
	sprintf(errbuf,"NODE_STATUS Now sendtable %d\n",sendtable);
	Debug(errbuf,2);

	signal(SIGALRM, set_counter);
}


int fake_alloc = 0;
int main (int argc, char *argv[])
{
	static struct timeval wait = {5,0};
	static struct timeval totwait = {15,0};
	int st;
	time_t last_check = 0;
    
	am_master = FALSE;

	set_counter();

	pmap_settimeouts(wait, totwait);

	/* get the rpc handles set up */
	open_status_client("localhost");
	if (status_client == NULL)
	{
		shout("Node_status cannot init_bind\n");
		exit(1);
	}

	while ((st = load_config()) == -2)
		sleep(30);


	check_servers();
    
	if (st == -1)		/* not registered in booking system, so nothing to do */
		exit(0);

	get_attr_types();
	get_impl_list();

	/* settimeouts for creating client handles*/

	if (argc>1)
	{
		fake_alloc = 1;
	}

	send_new_table();		/* make sure the table structure is recent */
	sendtable = 1;
	while (1)
	{
		if (time(0) - last_check > 37*60)
		{
			last_check = time(0);
			check_servers();
		}
		if (sendtable)
		{
			int nexttime;
			sprintf(errbuf, "NODESTATUS wakeup %d\n",(int)time(0));
			Debug(errbuf,1);
			alarm(0);
			sendtable = 0; 
			nexttime = propagate_table();
			if (nexttime > 2) alarm(nexttime);
			else sendtable = 1;

			sprintf(errbuf,"NODESTATUS sleep %d\n", (int)time(0));
			Debug(errbuf,1);
		}
		if (sendtable==0) pause();
	}
	exit(0);
}
