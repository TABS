#include <windows.h>
#include <wtsapi32.h>
#include <time.h>
#include "helper.h"

static char current_username[32];
static int current_uid;
static time_t current_when;

int get_whoison(char *wsname, time_t *when)
{
    BYTE sid_buffer[256];
    DWORD sid_buffer_length = sizeof(sid_buffer);
    TCHAR domain[256];
    DWORD domain_length = sizeof(domain);
    SID_NAME_USE sid_name_use;
    PSID psid;
    char *username;
    DWORD length, rid;
    int uid;

    if (strcmp(my.hostname, wsname) != 0)
	return -1;

    if (!WTSQuerySessionInformation(WTS_CURRENT_SERVER_HANDLE, WTS_CURRENT_SESSION,
				WTSUserName, &username, &length))
    {
	shout("WTSQuerySessionInformation failed\n");
	return -1;
    }

    if (!username[0])
	return -1;

    if (strncmp(current_username, username, sizeof(current_username)-1) == 0)
    {
	WTSFreeMemory(username);
	*when = current_when;
	return current_uid;
    }

    strncpy(current_username, username, sizeof(current_username)-1);
    current_username[sizeof(current_username)-1] = 0;
    WTSFreeMemory(username);

#if 0
    /* map username to uid using YP */
    p = ypmatch(username, "passwd.byname");
    if (p == NULL)
	return -1;

    p = strchr(p, ':'); /* skip username */
    if (p == NULL)
	return -1;

    p = strchr(p+1, ':'); /* skip password */
    if (p == NULL)
	return -1;

    uid = strtol(p+1, &p, 10);
    if (*p != ':')
	return -1;
#else
    /* map username to uid using Samba */
    if (!LookupAccountName(NULL, current_username, sid_buffer, &sid_buffer_length, domain, &domain_length, &sid_name_use))
    {
	shout("LookupAccountName failed\n");
	current_username[0] = 0;
	return -1;
    }
    psid = (PSID)sid_buffer;
    if (!IsValidSid(psid))
    {
	shout("SID not valid\n");
	current_username[0] = 0;
	return -1;
    }

    if ((*GetSidSubAuthorityCount(psid)) == 0)
    {
	shout("No subauthorities\n");
	current_username[0] = 0;
	return -1;
    }
    rid = *GetSidSubAuthority(psid, (*GetSidSubAuthorityCount(psid))-1);
    uid = (rid - 1000)/2; /* Samba algorithmic mapping */
#endif

    current_uid = uid;
    current_when = time(when);
    return uid;
}

int send_message(char *mesg)
{
    char *title = "Booking System Message";
    DWORD response;
    return WTSSendMessage(WTS_CURRENT_SERVER_HANDLE, WTS_CURRENT_SESSION,
				title, strlen(title), mesg, strlen(mesg),
				MB_OK, 0, &response, FALSE) ? 0 : -1;
}

void force_logoff(char *wsname)
{
    WTSLogoffSession(WTS_CURRENT_SERVER_HANDLE, WTS_CURRENT_SESSION, FALSE);
}
