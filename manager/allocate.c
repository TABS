
#include	<unistd.h>
#include	<stdlib.h>
#include	<time.h>
#include	"helper.h"
/*
 * allocations:
 *
 * 1/ For each lab in table that needs allocation
 *   a/ get all bookings
 *   b/ determine which bookings should be considered "tentative"
 *		This is if they are allocated, but not logged in
 *   c/ sort bookings putting late and tentative bookings last
 *   d/ find out which workstations have to-be-allocated users on
 *      and if they fit the requirement, mark them
 *   e/ sort available workstations putting
 *		un-used first
 *		then people who have not been booked for a long time
 *		then booked users who are not now allocated
 *		then workstations with to-be-allocated users
 *   f/ allocate bookings to workstations looking forward for
 *      a workstation that the user is on.
 *      Determine what bookings cannot be honoured
 * 2/ Rebuild table with new workstation reserve records and and
 *    refund records
 * 3/ Lodge the new table with the server
 * 4/ inform the server as to how each booking was dealt with
 * 5/ Done.
 *
 */

/*
 * for each lab we need
 *  The original list of bookings
 *  An array containing pointers plus some extra info per booking
 *    1/ where allocated
 *    2/ if tentative (no, change status to match)
 *  Array containing list of workstation and table indexes && state pointers
 *    and whether to-be-allocated user is on
 *  How many workstation are up, how many allocated
 */

typedef struct labinfo
{
	struct labinfo *next;
	entityid	labid;
	bookhead	booklist;
	struct bk
	{
		booklist b;
		entityid *whereto;
		int	 num_alloc;
		int	 already_on;
	} *bookings;
	int bookcnt;
	struct ws
	{
		entityid 		wsid;
		int			status_index;
		int			res_index;
		workstation_state	*wsstate;
		struct bk		*booking;
		book_status		newstatus;
		int			sorttype; /* unused, used, to_be_alloc, broken */
		time_t			atime; /* when booking made, or last alloc */
		int			whofor; /* if 2B_ALLOC, who is on or reserved */
	} *ws;
	int ws_total;
	int ws_up;
	int ws_alloc;
	int alloc_index;
	int doy, pod;
	time_t alloctime;
	book_status	newstatus;
} *labinfo;

#define WS_UNUSED	0
#define	WS_USED		1
#define	WS_2B_ALLOC	2
#define	WS_BROKEN	3


static void free_labinfo(labinfo lab)
{
	if (lab)
	{
		free_labinfo(lab->next);
		xdr_free((xdrproc_t)xdr_bookhead, (char*) &lab->booklist);
		if (lab->bookings)
			free(lab->bookings);
		if (lab->ws)
			free(lab->ws);
		free(lab);
	}
}

static void update_table(table *tab, labinfo lab)
{
	int *d;
	int failed_allocs;
	int w,b;
    
	/* for each workstation in ws,
	 *  set time, whofor, status
	 * for each booking
	 *  if not-allocated, record in lab field
	 * set lab allocate time and status
	 *
	 */
	for (w=0; w < lab->ws_total ; w++)
	{
		/* make sure the data field is long enough */
		cluster *r = &tab->table_val[lab->ws[w].res_index];
		if (r->data.data_len < 3)
		{
			r->data.data_val = (int*)realloc(r->data.data_val, 3*sizeof(int));
			r->data.data_len = 3;
			r->data.data_val[2] = 0;
		}
	
		Cluster_modtime(tab, lab->ws[w].res_index) = lab->alloctime;
		Res_status2(tab, lab->ws[w].res_index) = 0;
		if (lab->ws[w].newstatus == NODEOUTOFSERVICE
		    || lab->ws[w].newstatus == NODEBROKEN
		    || lab->ws[w].newstatus == NODENOTBOOKABLE
			)
		{
			if (lab->newstatus == LABCLOSED)
			{ /* FIXME maybe should get evictions to check lab status rather than this hack */
				Res_user(tab, lab->ws[w].res_index) = -2;
				Res_status(tab, lab->ws[w].res_index) = NODEALLOCATED;
				Res_status2(tab, lab->ws[w].res_index) = 2;
			}
			else
			{
				Res_status(tab, lab->ws[w].res_index) =
					lab->ws[w].newstatus;
				Res_user(tab, lab->ws[w].res_index) = -1;
			}
		}
		else
			switch (lab->newstatus)
			{
			default: break; /* keep -Wall quite */
			case LABCLOSED:
				Res_user(tab, lab->ws[w].res_index) = -2;
				Res_status(tab, lab->ws[w].res_index) = NODEALLOCATED;
				Res_status2(tab, lab->ws[w].res_index) = 2;
				break;
			case PERIODFREE: /* synonym for not closed and not open */
				Res_user(tab, lab->ws[w].res_index) = -1;
				Res_status(tab, lab->ws[w].res_index) = NODENOTALLOC;
				break;
			case PERIODFULL: /* really means "OPEN" at this stage */
				if (lab->ws[w].booking == NULL)
				{
					/* nothing allocated here */
					Res_user(tab, lab->ws[w].res_index) = -1;
					Res_status(tab, lab->ws[w].res_index) = NODENOTALLOC;
				}
				else
				{
					Res_user(tab, lab->ws[w].res_index) =
						lab->ws[w].booking->b->who_for;
					if (query_bit(&lab->ws[w].booking->b->mreq, attr_types.firmalloc))
					{
						Res_status(tab, lab->ws[w].res_index) = NODEALLOCATED;
						Res_status2(tab, lab->ws[w].res_index) = 1; /* Firm alloc */
					}
					else
					{
						Res_status(tab, lab->ws[w].res_index) =
							lab->ws[w].booking->b->status == B_TENTATIVE?
							NODETENTATIVE : NODEALLOCATED;
						Res_status2(tab, lab->ws[w].res_index) = 0; /* loose alloc */
					}
					/* if already on, update whenalloc */
					if (lab->ws[w].booking->b->who_for ==
					    Ws_user(tab, lab->ws[w].status_index))
					{
						Ws_whenalloc(tab, lab->ws[w].status_index) = lab->alloctime;
						lab->ws[w].booking->already_on= 1;
					}
		    
				}
		
				break;
			}
	}

	failed_allocs = 0;
	for (b=0 ; b<lab->bookcnt ; b++)
	{
		if (lab->bookings[b].num_alloc == 0
		    && lab->bookings[b].b->who_for < get_class_min())
			failed_allocs++;
	}
	tab->table_val[lab->alloc_index].timestamp = lab->alloctime;
	tab->table_val[lab->alloc_index].data.data_len =
		failed_allocs + 3; /* status, who, when failed */
	free(tab->table_val[lab->alloc_index].data.data_val);
	d = tab->table_val[lab->alloc_index].data.data_val =
		(int*)malloc(sizeof(int)*
			     tab->table_val[lab->alloc_index].data.data_len);

	if (lab->newstatus == PERIODFULL)
	{
		int spare = lab->ws_up - lab->ws_alloc;
		if (lab->ws_total <=0 || ((spare*100) / lab->ws_total) >= 10)
			lab->newstatus = PERIODFREE;
	}
	d[0] = lab->newstatus;
	d[1] = my.hostid;
	d[2] = time(0);
	failed_allocs = 0;
	for (b=0 ; b<lab->bookcnt ; b++)
	{
		if (lab->bookings[b].num_alloc == 0
		    && lab->bookings[b].b->who_for < get_class_min())
			d[3+(failed_allocs++)] = lab->bookings[b].b->who_for;
	}

}

static void make_ws_list(labinfo lab, table *tab)
{
	int ind;
	int ws;

	for (ind = 0 ; ind < my.hgdata.labs.entitylist_len ; ind++)
		if (my.hgdata.labs.entitylist_val[ind] == lab->labid)
			break;

	if (ind == my.hgdata.labs.entitylist_len)
		bailout("make_ws_list: cannot find lab in hostgroup", 2);
	lab->ws_total = my.labdata[ind].workstations.entitylist_len;
	lab->ws = (struct ws*)malloc(sizeof(struct ws) * lab->ws_total);

	for (ws = 0 ; ws < lab->ws_total ; ws++)
	{
		int i;
		lab->ws[ws].wsid = my.labdata[ind].workstations.entitylist_val[ws];
		for (i=0 ; i<tab->table_len ; i++)
			switch(Cluster_type(tab,i))
			{
			default: break; /* keep -Wall quite */
			case WSRESCLUSTER:
				if (Cluster_id(tab,i) == lab->ws[ws].wsid)
					lab->ws[ws].res_index = i;
				break;
			case WSSTATUSCLUSTER:
				if (Cluster_id(tab,i) == lab->ws[ws].wsid)
					lab->ws[ws].status_index = i;
				break;
			}
		lab->ws[ws].wsstate = get_wsstate(lab->ws[ws].wsid, 1);
		lab->ws[ws].booking = NULL;
		lab->ws[ws].newstatus = 0;
	}
}

static void calc_ws_state(labinfo lab, table *tab)
{
	/* for each workstation
	 *  if out of service, mark OUTOFSERVICE
	 *  if down, mark BROKEN
	 *  if controling host is down, mark BROKEN
	 *  else mark NOTALLOC
	 *
	 *  calculate ws_up on the way
	 */
	int w;
	int up = 0;

	for (w=0 ; w<lab->ws_total ; w++)
	{
		int st;
		int i;
	
		if (query_bit(&lab->ws[w].wsstate->state, attr_types.available) == 0)
			st = NODEOUTOFSERVICE;
		/*
		 * FIXME
		 * somehow status_index was 8388608.
		 #0  0x804a671 in calc_ws_state (lab=0x80677e8, tab=0x805ba7c) at /tmp_amd/glass/import/1/neilb/Common/work/book/manager/allocate2.c:279
		 279             else if (Ws_status(tab, lab->ws[w].status_index) == NODEDOWN)
		 (gdb) where
		 #0  0x804a671 in calc_ws_state (lab=0x80677e8, tab=0x805ba7c) at /tmp_amd/glass/import/1/neilb/Common/work/book/manager/allocate2.c:279
		 #1  0x804b850 in allocate (tab=0x805ba7c) at /tmp_amd/glass/import/1/neilb/Common/work/book/manager/allocate2.c:643
		 #2  0x804d170 in propagate_table () at /tmp_amd/glass/import/1/neilb/Common/work/book/manager/pass_table.c:292
		 #3  0x80498ca in main (argc=1, argv=0xbffffd5c) at /tmp_amd/glass/import/1/neilb/Common/work/book/manager/book_manager.c:97
		 (gdb) print w
		 $1 = 3
		 (gdb) print lab->ws[w]
		 $2 = {wsid = 264, status_index = 8388608, res_index = 0, wsstate = 0x8062814, booking = 0x0, newstatus = 0, sorttype = 33554432, atime = 1023676987, 
		 whofor = 16777216}

		*/
		else if (Ws_status(tab, lab->ws[w].status_index) == NODEDOWN)
			st = NODEBROKEN;
		else if (lab->ws[w].wsstate->host >= 0 &&
			 (i = lookup_table(tab, HOSTCLUSTER, lab->ws[w].wsstate->host )) > 0
			 && Host_status(tab,i) == NODEDOWN)
			st = NODEBROKEN;
		else
		{
			item d;
			desc_cpy(&d, &lab->ws[w].wsstate->state);
			process_exclusions(lab->pod, lab->doy, &d);
			if (!query_bit(&d, attr_types.open))
				st = NODENOTBOOKABLE;
			else
				st = NODENOTALLOC;
			free(d.item_val);
		}

		lab->ws[w].newstatus = st;

		if (st == NODENOTALLOC)
		{
			up++;
			if (Ws_user(tab, lab->ws[w].status_index) >= 0)
			{
				/* atime should reflect how much un-booked time
				 * has been used
				 */
				lab->ws[w].sorttype = WS_USED;
				lab->ws[w].atime = Ws_whenalloc(tab, lab->ws[w].status_index);
			}
			else
				lab->ws[w].sorttype = WS_UNUSED;
		}
		else
			lab->ws[w].sorttype = WS_BROKEN;
	}
	lab->ws_up = up;
}

static int ws_cmp(const void *av, const void *bv)
{
	const struct ws *a=av, *b=bv;
	/* compare two workstations.
	 * unused come first
	 * then inuse, but not to-be-alloc
	 * then to-be-alloc
	 * then broken or outofservice
	 */

	if (a->sorttype != b->sorttype)
		return a->sorttype - b->sorttype;

	switch(a->sorttype)
	{
	case WS_UNUSED:
		return a->wsid - b->wsid;
	case WS_USED:
		return a->atime - b->atime; /* longer without booking -> earlier in list */
	case WS_2B_ALLOC: /* used but user booked */
		return b->atime - a->atime; /* an earlier booking sorts to end */
	case WS_BROKEN: /* not available */
		return a->wsid - b->wsid;
	default: return 0; /* keep -Wal quiet */
	}
}

static void sort_ws_list(labinfo lab)
{
	qsort(lab->ws, lab->ws_total, sizeof(struct ws), ws_cmp);
}

static int get_bk_list(labinfo lab)
{
	/* get the list of bookings for this lab
	 * and make the bookings list of pending bookings
	 */
	char k[20];
	int cnt;
	booklist bp;

	lab->booklist.data = NULL;
	switch (db_read(bookkey2key(k, make_bookkey(lab->doy, lab->pod, lab->labid)),
			&lab->booklist, (xdrproc_t)xdr_bookhead, BOOKINGS))
	{
	case R_RESOK:
		break;

	case R_NOMATCH:
		lab->booklist.data = NULL;
		break;
	default:
		return -1;
	}
	cnt = 0;
	for (bp= lab->booklist.data ; bp ; bp=bp->next)
		if (bp->status == B_PENDING)
			cnt++;
	lab->bookings = (struct bk*)malloc(sizeof(struct bk) * cnt);
	lab->bookcnt = cnt;
	cnt = 0;
	for (bp= lab->booklist.data ; bp ; bp=bp->next)
		if (bp->status == B_PENDING)
		{
			lab->bookings[cnt].b = bp;
			lab->bookings[cnt].whereto = (entityid*)malloc(sizeof(entityid)*bp->number);
			lab->bookings[cnt].num_alloc = 0;
			lab->bookings[cnt].already_on=0;
			cnt++;
		}
	return 0;
}


static void pre_allocate(labinfo lab, table *tab)
{
	/* for each bookings,
	 *  if user is allocated in lab >5minutes ago, change to tentative
	 *  else if user is a defaulter, change to tentative;
	 *  if user is logged on anywhere, change sorttype, and reset booking type
	 */
	int b;
	for (b=0; b<lab->bookcnt ; b++)
	{
		int w;
		for (w=0 ; w<lab->ws_total ; w++)
		{
			if ((Res_status(tab, lab->ws[w].res_index) == NODEALLOCATED ||
			     Res_status(tab, lab->ws[w].res_index) == NODETENTATIVE)
			    && Res_user(tab, lab->ws[w].res_index) == lab->bookings[b].b->who_for
			    && Cluster_modtime(tab, lab->ws[w].res_index) < time(0)-5*60 /*FIXME not time(0) */
				)
				lab->bookings[b].b->status = B_TENTATIVE;
		}
		if (lab->bookings[b].b->status != B_TENTATIVE
		    && is_defaulter(lab->bookings[b].b->who_for))
			lab->bookings[b].b->status = B_TENTATIVE;
		/*
		 * NOTE:
		 * IF 'a' is allocated to 'w' but doesn't show up,
		 *   and 'b' logs on
		 *   and then 'a' claims the booking at 17minutes past
		 *   and 'a' and 'b' are both booked for there next period
		 *   then who gets 'w'?? Both have a claim.
		 * The following code will pick whichever booking is first in the list,
		 *     which is as arbitrary as anything else.
		 */
		for (w=0 ; w<lab->ws_total ; w++)
		{
			/* FIXME next test should be for all labs */
			if (lab->ws[w].sorttype == WS_USED
			    && Ws_user(tab, lab->ws[w].status_index) == lab->bookings[b].b->who_for)
			{
				lab->ws[w].sorttype = WS_2B_ALLOC;
				lab->ws[w].whofor = lab->bookings[b].b->who_for;
				lab->ws[w].atime = lab->bookings[b].b->time;
				lab->bookings[b].b->status = B_PENDING;
			}
			if ((lab->ws[w].sorttype == WS_USED || lab->ws[w].sorttype == WS_UNUSED)
			    && Res_status(tab, lab->ws[w].res_index) == NODEALLOCATED
			    && Res_user(tab, lab->ws[w].res_index) == lab->bookings[b].b->who_for
			    && Cluster_modtime(tab, lab->ws[w].res_index) > time(0)-5*60)
			{
				/* a recent allocation, allocate same again */
				lab->ws[w].sorttype = WS_2B_ALLOC;
				lab->ws[w].whofor = lab->bookings[b].b->who_for;
				lab->ws[w].atime = lab->bookings[b].b->time;
				lab->bookings[b].b->status = B_PENDING;
			}
		}
	}
}

static int bk_cmp(const void *av, const void *bv)
{
	const struct bk *a=av, *b=bv;
	/* sort pending first, then tentative
	 * with in those, early bookings first 
	 */

	if (a->b->status != b->b->status)
		return a->b->status - b->b->status;
	return a->b->time - b->b->time;
}

static void bk_sort(labinfo lab)
{
	qsort(lab->bookings, lab->bookcnt, sizeof(struct bk), bk_cmp);
}



static void do_allocate(labinfo lab, table *tab)
{
	/* for each booking, 
	 * look for workstation already logged on
	 * and allocate if right attributes
	 * else allocate to first free ws with right attributes
	 */
	int b;
	for (b=0 ; b<lab->bookcnt ; b++)
	{
		int first = 0;
		while (first != -1
		       && lab->bookings[b].num_alloc < lab->bookings[b].b->number)
		{
			int w;
			first = -1;
			for (w=0 ; w < lab->ws_up ; w++)
				if (lab->ws[w].booking == NULL
				    && desc_member(&lab->bookings[b].b->charged,
						   &lab->ws[w].wsstate->state) == 1
				    && desc_member(&lab->ws[w].wsstate->state,
						   &lab->bookings[b].b->charged) == 1
					)
				{
					/* potential workstation */
					if (first < 0) first = w;
					if (lab->ws[w].sorttype == WS_2B_ALLOC &&
					    lab->ws[w].whofor == lab->bookings[b].b->who_for)
					{
						/* user already logged on here, or recently allocated */
						first = w;
					}
				}
			if (first >= 0)
			{
				lab->ws[first].booking = & lab->bookings[b];
				lab->bookings[b].whereto[lab->bookings[b].num_alloc] = lab->ws[first].wsid;
				lab->bookings[b].num_alloc ++;
				lab->ws_alloc++;
			}
		}
	}
}

static labinfo get_labs(table *tab, time_t then)
{
	/* make linked list of labinfos for each ALLOCCLUSTER in
	 * the table
	 */
	labinfo rv = NULL;
	int c;

	for (c=0 ; c < tab->table_len ; c++)
		if (Cluster_type(tab, c)== ALLOCCLUSTER)
		{
			extern int fake_alloc;
			time_t alloctime;
			int olddoy, oldpod;
			int doy,pod;
			alloctime = Cluster_modtime(tab, c);
			get_doypod(&alloctime, &olddoy, &oldpod);

			if (fake_alloc)
				alloctime += 30*60;
			else
				alloctime = then;
			get_doypod(&alloctime, &doy, &pod);
			if (doy != olddoy || pod != oldpod)
			{
				/* this lab needs allocation */
				labinfo l = (labinfo)malloc(sizeof(struct labinfo));
				l->labid = Cluster_id(tab, c);
				l->booklist.data = NULL;
				l->bookings = NULL;
				l->bookcnt = 0;
				l->ws = NULL;
				l->ws_total = l->ws_up = l->ws_alloc = 0;
				l->alloc_index = c;
				l->alloctime = alloctime;
				l->doy = doy;
				l->pod = pod;
				l->newstatus = lab_status(doy, pod, l->labid);
				l->next = rv;
				rv = l;
			}
		}
	return rv;
}

static void record_table(table *tab)
{
	FILE *f = fopen("/var/book/book.dump", "a");
	if (f)
	{
		print_tab(tab, f);
		fclose(f);
	}
}

static void record_alloc(labinfo lab)
{
	/* create a book_chng list and send to server */
	book_ch_list chlist;
	book_ch_ent *bp;
	book_change ch;
	int i;

	if (lab->bookcnt)
	{
		chlist.book_ch_list_len = lab->bookcnt;
		chlist.book_ch_list_val = bp = (book_ch_ent*)malloc(sizeof(book_ch_ent)*lab->bookcnt);

		for (i=0; i<lab->bookcnt ; i++, bp++)
		{
			bp->user = lab->bookings[i].b->who_for;
			bp->when = lab->bookings[i].b->time;
			bp->priv_refund = 0;
			bp->tentative_alloc = 
				lab->bookings[i].b->status == B_TENTATIVE;
			if (lab->bookings[i].already_on)
				bp->tentative_alloc |= 2;
			if (lab->bookings[i].num_alloc == 0)
				bp->status = B_REFUNDED, bp->priv_refund = 1;
			else if (lab->newstatus == PERIODFREE)
				bp->status = B_FREEBIE;
			else if (query_bit(&lab->bookings[i].b->mreq, attr_types.reusable))
				bp->status = B_RETURNED;
			else
				bp->status = B_ALLOCATED;
			if (lab->bookings[i].num_alloc > 0)
			{
				bp->allocations.entitylist_len = lab->bookings[i].num_alloc;
				bp->allocations.entitylist_val = (entityid*)memdup(lab->bookings[i].whereto, lab->bookings[i].num_alloc * sizeof(entityid));
			}
			else
			{
				bp->allocations.entitylist_len = 0;
				bp->allocations.entitylist_val = NULL;
			}
		}

		ch.chng = CHANGE_BOOKING;
    
		ch.book_change_u.changes.slot = make_bookkey(lab->doy, lab->pod, lab->labid);
		ch.book_change_u.changes.chlist = chlist;
		send_change(&ch);
		free(chlist.book_ch_list_val);
	}
}

int allocate(table *tab)
{
	labinfo labs;
	time_t then, cutoff;
	labinfo l;
	extern int fake_alloc;
    
	cutoff = 0;
	cutoff = get_configint("alloctime");
	if (cutoff <= 0 )
		cutoff = 60*7; /* 7 minutes by default */

	then = time(0) + cutoff;
    
	labs = get_labs(tab, then);
	if (labs == NULL)
		return 0; /* no allocation needed */
    

	for (l=labs ; l ; l=l->next)
		get_bk_list(l);
	for (l=labs ; l ; l=l->next)
		make_ws_list(l, tab);
	for (l=labs ; l ; l=l->next)
		calc_ws_state(l, tab);
	for (l=labs ; l ; l=l->next)
		pre_allocate(l, tab);
    
	for (l=labs ; l ; l=l->next)
		sort_ws_list(l);

	for (l=labs ; l ; l=l->next)
		bk_sort(l);
    
	for (l=labs ; l ; l=l->next)
		do_allocate(l, tab);

	for (l=labs ; l ; l=l->next)
		update_table(tab, l);

	record_table(tab);

	if (fake_alloc)
		abort();
    
	for (l=labs ; l ; l=l->next)
		record_alloc(l);
    
	free_labinfo(l);
	return 1;
}
