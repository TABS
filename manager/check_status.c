
#include	<unistd.h>
#include	<stdlib.h>
#include	<time.h>
#include	"helper.h"
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<netdb.h>
#include	<netinet/tcp.h>


/* check_status
 * ping any workstation connected to me
 * and check whoison file for any workstation
 * logged onto me, or not logged on and not tied to non-me host
 *
 * only check if info > 5 minutes old
 *
 */

static int can_ping(entityid wsid)
{
    /* attempt to ping by connecting to X11 port */
    char whost[200];
    char *colon;
    int port;
    struct hostent *he;
    struct sockaddr_in sin;
    int sock;
    char *wsname = get_mappingchar(wsid, M_WORKSTATION);
    if (wsname == NULL) return FALSE;
    strcpy(whost, wsname);
    free(wsname);
    colon = strchr(whost, ':');
    if (colon == NULL) return TRUE; /* assume it is there?? */
    *colon++ = '\0';
    he = gethostbyname(whost);
    if (he == NULL)
    {
	strcat(whost, ".x"); /* FIXME change out xterminal names in booking system */
	he = gethostbyname(whost);
    }
    if (he == NULL)
	return FALSE;
    port = 6000 + atoi(colon);
    memset(&sin, 0, sizeof(sin));
    memcpy(&sin.sin_addr, he->h_addr_list[0], he->h_length);
    sin.sin_port = htons(port);
    sin.sin_family = AF_INET;
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) return TRUE;
#ifdef TCP_CONN_ABORT_THRESHOLD
    {
	int thresh;
	thresh = 5*1000;
	setsockopt(sock, IPPROTO_TCP, TCP_CONN_ABORT_THRESHOLD, (char*)&thresh, sizeof(thresh));
#ifdef TCP_CONN_NOTIFY_THRESHOLD
	setsockopt(sock, IPPROTO_TCP, TCP_CONN_NOTIFY_THRESHOLD, (char*)&thresh, sizeof(thresh));
#endif
    }
#endif
    if (connect(sock, (struct sockaddr *)&sin, sizeof(sin))== 0)
    {
	close(sock);
	return TRUE;
    }
    else
    {
	close(sock);
	return FALSE;
    }
}


#ifdef WIN32
/* since there are no book_login/book_logout, we are responsible for updating things here,
   hence need a more frequent update cycle */
#define CHECK_FREQUENCY	10
#else
#define CHECK_FREQUENCY	5*60
#endif

void check_status(table *t)
{
    time_t now = time(0);
    time_t then = now - CHECK_FREQUENCY;
    static time_t firsttime = 0;
    int ind;

    /* make sure all workstations get pings ASAP after reboot */
    if (firsttime == 0) firsttime = now;
    if (then < firsttime) then = firsttime;
    
    for (ind = 0 ; ind < t->table_len && now < then + CHECK_FREQUENCY+20; ind++)
	if (Cluster_type(t,ind) == WSSTATUSCLUSTER
	    && Cluster_modtime(t,ind) < then
	    )
	{
	    cluster *cl = &t->table_val[ind];
	    workstation_state *ws = get_wsstate(cl->identifier,0);
	    if ((ws_user(cl) >= 0 && ws_whereon(cl) == my.hostid)
		||
		(ws &&
		 ws_user(cl) < 0
		 && ( ws->host == my.hostid || ws->host < 0)
		 )
		)
	    {
		/* logged onto me, or
		 *  nobody logged in, and not someone elses
		 */
		int who;
		time_t when;
		who = check_whoison(cl->identifier, &when);
		if (who != cl->data.data_val[0])
		{
		    /* this info is WRONG, correct it */
		    cl->data.data_val[0] = who;
		    if (who >= 0)
		    {
			ws_whenon(cl) = when;
			ws_whereon(cl) = my.hostid;
		    }
		    else
			ws_whereon(cl) = -1;
		}

		if (ws)
		{
		    if (ws->host == my.hostid
			|| (am_master && ws->host <= 0))
		    {
			if (can_ping(cl->identifier))
			    ws_status(cl) = NODEUP;
			else
			    ws_status(cl) = NODEDOWN;
		    }
		    
		}
		cl->timestamp = now;
	    }
	    else
	    {
		/* if there is a controling host, which is down, mark ws as down, 5 minutes ago, so when host
		   comes up it will mark ws as up  */
		if (ws && ws->host >= 0 && cl->data.data_val[1] == NODEUP && am_master)
		{
		    int i;
		    for (i=0 ; i<t->table_len ; i++)
			if (t->table_val[i].cltype == HOSTCLUSTER
			    && t->table_val[i].identifier == ws->host
			    && host_status(&t->table_val[i]) == NODEDOWN)
			{
			    cl->data.data_val[1] = NODEDOWN;
			    if (cl->timestamp >= now-5*60)
				cl->timestamp++;
			    else
				cl->timestamp = now - 5*60;
			}
		}
	    }
	    now = time(0);
	}
}
