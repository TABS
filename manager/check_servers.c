
/*
 * check servers:
 *  make sure the servers list in the status server
 *  is vaguely correct
 *
 * use refresh_db_client to find atleast one database.
 * walk though the replicas table to get a list of all names
 * for each database, contact it and inform book_status of state.
 * call get_database_2 to get local idea
 * for each in local list but not in replica list,
 * tell book_status to forget it.
 * 
 */

#include	"helper.h"
#include	"../lib/dlink.h"

void check_servers(void)
{
    void *replicas = dl_head();
    database_list *dblist;
    query_req request;
    query_reply *result;
    char *rep;

    if (refresh_db_client()== 0)
	return; /* cannot do anything!! */


    request.key.item_val = memdup("dummy", 6);
    request.key.item_len = 5;
    request.type = M_FIRST;
    request.database = REPLICAS;

    result = query_db_2(&request, db_client);
    request.type = M_NEXT;
    
    while (result != NULL && result->error == R_RESOK)
    {
	char *nm;

	free(request.key.item_val);
	request.key = result-> query_reply_u.data.key;
	request.key.item_val = memdup(result-> query_reply_u.data.key.item_val,result-> query_reply_u.data.key.item_len);
	nm = dl_strndup(result->query_reply_u.data.key.item_val,
			result->query_reply_u.data.key.item_len);
	dl_insert(replicas, nm);
	xdr_free((xdrproc_t)xdr_query_reply, (char*) result);
	result = query_db_2(&request, db_client);
    }
    free(request.key.item_val);
    xdr_free((xdrproc_t)xdr_query_reply, (char*) result);

    for (rep = dl_next(replicas) ; rep != replicas ; rep=dl_next(rep))
    {
	/* check out this replica */
	database_state st;
	open_db_client(rep, NULL);
	if (db_client)
	{
	    update_status *upst;
	    upst = get_updatest_2(NULL, db_client);
	    if (upst == NULL)
		st = DB_DOWN;
	    else if (*upst != FULL)
		st = DB_READONLY;
	    else
		st = DB_UP;
	}
	else
	    st = DB_DOWN;
	{
	    /* better try telling the status about the difference */
	    database_info dbi;
	    dbi.name = rep;
	    dbi.state = st;
	    set_database_2(&dbi, status_client);
	}
    }

    if (result && result->error == R_NONEXT)
    {
	dblist= get_databases_2(NULL, status_client);
	if (dblist != NULL)
	{
	    int db;
	    for (db = 0
		 ; db < dblist->database_list_len
		   && dblist->database_list_val[db].state != DB_UNREGISTERED
		 ; db++)
	    {
		for (rep = dl_next(replicas) ; rep != replicas &&
		         strcmp(rep, dblist->database_list_val[db].name)!= 0
		     ; rep = dl_next(rep));
		if (rep == replicas)
		{
		    /* this database  nolonger exists, trash it */
		    database_info dbi;
		    dbi.name = dblist->database_list_val[db].name;
		    dbi.state = DB_UNREGISTERED;
		    set_database_2(&dbi, status_client);
		}
		
	    }
	    xdr_free((xdrproc_t)xdr_database_list, (char*)dblist);
	}
    }
    while (dl_next(replicas)!= replicas)
    {
	rep = dl_next(replicas);
	dl_del(rep);
	dl_free(rep);
    }
    refresh_db_client();
}
