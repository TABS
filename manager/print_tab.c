
#include	"../db_client/db_client.h"

static char *cltname[] = { NULL, "TABLE", NULL, "ALLOC", NULL, "HOST ", NULL, "STATE", NULL, "RESV " };
static char *periods[] = { NULL, "FREE", "FULL", "CLOSED" };
static char *bstatus[] = { "<null>", "OutOfService", "Broken", "NotBookable",
		"UnAllocated", "Allocated", "Tentative" };

void print_tab(table *tab, FILE *f)
{
    int r;
    for (r=0; r<tab->table_len ; r++)
    {
	cluster *c = &tab->table_val[r];
	int d;
	char *nm = NULL;
	nmapping_type mp = M_CLASS;
	switch(c->cltype)
	{
	case TABLECLUSTER: mp = M_HOSTGROUP; break;
	case HOSTCLUSTER: mp = M_HOST ; break;
	case ALLOCCLUSTER: mp = M_ATTRIBUTE; break;
	case WSRESCLUSTER:
	case WSSTATUSCLUSTER: mp = M_WORKSTATION; break;
	}
	nm = get_mappingchar(c->identifier, mp);
	if (nm)
	    fprintf(f, "%02d: %s %-12s %s (%d):", r, cltname[c->cltype], nm, myctime(c->timestamp), c->data.data_len);
	else
	    fprintf(f, "%02d: %s %d %s %d:", r, cltname[c->cltype], c->identifier, myctime(c->timestamp), c->data.data_len);
	if (nm) free(nm);
	nm=NULL;
	for (d=0 ; d<c->data.data_len ; d++)
	{
	    int v= c->data.data_val[d];
	    switch(d*10+c->cltype)
	    {
	    default:
		fprintf(f, " %d", v);
		break;

		
	    case ALLOCCLUSTER+0: /* alloc status */
		fprintf(f, " %s", v>=PERIODFREE && v<= LABCLOSED? periods[v]:"???");
		break;
	    case TABLECLUSTER+0: /* host who created */
	    case ALLOCCLUSTER+10: /* alloc host */
		nm = get_mappingchar(v, M_HOST);
		if (nm)
		{
		    fprintf(f, " on %s", nm);
		}
		else fprintf(f, " on host-%d", v);
		break;
	    case ALLOCCLUSTER+20:
	    case WSSTATUSCLUSTER+20:
	    case WSSTATUSCLUSTER+40:
		/* print a time..*/
		fprintf(f, " %s", myctime((time_t)v));
		break;
	    case ALLOCCLUSTER+30:
		fprintf(f, " %d failed allocs: %d",
			c->data.data_len-3, v);
		break;
	    case HOSTCLUSTER+0:
	    case WSSTATUSCLUSTER+10:
		fprintf(f," %s", v==NODEUP?"UP":v==NODEDOWN?"DOWN":"unknown");
		break;
	    case WSSTATUSCLUSTER+0:
	    case WSRESCLUSTER+0:
		if (v==-2) nm="CLOSED";
		else if (v==-1) nm="-";
		else nm = find_username(v);
		if (nm) fprintf(f, " %s", nm);
		else fprintf(f, " user-%d", v);
		if (v<0) nm=NULL; /* avoid free */
		break;
	    case WSSTATUSCLUSTER+30: /* host logged on to */
		nm = get_mappingchar(v, M_HOST);
		if (nm)
		    fprintf(f, " %s", nm);
		else fprintf(f," host-%d", v);
		break;
	    case WSRESCLUSTER+10: /* status */
		if (v>=NODEOUTOFSERVICE && v<=NODETENTATIVE)
		    fprintf(f, " %s", bstatus[v]);
		else
		    fprintf(f, " status-%d", v);
		break;
	    case WSRESCLUSTER+20: /* firmness */
		fprintf(f, " %s", v==0?"normal":v==1?"firm":"unknown-firmness");
		break;
	    }
	    if (nm)
		free(nm);
	    nm=NULL;
	}
	fprintf(f, "\n");
    }
}
