
#include	"../db_client/db_client.h"

/*
 * return a client handle to a book_status on a host in the given
 * hostgroup. If last, find last up host, otherwise find first.
 */

CLIENT *up_host(hostgroup_info *hg, bool_t last)
{

    int h;
    CLIENT *rv = NULL;
    static struct timeval tv = {3,0};
    static struct timeval total = {13,0};

    pmap_settimeouts(tv, total);
    
    for (h= 0; rv == NULL && h < hg->hosts.entitylist_len ; h++)
    {
	entityid hostid;
	char *hname;
	if (last)
	    hostid = hg->hosts.entitylist_val[hg->hosts.entitylist_len-1-h];
	else
	    hostid = hg->hosts.entitylist_val[h];

	hname = get_mappingchar(hostid, M_HOST);
	if (hname)
	{
	    static struct timeval t = { 10, 0};
	    rv = clnt_create(hname, BOOK_STATUS, BOOKVERS, "udp");
	    if (rv &&
		clnt_call(rv, NULLPROC, (xdrproc_t)xdr_void, NULL, (xdrproc_t)xdr_void, NULL, t)!= 0)
	    {
		clnt_destroy(rv);
		rv = NULL;
	    }
	    free(hname);
	}
    }
    return rv;
}

	
