
#include	<rpc/rpc.h>
#include	"statush.h"

int lookup_table(table * tbl, cluster_type cltype, int identifier)
{
    int i;

    if (tbl == NULL)
	return -1;
    
    for (i=0 ; i <tbl->table_len;i++)
	if ((cltype == tbl->table_val[i].cltype) &&
	    (identifier == tbl->table_val[i].identifier))
	    return i;
    return -1;
}
