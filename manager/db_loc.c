
#include	<time.h>
#include	<unistd.h>
#include	"statush.h"

/* manage list of databases
 * this is initialised from a file
 * and periodically maintained by the helper processes
 */

static database_list databases;

/* keep a list of "close" databases
 * this is initialised from comm line args
 * names removed when told UNREGISTERED
 * names added to top when told CLOSE
 *
 * names are stored so that a bigger index number is closer 
 */
char **close_dbs;
int close_db_cnt = 0;

void add_close(char *name)
{
	char **new = (char**)malloc((close_db_cnt+1)*sizeof(char*));
	int i;
	for (i=0 ; i<close_db_cnt ; i++)
		new[i] = close_dbs[i];
	new[close_db_cnt] = strdup(name);
	if (close_db_cnt) free(close_dbs);
	close_db_cnt++;
	close_dbs = new;
}

int find_close(char *name)
{
	int i;
	for (i=0 ; i < close_db_cnt ; i++)
		if (strcmp(name, close_dbs[i])==0)
			return i;
	return -1;
}

void del_close(int num)
{
	int i;
	if (num < 0 || num >= close_db_cnt)
		return;
	free(close_dbs[num]);
	for (i=num+1 ; i < close_db_cnt ; i++)
		close_dbs[i-1] = close_dbs[i];
	close_db_cnt--;
}

static int cmp_dbs(const void *av, const void *bv)
{
	const database_info *a = av;
	const database_info *b = bv;
	int ca, cb;
	if (a->state != b->state)
		return a->state - b->state;

	ca = find_close(a->name);
	cb = find_close(b->name);
	if (ca != cb)
		return cb - ca;
	return strcmp(a->name, b->name);
}

    
static void sort_dbs()
{
	qsort(databases.database_list_val,
	      databases.database_list_len,
	      sizeof(database_info),
	      cmp_dbs);
}

void set_database(char *name, database_state state)
{
	int i;
	if (state == DB_UP && find_close(name)>= 0)
		state = DB_CLOSE;
	if (state == DB_CLOSE && find_close(name)<0)
		add_close(name);
    
	for (i=0 ; i<databases.database_list_len ; i++)
		if (strcmp(databases.database_list_val[i].name,
			   name)==0)
		{
			databases.database_list_val[i].state = state;
			break;
		}

	if (i == databases.database_list_len)
	{
		database_info *new = (database_info*)malloc(sizeof(database_info)*(i+1));

		for (i=0 ; i < databases.database_list_len ; i++)
			new[i] = databases.database_list_val[i];
		new[i].name = strdup(name);
		new[i].state = state;
		if (databases.database_list_val)
			free(databases.database_list_val);
		databases.database_list_val = new;
		databases.database_list_len ++;
	}
	sort_dbs();
    
}

database_list *SVC(get_databases_2)(void *x, struct svc_req *rq)
{
	/* make sure the helper runs atleast every 10 minutes to make sure I am uptodate */
	static time_t last=0;
	if (last + 10*60 < time(0))
	{
		check_helper(0);
		last = time(0);
	}
	return &databases;
}

void *SVC(set_database_2)(database_info *i, struct svc_req *rq)
{
	static int x;
    
	set_database(i->name, i->state);
	write_databases();
	return &x;
}

void init_databases(int argc, char *argv[])
{
	FILE *dbf;
	int arg;
	for (arg = 0 ; arg<argc ; arg++)
		add_close(argv[arg]);
    
	databases.database_list_val = NULL;
	databases.database_list_len = 0;

	dbf = fopen(DB_LOC_FILE, "r");
	if (dbf)
	{
		char buf[200];
		while (fgets(buf, 200, dbf)!=  NULL)
		{
			char *e = strchr(buf, '\n');
			if (e) *e = '\0';
			set_database(buf, DB_UNKNOWN);
		}
		fclose(dbf);
	}
}

int write_databases(void)
{
	FILE *dbf;
	int i;

	dbf = fopen(DB_LOC_TMP, "w");
    
	for (i= 0 ; i < databases.database_list_len ; i++)
		if (databases.database_list_val[i].state != DB_UNREGISTERED)
			fprintf(dbf, "%s\n", databases.database_list_val[i].name);
	fflush(dbf);
	if (ferror(dbf))
	{
		fclose(dbf);
		unlink(DB_LOC_TMP);
		return -1;
	}
	fclose(dbf);
	rename(DB_LOC_TMP, DB_LOC_FILE);
	return 0;
}

