/* this module caches information from the server and is responsible */
 /* for maintaining contact with a server. */
/* this module should be run on every node along with a book_helper */
/* all information is dynamic.. i.e. it will not be saved */


#include	<stdio.h>
#include	<signal.h>
#include	<rpc/rpc.h>
#include	<rpc/pmap_clnt.h>
#include	<sys/wait.h>
#include	"statush.h"
#include	<sys/resource.h>

extern int helper_pid;

static SIGRTN die(int sig)
{
	if (helper_pid > 0)
		kill(helper_pid, SIGKILL);
	(void)pmap_unset(BOOK_STATUS, BOOKVERS);
	if (sig)
		exit(0);
}

void * SVC(status_die_2)(void * noarg,struct svc_req *rqstp)
{
	static char dummy;

	if (ntohs(svc_getcaller(rqstp->rq_xprt)->sin_port) > 1024) {
		/* FIXME check host */
		return (&dummy);
	}


	/* reply that the die is about to happen */
	svc_sendreply(rqstp->rq_xprt, (xdrproc_t)xdr_char, &dummy);
	die(0);
	exit(0);
}

int main (int argc, char *argv[])
{
	extern void book_status_2();
	SVCXPRT *transp;
	char *maint_prog = "/usr/local/etc/book_manager";

	if (argc > 2 && (strcmp(argv[1], "-H")==0 || strcmp(argv[1], "-M")==0))
	{
		/*  Helper or Maint */
		maint_prog = argv[2];
		argc -= 2;
		argv += 2;
	}
	/* trap an alarm signal This is the way to kill the bind_process*/
	signal(SIGTERM, die);

	/* check to see if already running */

	callrpc("localhost", BOOK_STATUS, BOOKVERS, STATUS_DIE,
		(xdrproc_t)xdr_void, (char *)NULL, 
		(xdrproc_t)xdr_void, (char *)NULL);

	(void)pmap_unset(BOOK_STATUS, BOOKVERS);
    
	transp = svcudp_create(RPC_ANYSOCK);
	if (transp == NULL) {
		(void)fprintf(stderr, "cannot create udp service.\n");
		exit(1);
	}
	if (!svc_register(transp, BOOK_STATUS, BOOKVERS, book_status_2, IPPROTO_UDP)) {
		(void)fprintf(stderr, "unable to register (BOOK_STATUS, BOOKVERS, udp).\n");
		exit(1);
	}
    
	transp = svctcp_create(RPC_ANYSOCK, 0, 0);
	if (transp == NULL) {
		(void)fprintf(stderr, "cannot create tcp service.\n");
		exit(1);
	}
	if (!svc_register(transp, BOOK_STATUS, BOOKVERS, book_status_2, IPPROTO_TCP)) {
		(void)fprintf(stderr, "unable to register (BOOK_STATUS, BOOKVERS, tcp).\n");
		exit(1);
	}
    
	init_databases(argc-1, argv+1);
    
	/* now fork a manager process to handle the forwarding of tables */
	start_node_manager(maint_prog); 
    
	svc_run();
	(void)fprintf(stderr, "svc_run returned\n");
	exit(1);
    
}
