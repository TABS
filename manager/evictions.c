
#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<fcntl.h>
#include	<time.h>
#include	"helper.h"
#include	<sys/stat.h>

#include	"../lib/skip.h"
/*
 * current evictions are store in a skip_list
 * index is workstation-id
 * content is userid, wsname, time to leave, time of last warning
 */

char EvictLog[] = "/var/book/evict_log";
void *evictlist = NULL;

typedef struct evicts {
    entityid	wsid;
    bookuid_t	whom;
    char	*wsname;
    time_t	leavetime;
    time_t	lastwarn;
} *evicts;

static int cmp_ev(evicts a, evicts b, entityid *c)
{
    entityid k;
    if (b) k=b->wsid;
    else k = *c;
    return k - a->wsid;
}
static void free_ev(evicts a)
{
    free(a->wsname);
    free(a);
}

static void etrace(char *msg, evicts ev)
{
    int fd;

    fd = open(EvictLog, O_WRONLY|O_APPEND|O_CREAT, 0600);
    if (fd >= 0)
    {
	char buf[1024];
	struct tm *tm;
	time_t now;

	fchown(fd, 0, 0);
	fchmod(fd, 0640);

	time(&now);
	tm = localtime(&now);
	sprintf(buf, "%02d%02d%02d.%02d%02d%02d %s %s %s(%d) uid=%d leavein=%ds lastwarn=%ds\n",
		tm->tm_year, tm->tm_mon+1, tm->tm_mday,
		tm->tm_hour, tm->tm_min, tm->tm_sec,
		my.hostname, msg, ev->wsname, ev->wsid,
		(int)ev->whom, (int)(ev->leavetime-now), (int)(now-ev->lastwarn));

	write(fd, buf, strlen(buf));
	close(fd);
    }
}

static int am_evicting(int wsid)
{
    return evictlist != NULL
	&& skip_search(evictlist, &wsid)!= NULL;
}

static void forget_evict(int wsid)
{
    if (evictlist)
    {
	evicts ev, *evp;
	evp = skip_search(evictlist, &wsid);
	if (evp)
	{
	    ev = *evp;
	    etrace("forgeting", ev);
	    skip_delete(evictlist, &wsid);
	}
    }
}

/* this process ensures that the evict process is running */
static void ensure_evict(int time_to_leave, int wsid, int who)
{
    time_t now;
    int togo; /* how long until logoff */
    int sincelast; /* how long since last message */
    int dowarn = 0;
    evicts ev, *evp;

    if (evictlist==NULL)
	evictlist = skip_new(cmp_ev, free_ev, NULL);
    evp = skip_search(evictlist, &wsid);
    if (evp) ev = *evp;
    if (evp && ((*evp)->whom  != who))
    {
	forget_evict(wsid);
	evp= NULL;
    }
    if (evp == NULL)
    {
	ev = (evicts)malloc(sizeof(struct evicts));
	ev->wsid = wsid;
	ev->whom = who;
	ev->wsname = get_mappingchar(wsid, M_WORKSTATION);
	if (ev->wsname == NULL)
	{
	    free(ev);
	    return;
	}
	ev->lastwarn = 0;
	time(&now);
	if (now + 5*60 > time_to_leave)
	    ev->leavetime = now + 5*60;
	else
	    ev->leavetime = time_to_leave;
	skip_insert(evictlist, ev);
    }
    else
	ev = *evp;
    if (time_to_leave > ev->leavetime)
	ev->leavetime = time_to_leave;

    /* maybe show a message */
    time(&now);
    togo = ev->leavetime - now;
    sincelast = now - ev->lastwarn;

    if (togo < 2)
    {
	etrace("forced logoff", ev);
	force_logoff(ev->wsname);
    }
    else if (togo < 90)
    {
	if (sincelast > 60)
	    dowarn=1;
    }
    else if (togo < 900)
    {
	if (sincelast > 180)
	    dowarn = 1;
    }
    else if (sincelast > 360)
	dowarn = 1;

    if (dowarn)
    {
	char buf[1024];
	char tim[10];
	if (togo > 120)
	    sprintf(tim, "%d minutes", togo/60);
	else
	    sprintf(tim, "%d seconds", togo);
	etrace(tim,ev);
	sprintf(buf,
"%s\n\n" /* X11 send_message relies on this being here */
"This is a warning message from the Booking System\n\n"
"This message was sent at %s\n"
"This workstation is being reserved for another user\n"
"Please log off within the next %s\n\n"
"If you do not logoff within this time, you will be forced off\n",
		    ev->wsname, ctime(&now), tim);
	send_message(buf);
	ev->lastwarn = now;
    }
}


char *labof(int wsid)
{
    workstation_state *st;
    int labid;
    st= get_wsstate(wsid,0);
    labid = desc_2_labind(&st->state);
    return get_mappingchar(labid, M_ATTRIBUTE);
}

    
/* PERFORM_EVICTIONS(): run an evict process for each workstation we */
 /* control */
void perform_evictions(table * tab)
{
    int i;
    int book_lev = get_book_level();
    int grace = get_configint("grace");

    if (grace <=0) grace=7*60;

    /* for each workstation, check eviction */
    for (i=0 ; i < tab->table_len ; i++)
	if (Cluster_type(tab,i) == WSSTATUSCLUSTER
	    && (am_evicting(Cluster_id(tab,i))
		||Ws_whereon(tab,i) == my.hostid))
	{
	    /* some logged onto me, check for conflicting reservation */
	    
	    int wsid = Cluster_id(tab,i);
	    int resloc_in_table = lookup_table(tab, WSRESCLUSTER, wsid);
	    int evict=0;

	    sprintf(errbuf,"Check EVICT for ws %d\n",wsid);
	    Debug(errbuf,1);
	
	    if (   Res_user(tab,resloc_in_table) != -1
		&& Ws_user(tab,i) != -1)
	    {
		switch (book_lev)
		{
		case 0:
		    break;
		case 1:
		    if (Cluster_modtime(tab, resloc_in_table) + grace < time(0))
			;	/* do nothing, well into period */
		    else if (Res_user(tab, resloc_in_table) == -1
			     || Res_status(tab, resloc_in_table) == NODETENTATIVE)
			/* not reserved, do nothing */
			;
		    else if (Res_user(tab, resloc_in_table) == -2)
		    {
			/* lab is closed */
			char *labname = labof(wsid);
			if (labname && in_neverclose(Ws_user(tab,i), labname) == 0)
			    evict=1;
			if (labname) free(labname);
		    }
		    else if (in_class(Ws_user(tab, i), 
				      Res_user(tab, resloc_in_table))== 0)
			evict = 1;
		    /* if in exempt group, never evict */
		    if (in_exempt(Ws_user(tab,i))==1)
			    evict=0;
		    break;
		}
	    
	    }
	    if (evict)
		ensure_evict(Cluster_modtime(tab,resloc_in_table), wsid,
			     Ws_user(tab,i));
	    else
		forget_evict(wsid);
	}
}
