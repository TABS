
#include	<time.h>
#include	"helper.h"


/*
 * check if anyone appears to have defaulted on a booking
 * just now.
 * for each workstation
 *   if    it is allocated/tentative
 *     and it's allocation time is before now, but after last check
 *     and user allocated is not on
 *   then
 *     record default for allocated user in this lab for this period
 *
 */

void check_default(table *t)
{
    static time_t last_check = 0;
    time_t now = time(0);
    int lab;
    int grace = get_configint("grace");
    time_t alloctime;
    int ind;

    if (grace==0) grace=7*60;
    
    if (last_check == 0)
    {
	last_check = now;
	return;
    }

    for (ind=0 ; ind < t->table_len ; ind++)
    {
	if (Cluster_type(t,ind)== ALLOCCLUSTER)
	{
	    lab = Cluster_id(t,ind);
	    alloctime = Cluster_modtime(t,ind);
	}
	if (Cluster_type(t,ind) == WSRESCLUSTER
	    && (Res_status(t,ind) == NODEALLOCATED
		||Res_status(t,ind) == NODETENTATIVE)
	    && Res_user(t,ind) < get_class_min()
	    && alloctime +grace <= now
	    && alloctime +grace > last_check
	    && Cluster_type(t,ind-1) == WSSTATUSCLUSTER
	    && Cluster_id(t,ind-1) == Cluster_id(t,ind)
/*	    && Ws_user(t,ind-1) != Res_user(t,ind)   */
	    )
	{
	    /* ok, a rescluster which might be for us.
	     * must check the ws belong to us, and that
	     * who-on is correct
	     */
	    workstation_state *ws = get_wsstate(Cluster_id(t,ind),0);
	    if (ws
		&& (ws->host == my.hostid
		    || (ws->host <0 && Ws_whereon(t,ind-1)==my.hostid)
		    || (ws->host <0 && Ws_whereon(t,ind-1)<0 && am_master)
		    )
		)
	    {
		int doy, pod;
		book_set_claim *cp;
		book_change ch;
		time_t whenon;
		ch.chng = CLAIM_BOOKING;
		cp = & ch.book_change_u.aclaim;

		get_doypod(&alloctime, &doy, &pod);
		cp->slot = make_bookkey(doy, pod, lab);
		cp->user = Res_user(t,ind);
		cp->when = 0;
		cp->where = -1;
		if (check_whoison(Cluster_id(t,ind),  &whenon) ==
		    Res_user(t,ind))
		{
		    int delta_whenon = whenon - alloctime;
		    if (delta_whenon < -10*60)
			delta_whenon = grace;
		    cp->claimtime = DID_LOGIN | ((delta_whenon+30*60) <<4);
		    cp->where = Cluster_id(t,ind);
		}
		else if (Cluster_modtime(t,ind) > alloctime + 30)
		{
		    /* looks like they claimed, but haven't logged on yet, give them the benefit of the doubt */
		    cp->claimtime = 0;
		}
		else
		{
		    /* Ok, looks like a defaulter to me!! */
		    cp->claimtime = DID_DEFAULT;
		}
		{
		    char buf[100];
		    sprintf(buf, "check_default set %x for user %d on %d (d%d,p%d,l%d)\n",
			    cp->claimtime, (int)cp->user, cp->where, doy, pod, lab);
		    shout(buf);
		}
		send_change(&ch);
	    }
	}
    }
    last_check = now;
}
