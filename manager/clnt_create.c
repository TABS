
/* creates a rpc handle, bypassing the hostname lookup if possible. */
 /* This is achieved by cacheing the addresses.  This has been made */
 /* necessary by the lack of a reliable name server, it's not a bad */
 /* idea anyway.  Perhaps the entries should be timed out after a while??? */

#include	<rpc/rpc.h>
#include	<netdb.h>
#include	"../lib/skip.h"
#include	"../lib/misc.h"
extern char * strdup();

static void * sl = NULL;

/* the data structure for the skiplist cache */
typedef struct hname_node
{
    char * hostname;
    struct sockaddr_in data;
} hname_node;

/* a comparison function for the skiplist */
static int node_cmp(hname_node * h1, hname_node * h2, char * name)
{
    return ((h2 != NULL)? strcmp(h1->hostname, h2->hostname) :
	strcmp(h1->hostname, name));
    
}

/* a function to free a skiplist node */
static void node_free(hname_node * n1)
{
    free(n1->hostname);
}

/* set up the skiplist initially */
void hostcache_init()
{
    sl = skip_new(node_cmp,node_free, NULL);
}


CLIENT * my_clntudp_create(char * hostname, int prog, int vers )
{
    char errbuf[128];
    struct sockaddr_in sin;
    static struct timeval tv = {5,0 } ;
    int sock = RPC_ANYSOCK;
    hname_node **result;

    if (sl == NULL)
	hostcache_init();
    result = skip_search(sl, hostname);
    
    
    /* if not present look it up and add it */
    if (result == NULL) {
	struct hostent * he;

/*	printf("Not in cache %s\n",hostname); */
	he = gethostbyname(hostname);
	if (he == NULL) {
	    sprintf(errbuf, "Hostname lookup for %s failed\n",hostname);
	    shout(errbuf);
	    return  NULL;
	}
	else {
	    hname_node * new_el = (hname_node*)malloc(sizeof(hname_node));
	    sin.sin_family = he -> h_addrtype;
	    sin.sin_port =0;
	    bzero(sin.sin_zero, sizeof(sin.sin_zero));
	    bcopy(he->h_addr, &sin.sin_addr, he->h_length);
	    /*printf("Adding %s as %s\n", hostname,
		   inet_ntoa(sin.sin_addr));
		   */
	    /* fix up node to add to cache */
	    new_el->hostname = strdup(hostname);
	    bcopy(&sin, &new_el->data, sizeof(struct sockaddr_in));
 	    skip_insert(sl, new_el); 
	    
	}
    }
    else
    {
/*	printf("Found in cache %s\n",hostname); */
	bcopy(&(*result)->data, &sin, sizeof(struct sockaddr_in));
    }
/*    printf("Address to contact %s\n",inet_ntoa(sin.sin_addr)); */
    
    return(clntudp_create(&sin, prog, vers, tv, &sock));
}








