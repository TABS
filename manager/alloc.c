#include	<time.h>
#include	"helper.h"

bool_t next_period_due(table * tab, int * doy, int *pod)
{
	int oldpod, olddoy;
	time_t next_period, cutoff;
	time_t alloctime = time(0);
	int i;


	for (i=0 ; i<tab->table_len ; i++)
		if (Cluster_type(tab, i) == ALLOCCLUSTER
		    && Cluster_modtime(tab,i) < alloctime)
			alloctime = Cluster_modtime(tab,i);
    
	get_doypod(&alloctime, &olddoy, &oldpod);
    
	cutoff = 0;
	cutoff = get_configint("alloctime");
	if (cutoff <= 0 )
		cutoff = 7*60; /* 7 minutes by default */

	next_period = time(0) + cutoff; /* want 7 mins before next time */
	/* starts */
	get_doypod(&next_period, doy, pod);
	return ((*doy != olddoy) || (*pod != oldpod));
}


/* LAB_STATUS :find out from the exclusions table what should happen */
 /* this period.  Maybe this should update the status of the machines */
 /* in the labs at the same time */

/* should the labindex go into the table?? */
int lab_status(int doy, int pod, int lab)
{
	extern description * process_exclusions();
	static description res;
	int st;


	if (res.item_len == 0)
		res = new_desc();
	zero_desc(&res);
	set_a_bit(&res, lab);

	if (!get_attr_types()) return PERIODFREE;
    
	/* what does the routine that processes the exclusions do?? */
	/* needs to take lab ind ( the labind can be in the description!!, */
	/* doy, pod and return if lab is open or  not.  */

	process_exclusions(pod, doy, &res);

	/* now find out which bit is set */
	/* these should not be numbers */
	if (query_bit(&res, attr_types.open) == TRUE)
		st = PERIODFULL;
	else if (query_bit(&res, attr_types.closed) == TRUE)
		st = LABCLOSED;
	else
		st = PERIODFREE;
    
	sprintf(errbuf, "Lab status (pod %d, doy %d) %d\n", pod,
		doy, st);
	shout(errbuf);

	return st;
}
