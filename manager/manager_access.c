
#include	<rpc/rpc.h>
#include	"status.h"

CLIENT *status_client;
void open_status_client(char * host)
{
    if (status_client)
	clnt_destroy(status_client);
    status_client = clnt_create(host, BOOK_STATUS, BOOKVERS, "udp");
}
