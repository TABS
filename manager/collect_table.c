
#include	"statush.h"

bool_t collect_tab(table * newtab, CLIENT *cl)
{
    static int index;
    char * xdrbuf;
    XDR xdrs;
    table_chunk * result;
    int timestamp;
    bool_t status;
    int max;
    
    index =0;
    result = get_chunk_2(&index, cl);
    if (result == NULL) {
	return FALSE;
    }
    max = result->chnk.max;
    
    xdrbuf = (char*)malloc(max);
    timestamp = result->chnk.timestamp;
    
    while (index < max) {
	bcopy(result->data.data_val, xdrbuf + result->chnk.index,
	      result->data.data_len);
	index += result->data.data_len;
	
	xdr_free((xdrproc_t)xdr_table_chunk, (char*) result);
	/* check for finish */
	if (index == max)
	    break;
	
	result = get_chunk_2(&index, cl);
	if (result == NULL)
	{
	    return FALSE;
	}
	
    }
    xdrmem_create(&xdrs, xdrbuf, max, XDR_DECODE);
    bzero(newtab, sizeof(table));
    status = xdr_table(&xdrs, newtab);
    
    free(xdrbuf); 
    return(status);
	
}
