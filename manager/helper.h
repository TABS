
#include	"../db_client/db_client.h"


extern struct mine {
    char *hostname;
    entityid hostid, hgid;
    hostinfo hostdata;
    hostgroup_info hgdata;
    hostinfo *labdata;
    int labcnt;
    int wscnt;
    void *wslist; /* skip list of workstations */
} my;

extern int am_master;

extern CLIENT *status_client;

extern char errbuf[200];

/* from allocate2.c */
int allocate(table *tab);

/* from create_table.c */
int check_whoison(int wsid, time_t *when);
workstation_state *get_wsstate(entityid wsid, int no_cache);
int  load_config(void);
void new_table(table *newtab);

/* from pass_table.c */
bool_t isvalid_table(table * tab);
int send_table(table * tab, CLIENT * cl);
int next_host(table * tab, int index);
bool_t give_table(table * t, int index);
int propagate_table();
void send_new_table(void);

/* from evictions.c */
char *labof(int wsid);
void perform_evictions(table * tab);

void force_logoff(char *ws);
int send_message(char *mesg);

/* from check_status.c */
void check_status(table *t);

/* from check_default.c */
void check_default(table *t);

/* from alloc.c */
bool_t next_period_due(table * tab, int * doy, int *pod);
int lab_status(int doy, int pod, int lab);


/* from clnt_create.c */
CLIENT * my_clntudp_create(char * hostname, int prog, int vers );
