
#include	<rpc/rpc.h>
#include	"../manager/status.h"
#include	<string.h>
#include	<stdio.h>

#ifdef linux
#define SVC(x) x ## _svc
#else
#define SVC(x) x
#endif
#define DB_LOC_FILE "/var/book/database_loc"
#define	DB_LOC_TMP  "/var/book/database_loc.new"


#define cluster_type(c) (c)->cltype
#define cluster_modtime(c) (c)->timestamp
#define cluster_id(c) (c)->identifier

#define alloc_status(c) (c)->data.data_val[0]
#define alloc_host(c) (c)->data.data_val[1]
#define	alloc_when(c) (c)->data.data_val[2]
#define alloc_numfailed(c) ((c)->data.data_len>3 ? ((c)->data.data_len - 3):0)
#define alloc_failid(c,n) ((c)->data.data_val[3+i])
#define host_status(c) (c)->data.data_val[0]
#define ws_user(c) (c)->data.data_val[0]
#define ws_status(c) (c)->data.data_val[1]
#define ws_whenon(c) (c)->data.data_val[2]
#define ws_whereon(c) (c)->data.data_val[3]
/* Whenalloc is updated at login and when reallocated to a machine */
#define	ws_whenalloc(c) (c)->data.data_val[4]
#define res_user(c) (c)->data.data_val[0]
#define res_status(c) (c)->data.data_val[1]
#define	res_status2(c) (c)->data.data_val[2]



#define Cluster_type(t,i) (t)->table_val[i].cltype
#define Cluster_modtime(t,i) (t)->table_val[i].timestamp
#define Cluster_id(t,i) (t)->table_val[i].identifier

#define Alloc_status(t,i) (t)->table_val[i].data.data_val[0]
#define Alloc_host(t,i) (t)->table_val[i].data.data_val[1]
#define	Alloc_when(t,i) (t)->table_val[i].data.data_val[2]
#define Alloc_numfailed(t,i) alloc_numfailed((t)->table_val+i)
#define Alloc_failid(t,i,n) ((t)->table_val[i].data.data_val[3+i])
#define Host_status(t,i) (t)->table_val[i].data.data_val[0]
#define Ws_user(t,i) (t)->table_val[i].data.data_val[0]
#define Ws_status(t,i) (t)->table_val[i].data.data_val[1]
#define Ws_whenon(t,i) (t)->table_val[i].data.data_val[2]
#define Ws_whereon(t,i) (t)->table_val[i].data.data_val[3]
#define	Ws_whenalloc(t,i) (t)->table_val[i].data.data_val[4]
#define Res_user(t,i) (t)->table_val[i].data.data_val[0]
#define Res_status(t,i) (t)->table_val[i].data.data_val[1]
#define	Res_status2(t,i) (t)->table_val[i].data.data_val[2]


/* from db_loc.c */
void add_close(char *name);
int find_close(char *name);
void del_close(int num);
void set_database(char *name, database_state state);
void init_databases(int argc, char *argv[]);
int write_databases(void);

/* from helper.c */
void start_node_manager(char *program);
void check_helper(int waken);

/* from lookup_table.c */
int lookup_table(table * tbl, cluster_type cltype, int identifier);
