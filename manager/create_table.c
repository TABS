
#include	<unistd.h>
#include	<stdlib.h>
#include	<time.h>
#include	"helper.h"
#include	<sys/stat.h>
#include	<sys/file.h>
#ifdef sun
#include	<sys/fcntl.h>
#endif
#include	"../lib/skip.h"

int check_whoison(int wsid, time_t *when)
{

	char *wsname;
	int r;
	wsname = get_mappingchar(wsid, M_WORKSTATION);
	if (wsname == NULL)
		return -1;
	r = get_whoison(wsname, when);
	free(wsname);
	return r;
}



static void init_cluster(cluster * cl, cluster_type ct, int id, int cltime, int dcount,
	     int a, int b)
{
	cl->cltype = ct;
	cl->identifier = id;
	cl->timestamp = cltime;
	cl->data.data_len = dcount;
	cl->data.data_val = (int*) malloc (dcount * sizeof(int));
	if (dcount >= 1)
		cl->data.data_val[0] = a;
	if (dcount >= 2)
		cl->data.data_val[1] = b;

	while (--dcount >= 2)
		cl->data.data_val[dcount] = 0;
}

struct mine  my = { 0 };

struct ws_state
{
	entityid wsid;
	workstation_state state;
};
static int ws_cmp(struct ws_state *a, struct ws_state *b, int *idp)
{
	int id;
	if (b) id = b->wsid;
	else id = *idp;
	return a->wsid - id;
}
static void ws_free(struct ws_state *a)
{
	xdr_free((xdrproc_t)xdr_workstation_state, (char*) &a->state);
	free(a);
}

workstation_state *get_wsstate(entityid wsid, int no_cache)
{
    
	struct ws_state **p, *ws;
	workstation_state state;
	char key[20];
    

	if (my.wslist == NULL)
		my.wslist = skip_new(ws_cmp, ws_free, NULL);
	
	p = skip_search(my.wslist, &wsid);
	if (p) {
		if (no_cache)
			skip_delete(my.wslist, &wsid);
		else
			return &(*p)->state;
	}

	memset(&state, 0, sizeof(state));
	if (db_read(key_int(key, C_WORKSTATIONS, wsid), &state, (xdrproc_t)xdr_workstation_state, CONFIG) != R_RESOK)
		return NULL;
	ws = (struct ws_state*)malloc(sizeof(struct ws_state));
	ws->wsid = wsid;
	ws->state = state;
	skip_insert(my.wslist, ws);
	return &ws->state;
}


int  load_config(void)
{
	item key;
	char k[20];
	int i;

	if (my.hostname == NULL)
		my.hostname = get_myhostname();
	if (my.wslist == NULL)
		my.wslist = skip_new(ws_cmp, ws_free, NULL);

	memset(&my.hostdata, 0, sizeof(my.hostdata));
	memset(&my.hgdata, 0, sizeof(my.hgdata));
    
	my.hostid = get_mappingint(my.hostname, M_HOST);
	if (my.hostid < 0)
		return my.hostid;

	key = key_int(k, C_HOSTS, my.hostid);
	if (db_read(key, &my.hostdata, (xdrproc_t)xdr_hostinfo, CONFIG) != R_RESOK)
		return -1;

	my.hgid = my.hostdata.clump;

	key = key_int(k, C_HOSTGROUPS, my.hgid);
	if (db_read(key, &my.hgdata, (xdrproc_t)xdr_hostgroup_info, CONFIG) != R_RESOK)
	{
		xdr_free((xdrproc_t)xdr_hostinfo, (char*) &my.hostdata);
		return -1;
	}

	my.labcnt = my.hgdata.labs.entitylist_len;

	my.labdata = (hostinfo *)malloc(sizeof(hostinfo)*my.labcnt);
	memset(my.labdata, 0, sizeof(hostinfo)*my.labcnt);
	my.wscnt = 0;
	for (i=0 ; i<my.labcnt ; i++)
	{
		key = key_int(k, C_LABS, my.hgdata.labs.entitylist_val[i]);
		if (db_read(key, &my.labdata[i], (xdrproc_t)xdr_hostinfo, CONFIG) != R_RESOK)
		{
			xdr_free((xdrproc_t)xdr_hostinfo, (char*) &my.hostdata);
			xdr_free((xdrproc_t)xdr_hostgroup_info, (char*) &my.hgdata);
			while (--i >= 0)
				xdr_free((xdrproc_t)xdr_hostinfo, (char*) &my.labdata[i]);
			free(my.labdata);
			return -1;
		}
		my.wscnt += my.labdata[i].workstations.entitylist_len;
	}
	return 0;
}

static int ecmp(const void *av, const void *bv)
{
	const entityid *a=av, *b=bv;
	return *a - *b;
}
static void sort_entitylist(entitylist *l)
{
	qsort(l->entitylist_val, l->entitylist_len, sizeof(entityid), ecmp);
}

void new_table(table *newtab)
{
	int i,j;
	time_t now;
	int cnum;
    
	now = time(0);

	/*
	 * Need
	 * 1 for the hostgroup
	 * 1 cluster per lab
	 * 1 per host
	 * 2 per workstation
	 */
	newtab->table_len = 1 + my.labcnt + my.hgdata.hosts.entitylist_len + my.wscnt*2;
	newtab->table_val = (cluster*)malloc(newtab->table_len * sizeof(cluster));

	cnum = 0;

	/* table cluster, data is creator */
	init_cluster(&newtab->table_val[cnum++],
		     TABLECLUSTER, my.hgid, now,
		     1,  my.hostid, 0);

	/* host clusters. Data is NODE{UP,DOWN} */
	sort_entitylist(&my.hgdata.hosts);
	for (i=0 ; i<my.hgdata.hosts.entitylist_len ; i++)
		init_cluster(&newtab->table_val[cnum++],
			     HOSTCLUSTER,  my.hgdata.hosts.entitylist_val[i],
			     my.hgdata.hosts.entitylist_val[i] == my.hostid? now:0,
			     1, NODEUP, 0);

	/* lab clusters. Data is alloc status and host */
	for (i=0 ; i<my.labcnt ; i++)
	{
		init_cluster(&newtab->table_val[cnum++],
			     ALLOCCLUSTER, my.hgdata.labs.entitylist_val[i], 0,
			     2, PERIODFREE, -1);

		/* workstation and reservation clusters
		 * workstation: who, NODE{UP,DOWN}, when-on, hoston, when alloc
		 * reservation: whofor, alloc status
		 */
		sort_entitylist(&my.labdata[i].workstations);
		for (j = 0 ; j< my.labdata[i].workstations.entitylist_len ; j++)
		{
			int who;
			time_t when;
			int wsid = my.labdata[i].workstations.entitylist_val[j];
			who = check_whoison(wsid, &when);

			init_cluster(&newtab->table_val[cnum],
				     WSSTATUSCLUSTER, wsid, who>=0? now:0,
				     5, who, NODEUP);
			if (who>=0)
			{
				newtab->table_val[cnum].data.data_val[2] = when;
				newtab->table_val[cnum].data.data_val[3] = my.hostid;
				newtab->table_val[cnum].data.data_val[4] = now;
			}
			cnum++;

			init_cluster(&newtab->table_val[cnum++],
				     WSRESCLUSTER, wsid, 0,
				     3, -1, NODENOTALLOC);
		}
	}
}
