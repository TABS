
/*
 * chose and refresh database client handle.
 * client handle is stored in "db_client"
 * and refresh_db_client is called to chose a new database server
 */

/*
 * Call local book_status and get database_list
 * Iterate through databases calling them until one responds with
 * accessable status
 *
 * If the status we find disagrees with status in table, tell
 * the book_status about it
 */

#include	"../db_client/db_client.h"

int refresh_db_client(void)
{
	database_list *dblist;
	int db;
    
	if (db_client)
		clnt_destroy(db_client);
	db_client = NULL;

	if (status_client == NULL)
		open_status_client("localhost");
	if (status_client == NULL)
		return 0;
	dblist = get_databases_2(NULL, status_client);
	if (dblist == NULL)
	{
		open_status_client("localhost");
		if (status_client)
			dblist = get_databases_2(NULL, status_client);
	}
	if (dblist == NULL)
		return 0;
	for (db= 0 ;
	     db < dblist->database_list_len  &&
		     dblist->database_list_val[db].state != DB_UNREGISTERED ;
	     db++)
	{
		database_state st;
	
		open_db_client(dblist->database_list_val[db].name, NULL);
		if (db_client)
		{
			/* its up, lets check writable */
			update_status *upst;
			upst = get_updatest_2(NULL, db_client);
			if (upst == NULL)
				st = DB_DOWN;
			else if (*upst != FULL)
				st = DB_READONLY;
			else
				st = DB_UP;
		}
		else
			st = DB_DOWN;
		if (dblist->database_list_val[db].state == DB_CLOSE)
			dblist->database_list_val[db].state = DB_UP;
		if (st != dblist->database_list_val[db].state)
		{
			/* better try telling the status about the difference */
			database_info dbi;
			dbi = dblist->database_list_val[db];
			dbi.state = st;
			set_database_2(&dbi, status_client);
		}
		if (st == DB_UP)
		{
			xdr_free((xdrproc_t)xdr_database_list, (char*) dblist);
			return 1;
		}
		else
		{
			if (db_client)
				clnt_destroy(db_client);
			db_client = NULL;
		}
	}
	xdr_free((xdrproc_t)xdr_database_list, (char*) dblist);
	return 0;
}

