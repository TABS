
#include	<time.h>
#include	"statush.h"

static table tab;
static time_t last_change_time =0;

bool_t * SVC(new_user_2)(ws_user_pair * data, struct svc_req *rqstp)
{
    static bool_t result = FALSE;
    int labstart;
    int wsres,userres;
    int tableind;
    if (ntohs(svc_getcaller(rqstp->rq_xprt)->sin_port) > 1024) {
	return (&result);
    }

    /* THINKS:
       when a user logs on, if they are currently allocated somewhere else in the lab,
       we should re-allocate them.
       If THIS workstation is unallocated, just swap status and set alloc id
       If it is tentative  there could be a race with someone claiming.
       So must get "claim" to check if anyone logged in, and possibly reallocate.
     */
    
    tableind = lookup_table(&tab, WSSTATUSCLUSTER, data->wsid);
    if (tableind == -1)
	return (&result);

    /* look back for start of lab */
    for (labstart = tableind ; labstart>0 &&  Cluster_type(&tab,labstart)!=ALLOCCLUSTER ; labstart--)
	;
    /* now look for WSRESCLUSTER for this ws and this user */
    wsres=0; userres=0;
    for (labstart++ ; labstart < tab.table_len && Cluster_type(&tab,labstart) != ALLOCCLUSTER ; labstart++)
    {
	if (Cluster_type(&tab,labstart)== WSRESCLUSTER
	    && Cluster_id(&tab,labstart)== data->wsid)
	    wsres = labstart;
	if (Cluster_type(&tab,labstart) == WSRESCLUSTER
	    && (Res_status(&tab,labstart) == NODEALLOCATED || Res_status(&tab,labstart) == NODETENTATIVE)
	    && Res_user(&tab,labstart) == data->userid
	    )
	{
	    userres = labstart;
	}
    }
    if (userres>0 &&wsres > 0 && userres != wsres)
    {
	/* user is allocated somewhere else in the lab
	 */
	cluster cl;
	cl.data = tab.table_val[userres].data;
	tab.table_val[userres].data = tab.table_val[wsres].data;
	tab.table_val[wsres].data = cl.data;
	tab.table_val[userres].timestamp++;
	tab.table_val[wsres].timestamp++;
    }
	   
    Ws_user(&tab,tableind) = data->userid;
    Ws_whenon(&tab,tableind) = time(0);
    Ws_whereon(&tab,tableind) = data->hostid;
    /* set whenalloc to 15 minutes ago. Should be 30, but I am
     * being kind
     */
    Ws_whenalloc(&tab,tableind) = time(0) - (15*60);
    Cluster_modtime(&tab,tableind) = time(0);
    
    last_change_time = time(0);

    result = TRUE;
    return(&result);
}

bool_t * SVC(res_user_2)(ws_user_pair * data, struct svc_req *rqstp)
{
    static bool_t result = FALSE;
    int tableind;
    time_t now;
    
    if (ntohs(svc_getcaller(rqstp->rq_xprt)->sin_port) > 1024) {
	/* FIXME check host */
	return (&result);
    }
    tableind = lookup_table(&tab, WSRESCLUSTER, data->wsid);
    if (tableind == -1)
	return (&result);
    tab.table_val[tableind].data.data_val[0] = data->userid;
    tab.table_val[tableind].data.data_val[1] = NODEALLOCATED;
    now = time(0);
    now += 5*60; /* always make reservations in the future */
    if (tab.table_val[tableind].timestamp >= now)
	tab.table_val[tableind].timestamp++;
    else
	tab.table_val[tableind].timestamp = now;
    
    last_change_time = time(0);


/*     printf("Reserve for  user %d\n",*user); */
    result = TRUE;
    return(&result);
}

bool_t * SVC(res_user_time_2)(resinfo * data, struct svc_req *rqstp)
{
    static bool_t result = FALSE;
    int tableind;
    
    if (ntohs(svc_getcaller(rqstp->rq_xprt)->sin_port) > 1024) {
	/* FIXME check host */
	return (&result);
    }
    tableind = lookup_table(&tab, WSRESCLUSTER, data->wsid);
    if (tableind == -1)
	return (&result);
    tab.table_val[tableind].data.data_val[0] = data->userid;
    tab.table_val[tableind].data.data_val[1] = NODEALLOCATED;

    if (tab.table_val[tableind].timestamp >= data->when)
	tab.table_val[tableind].timestamp++;
    else
	tab.table_val[tableind].timestamp = data->when;
    
    last_change_time = time(0);

    result = TRUE;
    return(&result);
}


bool_t * SVC(set_bookstatus_2)(node_pair * np, struct svc_req *rqstp)
{
    int posintab;
    static bool_t result = FALSE;
    
    if (ntohs(svc_getcaller(rqstp->rq_xprt)->sin_port) > 1024) {
	/* FIXME check host */
	return (&result);
    }
    posintab = lookup_table(&tab, WSRESCLUSTER, np->wsid);
    if (posintab == -1)
	return (&result);
    tab.table_val[posintab].data.data_val[1] = np->status;
    tab.table_val[posintab].timestamp = time(0);
    last_change_time = time(0);

    result = TRUE;
    return(&result);
    
}
bool_t * SVC(set_wsstatus_2)(node_pair * np, struct svc_req *rqstp)
{
    int posintab;
    static bool_t result = FALSE;
    
    if (ntohs(svc_getcaller(rqstp->rq_xprt)->sin_port) > 1024) {
	/* FIXME check host */
	return (&result);
    }
    posintab = lookup_table(&tab, WSSTATUSCLUSTER, np->wsid);
    if (posintab == -1)
	return (&result);
    tab.table_val[posintab].data.data_val[1] = np->status;
    tab.table_val[posintab].timestamp = time(0);
    last_change_time = time(0);

    result = TRUE;
    return(&result);
    
}

/* MERGE_TABLE: merges the old table with the new */
/* careful about making sure data is COPIED, so that the one of the */
 /* tables can be freed */
/* Assume that the tables are sorted by clustertype and identifier */
/* careful about missing entries */
static void merge_table(table * newtab, table * oldtab)
{
    int i;
    /* in newtab, copy new entries from oldtab */

    for (i=0;i < newtab->table_len;i++)
    {
	int oldloc;
	oldloc = lookup_table(oldtab,newtab->table_val[i].cltype,
			      newtab->table_val[i].identifier);
	if (oldloc == -1)
	    continue;
	/* otherwise copy if timestamp in oldtab is newer */
	if (newtab->table_val[i].timestamp <
	    oldtab->table_val[oldloc].timestamp )
	{
	    int ind;

	    /* free the data in the new */
	    free(newtab->table_val[i].data.data_val);

	    /* copy the cluster accross */
	    bcopy(&oldtab->table_val[oldloc], &newtab->table_val[i],
		  sizeof(cluster));

	    /* allocate some more space for the data */
	    newtab->table_val[i].data.data_val =
		(int *)malloc(oldtab->table_val[oldloc].data.data_len * sizeof(int));
	    newtab->table_val[i].data.data_len =
		oldtab->table_val[oldloc].data.data_len;

	    /* copy the data in the oldtab cluster */
	    for (ind = 0; ind < newtab->table_val[i].data.data_len;ind++)
		newtab->table_val[i].data.data_val[ind] =
		    oldtab->table_val[oldloc].data.data_val[ind];
	    
	}
    }
}


table_chunk * SVC(get_chunk_2)(chunk_index * ch_ind, struct svc_req *rq)
{
    static table_chunk result;
    static char xdrbuf[8190];  /* this should be dynamic */
    static XDR xdrs;

/*     printf("tablen %d\n",tab.table_len); */
    
    if (tab.table_len == 0 || last_change_time > result.chnk.timestamp )
    {
/*	printf("Create copy (last_change %d, result.time %d)\n",
	       last_change_time, result.chnk.timestamp); */
	xdrmem_create(&xdrs, xdrbuf, sizeof(xdrbuf), XDR_ENCODE);
	if (!xdr_table(&xdrs, &tab)) {
	    /* this may not work */
	    result.data.data_val = NULL;
	    result.data.data_len =0;
	    return(&result);
	}
	result.chnk.max = XDR_GETPOS(&xdrs);
	result.chnk.timestamp = last_change_time;
    }
    if (*ch_ind > result.chnk.max) {
	result.data.data_val = NULL;
	result.data.data_len =0;
	return(&result);
    }
    result.chnk.index = *ch_ind;
    result.data.data_val =  xdrbuf + result.chnk.index ;
    result.data.data_len = result.chnk.max - result.chnk.index;
    if (result.data.data_len > XFERSIZE)
	result.data.data_len = XFERSIZE;

/*    printf("returning %d\n",result.data.data_len); */
    
    return(&result);
     
}
/* do a merge, free the old, copy in the update */
/* update will be freed somewhere else */
static void update_table(table * update)
{
    /* this is where there is no table */
    if (tab.table_len == 0) {
	table tmp;
	tmp = tab;
	tab = *update;
	*update = tmp;
	
	last_change_time = time(0);
	return;
    }

    if (update->table_len ==0 )
	return;

    /* the update is newer than this table... not a usual situation */
    /* FIX THIS */
    if (update->table_val[0].timestamp > tab.table_val[0].timestamp)
    {
	table tmp;
	tmp = tab;
	tab = *update;
	*update = tmp;
    }

    merge_table(&tab, update); /* no need to free update as it */
				   /* will be freed somewhere else*/

    last_change_time = time(0);
}

static int give_count = -2;
static time_t take_time = 0;
chnkxfer_status * SVC(set_chunk_2)(table_chunk * new, struct svc_req *rqstp)
{
    static char *xdrbuf;
    XDR xdrs;
    table newtab;
    static chnkxfer_status result;
    static int table_xfer_index = 0;
    static time_t table_xfer_time;
    static int table_xfer_from;
    time_t now;
    struct sockaddr_in * clxprt;

    clxprt = svc_getcaller(rqstp->rq_xprt);
    time(&now);
    if (new->chnk.index == 0)
    {
	/* limit incoming tables to two consecutive gives between takes */
	if (clxprt->sin_addr.s_addr == htonl(0x7f000001))
	{
	    /* it is from localhost, accept it */
	    give_count = 0;
	}
	if (give_count >= 2 && take_time < now-2*60)
	{
	    give_count++;
	    if (give_count > 20)
	    {
		give_count = 2;
		SVC(restart_helper_2)(NULL, NULL); /* the helper must be dead... */
	    }
	    else check_helper(1);
	    return NULL;
	}
    }
    /* check that time stamp is really an appropriate TIME */
    /* this makes sure the following check never blocks for ever */
    if (new->chnk.timestamp < now-180 || new->chnk.timestamp > now+120)
    {
	return NULL;
	/* either someones clock is wrong, or someone is doing the Wrong Thing (TM). In
	 * either case, pretend we never heard.
	 */
    }
    
    /* check that an old transfer has not aborted, always accepted newest table
     * this could cause to sender to continually override each other, but our
     * sender (node_manager) is too polite for that
     */
    if ((table_xfer_time != 0)  && (table_xfer_time < new->chnk.timestamp)) {
	free(xdrbuf);
	table_xfer_time = 0;
    }
    /* if this is old, reject it */
    if (table_xfer_time > new->chnk.timestamp)
    {
	result = CHUNK_REFUSED;
	return &result;
    }
    /* if same time, but different source machine, reject */
    if (table_xfer_time == new->chnk.timestamp
	&& table_xfer_from != clxprt->sin_addr.s_addr)
    {
	result = CHUNK_REFUSED;
	return &result;
    }
    /* this is the first... set things up */
    if (table_xfer_time == 0 && new->chnk.index == 0) {
	table_xfer_time = new->chnk.timestamp;
	table_xfer_index = 0;
	table_xfer_from = clxprt->sin_addr.s_addr;
	xdrbuf = (char *)malloc(new->chnk.max);
    }

    /* if same timestamp (and so same home) but lesser index, assume resend and accept */
    if (table_xfer_time == new->chnk.timestamp &&
	table_xfer_index > new->chnk.index)
    {
	result = CHUNK_GOT;
	return &result;
    }
    
    if (table_xfer_index != new->chnk.index)
    {
	result = CHUNK_REFUSED;
	return &result;
    }
    /* copy the data */
    bcopy(new->data.data_val, xdrbuf +new->chnk.index,
	  new->data.data_len);
    table_xfer_index += new->data.data_len;

    /* this is the end of the road */
    if (new->chnk.max == table_xfer_index)
    {

	xdrmem_create(&xdrs, xdrbuf, new->chnk.max, XDR_DECODE);

	/* what if this fails */
	memset(&newtab, 0, sizeof(newtab));
	xdr_table(&xdrs, &newtab);

	update_table(&newtab);
	give_count++;

	/* now signal the node_manager process so it can send the table on */
	check_helper(1);
	xdr_free((xdrproc_t)xdr_table, (char*) &newtab);
	free(xdrbuf);
	table_xfer_time =0;
    }
    result = CHUNK_GOT;
    return &result;
       
}
void * SVC(have_taken_2)(void * dummy, struct svc_req *rq)
{
    give_count = 0;
    time(&take_time);
    return dummy;
}
