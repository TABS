
#include	"statush.h"
extern CLIENT *status_client;


/* from choose_db.c */
int refresh_db_client(void);

/* from cache_names.c */
void open_cachef(char *mode);
void reset_cache();
int get_mappingint_cache(char *name, int type);
char *get_mappingchar_cache(int num, int type);

/* from collect_table.c */
bool_t collect_tab(table * newtab, CLIENT *cl);

/* from manager_access */
void open_status_client(char * host);

/* from process_exclusions.c */
description * process_exclusions(int pod, int doy, description * desc);

/* from whoison.c */
int get_whoison(char *wsname, time_t *when);
void set_whoison(char *wsname, int who);

/* from print_tab.c */
void print_tab(table *tab, FILE *f);

/* from up_host.c */
CLIENT *up_host(hostgroup_info *hg, bool_t last);

/* from check_servers.c */
void check_servers(void);
