
#include	<unistd.h>
#include	<stdlib.h>
#include	<time.h>
#include	"helper.h"

static int last_send;
    
#define WAITTIME 30 /* number of seconds to wait before sending */
		    /* another table on.. applies to the master */
#define OLDTIME 180  /* number of seconds before the table in the */
		     /* cache becomes invalid, and a newone is created */


/* before allocations are made, make sure that the table does not have */
 /* any nodes in it that are suspicioius.  Sus nodes have not */
 /* responded for > 5 mins even though they appear to be up...*/
bool_t isvalid_table(table * tab)
{
    int i;
    for (i=0;i < tab->table_len;i++)
	if ((tab->table_val[i].cltype == HOSTCLUSTER &&
	     Host_status(tab,i) == NODEUP)
	    && (time(0) > tab->table_val[i].timestamp + 5 *60))
	{
	    char buf[128];
	    sprintf(buf,"Table is invalid, node (index i %d) %d has bad time\n",
		    i, tab->table_val[i].identifier); 
	    shout(buf);
	    return FALSE;
	}
    
    return TRUE;
    
}


/* put table into a buffer.  send chunks of table until NULL or CHUNK */
 /* REFUSED is received or whole table send */
/* returns 0 on failure, 1 on refused, 2 on success */
int send_table(table * tab, CLIENT * cl)
{
    chnkxfer_status * result;
    static table_chunk tabdata;
    static time_t last_stamp = 0;
    XDR xdrs;
    int failcnt = 3;
    static int  xdrbuf[8190/4]; /* this should be dynamic,
				   must be int for proper alignment */
    
    xdrmem_create(&xdrs, (char*)xdrbuf, sizeof(xdrbuf), XDR_ENCODE);
    if (!xdr_table(&xdrs, tab))
    {
	shout("xdr_Table failed\n");
	return 0;
    }

    
    tabdata.chnk.max = XDR_GETPOS(&xdrs);
    tabdata.chnk.index = 0;
    tabdata.chnk.timestamp = time(0);
    if (tabdata.chnk.timestamp <= last_stamp)
	tabdata.chnk.timestamp = ++last_stamp;
    last_stamp = tabdata.chnk.timestamp;
    
/*    printf("max %d, index %d\n",tabdata.chnk.max, */
 /*    tabdata.chnk.index); */
    
    while (tabdata.chnk.index < tabdata.chnk.max) {
	tabdata.data.data_val =  ((char*)xdrbuf) + tabdata.chnk.index ;
	tabdata.data.data_len = tabdata.chnk.max - tabdata.chnk.index;
	if (tabdata.data.data_len > XFERSIZE)
	    tabdata.data.data_len = XFERSIZE;

	result = set_chunk_2(&tabdata, cl); 
	if (result == NULL)
	{
	    char buf[200];
	    sprintf(buf, "set_chunk_2: %d+%d of %d failed\n", tabdata.chnk.index,
		    tabdata.data.data_len,  tabdata.chnk.max);
	    shout(buf);
	    if (failcnt-- > 0)
		continue;
	    else
		return 0;
	}
	if (* result == CHUNK_REFUSED )
	    return 1;
	tabdata.chnk.index += tabdata.data.data_len;

    }
    return 2;
}

/* NEXT_HOST: given a table and an index, returns the index of the */
 /* next host entry in the table that is up.  If passed index ==0, */
 /* returns the master. */
int next_host(table * tab, int index)
{
    int pos;

    pos = index +1;
    pos = pos % tab->table_len;
    
    while (pos != index) {
	
	/* if it is a host record and the host is up, this is good */
	if ((Cluster_type(tab,pos) == HOSTCLUSTER)
	    && (Host_status(tab,pos) == NODEUP))
	    return pos;
	pos++;
	pos = pos % tab->table_len;
    }
    return pos;
    
}

/* GIVE_TABLE: send table onto next node.  If index == -1, send to */
 /* local bind */
/* if can't send to node, mod table send to self */
/* if can't send to self exit */
/* return status.  TRUE sent to another node */
/* FALSE.. sent to self */
bool_t give_table(table * t, int index)
{
    char errbuf[200];
    char * hostname;
    CLIENT * cl;
    bool_t result = TRUE ;
    int send_result;
    

    /* create the RPC handle */
    if (index == -1)
    {
	cl = status_client;
	Debug("send to self\n", 1);
    }
    else
    {
	/* convert to a hostname */
	hostname = get_mappingchar(Cluster_id(t,index), M_HOST);
	
	if (hostname == NULL) {
	    sprintf(errbuf,"Bad mapping for index %d,id =%d\n", index,
		    Cluster_id(t,index)); 
	    shout(errbuf);
	    exit(5);
	}
	sprintf(errbuf,"send host hostname = %s\n",hostname);
	Debug(errbuf,1);
	/* create client */
 	cl = my_clntudp_create(hostname, BOOK_STATUS, BOOKVERS); 
	if (cl == NULL) {
/*	    clnt_pcreateerror(hostname); */
	    free(hostname);
	    if (Cluster_id(t,index) != my.hostid)
		Host_status(t,index) = NODEDOWN;
	    Cluster_modtime(t,index) = time(0);
	    give_table(t, -1);
	    return FALSE;
	}
	free(hostname);
    }
    /* now send table */
    while ((send_result = send_table(t, cl))==1)
    {
	Debug("Send was not accepted, sleep 2...\n", 1);
	sleep(2);
    }
    if (send_result == 0)
    {
	if (index == -1)
	{
	    /* could not send to self */
	    shout("Could not contact our bind\n");
	    if (getppid()==1)
		exit(6);
	    open_status_client("localhost");
	}
	else
	{
	    /* mod table */
	    sprintf(errbuf,"send failed\n");
	    Debug(errbuf,1);

	    Host_status(t,index) = NODEDOWN;
	    Cluster_modtime(t, index) = time(0);

	    /* give back to self */
	    give_table(t, -1);
	}
	result = FALSE;
    }
    if (index != -1)
    {
	if (result != FALSE)
	    last_send = time(0);
	clnt_destroy(cl);
    }
    return result;
}


/* if a host is marked down, we never try to talk to it, so it may
 * come back up and not be noticed.
 * This routine will mark one down-host as up so that a table gets sent
 * to it. If it is still down, this will be noticed and the host will be marked
 * down again.
 * Only consider hosts which were last checked more than 5 minutes ago
 */
static void check_down(table *new)
{
    int i;
    int oldest =  -1;

    for (i=0;i< new->table_len;i++)
    {
	/* look for oldest host that is down */
	if ((Cluster_type(new,i) == HOSTCLUSTER) &&
	    (Host_status(new,i) == NODEDOWN))
	{
	    if (oldest < 0
		|| Cluster_modtime(new,i) <Cluster_modtime(new,oldest))
		oldest =i;
	}
    }
	    
    /* if found, and down for 5 mins , update it */
    if ((oldest >= 0) &&(Cluster_modtime(new,oldest) +
				300 < time(0)) )
    {
	sprintf(errbuf, "MANAGER setting %d up again\n",
		Cluster_id(new,oldest ));
	Debug(errbuf,1);
	Cluster_modtime(new,oldest) = time(0);
	Host_status(new, oldest) = NODEUP;
    }
	    
}

/* PROPAGATE_TABLE: called when a table has been received by the */
 /* manager or WAITTIME seconds */ 
 /* has elapsed.  It performs allocations if necessary, and evicts the */
 /* current user if necessary.  It also tries to send the table on to */
 /* the next node in the table */
 /* return value is seconds to wait until time to try again */
int propagate_table()
{
    char errbuf[200];
    static table  new;
    bool_t made_newtable = FALSE;
    bool_t allocated = FALSE ;
    int my_index;
    int master_index;

    int nexttime;
    


    /* if could not get table */
    /* book_bind may have been restarted, and we were not killed */
    if (! collect_tab(&new, status_client))
    {
	sprintf(errbuf,"Error getting table from host\n");
	Debug(errbuf,1);
	if (getppid() == 1)
	    exit(1);
	else
	{
	    /* try again soon */
	    return 10;
	}
    }
    have_taken_2(NULL, status_client);

    sprintf (errbuf,"propagate\n");
    Debug(errbuf,1);

    if (new.table_len == 0)
    {
	new_table(&new);
	made_newtable = TRUE;
    }
    else
    {
	if (am_master)
	{
	    time_t wait_time;
	    wait_time = last_send + WAITTIME - time(0);
	    if (wait_time > 2)
	    {
		xdr_free((xdrproc_t)xdr_table, (char*) &new);
		return wait_time;
	    }
	    check_down(&new);
	}
	/* check on the status of the workstations attached*/
	check_status(&new);
    }

    my_index = lookup_table(&new, HOSTCLUSTER, my.hostid);
    /* make sure I am marked as up, now! */
    Host_status(&new,my_index) = NODEUP;
    Cluster_modtime(&new,my_index) = time(0);

    master_index = next_host(&new, 0);
    am_master = (my_index == master_index);


    if (am_master
	&& isvalid_table(&new)
	&& allocate(&new)>0)
    {
	allocated = TRUE;
    }

    if (allocated || made_newtable)
    {
	/* made significant change, make sure info safe with my book_status */
	give_table(&new,-1);
	nexttime = 2;
    }
    else
    {
	if (give_table(&new, next_host(&new, my_index)))
	    nexttime = 3 * WAITTIME;
	else
	    nexttime = 2;
    }
    /* perform evictions and check for defaulters after table safely passed on */
    check_default(&new);
    perform_evictions(&new);

    xdr_free((xdrproc_t)xdr_table, (char*) &new);
    return nexttime;
}

void send_new_table(void)
{
    table new;
    new_table(&new);
    give_table(&new, -1);
}
