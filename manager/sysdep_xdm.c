#include	<stdio.h>
#include        <stdlib.h>
#include	<string.h>
#include        <unistd.h>
#include	<fcntl.h>
#include        <signal.h>
#include        <sys/socket.h>
#include        <netinet/in.h>
#include        <netdb.h>
#include        "helper.h"

char control_pid_dir[] = "/var/book/xdm-pid/"; /*  /var/X11/xdm/xdm-pids */

int send_message(char *mesg)
{
    int sock;
    int lport = IPPORT_RESERVED -1;
    static int port = 0;
    
    struct sockaddr_in server;

    if (port == 0)
    {
	struct servent * service;

	if ((service = getservbyname("message", "tcp")) == NULL )
	{
	    perror("getting service by name");
	    return(-1);
	}
	port = service->s_port;
    }

    sock = rresvport(&lport);
    if (sock < 0) {
/*	    perror("rresvport: socket"); */
	    return(-1);
    }

    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(0x7f000001);
    server.sin_port = port;

    if (connect(sock, (struct sockaddr *) &server, sizeof(server)) != 0)
    {
	close(sock);
/*	perror("Connect failed"); */
	return(-1);
    }

    write(sock, mesg, strlen(mesg));
    close(sock);
    return 1;
}

static int find_xdmpid(char * wsname)
{
    char buf[128];
    int fd;
    int pid=0;
    strcpy(buf, control_pid_dir);
    strcat(buf, wsname);
    fd = open(buf, 0); 


    if (fd >= 0)
    {
	read(fd, buf, 20);
	pid = atoi(buf);
	close(fd);
    }
    return pid;
}

void force_logoff(char *ws)
{
    pid_t xdmpid;

    xdmpid = find_xdmpid(ws);
    /* changed to HUP - simonb 20sep2005. gdm doesn't deal with TERM quite as nicely
     * and xdm seems to not mind.
     * 25oct2005 simonb - change back to TERM after fixing gdm. xdm did handle
     *                    it differently, and gdm mismanaged this situation with its slave IMO.
     */
    kill(xdmpid, SIGTERM); /* FIXME check something... */

    //kill(xdmpid, SIGHUP); /* FIXME check something... */

}
