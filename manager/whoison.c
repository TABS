
#include	<unistd.h>
#include	<stdlib.h>
#include	<time.h>
#include	<sys/file.h>
#ifdef sun
#include	<sys/fcntl.h>
#endif
#include	<sys/stat.h>
#include	<string.h>
#include	<stdio.h>
 
#define WhoIsOn  "/var/book/whoison"

int get_whoison(char *wsname, time_t *when)
{
	char fname[1024];
	char buf[20];
	struct stat stb;
	int fd, n;

	strcpy(fname,WhoIsOn);
	strcat(fname,"/");
	strcat(fname,wsname);
    
	fd = open(fname, O_RDONLY);
	if (fd == -1 )
	{
		if(when)
			time(when);
		return -1;
	}

	n = read (fd, buf, 20);
	fstat(fd, &stb);
	close(fd);
	if (when)
		*when = stb.st_mtime;
	if (n == 0)
		return -1;

	return(atoi(buf));
}

void set_whoison(char *wsname, int who)
{
	char fname[1024];

	strcpy(fname,WhoIsOn);
	strcat(fname,"/");
	strcat(fname,wsname);

	if (who < 0)
		unlink(fname);
	else
	{
		char buf[20];
		int fd;
		fd = open(fname, O_WRONLY|O_CREAT|O_TRUNC, 0600);
		if (fd == -1 )
			return;

		if (who>= 0)
		{
			sprintf(buf, "%d\n", who);
			write(fd, buf, strlen(buf));
		}
		close(fd);
	}
}
