

const  XFERSIZE = 1000;


enum database_state {
    DB_CLOSE	= 0,	/* up, writable, and near by */
    DB_UP 	= 1,	/* up, writable */
    DB_UNKNOWN	= 2,	/* don't know the state of this DB */
    DB_READONLY = 3,	/* not writable, but up */
    DB_DOWN	= 4,	/* not contactable */
    DB_UNREGISTERED= 5	/* not registered as a valid server */
};

struct database_info {
	string 		name<>;
	database_state	state;
    };

typedef database_info database_list<>;

/* this data structure  is used to implement the status of the lab. */
/* This is dynamic information that concerns who is on a machine, who */
/* has reserved a machine, etc.*/

enum node_status
{
	NODEUP = 1,
	NODEDOWN = 2
};


enum book_status
{
	NODEOUTOFSERVICE = 1,
	NODEBROKEN = 2,
	NODENOTBOOKABLE = 3,
	NODENOTALLOC = 4,
	NODEALLOCATED = 5,
	NODETENTATIVE = 6
};

enum period_status
{
	PERIODFREE = 1,
	PERIODFULL = 2,
	LABCLOSED = 3
};

enum cluster_type
{
	TABLECLUSTER = 1,
  	ALLOCCLUSTER = 3,
	HOSTCLUSTER = 5,
	WSSTATUSCLUSTER =7,
 	WSRESCLUSTER = 9
};
struct cluster{
	cluster_type cltype;
	int identifier;
	int timestamp;
	int data<>;
};

struct table_key{
	cluster_type cltype;
	int identifier;
};
typedef  cluster table<>;

struct node_pair {
	book_status status;
	int wsid;
};

struct ws_user_pair {
	int wsid;
	int userid;
	int hostid;
};

struct resinfo {
	int wsid;
	int userid;
	int when;
};

/* some stuff for xfer of tables */
enum chnkxfer_status
{
	CHUNK_GOT = 1,
	CHUNK_REFUSED = 2
};
typedef int chunk_index;
struct chunk_info{
	chunk_index index;
	chunk_index max;
	int timestamp;
};
struct table_chunk
{
	opaque data<>;
	chunk_info chnk;
};

program BOOK_STATUS {
	version BOOKVERS {

	        database_list
			GET_DATABASES(void) = 30;
		void
			SET_DATABASE(database_info) = 32;

		bool
			NEW_USER(ws_user_pair) = 42;
		bool
			RES_USER(ws_user_pair) =  44;
		bool
			RES_USER_TIME(resinfo) = 45;
		bool
			SET_BOOKSTATUS(node_pair) = 46;

		table_chunk
			GET_CHUNK(chunk_index) =53;
		chnkxfer_status
			SET_CHUNK(table_chunk) = 55;
		void
			HAVE_TAKEN(void)  = 56;
						
		int
			RESTART_HELPER(int) = 60;

		void 	STATUS_DIE(void) = 100;
	} = 2;
} = 0x20304051;
