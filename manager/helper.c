
#include	<unistd.h>
#include	"statush.h"
#include	"../lib/misc.h"
#include	<sys/wait.h>
#include	<sys/signal.h>

int helper_pid;

static char *helper_prog;

/* forks a manager process */
void start_node_manager(char *program)
{
	int i;

	static char *args[2];
	if (program)
		helper_prog = program;
	if ((helper_pid = fork()) == 0) {
		/* close all sockets opened by parent except standard ones */
		for (i = 3 ; i <20;i ++)
			close(i);
		/* have to do an exec.  can't just call the helper as another */
		/* procedure as both the client and server stubs would have to */
		/* be linked in.  both client and server stubs contain */
		/* different definitions for remote procedures. */
		args[0] = helper_prog;
		args[1] = NULL;
		i = execv(helper_prog,args);
		if (i != 0) {
			printf("exec failed!, status %d\n",i);
			exit(4);
		}
	}
}

int * SVC(restart_helper_2)(int *dummy, struct svc_req *rq)
{
	char errbuff[200];
	static int result;
    
	result = kill(helper_pid,SIGTERM );
	if (wait(NULL) == -1)
	{
/*	printf("Child was dead already!!!!\n"); */
	}
    
    
	start_node_manager(NULL);
	result = helper_pid;
	sprintf(errbuff, "New Node Manager: pid = %d\n",helper_pid);
	shout(errbuff);
/*    printf("Restart %s", errbuff);*/
    
	return(&result);
}

void check_helper(int waken)
{
	/* restart helper if it is dead */
	int pid;
#ifdef ultrix
	union wait status;
	while ( (pid = wait3(&status, WNOHANG, (char*) NULL)) >  0)
		;
#else	
	int status;
	while ( (pid = waitpid((pid_t) -1, &status, WNOHANG)) > 0)
		;
#endif
	if (kill(helper_pid, 0) < 0) {
		/* it died */
		start_node_manager(NULL);
	}  else if (waken)  {
		/* just make sure it is awake */ 
		kill(helper_pid, SIGALRM);
	}
}
		
