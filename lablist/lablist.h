

/*
 * interface to the lab listing programs "lab" and "skyterm"
 *
 * will provide a list of ws or hosts for each lab in a list
 *
 * make_lablist() -> empty list
 * add_lab(lablist, name) -> success if lab is know, this supports lab groups
 *				through attributes common to a set of labs
 * load_labs(lablist)
 *  fills in info, freeing old info
 *
 * the info in each list is an array of workstations - see below
 * and a labinfo structure for the lab
 */

#include	"../db_client/db_client.h"
#include	"../lib/dlink.h"

typedef struct hostg
{
    int		hgid;
    CLIENT	*handle;
    hostgroup_info info;
    table	tab;
    time_t	tabtime;
} *hostg;

typedef struct workstation
{
    entityid	wsid;
    cluster	*status;
    cluster	*alloc;
} *workstation;

typedef struct labinfo
{
    entityid	labid;
    char	*labname;
    hostinfo	info;
    hostg	hg;

    cluster	*alloc;

    workstation	wslist;
    int		wscnt;
} *labinfo;

#define	lablist_make() (dl_head())
#define	lablist_empty(l) (l == dl_next(l))
#define lablist_next(l) ((labinfo)dl_next(l))

char *add_lab(void *, char *);
void load_labs(void *);
