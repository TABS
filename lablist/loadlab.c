#include	<time.h>
#include	"lablist.h"
    
int load_table(hostg hg)
{
    if (hg->tabtime)
	xdr_free((xdrproc_t)xdr_table, (char*) &hg->tab);
    hg->tabtime = 0;

    if (hg->handle == NULL
	|| collect_tab(&hg->tab, hg->handle)== 0)
    {
	if (hg->handle) clnt_destroy(hg->handle);
	hg->handle = up_host(&hg->info, TRUE);
	if (hg->handle == NULL
	    || collect_tab(&hg->tab, hg->handle)== 0)
	    return -1;
    }
    hg->tabtime = time(0);
    return 0;
}



int load_lab(labinfo li, time_t start)
{
    table *tab;
    int labstart;
    int c;
    int i;
    if (li->hg->tabtime < start)
    {
	li->alloc = NULL;
	if (li->wslist) free(li->wslist);
	li->wslist = NULL;
	if (load_table(li->hg) < 0)
	    return -1;
    }
    
    tab = &li->hg->tab;

    labstart = -1;
    for (c=0 ; labstart == -1 && c<tab->table_len ; c++)
	if (Cluster_type(tab, c)== ALLOCCLUSTER
	    && Cluster_id(tab, c) == li->labid)
	    labstart = c+1;
    if (labstart == -1)
	return -1;

    li->alloc = &tab->table_val[labstart-1];

    if (li->wslist == NULL)
    {
	li->wscnt = li->info.workstations.entitylist_len;
	li->wslist = (workstation)malloc(sizeof(struct workstation)*li->wscnt);
    }
    for (i=0 ; i<li->wscnt ; i++)
    {
	int wsid = Cluster_id(tab,labstart+2*i);
	workstation ws = &li->wslist[i];
	assert(Cluster_type(tab,labstart+2*i) == WSSTATUSCLUSTER, "not a statuscluter");
	assert(Cluster_type(tab,labstart+2*i+1) == WSRESCLUSTER, "not a rescluster");
	assert(Cluster_id(tab,labstart+2*i+1) == wsid, "wrong wsid");

	ws->wsid = wsid;
	ws->status = & tab->table_val[labstart+2*i];
	ws->alloc = & tab->table_val[labstart+2*i+1];
    }
    return 0;
}

void load_labs(void *lablist)
{
    labinfo li;
    time_t start = time(0);
    
    for (li = dl_next(lablist) ; li != lablist ; li = dl_next(li))
	load_lab(li, start);
}

