
#include	<sys/types.h>
#include	<time.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	<sys/ioctl.h>
#include	"lablist.h"
#include	"../lib/skip.h"
#include	<termios.h>
#include	<curses.h>
#include	<term.h>
#include	<getopt.h>
/*
 * bookterm - repeatedly display allocation info with curses
 *
 * display is a 2 line banner, a blank line, a number of columns,
 * and a final blank line.
 * Each column is 24 characters wide with a 3 space column separator
 *
 * The banner gives {}{labnames}{Page n of m}
 *                  {reservations for Period}{}{date-of-display}
 *
 * Each row in the columns contains some info about state of a
 * workstation and the name of the workstation, with a row of dots between
 * to stretch the column to full width.
 *
 * Labs are separated by two blank rows.
 *
 * each workstation produces one or possibly two rows.
 * Also, each unsucessfull allocation may produce a row.
 *
 * The different rows that are possible are:
 * DOWN..........workstation	workstation appears to be down
 * DEAD..........workstation	workstation is out-of-service
 * AVAILABLE.....workstation	workstation is free for anyone to use
 * INUSE.........workstation	workstation is inuse
 * sur-username..workstation	workstation is reserved for username
 *				either alloc still valid, or not inuse
 * sur!username..workstation	user is logged on, but noone allocated
 * sur+username..workstation	user is on and allocated
 * sur?username..workstation	user is on, but allocated to someone else
 * sur*username..workstation	user is allocated, but will have to claim
 * sur*username........CLAIM	user is allocated but must reclaim
 *
 * sur-username.....REFUNDED	allocation could not be made
 *
 * the "sur" means the first three characters of the user's surname
 *
 * The rows are sorted either by workstation name or by state.
 * when by state, the username rows come first, then AVAILABLE INUSE DOWN DEAD
 *
 * Each row type can be selectivly ignored.
 *
 */

#define	USER_ALLOC	1
#define	USER_ON		2
#define	USER_ONALLOC	4
#define	USER_CONFLICT	8
#define	USER_CLAIM	16
#define	USER_REFUND	32
#define	USER_CLAIM2	64
#define USER	       127
#define	AVAIL		128
#define	INUSE		256
#define	DOWN		512
#define	DEAD		1024
#define	TIME_ON		2048

#define	PAGE_SLEEP	5
#define	COL_SLEEP	3

// whether or not to show the first few chars of the surname
// on user lines, default to yes.
static int show_surname = 1;

int row_type(cluster *st, cluster *res, time_t now)
{
	static int grace=0;
	int rv=0;
	if (ws_status(st) == NODEDOWN)
		return DOWN;

	if (grace == 0)
	{
		grace = get_configint("grace");
		if (grace<=0) grace=7*60;
	}

	if (res_status(res) == NODEOUTOFSERVICE)
	{
		rv = DEAD;
		/* there should be no-one on, but check anyway */
		if (ws_user(st) >= 0)
			rv |= USER_ON;
		return rv;
	}
	if (res_status(res) == NODETENTATIVE
	    || (res_status(res) == NODEALLOCATED
		&& now-cluster_modtime(res) > grace))
	{
		/* tentatively reserved */
		if (ws_user(st)< 0)
			rv |= USER_ALLOC;
		else if (in_class(ws_user(st), res_user(res))==0)
			rv |= USER_CLAIM|USER_CLAIM2;
	}
	else if (res_status(res) == NODEALLOCATED
		 && in_class(ws_user(st), res_user(res))==0)
		rv |= USER_ALLOC;
    
	if (ws_user(st) >= 0)
	{
		if (rv & USER_ALLOC)
			rv |= USER_CONFLICT;
		else if (in_class(ws_user(st), res_user(res))==1)
			rv |= USER_ONALLOC;
		else
			rv |= USER_ON;
	}
	if (rv & (USER_ON|USER_ONALLOC))
		rv |= INUSE;
	if (ws_user(st) < 0 && (rv & USER_ALLOC)==0)
		rv |= AVAIL;
	return rv;
}

/* the rows for each lab are built up in an array of the following
 * structure
 */

typedef struct row
{
	int type;
	char text[25];
	int	hoststart;
} *row;

static int host_cmp(row a, row b)
{
	return strcmp(a->text+a->hoststart, a->text+b->hoststart);
}
static int user_cmp(row a, row b)
{
	if (a->type == b->type)
		return strcmp(a->text, b->text);
	else
		return a->type - b->type;
}

/* cache usernames and fullnames to reduce lookups */
typedef struct uscache {
    int id;
    char info[30];
} *uscache;
static int us_cmp(uscache a, uscache b, int *u)
{
	if (b) u = & b->id;
	return a->id - *u;
}
static void us_free(uscache a)
{
	free(a);
}

static void *uslist = NULL;
void set_user(char *r, int uid, char sep)
{
	char *name, *sur;
	uscache *cp;
	int i=0;

	if (uslist == NULL) uslist = skip_new(us_cmp, us_free, NULL);
	cp = skip_search(uslist, &uid);
	if (cp)
	{
		strncpy(r, (*cp)->info, 24);
		if (show_surname) {
			r[3] = sep;
			i += 4;
		}
		r[20+i] = 0;
	}
	else
	{
		uscache ce= (uscache)malloc(sizeof(struct uscache));
		ce->id = uid;
		name = find_username(uid);
		if (name == NULL)
			name = strdup("????");
		if (show_surname) {
			sur = user_surname(name);
			if (sur == NULL) sur = strdup(name);
			strncpy(r, sur, 3);
			i+=3;
			r[i] = 0;
			strcat(r, "---");
			r[i] = sep;
			i++;
			free(sur);
		}
		strncpy(r+i, name, 20);
		r[20+i] = 0;
		strcpy(ce->info, r);
		skip_insert(uslist, ce);
		free(name);
	}
}


static void make_row(row r, cluster *ws, cluster *res, int type, int timeon, char *labname)
{
	int uid = -1;
	int l1, l2;
	char sep=0;
	char *str = NULL;
	char *wsname = NULL;
    
	if (type & USER)
		r->type = USER;
	else
		r->type = type;
    
	switch(type)
	{
	case USER_ALLOC:
		uid = res_user(res); sep = '-'; break;
	case USER_ON:
		uid = ws_user(ws); sep = '!'; break;
	case USER_ONALLOC:
		uid = ws_user(ws); sep = '+'; break;
	case USER_CONFLICT:
		uid = ws_user(ws); sep = '?'; break;
	case USER_CLAIM:
		uid = res_user(res); sep = '*'; break;
	case USER_CLAIM2:
		uid = res_user(res); sep = '*';
		wsname = strndup("CLAIM(", 6+strlen(labname)+2);
		strcat(wsname, labname);
		strcat(wsname, ")");
		break;
	case AVAIL:
		str = "AVAILABLE" ; break;
	case INUSE:
		str = "INUSE"; break;
	case DOWN:
		str = "DOWN"; break;
	case DEAD:
		str = "DEAD"; break;
	}

	if (str)
		strcpy(r->text, str);
	else
	{
		set_user(r->text, uid, sep);
		if (timeon)
		{
			time_t length = time(0)-ws_whenon(ws);
			char tm[4];
			length /= 60;
			if (length < 90)
				sprintf(tm, "%02dm", (int)length);
			else
				sprintf(tm, "%02dh", (int)(length/60));
			strncpy(r->text, tm, 3);
		}
	}

	if (wsname == NULL)
		wsname = get_mappingchar(cluster_id(ws), M_WORKSTATION);
	if (wsname == NULL)
		wsname = strdup("*unknown*");

	l1 = strlen(r->text);
	l2 = strlen(wsname);
	strcpy(r->text+24-l2, wsname);
	while (l1+l2 < 24)
		r->text[l1++] = '.';
	r->hoststart = 24 - l2;
	free(wsname);
}

void make_refund(row r, int uid)
{
	int l1, l2;
	r->type = USER;
	set_user(r->text, uid, '-');
    
	l1 = strlen(r->text);
	l2 = 8;
	strcpy(r->text+24-l2, "REFUNDED");
	while(l1+l2 < 24)
		r->text[l1++] = '.';
	r->hoststart = 24-l2;
}

struct column
{
	row rows;
	int cnt;
};
struct column make_rows(labinfo lab, int types)
{
	row rv;
	int i;
	struct column r2;
    
	int max;
	int rn;
	time_t now = time(0);
	max = lab->wscnt * 3;
	if (alloc_numfailed(lab->alloc)>0)
		max += alloc_numfailed(lab->alloc);
	rv = (row)malloc(sizeof(struct row) * max);
    
	rn = 0;
	if (types & USER_REFUND)
		for (i=0; i < alloc_numfailed(lab->alloc); i++)
			make_refund(&rv[rn++], alloc_failid(lab->alloc,i));
	for (i=0 ; i < lab->wscnt ; i++)
	{
		int t = row_type(lab->wslist[i].status,
				 lab->wslist[i].alloc,
				 now);
		t &= types;
		if (t)
		{
			int t2;
			for (t2 = 1 ; t2 <= DEAD ; t2<<= 1)
				if (t&t2)
					make_row(&rv[rn++],
						 lab->wslist[i].status,
						 lab->wslist[i].alloc,
						 t2,
						 types& TIME_ON, lab->labname);
		}
	}
	r2.rows = rv;
	r2.cnt= rn;
	return r2;
}


void show_banner(char *title, int page, int pages, int pod, int width)
{
	time_t now;
	int pad;
	char pageof[20];
	char reshead[60];
	char *nows;
    
	time(&now);
	sprintf(pageof, "Page %d of %d", page+1, pages);

	pad = width-strlen(title);
	move(0,0);
	printw("%*s%s%*s%s\n",pad/2, "", title,
	       (pad+1)/2 - strlen(pageof), "", pageof);

	sprintf(reshead, "Reservations for Period %d:%02d-%d:%02d",
		pod/2, (pod%2)*30, (pod +1)/2, ((pod+1)%2)*30);

	nows = ctime(&now);
	pad = width - strlen(reshead) - (strlen(nows)-1);
	if (pad<0) pad=0;
	move(1,0);
	printw("%s%*s%s", reshead, pad, "", nows);
}

void put_row(char *rw, int *r, int *c, int *p,
	int rows, int cols, int pages, char *title, int pod)
{
	/* if page is full, display it and pause and clear
	 * add new row
	 */
	if (rw == NULL ||
	    ( *r == 0 && *c == 0 && *p != 0))
	{
		show_banner(title, (*r==0 && *c == 0)?(*p-1):(*p), pages, pod, cols*(24+3)-3);
		refresh();
		if (rw)
			sleep(PAGE_SLEEP+cols*COL_SLEEP);
		erase();
	}
	if (rw)
	{
		move(3+ *r, *c *(24+3));
		printw("%s", rw);
		if ((++*r)>= rows)
		{
			*r = 0;
			if ((++*c) >= cols)
			{
				*c = 0;
				++*p;
			}
		}
	}

}

/* returns columns on last page */
int  show_pages(char *title, int width, int height, int pod, void *collist)
{

	int cols = (width+3)/(24+3);
	int rows = height - 3;
	int pages;
	int total_rows;
	int c,r,p;

	struct column *cl;
	int rw;

	total_rows = -2;
	for (cl=dl_next(collist) ; cl != collist ; cl =dl_next(cl))
		total_rows += cl->cnt + 2;
	pages = (total_rows-1) / (rows*cols) +1;

	erase();
	c=0; r=0; p=0;
	for (cl=dl_next(collist) ; cl != collist ; cl=dl_next(cl))
	{
		for (rw = 0 ; rw < cl->cnt ; rw++)
			put_row(cl->rows[rw].text, &r, &c, &p,
				rows, cols, pages, title, pod);
		if (dl_next(cl) != collist)
		{
			if (r)
				put_row("                        ",  &r, &c, &p,
					rows, cols, pages, title, pod);
			if (r)
				put_row("                        ",  &r, &c, &p,
					rows, cols, pages, title, pod);
		}
	}
	put_row(NULL,  &r, &c, &p,
		rows, cols, pages, title, pod);
	return c;
}

void sort_rows(struct column *c, int (*fn)())
{
	qsort(c->rows, c->cnt, sizeof(struct row), fn);
}

int show_labs(char *title, int width, int height, void *labs, int types, int byws)
{
	void *cols = dl_head();
	labinfo l;
	int pod, doy;
	int last_cols;


	for (l =lablist_next(labs) ; l != labs ; l = lablist_next(l))
		if (l->alloc)
		{
			struct column *c;
			time_t mt;
			c= dl_new(struct column);
			*c = make_rows(l, types);
			sort_rows(c, byws? host_cmp : user_cmp);
			dl_add(cols, c);
			mt = cluster_modtime(l->alloc);
			get_doypod(&mt, &doy, &pod);
		}

	last_cols = show_pages(title, width, height, pod, cols);

	while (dl_next(cols) != cols)
	{
		struct column *c = dl_next(cols);
		dl_del(c);
		free(c->rows);
		dl_free(c);
	}
	dl_free(cols);
	return last_cols;
}

int size_term(int *width, int *height)	
{
	struct winsize win;
	int w = *width;
	int h = *height;
	if (ioctl(1,TIOCGWINSZ,&win) == -1 || win.ws_col == 0)
	{
		*width = 80;
		*height = 24;
	}
	else
	{
		*width = win.ws_col;
		*height = win.ws_row;
	}

	if (w== *width && h == *height)
		return 0;
	else return 1;
}

char *Usage[] = {
    "Usage: bookterm -[wfS] labs",
    "  -w : sort by workstation",
    "  -f : show all records",
    "  -S : don't show surname and flag",
    NULL };


int main(int argc, char *argv[])
{
	int width, height;
	char title[200];
	int autosize = 1;
	void *lablist = lablist_make();
	char *msg;
	int types = USER_ALLOC|USER_CLAIM2|USER_REFUND|AVAIL|DOWN|DEAD;
	int byws = 0;
	int opt;
	time_t new_page = 0;
	extern char *getenv();
	char *term = getenv("TERM");
	int noclear = 0;

	while ((opt = getopt(argc, argv, "wfnS"))!= EOF)
		switch(opt)
		{
		case 'n':
			noclear = 1;
			break;
		case 'w':		/* sort by workstation */
			byws = 1;
			break;
		case 'f':		/* full information */
			types = (USER|AVAIL|DOWN|DEAD|TIME_ON)&~USER_CLAIM2;
			break;
		case 'S':
			show_surname = 0;
			break;
		default:
			usage();
			exit(2);
		}

	title[0]=0;
	for (; optind < argc ; optind++)
	{
		msg = add_lab(lablist, argv[optind]);
		if (msg)
		{
			fprintf(stderr, "bookterm: could not add lab %s: %s\n", argv[optind], msg);
			exit(1);
		}
		if (title[0] == 0)
			strcpy(title, argv[optind]);
		else
			if (strlen(title)+1+strlen(argv[optind])+1 < sizeof(title))
				strcat(strcat(title, "/"), argv[optind]);

	}
	if (lablist_empty(lablist))
	{
		fprintf(stderr, "bookterm: no labs found\n");
		exit(0);
	}

	size_term(&width, &height);
	initscr();
	setterm(term);
	clear();
	refresh();
    
	while(1)
	{
		time_t delay;
		int last_cols;
		load_labs(lablist);
		delay = new_page-time(0);
		if (delay>0) sleep(delay);
		if (autosize)
			if (size_term(&width, &height))
			{
				initscr();
				setterm(term);
				clear();
				refresh();
			}
		if (!noclear)
		{
			clear();
			refresh();
		}
		last_cols=show_labs(title, width, height, lablist, types, byws);
		new_page = time(0)+PAGE_SLEEP+COL_SLEEP*last_cols;
	}

}
