
/* labmon - monitor all labs printing messages which can be sent by pager.
 *   with '-v' produce status messages, not just errors
 */

#include	"lablist.h"
#include	<time.h>

int main(int argc, char *argv[])
{
	void *lablist = lablist_make();
	char *msg;
	labinfo li;
	int verbose=0;
	char strange_host[1024];
	strcpy(strange_host, "");

	if (argc>1) verbose=1;
    
	msg = add_lab(lablist, NULL);

	if (msg)
	{
		printf("labmon: could not find all labs: %s\n", msg);
		exit(1);
	}
	load_labs(lablist);

	for (li = lablist_next(lablist); li != lablist ; li = lablist_next(li))
		if (li->alloc)
		{
			/* checks:
			 *  if allocation date is more than 30 minutes old
			 *  if any host is up, but not mentioned for > 10 minutes
			 */
			if (li->alloc->data.data_len < 3)
				printf("labmon: no allocation in %s\n", li->labname);
			else if (cluster_modtime(li->alloc) +30*60 < time(0))
				printf("labmon: no allocation in %s for %d minutes\n", li->labname, (int)(time(0)-cluster_modtime(li->alloc))/60);
			else
			{
				table *t = &li->hg->tab;
				int r;
				if (verbose)
					printf("labmon: lab %s allocate %d minutes ago - OK\n", li->labname,  (int)(time(0)-cluster_modtime(li->alloc))/60);
				for (r=0; r <t->table_len ; r++)
					if (Cluster_type(t,r) == HOSTCLUSTER)
					{
						if (Host_status(t,r) == NODEUP && Cluster_modtime(t,r)+5*60 < time(0))
						{
							strcat(strcat(strange_host, " "), li->labname);
							break;
						}
						else if (verbose)
							printf("labmon: host %s %s for %d minutes - OK\n",
							       get_mappingchar(Cluster_id(t,r), M_HOST),
							       (Host_status(t,r) == NODEUP)?"Up":"Down",
							       (int)(time(0)-Cluster_modtime(t,r))/60);
					}
			}
		}
		else
		{
			printf("labmon: cannot get table for %s\n", li->labname);
		}
	if (strange_host[0])
		printf("labmon: out-of-date hosts in %s\n", strange_host);
	exit(0);
}

	
