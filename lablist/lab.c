
#include	"lablist.h"
/*#include <signal.h>*/
#include <unistd.h>

/*
 * lab: print out status of some labs.
 * possibly with heading, possibly only down or outofservice machines
 */

char *periodname[] = { "<null>", "FREE", "FULL", "CLOSED" };
char *nstatus[] = { "<null>", "Up", "Down"};
char *bstatus[] = { "<null>", "OutOfService", "Broken", "NotBookable",
		"UnAllocated", "Allocated", "Tentative" };

enum { N_UP, N_DOWN, N_OUT, N_ALL } class; /* which workstations to show */
bool_t show_header;
bool_t phost;

void print_lab(labinfo li)
{
	int i;
	if (show_header)
	{
		char *name, *host;
		int xdrname = 1, xdrhost = 1;
		name = get_mappingchar(li->labid, M_ATTRIBUTE);
		if (name == NULL) {
			name= "<unknown>";
			xdrname = 0;
		}
		host = get_mappingchar(alloc_host(li->alloc), M_HOST);
		if (host == NULL) {
			host = "<unknown>";
			xdrhost = 0;
		}
		if (li->alloc->data.data_len <3)
			printf("\nLab %s is %s: no allocations\n\n",
			       name, periodname[alloc_status(li->alloc)]);
		else
		{
			time_t period = cluster_modtime(li->alloc);
			int doy, pod;
			char doys[10], pods[10];
			get_doypod(&period, &doy, &pod);
			printf("\nLab %s is %s, last allocation by %s for %s %s (at %s)\n\n",
			       name, periodname[alloc_status(li->alloc)],
			       host,
			       doy2str(doys, doy), pod2str(pods, pod),
			       myctime(alloc_when(li->alloc))
				);
		}
		if (xdrname)
			free(name);
		if (xdrhost)
			free(host);
	}

	for (i=0 ; i < li->wscnt ; i++)
	{
		workstation w = &li->wslist[i];
		if (
			((class==N_UP) && ws_status(w->status) == NODEUP)
			|| ((class==N_DOWN) && ws_status(w->status) == NODEDOWN)
			|| ((class==N_OUT) && res_status(w->alloc) == NODEOUTOFSERVICE)
			|| (class== N_ALL)
			)
		{
			char *wsname;
			char *useron = NULL;
			char *userres = NULL;
			char c1 = ' ' , c2= ' ' ;

			wsname = get_mappingchar(w->wsid, M_WORKSTATION);
			if (wsname == NULL) wsname = strdup("<unknown>");
			if (ws_user(w->status) >= 0)
			{
				useron = find_username(ws_user(w->status));
				if (useron == NULL) useron = strdup("<who?>");
			}
			if (res_user(w->alloc) >= 0)
			{
				userres = find_username(res_user(w->alloc));
				if (userres == NULL) userres = strdup("<who?>");
				c1 = '{' ; c2 = '}';
				if (in_class(ws_user(w->status), res_user(w->alloc))==1)
					c1='[', c2=']';
			}

			printf("%-12s:%-8s%12s: %-10s %c %-9s%c",
			       wsname, nstatus[ws_status(w->status)],
			       bstatus[res_status(w->alloc)],
			       useron?useron:"",
			       c1, userres?userres:"", c2);

			if (phost)
			{
				int h = ws_whereon(w->status);
				if (h >0)
				{
					char *hn = get_mappingchar(h, M_HOST);
					if (hn)
						printf(" on %s", hn);
					else
						printf("on host-%d", h);
				}
			}
			else
			{
				/* print login time */
				if (ws_whenon(w->status))
					printf(" since %s",
					       myctime(ws_whenon(w->status)));
			}
			printf("\n");
			if (class == N_OUT)
			{
				workstation_state ws;
				char k[20];
				memset(&ws, 0, sizeof(ws));
				if (db_read(key_int(k, C_WORKSTATIONS, w->wsid), &ws,
					    (xdrproc_t)xdr_workstation_state, CONFIG) == R_RESOK)
				{
					if (ws.why)
						printf(" %s (%s)\n", ws.why,
						       myctime(ws.time));
					xdr_free((xdrproc_t)xdr_workstation_state, (char*) &ws);
				}
			}
			if (useron) free(useron);
			if (userres) free(userres);
			free(wsname);
		}
	}
}

char *Usage[] = {
    "Usage: lab [-udosASht] labname ...",
    "  -u : only list UP workstations",
    "  -d : only list DOWN workstations",
    "  -o : only list out of service workstations, and give reason",
    "  -s : don't print header on lab listing",
    "  -t : show time of login (default)",
    "  -h : show host that ws is logged on to instead of time",
    "  -A : list all labs",
    "  -S : list all bookable labs",
    "  -R : repeat listing ever 30 seconds",
    "  labname: the name of any lab of group of labs",
    NULL
};

int main(int argc, char *argv[])
{
	extern int optind;
	extern char *optarg;
	char *msg;
	int opt;
	void *lablist = lablist_make();
	labinfo li;
	int repeat = FALSE;

	/* quit on receipt of sigusr1 - to help with memory debugging in loops */
/*
  struct sigaction sa;
  sigset_t ss;
  sa.sa_handler = exit;
  sa.sa_flags = 0;
  sigemptyset(&ss);
  sigaddset(&ss, SIGUSR1);
  sa.sa_mask = ss;
  sigaction(SIGUSR1, &sa, NULL);
*/
	class = N_ALL;
	show_header = TRUE;
	phost = FALSE;
	while ((opt = getopt(argc, argv, "udosASRht"))!= EOF)
		switch(opt)
		{
		case 'h': phost = TRUE; break;
		case 't': phost = FALSE; break;
		case 'u': class = N_UP ; break ;
		case 'd': class = N_DOWN ; break ;
		case 'o': class = N_OUT ; break ;
		case 's': show_header = FALSE ; break ;
		case 'R': repeat = TRUE; break;
		case 'A': 
			msg = add_lab(lablist, NULL);
			if (msg)
			{
				fprintf(stderr, "lab: could not find all labs: %s\n", msg);
				exit(1);
			}
			break;
		case 'S':
			msg = add_lab(lablist, "bookable");
			if (msg)
			{
				fprintf(stderr, "lab: could not find bookable labs: %s\n", msg);
				exit(1);
			}
			break;
		default:
			usage();
			exit(1);
		}
	for (; optind < argc ; optind++)
	{
		msg = add_lab(lablist, argv[optind]);
		if (msg)
		{
			fprintf(stderr, "lab: could not add lab %s: %s\n", argv[optind], msg);
			exit(1);
		}
	}
	if (lablist_empty(lablist))
	{
		fprintf(stderr, "lab: no labs given\n");
		exit(0);
	}


	do {
		load_labs(lablist);

		for (li = lablist_next(lablist) ; li != lablist ; li = lablist_next(li))
			if (li->alloc)
				print_lab(li);
			else
				printf("Cannot get table for %s\n", li->labname);
		if (repeat) {
			printf("---------------------\n");
			fflush(stdout);
			sleep(30);
		}
	} while (repeat);
	exit(0);
}
