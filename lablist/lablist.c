
#include	<time.h>
#include	"lablist.h"
#include	"../lib/skip.h"


static int hg_cmp(hostg a, hostg b, int *cp)
{
    int c;
    if (b) c = b->hgid;
    else c = *cp;
    return a->hgid - c;
}
static void hg_free(hostg a)
{
    clnt_destroy(a->handle);
    xdr_free((xdrproc_t)xdr_hostgroup_info, (char*) &a->info);
    xdr_free((xdrproc_t)xdr_table, (char*) &a->tab);
}

void *hgl = NULL;
hostg find_hg(int hgid)
{
    hostg *hgp, hg;
    char k[20];

    if (hgl == NULL)
	hgl = skip_new(hg_cmp, hg_free, NULL);
    hgp = skip_search(hgl, &hgid);
    if (hgp)
	return *hgp;
    hg = (hostg)malloc(sizeof(struct hostg));
    memset(hg, 0, sizeof(struct hostg));
    hg->hgid = hgid;
    db_read(key_int(k, C_HOSTGROUPS, hgid), &hg->info, (xdrproc_t)xdr_hostgroup_info, CONFIG);
    skip_insert(hgl, hg);
    return hg;
}


char *real_add_lab(void *list, int lab)
{
    labinfo li;
    char k[20];

    for (li = dl_next(list); li != list ; li = dl_next(li))
	if (li->labid == lab)
	    return NULL;
    
    li = dl_new(struct labinfo);
    memset(li, 0, sizeof(struct labinfo));
    li->labid = lab;
    li->labname = get_mappingchar(lab, M_ATTRIBUTE);
    if (li->labname == NULL)
    {
	dl_free(li);
	return "Cannot get labname";
    }
    switch (db_read(key_int(k, C_LABS, lab), &li->info, (xdrproc_t)xdr_hostinfo, CONFIG))
    {
    case R_RESOK:
	break;
    case R_NOMATCH:
	/* lab not setup yet, just ignore it */
	return NULL;
    default:
	free(li->labname);
	dl_free(li);
	return "Cannot get lab info";
    }
    li->hg = find_hg(li->info.clump);
    dl_add(list, li);
    return NULL;
}

char *add_lab(void *list, char *labname)
{
    /* labname is actually an attribute name
     * any lab that has this attribute set now will be added
     * (if not already in list)
     * if labname is NULL, then all labs are added
     */
    int attr;

    if (get_attr_types() == 0)
	return "Cannot contact database";

    if (labname == NULL)
	attr = -1;
    else
    {
	attr = get_mappingint(labname, M_ATTRIBUTE);
	if (attr < 0)
	    return "unknown lab";
    }
    if (attr >=0 && query_bit(&attr_types.lab, attr))
	return real_add_lab(list, attr);
    else
    {
	/* must check each lab to see if it has this bit */
	time_t now;
	int doy, pod;
	int lab;
	
	now = time(0);
	get_doypod(&now, &doy, &pod);
	for (lab =0 ; lab < BITINDMAX ; lab++)
	    if (query_bit(&attr_types.lab, lab))
	    {
		char *rv = NULL;
		if (attr == -1)
		    rv = real_add_lab(list, lab);
		else
		{
		    /* this is a lab bit, compute implications */
		    item d = new_desc();
		    set_a_bit(&d, lab);
		    process_exclusions(pod, doy, &d);
		    if (query_bit(&d, attr))
			rv = real_add_lab(list, lab);
		    free(d.item_val);
		}
		if (rv)
		    return rv;
	    }
	return NULL;
    }
}
