
/*
 * messaged: send a message to a login session.
 *   Currently only supports messages on X terminals
 *
 * Usage: messaged [-l] [-L port] [-t timeout] [-g geometry] [-f font] [-D auth-dir] [-d domain]
 *
 * -l:  messaged will listen on stdin to accept connections- each connection provides a message
 * -L:  messaged will listen on the given tcp port and accept connections.
 * with neither of these, messaged will read a single message from stdin and send it.
 *
 * -t:  When -l is used, messaged will exit after a period of inactiveity (no messages displayed).
 *      This period is forever by default, but can be set to a number of minutes with -t
 * -g:
 * -f:  these specify the geometry (just position, not size) and font to use for the message
 *      when displayed on an X terminal.
 *      default -g +100+100
 *	default -f -misc-fixed-*-*-normal-*-20-*-*-*-*-*-iso8859-*
 * -D:  this specifies a directory in which to find Xauthority files.
 *      default -D /var/X11/xdm/save_auth
 * -d:  this specifies an internet domain to accept connections from.
 *      all incoming connections must from a privileged port.
 *      normally only connections from localhost are accepted.
 *      If, however, a domain is specifed, connections from any host whose canonical
 *	name ends with domain are accepted.
 *
 * message is one line describing target and a number of lines of message
 * The total messages is limited to 2048 bytes
 */

#define	MaxMesgSize	2048
char *font = "-misc-fixed-*-*-normal-*-20-*-*-*-*-*-iso8859-*";
char *geometry = "+100+100";
char *auth_dir = "/var/X11/xdm/save_auth";
char *okdomain = (char*)0;
int timeout = -1;

#include	<sys/types.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<stdio.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<netdb.h>
#include	<syslog.h>
#include	<string.h>
#include	<setjmp.h>
#include	<signal.h>

#include	<X11/Intrinsic.h>
#include	<X11/Shell.h>
#include	<X11/StringDefs.h>
#include	<X11/Xaw/Form.h>
#include	<X11/Xaw/AsciiText.h>
#include	<X11/Xaw/Box.h>
#include	<X11/Xaw/Command.h>
#include	<X11/Xaw/Label.h>
#include	<X11/Xatom.h>
#define _WCHAR_T
#include	<unistd.h>
#include	<stdlib.h>

#include	"ok.xbm"

extern char *normalise_display(char*);

int listenon(char *portname)
{
	struct sockaddr_in sa;
	int sock;
	int i =1;
	memset(&sa, 0, sizeof(sa));
	if (portname[0]>='0' && portname[0]<='9')
		sa.sin_port = htons(atoi(portname));
	else
	{
		struct servent *sv;
		sv = getservbyname(portname, "tcp");
		if (sv == NULL)
		{
			fprintf(stderr,"messaged: unknown port: %s\n", portname);
			return -1;
		}
		sa.sin_port = sv->s_port;
	}
	sa.sin_family = AF_INET;
	sock = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&i, 4);
	if (bind(sock, (struct sockaddr *)&sa, sizeof(sa))!= 0)
	{
		perror("messaged: bind");
		exit(1);
	}
	listen(sock, 5);
	return sock;
}

static int address_ok(struct sockaddr_in *sa, char *host)
{
	struct hostent *he;
	int len;
	int a;
	static char name[256];
	if (host == NULL) host = name;

	if (ntohs(sa->sin_port) >= 1024 && geteuid() == 0)
		return 0;
	if (sa->sin_addr.s_addr == htonl(0x7f000001))
	{
		strcpy(host, "localhost");
		return 1; /* localhost */
	}
	if (okdomain == NULL) return 0;
	he = gethostbyaddr((char*)&sa->sin_addr, 4, AF_INET);
	if (he == NULL)
		return 0;
	strcpy(host, he->h_name);
	he = gethostbyname(host);
	if (he == NULL)
		return 0;
	for (a=0; he->h_addr_list[a] ; a++)
		if (memcmp(&sa->sin_addr, he->h_addr_list[a], 4)==0)
		{
			/* well, we have a believable name */

			len = strlen(host);
			if (len > strlen(okdomain) && strcmp(okdomain, host+len - strlen(okdomain))== 0)
				return 1;
			return 0;
		}
	return 0;
}

void set_auth(char *file)
{
	static char ** newenv = NULL;
	extern char **environ;
	char *cp;
	if (newenv)
		free(newenv[0]);
	else
	{
		int c;
		for (c=0; environ[c] ; c++);
		newenv = (char**)malloc((c+2) * sizeof(char*));
		for (c=0; environ[c] ; c++)
			newenv[c+1] = environ[c];
		newenv[c+1] = NULL;
	}
	cp = (char*)malloc(strlen("XAUTHORITY=") + strlen(file)+1);
	strcat(strcpy(cp, "XAUTHORITY="), file);
	newenv[0] = cp;
	environ = newenv;
}
    
XtAppContext app;
jmp_buf keep_going;

int active_cnt = 0;
	     

struct windata {
    Widget parent;
    Pixmap ok;
    Display *disp;
};

void deactive()
{
	active_cnt--;
	if (active_cnt>0) return;
	active_cnt=0;
	if (timeout < 0) return;
	if (timeout == 0) exit(0);
	if (timeout > 0)
		alarm(timeout*60);
}

void quit(Widget button, XtPointer data, XtPointer nothing)
{
	struct windata *wd = (struct windata *) data;
	XtDestroyWidget(wd->parent);
	XtCloseDisplay(wd->disp);
	free(wd);
	deactive();

}

int am_opening = 0;

int io_error(Display *D)
{
	if (!am_opening)
		XtCloseDisplay(D);
	deactive();
	am_opening = 0;
	longjmp(keep_going, 1);
}

void xterror(char *msg)
{
	/* just keep going... */
	return;
}

struct buf
{
	char buf[MaxMesgSize];
	int len;
};


void do_read(XtPointer buf, int *sock, XtInputId *id)
{
	int nargs;
	char *args[20];
	Display *Disp;
	char *display, *message;
	Widget shell;
	struct windata * context;

	Widget    btn_widget,form_widget,text_widget;
	Pixmap   ok_button_pixmap = 0;
	Arg      wargs[10];
	char auth_file[200];

	int nsock = *sock;
	struct buf *abuf = (struct buf*)buf;
	int n = 0;
	if (abuf->len < sizeof(abuf->buf)-2)
		n=read(nsock, abuf->buf+abuf->len, sizeof(abuf->buf)-abuf->len-1);
	if (n>0)
	{
		abuf->len += n;
		return;
	}
	close(nsock);
	XtRemoveInput(*id);

	if (n < 0 || abuf->len > sizeof(abuf->buf)-2)
	{
		/* error or too much data */
		free(abuf);
		deactive();
		return;
	}
	/* now find display name and display it ... FIXME */
	display = abuf->buf;
	abuf->buf[abuf->len] = 0;
	message = strchr(abuf->buf, '\n');
	if (!message) { free(abuf); return;}
	*message++ = 0;
        


	strcpy(auth_file, auth_dir);
	strcat(auth_file, "/");
	strcat(auth_file, normalise_display(display));

	set_auth(auth_file);
    
	nargs=0;
	args[nargs++] = "Messaged";
	args[nargs++] = "-geom";
	args[nargs++] = geometry;
	args[nargs++] = "-font";
	args[nargs++] = font;

	args[nargs] = NULL;
	am_opening = 1;
	Disp = XtOpenDisplay(app, display, "Memo", "Memo", NULL, 0, &nargs, args);
	am_opening = 0;
	if (Disp == NULL)
	{
		/* HACK try adding .x to host name */
		char d2[100];
		int l;
		strcpy(d2, display);
		l = strlen(d2);
		if (strcmp(d2+l-2, ":0")==0)
		{
			strcpy(d2+l-2, ".x:0");
			am_opening = 1;
			Disp = XtOpenDisplay(app, d2, "Memo", "Memo", NULL, 0, &nargs, args);
			am_opening = 0;
		}
	}
	if (Disp == NULL)
	{
		free(abuf);
		deactive();
		return;
	}
	shell = XtAppCreateShell("Memo", "Memo", applicationShellWidgetClass, Disp, NULL, 0);
	form_widget = XtCreateManagedWidget("buttonsForm", formWidgetClass, shell, (ArgList) NULL, 0);
	n=0;
	XtSetArg (wargs[n], XtNlabel, message); n++;
	text_widget = XtCreateManagedWidget ("message", labelWidgetClass,form_widget, wargs, n);
  
	n=0;
	XtSetArg (wargs[n], XtNfromVert, text_widget); n++;
	XtSetArg (wargs[n], XtNvertDistance, 5); n++;
	XtSetArg (wargs[n], XtNfromHoriz,NULL); n++;
	btn_widget = XtCreateManagedWidget("OK",commandWidgetClass,
					   form_widget,wargs,1);
	ok_button_pixmap = XCreateBitmapFromData(XtDisplay(btn_widget),
						 RootWindow(XtDisplay(btn_widget),
							    XtWindow(btn_widget)),
						 ok_bits,
						 ok_width, ok_height);
  
	n=0;
	XtSetArg(wargs[n], XtNbitmap, (XtArgVal) ok_button_pixmap);n++;
	XtSetValues(btn_widget, wargs, 1);
	context = (struct windata*)malloc(sizeof(struct windata));
	context->parent = shell;
	context->ok = ok_button_pixmap;
	context->disp = Disp;
	XtAddCallback(btn_widget,XtNcallback, quit,(XtPointer)context); 
	XtRealizeWidget(shell);
	free(abuf);
	XFlush(Disp);
}

void do_accept(XtPointer closure, int *sock, XtInputId *id)
{
	struct sockaddr_in sa;
	unsigned int salen = sizeof(sa);
	int nsock = accept(*sock, (struct sockaddr *)&sa, &salen);
	struct buf *abuf;


	if (nsock <0) return;
    

	if (sa.sin_family != AF_INET)
	{
		close(nsock);
		return;
	}

	if (!address_ok(&sa, NULL))
	{
		close(nsock);
		return;
	}
	abuf = (struct buf*)malloc(sizeof(struct buf));
	abuf->len = 0;
	active_cnt++;
	alarm(0);
	XtAppAddInput(app, nsock, (XtPointer)XtInputReadMask, do_read, abuf);

}

void catchalrm(int sig)
{
	exit(0);
}

int main(int argc, char *argv[])
{
	/* arg is port num */
	int sock = -1;
	int arg;
	extern char *optarg;
	extern int optind;


	while ((arg=getopt(argc, argv, "lL:t:g:f:D:"))!= -1)
		switch(arg)
		{
		case 'l':
			sock = 0; break;
		case 'L':
			sock = listenon(optarg);
			if (sock < 0)
			{
				fprintf(stderr, "messaged: cannot bind to %s\n", optarg);
				exit(1);
			}
			break;
		case 't':
			timeout = atoi(optarg);
			break;
		case 'g':
			geometry = optarg;
			break;
		case 'f':
			font = optarg;
			break;
		case 'D':
			auth_dir = optarg;
			break;
		case 'd':
			okdomain = optarg;
			break;
		default:
			fprintf(stderr, "messaged: unrecognised option in %s.\n", argv[optind]);
			exit(1);
		}
    


	XtToolkitInitialize();
	app = XtCreateApplicationContext();

	if (sock >= 0)
		XtAppAddInput(app, sock, (XtPointer)XtInputReadMask, do_accept, &sock);
	else
	{
		struct buf *abuf = (struct buf*)malloc(sizeof(struct buf));
		XtAppAddInput(app, 0, (XtPointer)XtInputReadMask, do_read, abuf);
		timeout = 0;
		active_cnt = 1;
	}

	signal(SIGALRM, catchalrm);
	setjmp(keep_going);
	XSetIOErrorHandler(io_error);
	XtSetErrorHandler(xterror);
	active_cnt++; deactive();
	XtAppMainLoop(app);
	exit(1);
}
