/*****************************************************************************/
/* program to send message to winmess...... my window message program        */

#include	<sys/types.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<stdio.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<netdb.h>
#include	<memory.h>

#define PORT 607;		/* inetd port */

void error(message)
char *message;
{
	fprintf(stderr,"ERROR: %s\n",message);
}

FILE *connection_init(char *host)                      /* connect to local host socket */
{
	int sock;
	struct sockaddr_in server;
	struct hostent *hp, *gethostbyname();
	struct servent *sev;
	int lport = IPPORT_RESERVED -1;
	FILE *write_file;
	/* Create Socket */
	if (geteuid() != 0)		/* if effective uid not privledged get out */
		exit(1);
	if ((sock= rresvport(&lport)) == -1 ) /* get priveldged socket */
	{
		perror("opening stream socket\n");
		exit(1);
	}
	server.sin_family = AF_INET;
	if ((hp = gethostbyname(host?host:"localhost")) == 0 )
	{
		fprintf(stderr,"Can't find %s??", host?host:"localhost");
		exit(1);
	}
	memcpy(&server.sin_addr,hp->h_addr,hp->h_length );
	if ( (sev = getservbyname("message","tcp")) == NULL )
	{
		fprintf( stderr,"Unable to get service %s\n","message" );
		exit(1);
	}
	server.sin_port = sev->s_port;
	if (connect(sock,(struct sockaddr *) &server,sizeof(server)) < 0)
	{
		perror("connection stream socket");
		exit(1);
	}
	write_file = fdopen(sock,"w");
	return(write_file);
}



int main(int argc, char *argv[])
{
	int c;
	char *first_line=NULL;
	FILE *write_file;
	char *host = NULL;
  
	if (argc > 3)
	{
		error("Usage: sendmessage [display [host]]");
		exit(1);
	}
	if (argc >= 2)
		first_line = argv[1];
	if (argc >=3)
		host = argv[2];
	write_file=connection_init(host);
	if (first_line)
	{
		write(fileno(write_file),first_line,strlen(first_line));
		write(fileno(write_file),"\n",1);
	}
	while ((c = getchar()) != EOF)
		putc(c,write_file);
	return 0;
}

