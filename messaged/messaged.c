/* inetwall: place an xwindow with a message onto a workstation:
   note that for apollos xmessage is execl however for everything else
   the window is generated */


#include	<sys/types.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<stdio.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<netdb.h>
#include	<syslog.h>

#ifndef apollo
#include	<X11/Intrinsic.h>
#include	<X11/StringDefs.h>
#include	<X11/Xaw/Form.h>
#include	<X11/Xaw/AsciiText.h>
#include	<X11/Xaw/Box.h>
#include	<X11/Xaw/Command.h>
#include	<X11/Xaw/Label.h>
#include	<X11/Xatom.h>

#include	"ok.xbm"
#include	"ok1.xbm"
# define rootwin(x)       RootWindow(XtDisplay(x), XtWindow(x))


/* quit() causes the xmessage to disappear when */
/* the OK button is pressed. */
void quit(w,client_data,call_data)                 /* quit callback function */
Widget w;
XtPointer client_data,call_data;

{
  Arg wargs[1];
  int n;
  exit(0);
}
#endif

#ifndef apollo
#ifdef SYS5
/* makenv makes a string that can be used to be put into 
 * the environment.
*/
char * mkenv( char * a,char * b )
{
  char * c;
  c = (char *) malloc (strlen(a)+1+strlen(b)+1);
  sprintf( c,"%s=%s",a,b );
  return c;
}
#endif

char *normalise_display(char *disp)
{
    /* normalise display name.
     * it should be host:num[.0]
     * if host == unix or thing host, remove it
     * remove any dot component of host
     * if not .0, add .0
     */
    static char rv[200];
    char hname[256];
    char *p;

    strcpy(rv, disp);
    p = strchr(rv, ':');
    if (p == NULL)
	return rv;
    *p = '\0';
    p = strchr(rv, '.');
    if (p) *p = '\0';
    if (strcmp(rv, "unix") == 0)
	rv[0] = '\0';
    gethostname(hname, 256);
    if (strcmp(rv, hname) == 0)
	rv[0] = '\0';

    p = strchr(disp, ':');
    strcat(rv, p);
    p = strchr(rv, '.');
    if (p == NULL)
	strcat(rv, ".0");
    return rv;
}

	
    
/* create_xmessage() sends a xmessage to the screen. */
create_xmessage(char * message, char *display)
{
  Widget toplevel;
  char *temparg[20];
  Widget    btn_widget,form_widget,text_widget;
  Pixmap   ok_button_pixmap;
  Arg      wargs[10];
  char auth_file[200];
  XtAppContext app_con;
  int n = 3;

  strcpy(auth_file, "/var/X11/xdm/save_auth/");
  strcat(auth_file, normalise_display(display));

#ifdef SYS5
  putenv( mkenv( "XAUTHORITY", auth_file));
#else
  setenv( "XAUTHORITY", auth_file, 1);
#endif
  
  temparg[0] = "messaged";
  temparg[1] = "-display";
  temparg[2] = display;
  temparg[3] = "-geom";
  temparg[4] = "+100+100";
  temparg[5] = "-font";
  temparg[6] = "-misc-fixed-*-*-normal-*-20-*-*-*-*-*-iso8859-*";
  temparg[7] = NULL;

  n=7;
  toplevel = XtInitialize("Memo","Memo",NULL,0,&n,temparg);
  
  form_widget = XtCreateManagedWidget("buttonsForm",
				      formWidgetClass,
				      toplevel,
				      (ArgList) NULL, 0);
  n=0;
  XtSetArg (wargs[n], XtNlabel, message); n++;
  text_widget = XtCreateManagedWidget ("message", labelWidgetClass,form_widget, wargs, n);
  
  n=0;
  XtSetArg (wargs[n], XtNfromVert, text_widget); n++;
  XtSetArg (wargs[n], XtNvertDistance, 5); n++;
  XtSetArg (wargs[n],XtNfromHoriz,NULL); n++;
  btn_widget = XtCreateManagedWidget("OK",commandWidgetClass,
				     form_widget,wargs,1);
  ok_button_pixmap = XCreateBitmapFromData(XtDisplay(btn_widget),
					   rootwin(btn_widget),
					   ok_bits,
					   ok_width, ok_height);
  
  n=0;
  XtSetArg(wargs[n], XtNbitmap, (XtArgVal) ok_button_pixmap);n++;
  XtSetValues(btn_widget, wargs, 1);
  XtAddCallback(btn_widget,XtNcallback, quit,0); 
  XtRealizeWidget(toplevel);

  XtMainLoop();
}

#endif

/*****************************************************************************/

#ifdef apollo		                    /* APOLLO specific stuff */

char X_SOCK[] = "/tmp/.X11-unix/X0";
#include	<apollo/base.h>
#include	<apollo/error.h>
#include	<apollo/pad.h>
#include	<apollo/gpr.h>


dm_message(char * message, int lines, int columns)               /* write to the DM  */
{
    ios_$id_t wd;
    pad_$window_desc_t pos;
    short fid;
    status_$t status;
    int f2;
    char c;
    int i = 0;

    pos.top = 30;
    pos.left = 30;
    pos.width = columns*11+13;
    pos.height = lines*22+46;
    if (pos.height > 770) pos.height = 770;
    pad_$create_window("",0, pad_$transcript, 1, pos, &wd, &status);
    
    pad_$load_font(wd, "/sys/dm/fonts/f9x15", 19, &fid, &status);
    pad_$use_font(wd, fid, &status);
    while( (i += write(wd,&message[i],strlen(&message[i])+1)) < strlen(message) + 1 );
    close(wd);
}

xmessage(char * message, int lines, int columns) /* executed on apollos: fires xmess */
{
    char geom[50];

    sprintf(geom,"%dx%d+30+30",columns*10+16, lines*17+45);
    execl("/usr/local/X11/bin/xmessage","xmessage",
	  "-font","-misc-fixed-*-*-normal-*-20-*-*-*-*-*-iso8859-*",
	  "-display","unix:0.0",
	  "-nsb",
	  "-b", "OK",
	  "-geometry",geom,
	  "-message",message,
	  0L);
     exit(1);
 
}

#include	<sys/un.h>
#include	<signal.h>
#include	<setjmp.h>
jmp_buf jb;

catch()
{
    alarm(0);
    longjmp(jb, 1);
}
check_X()			/* check if the apollo machine is running X  */
{
    struct stat sb;
    int sd;
    struct sockaddr_un name;
    FILE * f;

    if (stat(X_SOCK, &sb)== -1
	|| ((sb.st_mode)&S_IFMT) != S_IFSOCK)
	return 0;
    signal(SIGALRM, (void *) catch);
    if (setjmp(jb)) { close(sd); return 0;}
    sd = socket(AF_UNIX, SOCK_STREAM, 0);
    name.sun_family = AF_UNIX;
    strcpy(name.sun_path, X_SOCK);
    alarm(6);
    if (connect(sd, (struct sockaddr *) &name, sizeof(name))== -1)
    {
	alarm(0);
	close(sd);
	return 0;
	
    }
    alarm(0);
    sleep(3);
    close(sd);
    return 1;
}

/* check_dm() checks if the apollo is running the dm. */
check_dm()
{
    gpr_$offset_t size;
    gpr_$bitmap_desc_t bd;
    status_$t status;
    int rv;

    size.x_size = 20;
    size.y_size = 20;
    gpr_$init(gpr_$borrow_nc, 1, size, 0, &bd, &status);
    if (status.all == 0)
	rv = 1;
    else
	rv = 0;
    gpr_$terminate(false, &status);
    return rv;
}
#endif

/*****************************************************************************/



/* read_file() returns the contents of a file */
char *read_file(int fd)
{
    int len = lseek(fd, 0L, 2);
    char *rv = (char*)malloc(len+1);
    lseek(fd, 0L, 0);
    read(fd, rv, len);
    rv[len] = 0;
    return rv;
}    

main(argc,argv)
char *argv[];
{
    FILE *f;
    int cols=0;
    int sock = 0;
    int fd;
    int ccnt=0;			/* records the maximum width needed for a message in a box */
    int lines=0;
    char c;
    char *message;
    char mfile[40];
    struct sockaddr_in name;
    int namelen = sizeof(name);
    char first_line[200];
    int flp = 0;

    strcpy(mfile,"/tmp/message.XXXXXX");

    if (strcmp(argv[0],"messaged")==0)
    {
	if (getpeername(sock,(struct sockaddr *) &name,&namelen) == 0)
	{
	    /* check to see if incoming message is coming from the localhost
	     * That is, the machine that messaged is on.
	     */
	    if (ntohs(name.sin_port) >= 1024) exit(1);
	    if (name.sin_addr.s_addr != htonl(0x7f000001))
	    {
		struct hostent *he;
		int a;
		char host[1024];
		he = gethostbyaddr((char*)&name.sin_addr, 4, AF_INET);
		if (he == NULL)
		    exit(1);
		strcpy(host, he->h_name);
		he = gethostbyname(host);
		if (he == NULL)
		    return exit(1);;
		for (a=0; he->h_addr_list[a] ; a++)
		    if (memcmp(&name.sin_addr, he->h_addr_list[a], 4)==0)
		    {
			/* well, we have a believeable name */
			char *tail = ".cse.unsw.EDU.AU";
			int len;

			len = strlen(host);
			if (len > strlen(tail) && strcmp(tail, host+len - strlen(tail))== 0)
			    break;
			exit(1);
		    }
		if (he->h_addr_list[a]==0)
		    exit(1);
	    }
	}
	else
	    exit(1);
    }
    
    fflush(stdout);

    /* test to see if a temporary file can be created.
     * If not, the program is exitted.
     */
    mktemp(mfile);
    if ((fd = open(mfile,O_CREAT|O_RDWR,0600)) < 0) 
	exit(1);
    unlink(mfile);
  
    /* collect the message from the incoming socket. */
    while(read(sock,&c,1) == 1)
    {
	if (lines != 0)
	    write(fd,&c,1);
	else
	{
	    if (flp < sizeof(first_line))
		first_line[flp++] = c;
	}
	if ( c == '\n')
	{
	    if (cols < ccnt)
		cols = ccnt;
	    lines++;
	    ccnt=0;
	}
	ccnt++;
    }
    lines --;
    message = read_file(fd);
    close(fd);

    if (first_line[flp-1] == '\n')
	flp--;
    first_line[flp] = 0;
    
#ifndef apollo

    /****************************************************************************/
    /* This is the X stuff that all the other machines use: should be in other  */
    /* proc but I cant get it to work                                           */
  
    create_xmessage(message, first_line);
    /****************************************************************************/
  
#else
    /* APOLLO stuff */
    if (!check_dm()) 
	xmessage(message,lines,cols);
    else
	dm_message(message, lines, cols); 
#endif
}



