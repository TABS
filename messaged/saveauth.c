
#include	<errno.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<string.h>

#include	<X11/Xlib.h>
#include	<X11/Xauth.h>

#define _WCHAR_T
#include	<unistd.h>
#include	<stdlib.h>

#define FAILURE 0
#define SUCCESS 1
#ifndef NULL
#define NULL '\0'
#endif 
#ifndef BOOLEAN
#define BOOLEAN char
#define FALSE 0
#define TRUE 1
#endif

#define XauthFile "/var/X11/xdm/save_auth/"

static int verbosity = 0;
extern char *normalise_display(char*);


char *old_normalise_display(char *disp)
{
	/* normalise display name.
	 * it should be host:num[.0]
	 * if host == unix or this host, remove it
	 * remove any dot component of host
	 * if not .0, add .0
	 */
	static char rv[200];
	char hname[256];
	char *p;

	strcpy(rv, disp);
	p = strchr(rv, ':');
	if (p == NULL)
		return rv;
	*p = '\0';
	p = strchr(rv, '.');
	if (p) *p = '\0';
	if (strcmp(rv, "unix") == 0)
		rv[0] = '\0';
	gethostname(hname, 256);
	if (strcmp(rv, hname) == 0)
		rv[0] = '\0';

	p = strchr(disp, ':');
	strcat(rv, p);
	p = strchr(rv, '.');
	if (p == NULL)
		strcat(rv, ".0");
	return rv;
}

int copy_xauth(char *disp)
{
	FILE 
		* xau_read,
		* xau_write;
	Xauth
		* xauthrec = NULL;
	int
		success = SUCCESS;
	struct stat
		buff;

	char authfile[200];

	strcpy(authfile, XauthFile);
	strcat(authfile, disp);
	/* test for existance of file */
	if ( stat(authfile,&buff) == 0 )
	{
		/* file exists. Let's remove it. */
		if ( unlink(authfile) < 0 )
		{
			perror( "unlink:" );
			return FAILURE;
		}
	}
	else if ( errno != ENOENT )
	{
		perror( "stat:" );
		return FAILURE;
	}
	if ( (xau_read = fopen( XauFileName(),"r" )) == NULL )
	{
		fprintf( stderr,"Unable to open %s for reading.\n",XauFileName() );
		return FAILURE;
	}
	if ( verbosity )
		printf( "Finished opening file to read.\n" );
	umask(0177);
	if ( (xau_write = fopen(authfile,"w" )) == NULL )
	{
		fprintf( stderr,"Unable to open %s for writing.\n",authfile );
		return FAILURE;
	}
	if ( verbosity )
		printf( "Finished opening file to write.\n" );
	while( (xauthrec = XauReadAuth(xau_read)) != NULL && (success == SUCCESS) )
		if ( XauWriteAuth(xau_write,xauthrec) == 0 )
		{
			fprintf( stderr,"Unable to write data to file %s.\n",authfile );
			success = FAILURE;
		}
	if ( chmod(authfile,0600) < 0 )
	{
		perror( "chmod:" );
		return FAILURE;
	}
	return success;
}

  
  

BOOLEAN can_connect_to_display()
{
	Display * display;

	if ( (display = XOpenDisplay(NULL)) == NULL )
		return FALSE;
	XCloseDisplay( display );
	return TRUE;
}

int main(int argc,char * argv[])
{

#if 0
	while((c = getopt(argc,argv,"v")) != EOF)
		switch(c)
		{
		case 'v':
			verbosity = 1;
			break;
		}
#endif
	if ( !can_connect_to_display() )
	{
		fprintf( stderr,"Unable to connect to display.\n" );
		exit(1);
	}
	if ( verbosity )
		printf( "Connected to display.\n" );
	if ( (copy_xauth(normalise_display(XDisplayName(NULL))) == FAILURE)
	     & (copy_xauth(old_normalise_display(XDisplayName(NULL))) == FAILURE))
		exit(1);
	if ( verbosity )
		printf( "Able to copy xauthority.\n" );
	exit(0);
}

      
  
