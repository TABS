
#include	<unistd.h>
#include	<stdlib.h>
#include	<memory.h>
#include	<sys/types.h>
#include	<netdb.h>
#include	<ctype.h>
#include	<string.h>

char *normalise_display(char *disp)
{
    /* normalise display name.
     * normalised name should be FQDN:num.0
     * original name may start unix: or just :
     * in which case hostname is used.
     */
    static char rv[200];
    char *colon;
    char ad[40];
    struct hostent *he;
    int i;

    colon = strchr(disp, ':');
    if (colon == NULL) return NULL;
    
    strcpy(rv, disp);
    rv[colon-disp]='\0';

    if (strcmp(rv, "unix")==0 || rv[0] == '\0')
	gethostname(rv, 200);
    he = gethostbyname(rv);
    if (he == NULL)
	return NULL;
    /* because of solaris, we must take an address and map it back */
    memcpy(ad, he->h_addr_list[0], he->h_length);
    he = gethostbyaddr(ad, he->h_length, he->h_addrtype);
    if (he == NULL) return NULL;

    strcpy(rv, he->h_name);
    for (i=0; rv[i]; i++)
	if (isupper(rv[i])) rv[i]=tolower(rv[i]);

    strcat(rv, colon);
    if (strchr(colon, '.')==0)
	strcat(rv, ".0");
    return rv;
}
