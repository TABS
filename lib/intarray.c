
#include	<sys/types.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	"misc.h"


/* ADD_INTARRAY: adds an integer to a array */
int add_intarray(int_array * array, int newval)
{
    int i;
    /* should check existence */
    for (i=0;i < array->array_len;i++)
	if (array->array_val[i] == newval)
	    return 0;
    
    array->array_len++;
    if (array->array_val == (int*)0)
	array->array_val = (int *)malloc(array->array_len * sizeof (int));
    else
	array->array_val = (int *)realloc(array->array_val, array->array_len *
					  sizeof (int)); 
    array->array_val[array->array_len -1] = newval;
    return 1;
}

int del_intarray(int_array * array, int oldval)
{
    int i;
    int found = 0;
    for (i = 0; i < array->array_len;i++) {
	found = found || (array->array_val[i] == oldval);
	if (found)
	    array->array_val[i] = array->array_val[i+1];
    }
    if (found)
	array->array_len --;

    return(found);
}

#if 0
void print_intarray(int_array * ar)
{
    int i;
    for (i=0; i < ar->array_len;i++)
	printf("VAl %d = %d\n",i, ar->array_val[i]);
}
#endif
