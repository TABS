
/* from utils.c */
extern void shout(char * message);
extern void bailout(char * message, int n);
extern int assert(int condition, char * message);
extern char * get_myhostname(void);
#ifndef linux_why_not
extern void *memdup(void *m, int l);
#endif
char * make_pathname(char * name);
char * make_tmp_pathname(char * name);

extern char BookBase[];

/* from debug.c */
void set_bookdebug_level(void);
extern void Debug(char *message, int level);


/* from strccmp.c */

int strnccmp(char *a, char *b, int n);
int strccmp(char *a, char *b);

/* from timeutils.c */

int dayofweek(char *str);
int doy_2_dow(int doy);
int pod_2_hrs(int pod);
int podofday(char *str);
int dayofyear(char *str);
char *doy2str(char *rv, int doy);
char *pod2str(char *rv, int pod);
int minuteofweek(char *str);
void get_doypod(time_t *time_in_secs, int *doy, int *pod);
char *myctime(time_t time_in_secs);
time_t unmyctime(char *tim);

/* from strdup.c */
#ifndef linux
char *strdup(const char *s);
char *strndup(const char *s, size_t a);
#endif

/* from usage.c */
void usage(void);
extern char *Usage[];

/* from intarray.c */
typedef struct int_array
{
    int array_len;
    int * array_val;
} int_array;
int add_intarray(int_array * array, int newval);
int del_intarray(int_array * array, int oldval);


/* from pmap_getport.c */

#include	<sys/time.h>
void pmap_settimeouts(struct timeval intertry, struct timeval percall);
