/*
 * dayofyear is 2 digits and 3 letters, e.g. 13mar
 * minuteofweek is 2letters and 4 digits, e.g. Mo0800 Mo1800
 * attribute is a string
 *
 */

#include	<unistd.h>
#include	<stdlib.h>
#include	<stdio.h>
#include	<time.h>
#include	<string.h>
#include	<ctype.h>
#include	"misc.h"

char *months[12] =
{ "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec" };
int monsize[12] = {
    31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };
char *days[7] =
{ "su", "mo", "tu", "we" ,"th", "fr", "sa" };

char*strchr();


int dayofweek(char * str)
{
    int i;
    for (i=0;i < 7;i++)
	if (strcmp(days[i],str) == 0)
	    return i;
    return(-1);
    
}

int doy_2_dow(int doy)
{
    struct tm * tm;
    time_t now;
    int dow;

    if (doy == -1)
	return -1;

    time(&now);
    tm = localtime(&now);

    /* the difference in numbers of days */
    dow  = (tm->tm_wday + (doy+(53*7) - tm->tm_yday)) % 7;

    return dow;
}

int pod_2_hrs(int pod)
{
    int result;
    
    if (pod == -1)
	return -1;
    result = (pod / 2) * 100;
    if (pod % 2)
	result += 30;

    return result;
    
}

int podofday(char *str)
{
    /* str is hh:mm or h:mm
     * where mm is 00 or 30
     * return 0 to 47 ( any other number)
     *
     */
    int l = strlen(str);
    if (l!=4 && l != 5)
	return -1;
    if (str[l-3] != ':' || str[l-1] != '0'
	|| (str[l-2] != '0' && str[l-2] != '3')
	|| (str[l-4] < '0' || str[l-4] > '9')
	|| (str[0] < '0' || str[0] > '9')
	)
	return -1;
    return atoi(str)*2 + (str[l-2]&1);
}

int dayofyear(char *str)
{
    int day, mon, m;
    time_t now;
    struct tm *tm;
    int leapyear;
    if (strlen(str) != 5)
	return -1;
    if (!isdigit(str[0]) || !isdigit(str[1]))
	return -1;
    for (mon=0 ; mon<12 ; mon++)
	if (strcmp(months[mon], str+2)==0)
	    break;
    if (mon == 12)
	return -1;

    time(&now);
    tm = localtime(&now);
    if (tm->tm_year%4 /* || ((1900 + tm->tm_year)%400) == 0*/)
	leapyear=0;
    else
	leapyear=1;
    
    day = atoi(str);
    if (day<1 || day > monsize[mon])
	return -1;
    day -= 1;
    for (m = 0 ; m < mon ; m++)
	day += monsize[m];
    if (mon >= 2 && ! leapyear)
	day -= 1;
#ifdef DEBUG
    printf("%s -> %d\n", str, day);
#endif    
    return day;
}

char *doy2str(char *rv, int doy)
{
    struct tm *tm;
    time_t now;
    int leapyear;
    int mon;
    
    if (doy <= 0)
	strcpy(rv, "01jan");
    else if (doy>=365)
	strcpy(rv, "31dec");
    else
    {

	time(&now);
	tm = localtime(&now);
	if (tm->tm_year%4 /* || ((1900 + tm->tm_year)%400) == 0*/)
	    leapyear=0;
	else
	    leapyear=1;
	if (!leapyear && doy >= 31+28) doy++;
	mon = 0;
	while (doy >=  monsize[mon])
	    doy -= monsize[mon++];
	sprintf(rv, "%02d%3.3s", doy+1, months[mon]);
    }
    return rv;
}

char *pod2str(char *rv, int pod)
{
    sprintf(rv, "%02d:%02d", pod/2, (pod%2)*30);
    return rv;
}


int minuteofweek(char *str)
{

    int day, hr, min;
    char h[3];
    if (strlen(str) != 6)
	return -1;

    if (str[5] != '0')
	return -1;
    if (str[4] != '0' && str[4] != '3')
	return -1;
    strncpy(h, str+2, 2);
    h[2] = '\0';
    hr = atoi(h);
    min = str[4] == '3'? 30:0;

    for (day=0 ; day<7 ; day++)
	if (strncmp(str, days[day], 2)==0)
	    break;
    if (day == 7)
	return -1;
    min = ((day*24)+hr)*60+min;
#ifdef DEBUG
    printf("%s -> %d\n", str, min);
#endif
    return min;
    
}

/* get_doypod: given a time in seconds, produces the day of the year */
 /* and period of the day (localtime!).  It also rounds the time in */
 /* seconds down to the start of the booking period!!! */    
void get_doypod(time_t * time_in_secs, int * doy, int * pod)
{
    int min_diff;
    
    struct tm * tm = localtime(time_in_secs);
    *doy = tm->tm_yday;
    *pod = (2 * tm->tm_hour);
    if (tm->tm_min >= 30)
	(*pod)++;

    /* now make the time_in_secs equal to the start of the period */
    min_diff = tm->tm_min % 30;
    *time_in_secs -= (min_diff * 60) + tm->tm_sec;
    
#ifdef DEBUG
    printf("pod = %d, doy = %d, min diff = %d, time  = %d\n", *pod,
	   *doy, min_diff, *time_in_secs);
#endif
}



char * myctime(time_t time_in_secs)
{
    static char result[30];
    struct tm * tm = localtime(&time_in_secs);
    
    sprintf(result, "%02d/%02d;%02d:%02d:%02d", tm->tm_mday,
	    tm->tm_mon + 1,
	    tm->tm_hour, tm->tm_min, tm->tm_sec);
    return(result);
    	
}

time_t unmyctime(char *tim)
{
    int dy, mo, hr, mn, sc;
    int m;
    int cnt = 5;
    time_t now, oldnow;
    struct tm tm;
    time(&now);
    do
    {
	if (cnt-- < 0) return 0;
        oldnow= now;
	tm = *localtime(&now);
	sscanf(tim, "%02d/%02d;%02d:%02d:%02d", &dy, &mo, &hr, &mn, &sc);
	mo-= 1;
	now += sc - tm.tm_sec;
	now += 60* (mn - tm.tm_min);
	now += 3600 * (hr - tm.tm_hour);
	now += 3600*24 * (dy - tm.tm_mday);
	for (m=0 ; m< 12 ; m++)
	{
	    if (m < mo && m>= tm.tm_mon) now += 24*3600*monsize[m] ;
	    if (m >=mo && m<  tm.tm_mon) now -= 24*3600*monsize[m] ;
	}
	if (tm.tm_year%4 || ((1900 + tm.tm_year)%400) == 0)
	    ;
	else
	{
	    /* leapyear=1; */
	    if (mo <= 1 && tm.tm_mon > 1) now -= 24*3600;
	    if (mo >  1 && tm.tm_mon <=1) now += 24*3600;
	}
    } while (now != oldnow);
    return now;
}
