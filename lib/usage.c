
#include	<sys/types.h>
#include	<stdio.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	"misc.h"

extern char *Usage[];


void usage(void)
{
    int i;
    for (i=0 ; Usage[i] ; i++)
	fprintf(stderr, "%s\n", Usage[i]);
}
