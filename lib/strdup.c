#define NULL 0

#include	<sys/types.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	<string.h>
#include	"misc.h"


char *strdup(const char *s)
{
    if (s==NULL)
	return NULL;
    return (char *)strcpy((char*)malloc(strlen(s)+1), s);
}

char *strndup(const char *s, size_t a)
{
    char *r;
    if (s == NULL)
	return NULL;

    r = (char*)malloc(a+1);
    strncpy(r, s, a);
    r[a] = 0;
    return r;
}
