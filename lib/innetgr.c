/*
 * Use a local magic map to do innetgr lookups.
 * The netgroup
 *    innetgr:CLASS,HOST,USER,DOMAIN
 * will contain either (host,user,domain) or (-,-,-)
 * - Neil Brown 24may2006
 */

#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <malloc.h>

int innetgr(const char *name, const char *host, const char *user, const char *ypdomain)
{
	char *group=NULL;
	asprintf(&group, "innetgr:%s,%s,%s,%s",
		 name, host?host:"", user?user:"", ypdomain?ypdomain:"");
	if (setnetgrent(group)) {
		char *u, *h, *d;
		if (getnetgrent(&h,&u,&d)) {
			/* Successful lookup */
			int rv = 0;
			if ((h && strcmp(h,"-")) || (u && strcmp(u,"-")))
				rv = 1;
			endnetgrent();
			free(group);
			return rv;
		}
	}
	free(group);
	/* Failed :-( */
	return 0;
}

