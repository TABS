
/* include for skiplists */
#define _PROTO_
#ifdef _PROTO_
void *skip_new(int cmpfn(), void freefn(), void errfn());
char skip_insert(void *list, void *entry);
char skip_delete(void *list, void *key);
void *skip_search(void *list, void *key);
void *skip_first(void *list);
void *skip_next(void *last);
void skip_free(void *);
#else
void *skip_new();
char skip_insert();
char skip_delete();
void *skip_search();
void *skip_first();
void *skip_next();
#endif
