
#include	<sys/types.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	<string.h>
#include	"misc.h"

int strnccmp(char *a, char *b, int n)
{
    char ac, bc;

    while(n--)
    {
	ac = *a++;
	if (ac>='a' && ac <= 'z') ac -= 'a'-'A';
	bc = *b++;
	if (bc>='a' && bc <= 'z') bc -= 'a'-'A';
	if (ac == 0 || ac != bc) break;
    }
    if (ac<bc) return -1;
    if (ac > bc) return 1;
    return 0;
}

int strccmp(char *a, char *b)
{
    return strnccmp(a,b,strlen(a)+1);
}

