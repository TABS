

#include	<stdio.h>
#include	<sys/file.h>
#ifdef POSIX
#include	<sys/fcntl.h>
#endif
#include	<unistd.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>
#include	<memory.h>
#include	<sys/stat.h>
#include	"misc.h"


char BookBase[] = "/var/book";
#undef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN 256

void *memdup(void *m, int l)
{
	char *rv = (char*)malloc(l);
	memcpy(rv, m, l);
	return rv;
}


char * make_pathname(char * name)
{
	char * result;
	result = (char *) malloc (strlen(BookBase) + strlen(name) + 6);
	if (result == NULL)
		/* should be a syslog entry */
		fprintf(stderr, "makepathname: malloc failed\n");
	sprintf(result, "%s/%s",BookBase, name);
	return result;
}

char * make_tmp_pathname(char * name)
{
	/* FIXME free this and above somewhere */
	char * result;
	result = (char *) malloc (strlen(BookBase) + strlen(name) + 6);
	if (result == NULL)
		/* should be a syslog entry */
		fprintf(stderr, "makepathname: malloc failed\n");
	sprintf(result, "%s/#%s#",BookBase, name);
	return result;
}

char * get_myhostname(void)
{
	char * retstring;
	char buf[MAXHOSTNAMELEN];
	char * ptr = buf;
	gethostname(buf,MAXHOSTNAMELEN);
	/* strip off the domain!! */

	while ((*ptr != '.') && (*ptr != '\0'))
		ptr++;
    
	*ptr = '\0';
    
	retstring = (char *)calloc(strlen(buf)+1, sizeof(char));
	strcpy(retstring,buf);

	return(retstring);
}

int assert(int condition, char * message)
{
	if (!condition) {
		if (message == NULL) {
			printf("ASSERTION failed... non fatal\n");
			return 0;
		}
		else {
			printf("ASSERTION failed\n%s\n",message);
			exit(43);
	    
		}
	}
	else
		return 1;
}

void shout(char * message)
{
	int fid;
	char *ct;
	time_t now;

	/* there should also be a debug function as well */
	static char filename[] = "/var/book/book_errors";
    
	now = time(0);
	ct = ctime(&now);
    
	fid = open(filename,O_APPEND|O_CREAT| O_RDWR);
	fchmod(fid, S_IRUSR|S_IWUSR);
	write(fid, ct+4, 16);
	write(fid, message, strlen(message));
	close(fid);
    
}

void bailout(char * message, int n)
{
	shout(message);
	exit(n);
}

