/* a module containing some debug routines */
#include	<sys/file.h>
#include	<stdio.h>
#ifdef sun
#include	<sys/fcntl.h>
#endif
#include	<unistd.h>
#include	<stdlib.h>
#include	"misc.h"

#define DebugFile "/var/book/book_debug"
static int debug_level =0 ;

void set_bookdebug_level(void)
{
    int fd;
    char buf[20];
    char * debug;
    extern char * getenv();
    
    /* read the env variable BOOK_DEBUG and return the  debug_level */
    /* accordingly.  If This var is not set then look at */
    /* /var/book/book_debug for a number that is the book debug level */
    debug = getenv("BOOK_DEBUG");

    if (debug != NULL) {
	debug_level = atoi(debug);
/*	printf("DEBUG LEVEL = %d\n",debug_level);*/
	return;
    }

    /* try the file */
    fd = open (DebugFile, O_RDONLY);
    if (fd == -1)
    {
	debug_level = 0;
	return;
    }
    
    if (read (fd, buf, 20) == 0)
    {
	debug_level = 0;
	close(fd);
	return;
    }
    close(fd);
    
    debug_level = atoi(buf);
    return;
    
}

void Debug(char * message, int level)
{
    if (level < debug_level) {
/*	fprintf(stderr,"%s", message);*/
	shout(message);
    }
}
