
/* Skiplists.
   Provide three functions to skip_new:
     cmp(e1, e2, k)
     This should compare e1 with e2 or (if null) k (a key)
   free should free an entry
   errfn should do something with an error message
   
   skip_new(cmp,free,errfn) -> list
   skip_insert(list,entry) -> bool
   skip_delete(list,key) -> bool
   skip_search(list,key) -> *entry
   skip_first(list) -> *entry
   skip_next(*entry) -> *entry

   */
#include	<unistd.h>
#include	<stdlib.h>
#include	<string.h>

#define false 0
#define true 1
typedef char boolean;
#define BitsInRandom 31

#define MaxNumberOfLevels 16
#define MaxLevel (MaxNumberOfLevels-1)
#define newNodeOfLevel(l) (node)malloc(sizeof(struct node)+(l)*sizeof(node))

typedef void *keyType;
typedef void *valueType;


typedef struct node
{
    valueType value;		/* must be first for skip_next to work */
    struct node *forward[1];	/* variable sized array of forward pointers */
} *node;

typedef struct list
{
    int level;	/* Maximum level of the list
		   (1 more than the number of levels in the list) */
    node	header;	/* pointer to head of list */
    int	(*cmpfn)();	/* compares two values or a key an a value */
    void (*freefn)();	/* free a value */
    void (*errfn)();	/* when malloc error occurs */
} *list;

static void default_errfn(char *msg)
{
    write(2, msg, strlen(msg));
    write(2, "\n", 2);
    abort();
}

static int randomsLeft = 0;
static int randomBits;

list skip_new(cmpfn, freefn, errfn)
int (*cmpfn)();
void (*freefn)();
void (*errfn)();
{
    list l;
    int i;

    if (errfn == NULL)
	errfn = default_errfn;
    l = (list)malloc(sizeof(struct list));
    if (l == NULL)
    {
	(*errfn)("Malloc failed in skip_new");
	return NULL;
    }
    l->level = 0;
    l->header = newNodeOfLevel(MaxNumberOfLevels);
    if (l->header == NULL)
    {
	(*errfn)("Malloc failed in skip_new");
	return NULL;
    }
    for (i=0; i<MaxNumberOfLevels; i++)
	l->header->forward[i] = NULL;
    l->header->value = NULL;
    l->cmpfn = cmpfn;
    l->freefn = freefn;
    if (errfn)
	l->errfn = errfn;
    else
	l->errfn = default_errfn;
    return l;
}

void skip_free(l)
list l;
{
    register node p;
    p = l->header;
    while (p != NULL)
    {
	register node q;
	q = p->forward[0];
	if (p != l->header) /* header has no meaningful value */
	    (*l->freefn)(p->value);
	free(p);
	p = q;
    }
    free(l);
}

static int randomLevel()
{
    register int level = 0;
    register int b;
    do {
	if (randomsLeft == 0) {
#ifdef POSIX
	    randomBits = lrand48();
#else
	    randomBits = random();
#endif
	    randomsLeft = BitsInRandom/2;
        }
	b = randomBits&3;
	randomBits>>=2;
	randomsLeft--;
	if (!b) level++;
    } while (!b);
    return(level>MaxLevel ? MaxLevel : level);
}

boolean skip_insert(l, value)
list l;
valueType value;
{
    int k;
    node update[MaxNumberOfLevels];
    node p,q;
    int cm;

    p = l->header;
    for (k=l->level ; k>=0 ; k--)
    {
	cm = 1;
	while ( p->forward[k] != NULL
	       && (cm=(*l->cmpfn)(p->forward[k]->value, value, 0))<0)
	    p = p->forward[k];
	update[k] = p;
    }

    if (cm == 0)
	return false;

    k = randomLevel();
    if (k> l->level)
    {
	k = ++l->level;
	update[k] = l->header;
    }
    q = newNodeOfLevel(k);
    if (q == NULL)
    {
	(*l->errfn)("Malloc failed in skip_insert");
	return false;
    }
    q->value = value;
    for ( ; k>=0 ; k--)
    {
	p = update[k];
	q->forward[k] = p->forward[k];
	p->forward[k] = q;
    }
    return true;
}

boolean skip_delete(l, key)
list l;
keyType key;
{
    int k, m;
    node update[MaxNumberOfLevels];
    node p,q;
    int cm;

    p = l->header;

    for (k=l->level ; k>=0 ; k--)
    {
	cm = 1;
	while ( p->forward[k] != NULL
	       && (cm=(*l->cmpfn)(p->forward[k]->value, NULL, key))<0)
	    p = p->forward[k];
	update[k] = p;
    }

    if (cm == 0)
    {
	q = update[0]->forward[0];
	m=l->level;
	for (k=0; k<=m && (p=update[k])->forward[k] == q ; k++)
	    p->forward[k] = q->forward[k];
	if (l->freefn)
	    (*l->freefn)(q->value);
	free(q);
	while (l->header->forward[m] == NULL && m > 0)
	    m--;
	l->level = m;
	return true;
    }
    else
	return false;
}

valueType *skip_search(l, key)
list l;
keyType key;
{
    int k;
    node p;
    int cm;

    p = l->header;
    for (k=l->level ; k>=0 ; k--)
    {
	cm = 1;
	while ( p->forward[k] != NULL
	       && (cm=(*l->cmpfn)(p->forward[k]->value, NULL, key))<0)
	    p = p->forward[k];
    }
    if (cm != 0)
	return NULL;
    return &p->forward[0]->value;
}

valueType *skip_first(l)
list l;
{
    node p;

    p = l->header;
    if (p->forward[0] == NULL)
	return NULL;
    return & p->forward[0]->value;
}

valueType *skip_next(c)
valueType *c;
{
    node p;
    p = (node)c;
    if (p->forward[0] == NULL)
	return NULL;
    return & p->forward[0]->value;
}
