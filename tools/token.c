
#include	"../db_client/db_client.h"
#include	<getopt.h>

char *Usage[] = {
    "Usage: token [-v] [-l | tokens]   -- list (verbosely) some or all tokens",
    "       token -R oldname newname   -- rename a token",
    "       token [-f] -c tokename [-i idnum] attributes -- create a new token",
    "       token -[ads] attributes token[/EL] - change attributes in a token",
    "       token -D token             -- delete a token",
    NULL };

static int get_attrs(description d, char *names);
static void list_token(entityid id, int verbose);    
static int adj_token(char *tname, description *setp, description remove, description add);

int main(int argc, char *argv[])
{
	int list=0, ren=0, make=0, change=0, delete = 0;

	int verbose = 0;
	int listall = 0;
	int force = 0;
	char *newtoken= NULL;
	char *server = NULL;
	int newid = -1;
	description remove, add, set, *setp = NULL;

	extern int optind;
	extern char *optarg;
	int opt;

	remove = new_desc();
	add = new_desc();
	set = new_desc();
    
	while ((opt=getopt(argc, argv, "vlRDfc:i:a:d:s:r:"))!= EOF)
		switch(opt)
		{
		case 'r': server = optarg; break;
		case 'v': verbose = 1; break;
		case 'l': listall = 1; list=1; break;
		case 'R': ren=1 ; break;
		case 'D': delete =1; break;
		case 'f': force=1; break;
		case 'c': newtoken = optarg; make=1 ; break;
		case 'i': newid = atoi(optarg);
			if (newid <= 0)
			{
				fprintf(stderr, "token: id must be >= 1: %s\n", optarg);
				usage();
				exit(2);
			}
			break;
		case 'a':
			if (!get_attrs(add, optarg)) exit(2);
			change = 1;
			break;
		case 'd':
			if (!get_attrs(remove, optarg)) exit(2);
			change = 1;
			break;
		case 's':
			if (!get_attrs(set, optarg)) exit(2);
			setp = &set;
			change = 1;
			break;
		default:
			usage();
			exit(2);
		}
	if (ren+make+change+list+delete == 0)
		list = 1;
	if (ren+make+change+list+delete > 1)
	{
		fprintf(stderr, "token: multiple modes specified\n");
		usage();
		exit(2);
	}

	if (verbose && !list)
	{
		fprintf(stderr,"token: -v only allowed when listing\n");
		usage();
		exit(2);
	}
	if (force && !make)
	{
		fprintf(stderr,"token: -f only allowed with -c\n");
		usage();
		exit(2);
	}
	if (newid > 0 && !make)
	{
		fprintf(stderr,"token: -i only allowed with -c\n");
		usage();
		exit(2);
	}
	if (server)
		open_db_client(server, "tcp");

	if (delete)
	{
		int id;
		book_change ch;
		if (optind == argc)
		{
			fprintf(stderr, "token: must give a token to delete\n");
			usage();
			exit(2);
		}
		if (optind < argc-1)
		{
			fprintf(stderr, "token: can only delete one token at a time\n");
			usage();
			exit(2);
		}
		id = get_mappingint(argv[optind], M_TOKEN);
		if (id < 0)
		{
			fprintf(stderr, "token: unknown token: %s\n", argv[optind]);
			exit(2);
		}
		memset(&ch, 0, sizeof(ch)); /* this will null the token, hence delete */
		ch.chng = ADD_TOKEN;
		ch.book_change_u.addtok.tnum = id;
		if (send_change(&ch)!= 0)
		{
			fprintf(stderr,"token: delete token request failed\n");
			exit(3);
		}
		if (make_mapping(id, "", M_TOKEN)!= 0)
		{
			fprintf(stderr,"token: failed to remove name mapping for token\n");
			exit(3);
		}
	}
	if (list)
	{
		if (listall)
		{
			intpairlist toks= NULL, t;
			char k[20];
			if (db_read(key_int(k, C_ALLOTMENTS, 0), &toks, (xdrproc_t)xdr_intpairlist, CONFIG)
			    != R_RESOK)
			{
				fprintf(stderr,"token: cannot get list of tokens\n");
				exit(1);
			}
			for (t=toks ; t; t=t->next)
				list_token(t->data, verbose);
			xdr_free((xdrproc_t)xdr_intpairlist, (char*) &toks);
		}
		else if (optind == argc)
		{
			fprintf(stderr,"token: no tokens given\n");
			usage();
			exit(2);
		}
		else while (optind < argc)
		{
			int id = get_mappingint(argv[optind], M_TOKEN);
			if (id <0 )
				fprintf(stderr,"token: unknown token %s\n", argv[optind]);
			else
				list_token(id, 1/*verbose*/); /* when listing xplicit tokens, always be verbose */
			optind++;
		}
	}
	if (ren)
	{
		int id;
		if (argc - optind != 2)
		{
			fprintf(stderr,"token: must give old and new token names for rename\n");
			usage();
			exit(2);
		}
		id = get_mappingint(argv[optind], M_TOKEN);
		if (id <0)
		{
			fprintf(stderr,"token: cannot find old token %s\n", argv[optind]);
			exit(2);
		}
		if (get_mappingint(argv[optind+1], M_TOKEN) != -1)
		{
			fprintf(stderr,"token: new token name exists: %s\n", argv[optind+1]);
			exit(2);
		}
		if (make_mapping(id, argv[optind+1], M_TOKEN) != 0)
		{
			fprintf(stderr, "token: changing name failed.\n");
			exit(1);
		}
	}
	if (make)
	{
		/*
		 * create token called newtoken with id newid.
		 * if newid == -1, pick a free uid
		 * if force, then don't worry if it already exists
		 * if any args, set these attributes
		 */
		int id;
		token tok;
		book_change ch;
		id = get_mappingint(newtoken, M_TOKEN);
		if (id != -1)
		{
			/* already exists */
			if (id == -2)
			{
				fprintf(stderr, "token: problem communiating with database\n");
				exit(3);
			}
			if (!force || (newid >0 && newid != id))
			{
				fprintf(stderr,"token: token %s already exists with id %d\n",
					newtoken, id);
			}
			newid = id;
		}
		if (newid == -1)
			newid = choose_id(1, M_TOKEN);
		if (newid < 0)
		{
			fprintf(stderr, "token: problem communicating with database\n");
			exit(3);
		}
		while (optind < argc)
			if (!get_attrs(set, argv[optind++]))
				exit(2);
		if (make_mapping(newid, newtoken, M_TOKEN)!= 0)
		{
			fprintf(stderr, "token: cannot record token name with database\n");
			exit(3);
		}

		tok.advance = set;
		tok.late = set;
		ch.chng = ADD_TOKEN;
		ch.book_change_u.addtok.tnum = newid;
		ch.book_change_u.addtok.tok = tok;
		if (send_change(&ch)!= 0)
		{
			fprintf(stderr, "token: cannot store token info in database\n");
			exit(3);
		}
	}
	if (change)
	{
		if (optind == argc)
		{
			fprintf(stderr, "token: no tokens given\n");
			usage();
			exit(2);
		}
		for ( ; optind < argc ; optind++)
			if (!adj_token(argv[optind], setp, remove, add)) exit(1);
	}
	exit(0);
}

static int adj_token(char *tname, description *setp, description remove, description add)
{
	/* token may end /A or /L for only changing advance or late */
	int len = strlen(tname);
	int late = 1, adv = 1;
	int id;
	token tok;
	book_change ch;
	char name[200];
	char k[20];
	strcpy(name, tname);
	if (name[len-2] == '/')
	{
		if (name[len-1] == 'A')
			late = 0;
		else if (name[len-1] == 'L')
			adv = 0;
		else
		{
			fprintf(stderr,"token: unknown switch: %s\n", tname);
			return 0;
		}
		name[len-2] = '\0';
	}

	id = get_mappingint(name, M_TOKEN);
	if (id < 0)
	{
		fprintf(stderr, "token: unknown token %s\n", name);
		return 0;
	}

	memset(&tok, 0, sizeof(tok));
	if (db_read(key_int(k, C_TOKENS, id), &tok, (xdrproc_t)xdr_token, CONFIG)!= R_RESOK)
	{
		fprintf(stderr, "token: cannot find token %s, use -f -c\n", name);
		return 0;
	}
	if (adv)
	{
		if (setp)
		{
			zero_desc(&tok.advance);
			desc_add(tok.advance, *setp);
		}
		desc_sub(tok.advance, remove);
		desc_add(tok.advance, add);
	}
	if (late)
	{
		if (setp)
		{
			zero_desc(&tok.late);
			desc_add(tok.late, *setp);
		}
		desc_sub(tok.late, remove);
		desc_add(tok.late, add);
	}
	ch.chng = ADD_TOKEN;
	ch.book_change_u.addtok.tnum = id;
	ch.book_change_u.addtok.tok = tok;
	if (send_change(&ch)!= 0)
	{
		fprintf(stderr, "token: cannot store token info in database\n");
		return 0;
	}
	return 1;
}

static int get_attr(description d, char *name)
{
	/* name either names an attribute or a config entry
	 * which lists some attributes
	 */
	int a;
	char * attrs;
	a = get_mappingint(name, M_ATTRIBUTE);
	if (a>=0)
	{
		set_a_bit(&d, a);
		return 1;
	}
    
	attrs = get_configchar(name);
	if (attrs)
		return get_attrs(d, attrs);

	fprintf(stderr,"token: cannot find attribute %s\n", name);
	return 0;
}

static int get_attrs(description d, char *names)
{
	char buf[200];
	char *dot;

	while ((dot = strchr(names, '.')))
	{
		strncpy(buf, names, dot-names);
		buf[dot-names] = '\0';
		if (! get_attr(d, buf))
			return 0;
		names = dot+1;
	}
	return get_attr(d, names);
}



static void list_desc(char *prefix, description d)
{
	int first = 1;
	int i;
	for (i=0 ; i<d.item_len*8 ; i++)
	{
		if ((i%32)==0 && !first)
		{
			printf("\n");
			first = 1;
		}
		if (query_bit(&d, i))
		{
			char *n;
			if (first)
				printf("%s", prefix);
			first = 0;
			n = get_mappingchar(i, M_ATTRIBUTE);
			if (n== NULL) n = strdup("*unknown*attr*");
			printf("%s ", n);
			free(n);
		}
	}
	if (!first) printf("\n");
}

static void list_token(entityid id, int verbose)
{
	char *name = get_mappingchar(id, M_TOKEN);
	if (name == NULL)
		name = strdup("*unknown*token*");

	printf("%s\n", name);
	if (verbose)
	{
		token t;
		char k[20];
		memset(&t, 0, sizeof(t));
		if (db_read(key_int(k, C_TOKENS, id), &t, (xdrproc_t)xdr_token, CONFIG)== R_RESOK)
		{
			printf("Advance:\n");
			list_desc("  ", t.advance);
			printf("Late:\n");
			list_desc("  ", t.late);
			xdr_free((xdrproc_t)xdr_token, (char*) &t);
		}
		printf("\n");
	}
}
