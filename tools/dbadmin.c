
#include	"../db_client/db_client.h"
#include	"../lib/dlink.h"
#include	<time.h>
#include	<getopt.h>
/*
 * db_admin : administer the database servers.
 *
 * db_admin -s -A | servers -- list replicas know by one or all servers
 * db_admin -x servers      -- tell replica to exit
 * db_admin -d replica	    -- delete replica from list
 * db_admin -a replica      -- add an active replica to replicas list
 * db_admin -R replica 	    -- tell replica to reorganise itself
 *
 *  can take a -r replica argument to talk only to the named replica
 *
 */

char *Usage[] = {
    "Usage: db_admin [-r replica] -s [serverlist]      -- show replica status",
    "       db_admin -x replica                        -- tell replica to exit",
    "       db_admin [-r replica] -d replica           -- remove replica from list",
    "       db_admin [-r replica] -a replica           -- add a replica to list",
    "       db_admin -R replica                        -- tell replica to re-organise data files",
    NULL };

static void rep_status(char *replica);
static char *get_replica_list();

int main(int argc, char *argv[])
{

	book_change ch;
	int num;
	update_status *st;
	int sts;
    
	char *firstchoice = NULL;
	enum {Mstatus, Mexit, Mdelete, Madd, Mreorg, Mnone}  mode = Mnone;
	int All = 0;

	extern int optind;
	extern char *optarg;
	int opt;
    
	while ((opt = getopt(argc, argv, "r:sAxdaR"))!= EOF)
		switch(opt)
		{
		case 'r':
			if (firstchoice)
			{
				fprintf(stderr,"dbadmin: give only one first choice replica (-r)\n");
				usage();
				exit(2);
			}
			firstchoice = optarg;
			break;
		case 'R':
		case 's':
		case 'x':
		case 'd':
		case 'a':
			if (mode !=Mnone)
			{
				fprintf(stderr,"dbadmin: give only one mode (s,x,d,a)\n");
				usage();
				exit(2);
			}
			switch(opt) {
			case 'R': mode = Mreorg ; break;
			case 's': mode = Mstatus; break;
			case 'x': mode = Mexit; break;
			case 'd': mode = Mdelete; break;
			case 'a': mode = Madd; break;
			}
			break;
		case 'A':
			All = 1;
			break;
		default:
			usage();
			exit(2);
		}
	if (mode == Mnone)
	{
		fprintf(stderr, "dbadmin: must give one of s,x,d,a\n");
		usage();
		exit(2);
	}
	if (All && mode != Mstatus)
	{
		fprintf(stderr, "dbadmin: -A only allowed with -s\n");
		usage();
		exit(2);
	}
	if (firstchoice && mode == Mexit)
	{
		fprintf(stderr,"dbadmin: -r not meaningful with -x\n");
		usage();
		exit(2);
	}

	if (mode == Mstatus)
	{
		if (All && optind < argc)
		{
			fprintf(stderr,"dbadmin: no serverlist can be given with -A\n");
			usage();
			exit(2);
		}
		if (!All && optind == argc)
		{
/*
  fprintf(stderr,"dbadmin: -A or serverlist must be given with -s\n");
  usage();
  exit(2);
*/
			All = 1;
		}
	}
	else
	{
		if (optind == argc)
		{
			fprintf(stderr, "dbadmin: must specfy a replica name for action\n");
			usage();
			exit(2);
		}
		if (optind+1 < argc)
		{
			fprintf(stderr, "dbadmin: specify only one replica name for action\n");
			usage();
			exit(2);
		}
	}
	switch(mode)
	{
	case Mnone: /* keep -Wall quite */
		break;
	case Mstatus:
		if (All)
		{
			char *replist, *rep;
			if (firstchoice)
				open_db_client(firstchoice, "tcp");
			replist = get_replica_list();
			if (replist)
				for(rep = dl_next(replist) ; rep != replist ; rep=dl_next(rep))
					rep_status(rep);
			else
			{
				fprintf(stderr,"dbadmin: cannot get list of all replicas\n");
				exit(3);
			}
		}
		else
			for (  ; optind < argc ; optind++)
				rep_status(argv[optind]);
	
		break;
	case Mexit:
		open_db_client(argv[optind], "tcp");
		if (db_client== NULL)
		{
			fprintf(stderr,"dbadmin: cannot contact replica on %s\n", argv[optind]);
			exit(1);
		}
		else
		{
			int d = 0;
			if (terminate_2(&d, db_client)== NULL)
			{
				fprintf(stderr,"dbadmin: problem sending exit request to replica %s\n", argv[optind]);
				exit(1);
			}
		}
		break;
	case Mreorg:
		open_db_client(argv[optind], "tcp");
		if (db_client == NULL)
		{
			fprintf(stderr,"dbadmin: cannot contact replica on %s\n", argv[optind]);
			exit(1);
		}
		else
		{
			int d = 0;
			if (db_reorganize_2(&d, db_client)== NULL)
			{
				fprintf(stderr,"dbadmin: problem sending reoganise request to replica %s\n", argv[optind]);
				exit(1);
			}
		}
		break;
	case Mdelete:
		/* delete command must (appear to) come from the replica being
		 * deleted. If it is not contactable and firstchoice was given,
		 * then fake a change from the deleted replica
		 */
		open_db_client(argv[optind], "tcp");
		if (db_client == NULL
		    || (st = get_updatest_2(NULL, db_client)) == NULL
			)
		{
			/* have to fake it */
			book_changeent ce;
			book_changeres *result;
			if (firstchoice == NULL)
			{
				fprintf(stderr,"dbadmin: cannot contact %s, consider using -r\n", argv[optind]);
				exit(1);
			}
			open_db_client(firstchoice, "tcp");
			if (db_read(str_key(argv[optind]), &num, (xdrproc_t)xdr_replica_no, REPLICAS)
			    != R_RESOK)
			{
				fprintf(stderr,"dbadmin: %s doesn't seem to know %s\n",
					firstchoice, argv[optind]);
				exit(1);
			}
			ce.time = time(0);
			ce.machine = argv[optind];
			ce.changenum = num+1;
			ce.thechange.chng = DEL_SERVER;
			ce.thechange.book_change_u.delserver = argv[optind];
			result = change_db_2(&ce, db_client);
			if (result == NULL || *result != C_OK)
			{
				fprintf(stderr,"dbadmin: could not send delete request\n");
				exit(3);
			}
		}
		else
		{
			ch.chng = DEL_SERVER;
			ch.book_change_u.delserver = argv[optind];
			if (send_change(&ch)!= 0)
			{
				fprintf(stderr,"dbadmin: delete request failed\n");
				exit(3);
			}
		}
		break;
	case Madd:
		/* server must be running */
		open_db_client(argv[optind], "tcp");
		if (db_client == NULL
		    || (st = get_updatest_2(NULL, db_client)) == NULL
		    || (*st) == NONE)
		{
			fprintf(stderr,"dbadmin: replica at %s does not appear to be functional\n", argv[optind]);
			exit(1);
		}
	    
		open_db_client(firstchoice, NULL); /* null is ok here */
		if (db_read(str_key(argv[optind]), &num, (xdrproc_t)xdr_replica_no, REPLICAS)
		    != R_NOMATCH)
		{
			fprintf(stderr, "dbadmin: replica %s already registered?\n", argv[optind]);
			exit(1);
		}
		ch.chng = ADD_SERVER;
		ch.book_change_u.server = argv[optind];
		if ((sts=send_change(&ch))!= 0)
		{
			fprintf(stderr,"dbadmin: add server operation failed (%d)\n", sts);
			exit(3);
		}
		break;
	}
	exit(0);
}

static char *get_replica_list()
{
	/* get a list of replicas and return as a dlink list of char*s */
	query_req arg;
	query_reply *result;
	char *rv;

	arg.key.item_val = "dummy";
	arg.key.item_len = 5;
	arg.type = M_FIRST;
	arg.database = REPLICAS;

	if (db_client == NULL)
		refresh_db_client();
	if (db_client == NULL)
		return NULL;
    
	result = query_db_2(&arg, db_client);
	if (result== NULL)
		return NULL;
    
	rv = dl_head();
	while (result && result->error == R_RESOK)
	{
		char *rn;
		rn = dl_strndup(result->query_reply_u.data.key.item_val,
				result->query_reply_u.data.key.item_len);
		dl_add(rv,rn);
		arg.key.item_val = rn;
		arg.key.item_len = strlen(rn);
		arg.type = M_NEXT;
		xdr_free((xdrproc_t)xdr_query_reply, (char*) result);
		result = query_db_2(&arg, db_client);
	}
	return rv;
}


static char* statuses[] = { NULL, "NO", "CP", "UP"};
static void rep_status(char *replica)
{
	/* display status of replica */
	query_req arg;
	query_reply *result;
	update_status *stat;
	open_db_client(replica, "tcp");
	if (db_client == NULL)
	{
		printf("%s: NO-COMMUNICATION\n", replica);
		return;
	}
	stat = get_updatest_2(NULL, db_client);
	if (stat == NULL)
	{
		printf("%s: NO-SERVER\n", replica);
		return;
	}
	printf("%-10s %s: ",replica,statuses[*stat]);

	arg.key.item_val = "dummy";
	arg.key.item_len = 5;
	arg.type = M_FIRST;
	arg.database = REPLICAS;

	result = query_db_2(&arg, db_client);
	while (result && result->error == R_RESOK)
	{
		char *rn;
		int chn;
		rn = strndup(result->query_reply_u.data.key.item_val,
			     result->query_reply_u.data.key.item_len);
		chn = ntohl(*(int*)result->query_reply_u.data.value.item_val);
		printf("%s %d ", rn, chn);
	
		arg.key.item_val = rn;
		arg.key.item_len = strlen(rn);
		arg.type = M_NEXT;
		xdr_free((xdrproc_t)xdr_query_reply, (char*) result);
		result = query_db_2(&arg, db_client);
	}
	printf("\n");
}
