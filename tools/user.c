
#include	"../db_client/db_client.h"
#include	<getopt.h>

void show_tokens(char *user, intpairlist t, intpairlist p, int mach)
{
	/* Print info in t.
	 * for each, if matching element in p print t(p)
	 */
	int col= 0;

	for ( ; t ; t=t->next)
	{
		intpairlist match;
		char *name = get_mappingchar(t->data, M_TOKEN);
		if (name == NULL) name = strdup("<unknown>");
		match = findintkey(p, t->data);
		if (mach)
		{
			if (p)
				printf("%-15s %-16s %4d %4d\n",  user, name, t->num,
				       match?match->num:0
					);
			else
				printf("%-15s %-16s %4d\n", user, name, t->num);
		}
		else if (match)
			printf("%-16s%3d(%2d in use)  ", name, t->num, match->num);
		else
			printf("%-19s%4d  ", name, t->num);
	
		if (!mach && ++col == 3)
		{
			printf("\n");
			col = 0;
		}
		free(name);
	}
	if (col)
		printf("\n");
}


int show_bookings(int uid, userbookinglist ubl, int mach)
{
	booklist bookings, b;


	bookings = getfullbookings(ubl, uid);

	if (bookings== NULL)
	{
		fprintf(stderr,"user: cannot show booking information\n");
		return -1;
	}

	for (b = bookings ; b ; b=b->next, ubl = ubl->next)
		show_booking(b, ubl->slot);
	xdr_free((xdrproc_t)xdr_booklist, (char*)  &bookings);
	return 1;
}

static query_req request;
static char *prefix;

void userlist_init(int allotments)
{
	if (db_client == NULL)
		open_db_client(NULL, "tcp");
	if (allotments)
	{
		request.key.item_val=memdup("9_", 2);
		request.key.item_len=2;
		request.type = M_NEXT_PREFIX;
		request.database = CONFIG;
		prefix = "9_";
	}
	else
	{
		request.key.item_val=memdup("dumy", 4);
		request.key.item_len=4;
		request.type = M_FIRST;
		request.database = BOOKINGS;
		prefix="";
	}
}

char *userlist_next()
{
	while(1)
	{
		query_reply *result;
		result = query_db_2(&request, db_client);
		if (result == NULL) {
			return NULL;
		}
		if (request.type == M_FIRST)
			request.type = M_NEXT;
		free(request.key.item_val);
		if (result->error != R_RESOK ||
		    result->query_reply_u.data.key.item_len == 0 ||
		    result->query_reply_u.data.key.item_val== NULL)
		{
			return NULL;
		}
	
		request.key.item_len = result-> query_reply_u.data.key.item_len;
		request.key.item_val =
			memdup(result->query_reply_u.data.key.item_val,
			       request.key.item_len+1);
		request.key.item_val[request.key.item_len] = 0;
		xdr_free((xdrproc_t)xdr_query_reply, (char*) result);
		if (strncmp(request.key.item_val, prefix, strlen(prefix))==0
		    && strspn(request.key.item_val+strlen(prefix), "0123456789")
		    == request.key.item_len-strlen(prefix))
			return request.key.item_val+strlen(prefix);
	}
}

/*
 * user can print out
 *	total token allocation and usage	-t
 *	personal token allocation		-a
 *	pending bookings			-b
 *	past/cancelled bookings			-p
 *  these can be in a "machine readable" format -m
 *
 * user can also allocate tokens		-A
 *	which requires a count			-c
 *
 * user can also iterate though all users	ALL
 *
 * if no action flags are given, -tbp is assumed
 *
 */

char *Usage[] = {
    "Usage: user -[Dtabp] [-m] [user|ALL] ...",
    "       user [-C [-i id]] -A token -c count user ...",
    "       user -R oldname newname",
    "  -D : delete user record if no bookings",
    "  -t : total token allocation and usage",
    "  -a : personal token allocation",
    "  -b : pending bookings",
    "  -p : past/cancelled bookings",
    "  -m : machine readable, no headers",
    "  -A : change personal allocation of token to count or by +-count",
    "  -C : create class if it doesn't exist",
    "  -i : provide id number to use for class",
    "  -R : rename",
    NULL };

int main(int argc, char *argv[])
{
	extern int optind;
	extern char *optarg;
	int arg;
	int errs = 0;
	int all = 0;
	char *user;

	int tokens=0, alloc=0, bookings=0, past=0, mach=0;
	char *token = NULL;
	int tokid;
	int count = -1, relcount = 0;
	int clid = -1;
	int create = 0;
	int delete = 0;
	int rename = 0;
	char *server = NULL;

	while((arg=getopt(argc,argv,"tabpmA:c:Ci:Rr:D"))!=EOF)
		switch(arg)
		{
		case 'D': delete = 1; break;
		case 'r': server = optarg; break;
		case 'R': rename = 1; break;
		case 'C': create = 1; break;
		case 'i':
			if ((clid = atoi(optarg) < get_class_min()))
			{
				fprintf(stderr, "user: class id must be a least %d\n", get_class_min());
				usage();
				exit(2);
			}
			break;
		case 't': tokens = 1; break;
		case 'a': alloc = 1; break;
		case 'b': bookings = 1; break;
		case 'p': past = 1; break;
		case 'm': mach = 1; break;
		case 'A':
			token = optarg;
			break;
		case 'c':
			if (optarg[0] == '-')
				relcount = -1;
			else if (optarg[0] == '+')
				relcount = 1;
			if (relcount) optarg++;
			if (optarg[0] < '0' || optarg[0] > '9')
			{
				fprintf(stderr,"user: count must be [+/-]num\n");
				usage();
				exit(2);
			}
			count = atoi(optarg);
			break;
		default:
			usage();
			exit(2);
		}
	if (!(delete||create||tokens||alloc||bookings||past||token||rename))
		tokens = bookings = past = 1;

	if (rename && (delete||create||tokens||alloc||bookings||past||token))
	{
		fprintf(stderr,"user: no other flags allowed with rename (-R).\n");
		usage();
		exit(2);
	}

	if ((tokens|alloc|bookings|past|delete) && (token || count>=0 || create || clid>0))
	{
		fprintf(stderr,"user: cannot give token or count with a printing or deletion command.\n");
		usage();
		exit(2);
	}
	if (token && count<0)
	{
		fprintf(stderr, "user: must give a count for token allocation.\n");
		usage();
		exit(2);
	}
	if (token && mach)
	{
		fprintf(stderr, "user: -m flag not meaningful with -A\n");
		usage();
		exit(2);
	}
	if (server)
		open_db_client(server, "tcp");
	if (token)
	{
		tokid = get_mappingint(token, M_TOKEN);
		if (tokid < 0)
		{
			fprintf(stderr,"user: unknown token %s\n", token);
			exit(2);
		}
	}

	if (rename)
	{
		char *newname, *oldname;
		int id, otherid;
		if (argc-optind != 2)
		{
			fprintf(stderr,"user: two names required for rename\n");
			usage();
			exit(2);
		}

		oldname=argv[optind];
		newname=argv[optind+1];
		id = get_mappingint(oldname, M_CLASS);
		if (id <0)
		{
			fprintf(stderr,"user: old name '%s' not defined\n", oldname);
			exit(2);
		}
		otherid = get_mappingint(newname, M_CLASS);
		if (otherid >=0)
		{
			fprintf(stderr,"user: new name '%s' already in use, uid=%d!\n", newname, otherid);
			exit(2);
		}
		if (id < get_class_min())
		{
			fprintf(stderr,"user: can only rename classes, with id >= %d\n", get_class_min());
			exit(2);
		}


		if (make_mapping(id, newname, M_CLASS) != 0)
		{
			fprintf(stderr,"user: failed to make change in database\n");
			exit(3);
		}
		exit(0);
	}
    

	if (optind == argc && ! mach)
	{
		fprintf(stderr, "user: no users given\n");
		usage();
		exit(2);
	}

	if (optind+1 == argc && strcmp(argv[optind],"ALL")==0)
	{
		/* need to find ALL users */
		if (bookings || past || tokens)
			userlist_init(0);
		else
			userlist_init(1);
		all = 1;
	}
	for (user= all?userlist_next():argv[optind];
	     user != NULL ;
	     user = delete?user:all?userlist_next():argv[++optind])
	{
		users urec;
		int uid;
		char key[20];
		char *uname;

		if (user[0] >= '0' && user[0] <= '9') {
			uid = atoi(user);
			uname = find_username(uid);
		} else {
			uid = find_userid(user);
			uname = user;
		}
		if (uid < 0 && create)
		{
			uid = clid;
			if (uid == -1)
			{
				uid = choose_id(get_class_min(), M_CLASS);
				if (uid < 0)
				{
					fprintf(stderr,"user: problem communicating with database\n");
					exit(3);
				}
			}
			else
			{
				char *nm;
				if ((nm=get_mappingchar(uid, M_CLASS)) != NULL)
				{
					fprintf(stderr,"user: id %d already used for %s\n", uid, nm);
					free(nm);
					exit(2);
				}
			}
			if (make_mapping(uid, uname, M_CLASS)!= 0)
			{
				fprintf(stderr,"user: cannot store new class name in database!\n");
				exit(3);
			}
		}
	
		if (uid < 0)
		{
			fprintf(stderr,"user: unknown user or class: %s\n", uname);
			errs++;
			continue;
		}

		memset(&urec, 0, sizeof(urec));
		switch(db_read(user_key(key, uid), &urec, (xdrproc_t)xdr_users, BOOKINGS))
		{
		case R_RESOK:
			break;
		case R_NOMATCH:
			urec.bookings = NULL;
			urec.pastbookings = NULL;
			urec.tokens = NULL;
			break;
		default:
			fprintf(stderr,"user: cannot get information for %s\n", uname);
			continue;
		}

		if (tokens)
		{
			intpairlist all_tokens;
			all_tokens = get_alloc_tokenlist(uid);
			if (!mach)  printf("Token allocation for %s\n", uname);
			show_tokens(uname, all_tokens, urec.tokens, mach);
			xdr_free((xdrproc_t)xdr_intpairlist, (char*) &all_tokens);
		}
	
		if (alloc)
		{
			intpairlist alot = NULL;
			char k[20];
			if (db_read(key_int(k, C_ALLOTMENTS,uid), &alot, (xdrproc_t)xdr_intpairlist, CONFIG)!= R_RESOK)
				(!mach) &&  printf("No personal allocation for %s\n", uname);
			else
			{
				(!mach) &&  printf("Personal allocation for %s\n",  uname);
				show_tokens(uname, alot, NULL, mach);
				xdr_free((xdrproc_t)xdr_intpairlist, (char*) &alot);
			}
		}


		if (bookings)
		{
			if (urec.bookings == NULL)
			{
				if (!mach)  printf("No pending bookings\n");
			}
			else
			{
				if (!mach)  printf("Pending bookings for %s\n", uname);
				show_bookings(uid, urec.bookings, mach);
			}
		}

		if (past)
		{
			if (is_defaulter(uid))
				printf("Is a defaulter\n");
			if (urec.pastbookings == NULL)
			{
				if (!mach)  printf("No past bookings\n");
			}
			else
			{
				if (!mach)  printf("Past bookings for %s\n", uname);
				show_bookings(uid, urec.pastbookings, mach);
			}
		}

		if (token)
		{
			book_change ch;
			int change;
			if (relcount != 0)
				change = relcount * count;
			else
			{
				intpairlist alot = NULL;
				char k[20];
				change = count;
				if (db_read(key_int(k, C_ALLOTMENTS, uid), &alot, (xdrproc_t)xdr_intpairlist, CONFIG)== R_RESOK)
				{
					intpairlist t;
					t = findintkey(alot, tokid);
					if (t) change = count - t->num;
				}
			}
			if (change)
			{
				ch.chng = ADD_CLASS;
				ch.book_change_u.addcl.tnum = tokid;
				ch.book_change_u.addcl.count = change;
				ch.book_change_u.addcl.user = uid;
				if (send_change(&ch) != 0)
				{
					fprintf(stderr, "user: failed to send allocation change for %s\n",
						uname);
					errs++;
				}
				else
					if (!mach)  printf("Changes token allocation for %s by %d\n",
							   uname, change);
			}
			else
				if (!mach)  printf("No change required for %s\n", uname);
		}

		if (delete)
		{
			user = all?userlist_next():argv[++optind];
			if (urec.bookings || urec.pastbookings)
			{
				fprintf(stderr, "user: Cannot delete user with bookings or pastbookings: %s\n", uname);
			}
			else
			{
				book_change ch;
				printf("deleting %s...\n", uname);
				ch.chng = REMOVE_USER;
				ch.book_change_u.user_togo = uid;
				if (send_change(&ch) != 0)
				{
					fprintf(stderr, "user: failed to send delete request for %s\n",
						uname);
					errs++;
				}
			}
		}
	}
	exit(errs?1:0);
}
