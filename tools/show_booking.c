
#include	"../db_client/db_client.h"

static char *bstatus[] = { "", "PENDING",
			      "TENTATIVE",
			      "ALLOCATED",
			      "RETURNED",
			      "FREEBIE",
			      "REFUNDED",
			      "CANCELLED",
			       NULL
		      };
int char2bstatus(char *s)
{
    int i;
    for (i=0; bstatus[i] ; i++)
	if (strccmp(s, bstatus[i])==0)
	    return i;
    return -1;
}

void show_booking(booklist bl, book_key slot)
{
    char doys[20], pods[20];
    int pod, doy, lab;
    char *labname, *tokname;
    book_key_bits(&slot, &doy, &pod, &lab);
    doy2str(doys, doy);
    pod2str(pods, pod);
    labname = get_mappingchar(lab, M_ATTRIBUTE);
    tokname = get_mappingchar(bl->tnum, M_TOKEN);

    printf(" %s %s in %s %s x%d with %10s at %s ",
	   doys, pods, labname?labname:"-unknown-lab-", bstatus[bl->status], bl->number, tokname?tokname:"-unknown-token-", myctime(bl->time));
    free(labname);
    free(tokname);
    if (bl->who_by != bl->who_for)
    {
	char *other;
	other = find_username(bl->who_by);
	printf(" (by %s)", other);
	free(other);
    }
    if (bl->allocations.entitylist_len>0)
    {
	char sep = ' ';
	int i;
	printf(" alloc to");
	for (i=0; i<bl->allocations.entitylist_len ; i++)
	{
	    char *n = get_mappingchar(bl->allocations.entitylist_val[i], M_WORKSTATION);
	    if (n)
		printf("%c%s", sep, n);
	    else
		printf("%c%d", sep, bl->allocations.entitylist_val[i]);
	    sep = ',';
	    if (n) free(n);
	}
    }
    if (bl->claimed != 0)
    {
	printf(" (");
	if (bl->claimed & DID_LOGIN)
	{
	    int sec = ((bl->claimed>>4)-30*60);
	    char * sgn = "";
	    if (sec<0) sgn="-", sec = -sec;
	    printf("claimed at %s%dm%02ds", sgn, sec/60, sec%60);
	}
	if (bl->claimed & DID_DEFAULT)
	    printf(" defaulted");
	if (bl->claimed & WAS_TENTATIVE)
	    printf(" tentative");
	if (bl->claimed & LOGIN_ON_ALLOC)
	    printf(" on allocated ws");
	printf(")");
    }
    printf("\n");
}
