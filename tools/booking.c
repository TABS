
/*
 * extract and print booking information from the database
 * also allow state changes of a list of bookings
 * optionally remove the booking from the database
 *
 * given a set of days, periods, and lab, print out the
 *  bookings showing
 *    doy pod lab user count token time-made status (who_by)
 *
 * or... read in a list of bookings as
 *   doy pod lab user time
 *    and change the status of them all to something.
 *
 */

#include	"../lablist/lablist.h"

int podofday(char*);
int dayofyear(char*);

char *Usage[] = {
    "Usage: booking [-R] dates periods labs",
    "          dates == ddmmm[-ddmmm]  e.g. 29feb-05mar",
    "          periods == hh:mm[-hh:mm] e.g. 9:00-12:30",
    "          labs == labname or labgroup e.g. undercroft, ALL",
    "        -R removes bookings (different from unbooking)",
    "       booking -s new-status < booking-list",
    "          booking-list == {day period lab user time}",
    "       booking -t logintime < booking-list",
    NULL };


void show_bookings(int doy, int pod, int lab, int remove_bookings)
{
	book_key key = make_bookkey(doy, pod, lab);
	char k[20];
	bookhead bh;
	booklist bl;
	int pending = 0;

	bh.data= NULL;
	db_read(bookkey2key(k, key), &bh, (xdrproc_t)xdr_bookhead, BOOKINGS);

	for (bl = bh.data ; bl ; bl=bl->next)
	{
		char *un = find_username(bl->who_for);
		if (un)
		{
			printf("%-12s", un);
			free(un);
		}
		else
			printf("uid=%-8d", (int)bl->who_for);
		if (bl->status == B_PENDING)
			pending++;
		show_booking(bl, key);
	}
	if (bh.data != NULL && remove_bookings && pending == 0)
	{
		if (do_removal(key)!= 0)
		{
			fprintf(stderr, "failed to remove booking %d %d %d\n",
				doy, pod, lab);
		}
	}
	xdr_free((xdrproc_t)xdr_bookhead, (char*) &bh);
}

int get_range(char *str, int *start, int *finish, int (*fn)(char*))
{
	/* if str is either acceptable by fn, or two substrings separated by '-'
	 * which are both acceptable by fn and return numbers in order,
	 * then succeed and set start and finish to the value(s) returned by fn
	 * else fail
	 */

	int s,f;
	char b[200];
	char *dash;
	if ((s=fn(str)) >= 0)
	{
		*start = *finish = s;
		return 1;
	}
	strcpy(b, str);
	dash = strchr(b, '-');
	if (dash == NULL)
		return 0;
	*dash = '\0';
	s = fn(b);
	f = fn(dash+1);
	if (s>=0 && f>=0 && s<=f)
	{
		*start = s;
		*finish = f;
		return 1;
	}
	return 0;
}

void do_changes(int is_status, char *newstatus)
{
	/* read changes from stdin and change their status to newstatus */
	int status;
	int ontime;
	char buf[1024];

	if (is_status)
	{
		status = char2bstatus(newstatus);
		if (status<=0)
		{
			fprintf(stderr,"booking: %s is not a valid status\n", newstatus);
			exit(2);
		}
	}
	else
	{
		if (sscanf(newstatus, "%d", &ontime)!= 1)
		{
			fprintf(stderr,"booking: %s in not a value number\n", newstatus);
			exit (2);
		}
		if (ontime < -10*60 || ontime > 20*60)
		{
			fprintf(stderr, "booking: time %d out side range of -600 to 1200\n", ontime);
			exit(2);
		}
	}
	while (fgets(buf, sizeof(buf), stdin)!= NULL)
	{
		/* want doy pod lab user myctime */
		char doys[10], pods[10], labs[20], user[20], myct[30];
		if (sscanf(buf, "%s %s %s %s %s", doys, pods, labs, user, myct)!= 5)
		{
			fprintf(stderr, "booking: bad line %s", buf);
		}
		else
		{
			int doy, pod, lab, uid;
			time_t when;
			if ((doy=dayofyear(doys))<0)
				fprintf(stderr, "booking: bad day of year: %s\n", doys);
			else if ((pod=podofday(pods))<0)
				fprintf(stderr, "booking: bad period of day: %s\n", pods);
			else if ((lab = get_mappingint(labs, M_ATTRIBUTE))<0)
				fprintf(stderr, "booking: bad lab name: %s\n", labs);
			else if ((uid= find_userid(user))<0)
				fprintf(stderr,"booking: bad user name: %s\n", user);
			else if ((when = unmyctime(myct)) <= 0)
				fprintf(stderr,"booking: bad time: %s\n", myct);
			else
			{
				if (is_status)
				{
					book_change arg;
					book_ch_ent buents[2];

					arg.chng = CHANGE_BOOKING;
					arg.book_change_u.changes.slot = make_bookkey(doy, pod, lab);
					arg.book_change_u.changes.chlist.book_ch_list_len = 1;
					arg.book_change_u.changes.chlist.book_ch_list_val = buents;
					buents[0].user = uid;
					buents[0].when = when;
					buents[0].status = status;
					buents[0].priv_refund = 0;
					buents[0].tentative_alloc = 0;
					buents[0].allocations.entitylist_len = 0;

					if (send_change(&arg)==0)
						/*return TRUE */ ; 
					else
					{
						/* return FALSE;*/
						fprintf(stderr,"booking: change failed\n");
					}

				}
				else
				{
					book_change arg;
					book_set_claim *cp;
					arg.chng = CLAIM_BOOKING;
					cp = &arg.book_change_u.aclaim;
					cp->slot = make_bookkey(doy, pod, lab);
					cp->user = uid;
					cp->when = when;
					cp->where = -1;
					cp->claimtime = DID_LOGIN | ((ontime+30*60)<<4);
					if (send_change(&arg)==0)
						/*return TRUE */ ; 
					else
					{
						/* return FALSE;*/
						fprintf(stderr,"booking: change failed\n");
					}

				}
			}
		}
	}
}

int main(int argc, char *argv[])
{
	int podset=0, doyset=0;
	int pods, pode, doys, doye;
	int pod, doy;
	void *lablist = lablist_make();
	labinfo li;
	int arg=1;
	int doremove = 0;

	if (argc >= 2 && strcmp(argv[arg], "-r")==0)
	{
		open_db_client(argv[arg+1], "tcp");
		arg+= 2;
	}
	if (argc >= 2 && strcmp(argv[arg], "-R")==0)
	{
		doremove = 1;
		arg++;
	}
	if (argc >= 2 && strcmp(argv[arg], "-s")==0)
	{
		if (argc != 3)
		{
			fprintf(stderr,"booking: must give exactly one status with -s\n");
			exit(2);
		}
		do_changes(1, argv[2]);
		exit(0);
	}
	if (argc >= 2 && strcmp(argv[1], "-t")==0)
	{
		if (argc != 3)
		{
			fprintf(stderr,"booking: must give exactly one time with -t\n");
			exit(2);
		}
		do_changes(0, argv[2]);
		exit(0);
	}
	for (; arg < argc ; arg++)
	{
		char *msg;
		if (get_range(argv[arg], &pods, &pode, podofday))
		{
			if (podset)
			{
				fprintf(stderr,"booking: only give one period-of-day string\n");
				usage();
				exit(2);
			}
			podset = 1;
			/* make 06:00-07:00 be just two periods, not three */
			if (pode > pods) pode--;

		}
		else if (get_range(argv[arg], &doys, &doye, dayofyear))
		{
			if (doyset)
			{
				fprintf(stderr,"booking: only give one day-of-year string\n");
				usage();
				exit(2);
			}
			doyset = 1;
		}
		else if ((msg = add_lab(lablist, (strcmp(argv[arg],"ALL")==0)?NULL:argv[arg]))!=NULL)
		{
			if (strncmp(msg, "unknown", 7) == 0)
				fprintf(stderr, "booking: cannot understand string: %s\n", argv[arg]);
			else
				fprintf(stderr, "booking: %s: %s\n", argv[arg], msg);
			exit(2);
		}
	}
	if (lablist_next(lablist) == lablist)
	{
		fprintf(stderr, "booking: no labs specified\n");
		usage();
		exit(2);
	}
	if (doyset == 0)
	{
		fprintf(stderr, "booking: no day range specified\n");
		usage();
		exit(2);
	}
	if (podset == 0)
	{
		pods = 0;
		pode = 47;
	}
	for (doy = doys ; doy <= doye ; doy++)
		for (pod = pods ; pod <= pode ; pod++)
			for (li = lablist_next(lablist) ; li != lablist ; li = lablist_next(li))
				show_bookings(doy, pod, li->labid, doremove);
	exit(0);
}
