
/*
 * quick and dirty config seter
 * conf name    prints value
 * conf name value sets value
 *
 */

#include	"../db_client/db_client.h"

int main(int argc, char *argv[])
{

	char *val;
	switch(argc)
	{
	default:
		fprintf(stderr, "Usage: conf name [value]\n");
		exit(2);

	case 1:
		/* print all config entries */
	{
		query_req request;
		char prefix[10];

		sprintf(prefix, "%d_", C_GENERAL);
		request.key.item_val=memdup(prefix,strlen(prefix));
		request.key.item_len=strlen(prefix);
		request.type=M_NEXT_PREFIX;
		request.database = CONFIG;
		refresh_db_client();
		while(1)
		{
			query_reply *result;
			result = query_db_2(&request, db_client);
			if (result == NULL)
				exit(1);

			free(request.key.item_val);
			if (result->error != R_RESOK ||
			    result-> query_reply_u.data.key.item_len == 0 ||
			    result->query_reply_u.data.key.item_val== NULL)
				exit (1);
	
			request.key.item_len = result-> query_reply_u.data.key.item_len;
			request.key.item_val =
				memdup(result->query_reply_u.data.key.item_val,
				       request.key.item_len+1);
			request.key.item_val[request.key.item_len] = 0;
			xdr_free((xdrproc_t)xdr_query_reply, (char*) result);
	
			if (strncmp(request.key.item_val, prefix, strlen(prefix))==0)
			{
				val = get_configchar(request.key.item_val+strlen(prefix));
				if (val)
				{
					printf ("%s = %s\n", request.key.item_val+strlen(prefix), val);
					free(val);
				}
			}
		}
	}
	exit(0);
	case 2: /* print value */
		val = get_configchar(argv[1]);
		if (val)
			printf("%s\n", val);
		else
			printf("--Not-Defined--\n");
		exit(0);
	case 3: /* set value */
		make_config(argv[1], argv[2]);
		exit(0);
	}
}
