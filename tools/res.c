
/*
 * res host workstation user
 *
 * tell the host to reserve the workstation for the user
 *
 */
#include	"../db_client/db_client.h"

void main(int argc, char *argv[])
{
    int uid, wsid;
    static ws_user_pair arg;
    bool_t *result;
    
    if (argc != 4)
    {
	fprintf(stderr,"Usage: res host workstation user\n");
	exit(2);
    }

    status_client = clnt_create(argv[1], BOOK_STATUS, BOOKVERS, "udp");
    if (status_client == NULL)
    {
	fprintf(stderr,"res: Cannot contact %s\n", argv[1]);
	exit(2);
    }
    wsid = get_mappingint(argv[2], M_WORKSTATION);
    if (wsid < 0)
    {
	fprintf(stderr,"res: cannot find workstation %s\n", argv[2]);
	exit(2);
    }
    uid = find_userid(argv[3]);
    if (uid < 0)
    {
	fprintf(stderr, "res: cannot find user %s\n", argv[3]);
	exit(2);
    }
    arg.userid = uid;
    arg.hostid = 0;
    arg.wsid = wsid;
    result = res_user_2(&arg, status_client);
    if (result == NULL)
    {
	fprintf(stderr,"res: failed, sorry\n");
	exit(1);
    }
    exit(0);
}
