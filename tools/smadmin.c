
/*
 * smadmin - status/manager admin
 * Usage: smadmin -[tdDr] [-a dbserver] host
 *   -t  get table
 *   -d  get database list
 *   -D  update database list
 *   -r  restart manager
 *   -x  tell manager to exit
 *   -a server   add server to list of up databases
 *
 */


#include	"../db_client/db_client.h"
#include	<getopt.h>

char *Usage[] = {
    "Usage: smadmin -[tdDrx] [-a server] host",
    "  -t  : get status table",
    "  -d  : get database replica list",
    "  -D  : update database replica list",
    "  -r  : restart manager",
    "  -x  : exit",
    "  -a server : record server as a database server",
    NULL
};

char *statuses[]=
{ "Close", "Up", "Unknown", "ReadOnly", "Down", "Unregistered"};

int main(int argc, char *argv[])
{

	extern CLIENT *status_client;

	extern int optind;
	extern char *optarg;
    
	char *host;
	int get_tab=0;
	int get_db=0;
	int update_db=0;
	int restart=0;
	int doexit = 0;
	char *server = NULL;

	int arg;

	while ((arg=getopt(argc, argv, "tdDrxa:"))!= EOF)
		switch(arg)
		{
		case 't': get_tab=1; break;
		case 'd': get_db=1;  break;
		case 'D': update_db=1; break;
		case 'r': restart=1; break;
		case 'x': doexit = 1; break;
		case 'a': server = optarg; break;
		default:
			fprintf(stderr,"smadmin: Unknown option.");
			usage();exit(1);
	
		}

	if (get_tab+get_db+update_db+restart+doexit+(server!=NULL) != 1)
	{
		fprintf(stderr, "smadmin: must give examply one flag.\n");
		usage();
		exit(1);
	}
	if (optind >= argc)
	{
		fprintf(stderr, "smadmin: not host name given\n");
		usage();
		exit(1);
	}
	if (optind +1 < argc)
	{
		fprintf(stderr, "smadmin: only one host name may be given\n");
		usage();
		exit(1);
	}
	host = argv[optind];
	if (strcmp(host, ".")==0)
		host = "localhost";

	status_client = clnt_create(host, BOOK_STATUS, BOOKVERS, "udp");
	if (status_client == NULL)
	{
		fprintf(stderr, "smadmin: cannot connect to %s\n", host);
		exit(1);
	}

	if (get_tab)
	{
		table tab;
		if (collect_tab(&tab, status_client)== -1)
		{
			fprintf(stderr, "smadmin: cannot collect table\n");
			exit(1);
		}
		else
			print_tab(&tab, stdout);
	}
	if (get_db)
	{
		database_list *dblist;
		int i;
		dblist = get_databases_2(NULL, status_client);
		if (dblist == NULL)
		{
			fprintf(stderr, "smadmin: cannot get database list\n");
			exit(1);
		}
	
		for (i=0 ; i<dblist->database_list_len ; i++)
		{
			printf("%-20s %s\n", dblist->database_list_val[i].name,
			       statuses[dblist->database_list_val[i].state]);
		}
	}
	if (update_db)
	{
		check_servers();
	}
	if (restart)
	{
		int *pid;
		int dummy=0;
		pid = restart_helper_2(&dummy, status_client);
		if (pid && *pid > 1)
			printf("manager restarted with pid %d\n", *pid);
		else
		{
			fprintf(stderr,"smadmin: manager restart failed\n");
			exit(1);
		}
	}
	if (doexit)
	{
		if (status_die_2(NULL, status_client) == NULL)
		{
			fprintf(stderr, "smadmin: could not send kill command to book_status on %s.\n", host );
			exit(1);
		}
	}
	if (server)
	{
		database_info dbi;
		dbi.name = server;
		dbi.state = DB_UP;
		if (set_database_2(&dbi, status_client) == NULL)
		{
			fprintf(stderr, "smadmin: could not register %s as a server.\n", server);
			exit(1);
		}
	}
	exit(0);
}
