#include	"../db_client/db_client.h"
#include	"../lib/skip.h"
#include	"../lib/dlink.h"

char Digits[] = "0123456789";
/*
 * set up the attribute info
 *
 * we read a file listing attributes, implications
 * and general config entries
 * building a list
 * this is compared with the database and changes made
 * if necessary
 *
 * the file contains:
 *
 * attributes defines:   nnn:name type
 * implications:    day-day time-time dow-dow attr !attr -> new
 * config:          name=value
 *
 * config entries are checked and set whenever they are seen
 * attributes and implications are stored in tables
 * and then compared against the database
 *
 */

typedef struct attr {
    entityid num;
    char *name;
} *attr;

static int attr_cmp_num(attr a, attr b, int *np)
{
	int n;
	if (b != NULL)
		n = b->num;
	else n = *np;
	return a->num - n;
}

static int attr_cmp_name(attr a, attr b, char *n)
{
	if (b!=NULL)
		n = b->name;
	return strcmp(a->name, n);
}

static void attr_free(attr a)
{
	free(a->name);
	free(a);
}

void *nums, *names;
void attr_store(int num, char *name)
{
	attr a = (attr)malloc(sizeof(struct attr));
	a->num = num;
	a->name = name;
	skip_insert(nums, a);
	skip_insert(names, a);
}

void attr_init()
{
	nums = skip_new(attr_cmp_num, attr_free, NULL);
	names = skip_new(attr_cmp_name, NULL, NULL);
}

int attr_find(char *name)
{
	attr *ap = skip_search(names, name);
	if (ap)
		return (*ap)->num;
	else return -1;
}


void attr_update(int check)
{
	/* first make sure each name we know about is set
	 * then check all other numbers an clear them
	 */
	attr *ap;

	for (ap = skip_first(nums); ap ; ap=skip_next(ap))
	{
		attr a = *ap;
		char *nam;
		nam = get_mappingchar(a->num, M_ATTRIBUTE);
		if (nam == NULL || strcmp(nam, a->name)!= 0)
		{
			/* need to change name
			 * first make sure name not in use
			 */
			entityid other;
			other = get_mappingint(a->name, M_ATTRIBUTE);
			if (other>=0)
			{
				if (other == a->num)
					printf("WARNING: mapping confusion for %d %s\n",
					       a->num, a->name);
				else
				{
					make_mapping(other, "", M_ATTRIBUTE);
				}
			}
			if (make_mapping(a->num, a->name, M_ATTRIBUTE) != 0)
				fprintf(stderr,"attr: failed to make mapping %d -> %s\n",
					a->num, a->name);
		}
		if (nam) free(nam);
	}
	/* don't worry about others yet FIXME */
}

char *attr_add(char *line, attr_type_desc *at_type)
{
	/* it has been deduced that this line is an
	 * attribute line, probably because it has a colon.
	 * parse out the number, name and type(s) as
	 *  num:name type [type]
	 *
	 * if this does not conflict with list so far,
	 * store it and set appropriate bits in
	 * at_type
	 *
	 * types are: lab optional open closed available bookable reusable firmalloc
	 *  open et.al can only appear on one attribute.
	 */

	int digits;
	char *name;
	int num;
	char *cp;
	attr *a;

	digits = strspn(line, Digits);
	if (digits<0)
		return "no attribute number";
	num = atoi(line);
	if (num < 0 || num > BITINDMAX)
		return "bad attribute number";
	if (line[digits] != ':')
		return "colon not found after number";

	cp = line + digits+1;
	while (*cp == ' ' || *cp == '\t' ) cp++;
	if (!*cp)
		return "attribute name missing";
	name = cp;
	while (*cp && *cp != ' ' && *cp != '\t')
		cp++;
	name = strndup(name, cp-name);
	while (*cp == ' ' || *cp == '\t') cp++;

	set_a_bit(&at_type->all, num);
	set_a_bit(&at_type->mandatory, num);
	while (*cp) /* looking at a type */
	{
		char *type = cp;
		int *ip = NULL;
		while (*cp && *cp != ' ' && *cp != '\t')
			cp++;
		type = strndup(type, cp-type);
		while (*cp == ' ' || *cp == '\t') cp++;

		if (strcmp(type, "lab")==0)
			set_a_bit(&at_type->lab, num);
		else if (strcmp(type, "optional")==0)
			del_a_bit(&at_type->mandatory, num);
		else if (strcmp(type, "open")==0)
			ip = &at_type->open;
		else if (strcmp(type, "closed")==0)
			ip = &at_type->closed;
		else if (strcmp(type, "available")==0)
			ip = &at_type->available;
		else if (strcmp(type, "bookable")==0)
			ip = &at_type->bookable;
		else if (strcmp(type, "reusable")==0)
			ip = &at_type->reusable;
		else if (strcmp(type, "firmalloc") == 0)
			ip = &at_type->firmalloc;
		else
			return "unknown attribute type";

		free(type);
		if (ip)
		{
			if (*ip >= 0)
				return "multiple defination of singular type";
			else
				*ip = num;
		}
	}

	if ((a=skip_search(nums, &num)))
		return "repeat definition of attribute number";
	if ((a=skip_search(names, name)))
		return "repeat definition of attribute name";
    
	attr_store(num, name);
	return NULL;
    
}

/*
 * implicatations are stored in a dlist of implication
 * structures
 */

void *implist;
int impcnt = 0;

char *msg(char *b, char *m, char *a1, char *a2)
{
	sprintf(b,m,a1,a2);
	return b;
}

char *get_word(char **line)
{
	char *cp  = *line;
	char *rv;
	while (*cp == ' ' || *cp == '\t') cp++;
	rv = cp;
	while (*cp && *cp != ' ' && *cp != '\t')
		cp++;
	if (*cp) *cp++ = '\0';
	if (!*rv) rv = NULL;
	*line = cp;
	return rv;
}

char *impl_add(char *line)
{
	/* this appears to be an implication,
	 * attempt to extract relevant fields
	 * and add implication to implist
	 *
	 * fields are:
	 *   01jan[-31dec]  startday  endday
	 *   su[-sa]	startdow enddow
	 *   0000-2330	starttime endtime
	 *   xxxxx		included
	 *	!xxxxx		excluded
	 *   -> yyyyy	attribute
	 */
	static char buf[200];
	char *w;
	implication imp, *ip;
	imp.startday = 0;
	imp.endday = 366;
	imp.startdow = 0;
	imp.enddow = 6;
	imp.starttime = 0;
	imp.endtime = 24*60;
	imp.included = new_desc();
	imp.excluded = new_desc();
	imp.attribute = 0;

	while ((w=get_word(&line)) && strcmp(w, "->")!= 0)
	{
		int at;

		if ((at=attr_find(w)) >= 0)
			set_a_bit(&imp.included, at);
		else if (*w == '!' && (at=attr_find(w+1))>=0)
			set_a_bit(&imp.excluded, at);
		else
		{
			char *type;
			char *w2;
			int v1, v2;
			w2 = strchr(w, '-');
			if (w2) *w2++ = '\0';
			else w2 = w;

			if ((v1=dayofweek(w))>=0)
			{
				type="Day of Week";
				if ((v2=dayofweek(w2))>=0)
					imp.startdow = v1,
						imp.enddow = v2;
			}
			else if ((v1=dayofyear(w))>=0)
			{
				type = "Day of Year";
				if ((v2=dayofyear(w2))>= 0)
					imp.startday = v1,
						imp.endday = v2;
			}
			else if ((v1=podofday(w))>= 0)
			{
				type = "Time of day";
				if (v1 >= 48)
					return msg(buf, "Invalid time: %s", w, NULL);
				if ((v2 = podofday(w2))>= 0)
				{
					if (v2 >= 49)
						return msg(buf,"Invalid time: %s", w, NULL);
					imp.starttime = v1;
					if (w2==w) imp.endtime = v1+1;
					else imp.endtime = v2-1;
				}
			}
			else
				return msg(buf, "invalid implicand: %s", w, NULL);
			if (v2 < 0)
				return msg(buf, "%s is not a valid %s", w2, type);
			if (v2 < v1)
				return msg(buf, "%s preceeds %s", w2, w);
		}
	}
	if (w == NULL)
		return "missing ->";
	w = get_word(&line);
	if (w == NULL)
		return "missing implicate";
	imp.attribute = attr_find(w);
	if (imp.attribute < 0)
		return msg(buf, "unknown attribute: %s", w, NULL);

	ip = dl_new(implication);
	*ip = imp;
	if (impcnt == 0)
		implist = dl_head();
	dl_add(implist, ip);
	impcnt++;
	return NULL;
}

int impl_store()
{
	/* store implications to the database
	 * about 100 bytes each, so send 8 at a time
	 */

	int i;
	implication *next;

	if (implist)
	{
		next = dl_next(implist);

		for (i=0 ; i<impcnt ; i+= 8)
		{
			int i2;
			impls *ip;
			implication ilist[8];
			book_change ch;
			ch.chng = IMPLICATIONS;
			ip = &ch.book_change_u.implch;
			ip->first = i;
			ip->total = impcnt;
			ip->exlist.exlist_val = ilist;
			for (i2=0 ; i2<8 && i+i2 < impcnt ; i2++)
			{
				ilist[i2] = *next;
				next = dl_next(next);
			}
			ip->exlist.exlist_len = i2;
			if (send_change(&ch) != 0)
				return -1;
		}
	}
	return 0;
}

int impl_cmp()
{
	/* complare implications in implist with
	 * those in impl_list
	 * return 1 if different
	 */
	int i;
	implication *next = dl_next(implist);

	get_impl_list();
    
	if (impcnt != impl_list.total)
		return 1;

	for (i=0 ; i<impcnt ; i++, next = dl_next(next))
	{
		implication *ip = &impl_list.exlist.exlist_val[i];

		if (ip->startday != next->startday
		    || ip->endday != next->endday
		    || ip->starttime != next->starttime
		    || ip->endtime != next->endtime
		    || ip->startdow != next->startdow
		    || ip->enddow != next->enddow
		    || ip->attribute != next->attribute
		    || !desc_eql(ip->included, next->included)
		    || !desc_eql(ip->excluded, next->excluded)
			)
			return 1;
	}
	return 0;
}

void impl_update()
{
	if (implist && impl_cmp())
		impl_store();
}

attr_type_desc new_attrs;

int attr_type_update()
{
	get_attr_types();

	if (get_attr_types() == 0
	    || !desc_eql(new_attrs.mandatory, attr_types.mandatory)
	    || !desc_eql(new_attrs.lab, attr_types.lab)
	    || !desc_eql(new_attrs.all, attr_types.all)
	    || new_attrs.open != attr_types.open
	    || new_attrs.closed != attr_types.closed
	    || new_attrs.available != attr_types.available
	    || new_attrs.bookable != attr_types.bookable
	    || new_attrs.reusable != attr_types.reusable
	    || new_attrs.firmalloc != attr_types.firmalloc
		)
	{
		/* there is a difference, send the change */
		book_change ch;
		ch.chng = ATTR_TYPE;
		ch.book_change_u.attrs = new_attrs;
		if (send_change(&ch) != 0)
			return -1;
	}
	return 0;
}


void attr_print()
{
	/* list attributes definitions just as they would be read in */
	int i;
	if (get_attr_types()==0)
		printf("Cannot get attribute type information\n");
	else
		for (i=0 ; i <BITINDMAX ; i++)
			if (query_bit(&attr_types.all, i))
			{
				char *name = get_mappingchar(i, M_ATTRIBUTE);
				if (name)
				{
					printf("%d:%s", i, name);
					free(name);
				}
				else
					printf("%d:#%d", i, i);
				if (query_bit(&attr_types.mandatory, i)==0)
					printf(" optional");
				if (query_bit(&attr_types.lab, i)==1)
					printf(" lab");
				if (i == attr_types.open) printf(" open");
				if (i == attr_types.closed) printf(" closed");
				if (i == attr_types.available) printf(" available");
				if (i == attr_types.bookable) printf(" bookable");
				if (i == attr_types.reusable) printf(" reusable");
				if (i == attr_types.firmalloc) printf(" firmalloc");
				printf("\n");
			}
}
	    

void impl_print()
{
	/* list the implications in form suitable for input
	 */
	int i;
	if (get_impl_list() == 0)
		printf("Cannot get implication list\n");
	else
		for (i=0 ; i<impl_list.total ; i++)
		{
			implication *ip = &impl_list.exlist.exlist_val[i];
			char str[30];
			char *name;
			int j;

			if (ip->startday > 0 || ip->endday < 366)
			{
				printf("%s", doy2str(str, ip->startday));
				if (ip->startday != ip->endday)
					printf("-%s", doy2str(str, ip->endday));
				printf(" ");
			}
			if (ip->startdow > 0 || ip->enddow < 6)
			{
				extern char *days[];
				printf("%s", days[ip->startdow]);
				if (ip->startdow != ip->enddow)
					printf("-%s", days[ip->enddow]);
				printf(" ");
			}
			if (ip->starttime >0 || ip->endtime < 24*60)
			{
				printf("%s", pod2str(str, ip->starttime));
				if (ip->starttime != ip->endtime)
					printf("-%s", pod2str(str, ip->endtime+1));
				printf(" ");
			}
			for (j=0 ; j < BITINDMAX ; j++)
			{
				if (query_bit(&ip->included, j))
				{
					char *name = get_mappingchar(j, M_ATTRIBUTE);
					if (name)
						printf("%s ", name);
					else
						printf("#%d ", j);
					if (name)
						free(name);
				}
				if (query_bit(&ip->excluded, j))
				{
					char *name = get_mappingchar(j, M_ATTRIBUTE);
					if (name)
						printf("!%s ", name);
					else
						printf("!#%d ", j);
					if (name)
						free(name);
				}
			}
			name = get_mappingchar(ip->attribute, M_ATTRIBUTE);
			if (name)
				printf("-> %s\n", name);
			else
				printf("-> #%d\n", ip->attribute);
			if (name) free(name);
		}
}

static void list_desc(char *prefix, description d)
{
	int first = 1;
	int i;
	for (i=0 ; i<d.item_len*8 ; i++)
	{
		if ((i%32)==0 && !first)
		{
			printf("\n");
			first = 1;
		}
		if (query_bit(&d, i))
		{
			char *n;
			if (first)
				printf("%s", prefix);
			first = 0;
			n = get_mappingchar(i, M_ATTRIBUTE);
			if (n== NULL) n = strdup("*unknown*attr*");
			printf("%s ", n);
			free(n);
		}
	}
	if (!first) printf("\n");
}

int main(int argc, char *argv[])
{
	/* If no args, display attr file
	 * if -t then rest of args are time/date/attr - interpret
	 * else one arg- file to read and set
	 */

	if (argc == 1) {
		attr_print();
		impl_print();
	} else if (argc >= 2 && strcmp(argv[1], "-t")==0 ) {
		int a;
		int doy=-1, pod=-1;
		description bits = new_desc();
		for (a=2; a < argc ; a++) {
			int v;
			if ((v=get_mappingint(argv[a], M_ATTRIBUTE)) >= 0)
				set_a_bit(&bits, v);
			else if ((v=dayofyear(argv[a]))>=0)
				doy = v;
			else if ((v=podofday(argv[a]))>=0)
				pod = v;
			else if (strcmp(argv[a], "--")==0)
				break;
			else
				fprintf(stderr, "attr: cannot understand '%s' - ignoring\n", argv[a]);
		}
		if (doy == -1)
			fprintf(stderr, "attr: no day of year given\n");
		else if (pod == -1 && a == argc)
			fprintf(stderr, "attr: no period of day given\n");
		else {
			description *result;
			if (a == argc)
			{
				result = process_exclusions(pod, doy, &bits);
				list_desc("  ", *result);
			}
			else {
				int a1;
				int lo, hi;
				a++;

				if (pod==-1) lo=0, hi=47;
				else lo=hi=pod;
				for (; lo <= hi ; lo++) {
					char *prefix="";
					description b2;
					desc_cpy(&b2, &bits);
					result = process_exclusions(lo, doy, &b2);
					if (pod == -1) printf("%02d:%02d: ", lo/2, (lo&1)*30);
					for (a1=a ; a1 < argc ; a1++) {
						int v;
						v = get_mappingint(argv[a1], M_ATTRIBUTE);
						if (v <0 )
							fprintf(stderr, "attr: unknown attribute: '%s'\n", argv[a1]);
						else {
							if (query_bit(result, v)) {
								printf("%s%s", prefix, argv[a1]);
								prefix=", ";
							}
						}
					}
					if (*prefix || pod==-1) printf("\n");
				}
			}
		}
	} else if (argc == 2) {
		FILE *f;
		int ln = 0;
		int errs = 0;
		char *err;
		char line[1024];
		if (strcmp(argv[1], "-")==0)
			f = stdin;
		else
			f = fopen(argv[1], "r");
		if (f == NULL)
		{
			fprintf(stderr, "attr: cannot open ");
			perror(argv[1]);
			fprintf(stderr, "Usage: attr [file]\n");
			exit(1);
		}
		attr_init();
		new_attrs.mandatory = new_desc();
		new_attrs.lab = new_desc();
		new_attrs.all = new_desc();
		new_attrs.open = new_attrs.closed = new_attrs.available
			= new_attrs.bookable = new_attrs.reusable = new_attrs.firmalloc = -1;
		while (fgets(line, sizeof(line), f))
		{
			char *cp;
			ln++;
			cp = strchr(line, '\n');
			if (cp) *cp = '\0';
			cp = line+strlen(line);
			while (cp > line && (cp[-1]== ' ' || cp[-1] == '\t'))
				*--cp = '\0';
			cp = line;
			while (*cp==' ' || *cp == '\t')
				cp++;
			if (*cp == '#' || *cp == '\0')
				continue;
			if (strchr(cp, '>')==NULL)
				err = attr_add(cp, &new_attrs);
			else
				err = impl_add(cp);

			if (err)
			{
				fprintf(stderr,"attr: %s at line %d\n", err, ln);
				errs++;
			}
		}
		if (errs == 0)
		{
			attr_update(1);
			attr_type_update();
			impl_update();
		}
	} else if (argc != 2) {
		fprintf(stderr, "Usage: attr [file]\n");
		fprintf(stderr, "       attr -t time date lab ( -- attributes to check)\n");
		exit(2);
	}
	exit(0);
}
