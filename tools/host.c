
#include	"../db_client/db_client.h"
#include	<time.h>
#include	<getopt.h>
/*
 * host/hg/lab/ws - manage the booking system hardware
 *
 * argv0 determines what sort of object is being managed
 *
 * -R old new		rename
 * -D old		delete
 * [-c] -g hostgroup -l lab -d [+-]desc -r reason  obj ...
 *
 */

char *command;
extern char *optarg;
extern int optind;
int	nargs=0, create=0, all=0, force=0;
entityid objid;
char *nm = NULL;

void check_lab(item desc)
{
	/* check that no lab bits are set in desc */
	if (get_attr_types()!= 1)
	{
		fprintf(stderr,"Cannot get attribute descriptor!!\n");
		exit(1);
	}
	desc_sub(desc, attr_types.lab);
}

enum progtype { Phost, Phg, Plab, Pws, Pbad } ;

enum progtype whichprog(char * arg0)
{
	int len;
	len = strlen(arg0);
	if (strcmp(arg0+len-4, "host")==0)
		return Phost;
	else if (strcmp(arg0+len-2, "hg")==0)
		return Phg;
	else if (strcmp(arg0+len-3, "lab")==0 || strcmp(arg0+len-2, "lb")==0)
		return Plab;
	else if (strcmp(arg0+len-2, "ws")==0)
		return Pws;
	else return Pbad;
}

char *Usage[] = {
    "Usage:",
    "  prog -R old new",
    "  prog -D [-f] old",
    "  prog [-acn] [-g hostgroup] [-l lab] [-h host]",
    "       [-d [+-]desc] [-r reason] obj...",
    "Function:",
    "  Print, change or delete booking object(s).",
    "  The type of object to be manipulated is determined by the program name:",
    "  Name		Object Manipulated",
    "  bkhg		hg (hostgroup)",
    "  bklb		lb (lab)",
    "  bkhost	host",
    "  bkws		ws (workstation)",
    "Option	Description			Valid Object",
    "  -n	print object's numerical ids	hg, lb, host, ws",
    "  -c	create the object if necessary	hg, lb, host, ws",
    "  -a	iterate over all objects	hg, lb, host, ws",
    "  -R	rename object			hg, lb, host, ws",
    "  -D	delete object			hg, lb, host, ws",
    "  -f	force - delete subordinate	hg, lb, host, ws",
    "			objects if necessary",
    "  -g	move object into hostgroup	lb, host",
    "  -l	move ws into lab		ws",
    "  -h	assign ws to host		ws",
    "	  (host may be \"none\")",
    "  -d	assign, add or delete a		ws",
    "	  description to a ws",
    "  -r	put ws in/out of service	ws",
    "	  empty reason: into service",
    "	  non-empty reason: out of service",
    "",
    "obj...	name(s) of the object(s) to print, change or delete.",
    NULL };

void die1(int ret, char *emsg1)
{
	fprintf(stderr, "%s: %s\n", command, emsg1);
	usage();
	exit(ret);
}

void die2(int ret, char *emsg1, char *emsg2)
{
	fprintf(stderr, "%s: %s: %s\n", command, emsg1, emsg2);
	exit(ret);
}

void print_wsdata(workstation_state *ws, char *name)
{
	int first, i;
	if (ws->host < 0)
		printf("Workstation: %s\n", name);
	else
	{
		char *hn = get_mappingchar(ws->host, M_HOST);
		if (hn)
			printf("Workstation: %-15s On Host: %s\n", name, hn);
		else
			printf("Workstation: %-15s On Host: #%d\n", name, ws->host);
		if (hn) free(hn);
	}
	if (ws->why && ws->why[0]) {
		time_t tim = ws->time;
		printf("   %s since %s", ws->why, ctime(&tim));
	}
	first = 1;
	printf("   ");
	for (i=0; i<8*ws->state.item_len ; i++)
		if (query_bit(&ws->state, i))
		{
			char *nm = get_mappingchar(i, M_ATTRIBUTE);
			if (!first)
				printf(".");
			if (nm)
			{
				printf("%s", nm);
				free(nm);
			}
			else
				printf("#%d", i);
			first = 0;
		}
	printf("\n");
}

/* for use when stepping through the databases in getnextobj() */
query_req request;
char prefix[10];

int getnextobj(nmapping_type map, char *argv[])
{
	/*
	 * get the next object of type map.
	 * either:
	 * get all such objects in the database, or
	 * get only those passed by argument to this program.
	 */
	if (all)
	{
		query_reply *result;
	
		/* step through the database and return the next object
		 * of type map
		 * much of this code was adapted from conf.c
		 */
		objid = -1;
		if (all++ == 1)
		{
			/* set up first request */
			request.key.item_val = memdup("dummy", 4);
			request.key.item_len = 4;
			request.type = M_FIRST;
			request.database = CONFIG;
			sprintf(prefix, "%d_", C_NUMBERS+map);
		}
		/* get the next record matching the type of object we want */
		if (db_client == NULL && refresh_db_client() == 0)
			fputs("Cannot connect to db client\n", stderr);
		else
			do
			{
				do
				{
					result = query_db_2(&request, db_client);
					if (result == NULL)
						return 0;
					request.type = M_NEXT;
					free(request.key.item_val);
					if (result->error != R_RESOK ||
					    result->query_reply_u.data.key.item_len == 0 ||
					    result->query_reply_u.data.key.item_val == NULL)
						return 0;

					/* copy the results of the reply into the (next) request */
					request.key.item_len = result->query_reply_u.data.key.item_len;
					request.key.item_val =
						memdup(result->query_reply_u.data.key.item_val,
						       request.key.item_len+1);
					request.key.item_val[request.key.item_len] = 0;

					/* free up the current reply */
					xdr_free((xdrproc_t)xdr_query_reply, (char*) result);
				} while (strncmp(request.key.item_val, prefix, strlen(prefix)));

				objid = get_mappingint(request.key.item_val+strlen(prefix), map);
				/* lab objects are stored/indexed as general attributes.
				 * don't return general attributes if we actually want labs
				 */
			} while (map == M_ATTRIBUTE && ! query_bit(&attr_types.lab, objid));
		if (objid >= 0)
		{
			nm = get_mappingchar(objid, map);
			if (nm == NULL)
			{
				nm = strdup(request.key.item_val+strlen(prefix));
				fprintf(stderr, "%s: DB error - missing map from %d => %s\n", command, objid, nm);
			}
		}
	}
	else
	{
		/* step through the command line arguments, and return the
		 * next object of type map corresponding to the arg.
		 * Create the object if required and allowed.
		 */
		for (objid = -1; (objid < 0 && optind < nargs); optind++)
		{
			objid = get_mappingint(argv[optind], map);
			if (objid < 0)
			{
				if (objid == -1 && create)
				{
					/* create the object */
					objid = choose_id(1, map);
					if (objid < 0 || make_mapping(objid, argv[optind], map) != 0)
					{
						fprintf(stderr, "%s: DB error - cannot create mapping (%s, %d)\n", command, argv[optind], objid);
						objid = -1;
					}
				}
				else fprintf(stderr, "%s: '%s' does not exist\n", command, argv[optind]);
			}
			if (objid >= 0)
			{
				nm = get_mappingchar(objid, map);
				if (nm == NULL) nm = strdup(argv[optind]);
			}
		}
	}
	return(objid >= 0);
}

int remove_namenum(nmapping_type map, char *objtype, entityid objid, char *nm)
{
	/* remove the name/num mappings (nm <=> objid)
	 * iff the associated data has been removed successfully
	 * and we are not removing labs (which are defined as attributes
	 * and which need to be removed from the attr file).
	 */
	book_change ch;
	int res;

	if (map != M_ATTRIBUTE)
	{
		ch.chng = SET_NAMENUM;
		ch.book_change_u.map.type = map;
		ch.book_change_u.map.key = objid;
		ch.book_change_u.map.value = "";	/* triggers removal */
		res = send_change(&ch);
	} else res = 0;
	if (res == 0)
	{
		printf("Removed %s (%d,%s)\n", objtype, objid, nm);
		/* successfull removal changes current database pointer.
		 * all => we are stepping through all NAMENUM objects,
		 * so allow getnextobj to get the first object again
		 */
		if (map != M_ATTRIBUTE && all) all = 1;
	}
	else fprintf(stderr, "Failed to remove %s namenum mapping: (%d,%s) - send_change returns %d\n", objtype, objid, nm, res);
	return res;
}

int remove_ws(nmapping_type map, entityid objid, char *nm)
{
	item key;
	char k[20];
	int res;
	book_change ch;
	workstation_state wsdata;

	memset(&wsdata, 0, sizeof(wsdata));
	key = key_int(k, C_WORKSTATIONS, objid);
	switch (db_read(key, &wsdata, (xdrproc_t)xdr_workstation_state, CONFIG))
	{
	case R_RESOK:
		/* remove workstation from associated lab
		 * trigger this by removing all labs from ws's state
		 */
		desc_sub(wsdata.state, attr_types.lab);

		/* remove workstation from host
		 * trigger this by setting host to -1
		 */
		wsdata.host = -1;

		/* create and submit the change */
		ch.chng = CHANGE_WS;
		ch.book_change_u.newws.state = wsdata;
		ch.book_change_u.newws.ws = objid;
		if ((res = send_change(&ch)) != 0)
			fprintf(stderr, "Failed to remove workstation '%s' from host and/or labs - send_change returns %d\n", nm, res);
		break;

	case R_NOMATCH:
		fprintf(stderr, "Cannot find workstation object '%s' to delete\n", nm);
		res = 0;
		break;
	default:
		res = 1;
		break;
	}

	if (res == 0) res = remove_namenum(map, "workstation", objid, nm);
	return res;
}

int remove_labhost(nmapping_type map, entityid objid, char *nm)
{
	item key;
	char k[20], *s;
	hostinfo hdata;
	int i, res;
	book_change ch;

	s = ((map == M_HOST) ? "host" : "lab");
	memset(&hdata, 0, sizeof(hostinfo));
	key = key_int(k, (map == M_HOST ? C_HOSTS : C_LABS), objid);
	switch (db_read(key, &hdata, (xdrproc_t)xdr_hostinfo, CONFIG))
	{
	case R_RESOK:
		/* make sure that there are no workstations in this host/lab */
		res = 0;
		if ((i = hdata.workstations.entitylist_len) > 0)
		{
			if (force)
			{
				for (i=0; (i < hdata.workstations.entitylist_len && res == 0); i++)
				{
					nmapping_type wmap = M_WORKSTATION;
					entityid wsid = hdata.workstations.entitylist_val[i];
					char *wnm = get_mappingchar(wsid, wmap);
					res = remove_ws(wmap, wsid, wnm);
					if (wnm) free(wnm);
				}
			}
			else
			{
				fprintf(stderr, "Cannot remove %s '%s' - still has %d associated workstations\n", s, nm, i);
				res = 1;
			}
		}
		if (res == 0)
		{
			/* remove lab/host from hostgroup
			 * by setting clump to -1
			 */
			if (map == M_HOST)
			{
				ch.chng = CHANGE_HOST;
				ch.book_change_u.newhost.entity = objid;
				ch.book_change_u.newhost.clump = -1;
			}
			else
			{
				ch.chng = CHANGE_LAB;
				ch.book_change_u.newlab.entity = objid;
				ch.book_change_u.newlab.clump = -1;
			}
			if ((res = send_change(&ch)) != 0)
			{
				fprintf(stderr, "Failed to remove %s: '%s' from hostgroup - send_change returns %d\n", s, nm, res);
			}
		}
		break;
	case R_NOMATCH:
		fprintf(stderr, "Cannot find %s object '%s' to delete\n", s, nm);
		res = 0;
		break;
	default:
		res = 1;
		break;
	}

	if (res == 0) res = remove_namenum(map, s, objid, nm);
	return res;
}

int remove_hostgroup(nmapping_type map, entityid objid, char *nm)
{
	item key;
	char k[20];
	hostgroup_info hgdata;
	int i, res;
	nmapping_type map1;
	entityid objid1;
	char *nm1;

	memset(&hgdata, 0, sizeof(hostinfo));
	key = key_int(k, C_HOSTGROUPS, objid);
	switch (db_read(key, &hgdata, (xdrproc_t)xdr_hostinfo, CONFIG))
	{
	case R_RESOK:
		res = 0;
		/* check for labs or hosts in this hostgroup */
		if ((i = hgdata.hosts.entitylist_len) > 0) {
			if (force)
			{
				map1 = M_HOST;
				for (i=0; (i < hgdata.hosts.entitylist_len && res == 0); i++)
				{
					objid1 = hgdata.hosts.entitylist_val[i];
					nm1 = get_mappingchar(objid1, map1);
					res = remove_labhost(map1, objid1, nm1);
					if (nm1) free(nm1);
				}
			}
			else
			{
				fprintf(stderr, "Cannot remove hostgroup '%s' - still has %d associated hosts\n", nm, i);
				res = 1;
			}
		}
		if ((i = hgdata.labs.entitylist_len) > 0) {
			if (force)
			{
				map1 = M_ATTRIBUTE;
				for (i=0; (i < hgdata.labs.entitylist_len && res == 0); i++)
				{
					objid1 = hgdata.labs.entitylist_val[i];
					nm1 = get_mappingchar(objid1, map1);
					res = remove_labhost(map1, objid1, nm1);
					if (nm1) free(nm1);
				}
			}
			else
			{
				fprintf(stderr, "Cannot remove hostgroup '%s' - still has %d associated labs\n", nm, i);
				res = 1;
			}
		}
		/* We do not explicitly remove hostgroups.
		 * Instead, they are removed when all associated labs
		 * and hosts have been removed (case Plab | Phost below)
		 */
		break;
	case R_NOMATCH:
		fprintf(stderr, "Cannot find hostgroup object '%s' to delete\n", nm);
		res = 0;
		break;
	default:
		res = 1;
		break;
	}

	if (res == 0) res = remove_namenum(map, "hostgroup", objid, nm);
	return res;
}

int main(int argc, char *argv[])
{
	enum progtype which;
	int opt;
	int rename=0, delete=0;
	int numbers = 0;
	char *hg=NULL, *lb=NULL, *reason=NULL, *host = NULL;
	nmapping_type map;
	item desc;
	item add_desc, sub_desc, set_desc;
	int have_add=0, have_sub=0, have_set=0;
	int labid;
	int hostid;

	command = argv[0];
	which = whichprog(command);
	if (which == Pbad)
		die1(2, "Unknown program name");
	add_desc = new_desc();
	sub_desc = new_desc();
	set_desc = new_desc();
	while ((opt=getopt(argc, argv, "RDfach:g:l:d:r:n"))!= EOF)
		switch(opt)
		{
		case 'n': numbers = 1; break;
		case 'R': rename = 1; break;
		case 'D': delete = 1; break;
		case 'f': force=1; break;
		case 'a': all = 1; break;
		case 'c': create = 1; break;
		case 'g': hg = optarg; break;
		case 'r': reason=optarg; break;
		case 'l': lb = optarg; break;
		case 'h': host = optarg; break;
		case 'd':
			/* if +, add to add_desc,
			   if -, add to sub_desc,
			   else put in set_desc
			*/
			if (optarg[0] == '+')
			{
				char *err;
				if ((err = parse_desc(optarg+1, &desc)))
					die2(2, "Bad description", err);
				check_lab(desc);
				desc_add(add_desc, desc);
				desc_sub(sub_desc, desc);
				have_add = 1;
			}
			else if (optarg[0] == '-')
			{
				char *err;
				if ((err = parse_desc(optarg+1, &desc)))
					die2(2, "Bad description", err);
				check_lab(desc);
				desc_add(sub_desc, desc);
				desc_sub(add_desc, desc);
				have_sub = 1;
			}
			else
			{
				char *err;
				if ((err = parse_desc(optarg, &desc)))
					die2(2, "Bad description", err);
				check_lab(desc);
				if (have_set)
					die2(2, "Can only set description once!", "");
				desc_add(set_desc, desc);
				have_set = 1;
			}
			free(desc.item_val);
			break;

		default: usage(); exit(2);
		}
	nargs = argc;	/* transfer to a global var */

	if (all && create)
		die1(2, "-a (all) and -c (create) are mutually exclusive");
	if ((rename && delete)
	    || ((rename||delete)&&(create || hg||lb||host||(have_set|have_add|have_sub)||reason))
		)
		die1(2, "Inconsistant flags");
	if (have_set && (have_add||have_sub))
		die1(2, "Cannot both set and modify description");
	if (lb)
	{
		labid = get_mappingint(lb, M_ATTRIBUTE);
		if (labid <0)
			die2(1, "Unknown lab", lb);
	}
	switch(which){
	case Phost: map=M_HOST; break;
	case Phg: map=M_HOSTGROUP; break;
	case Pws: map=M_WORKSTATION; break;
	case Plab: map=M_ATTRIBUTE; break;
	case Pbad: break;
	}
	if (rename)
	{
		entityid uid;
		/* there must be two argument, we rename first too second */
		if (optind+2 != argc)
			die1(2, "There must be exactly two names given with -R");
		/* first name must exist, second must not */
		if (argv[optind][0] == '#')
			uid = atoi(argv[optind]+1);
		else
			uid = get_mappingint(argv[optind], map);
		if (uid < 0)
			die2(2, "Cannot find object", argv[optind]);
		if (get_mappingint(argv[optind+1], map) != -1)
			die2(2, "New name already exists", argv[optind+1]);
		if (make_mapping(uid, argv[optind+1], map)!= 0)
			die2(1, "Name change failed", argv[optind+1]);
	}
	else if (delete)
	{
		/* remove object from lab, host or hostgroup and remove mapping */
		get_attr_types();
		while (getnextobj(map, argv))
		{
			switch (which)
			{
			case Phg:	remove_hostgroup(map, objid, nm); break;
			case Plab:	remove_labhost(map, objid, nm);	break;
			case Phost:	remove_labhost(map, objid, nm);	break;
			case Pws:	remove_ws(map, objid, nm);	break;
			default:	break;
			}
		}	
	}
	else /* create or just print */
	{
		entityid hgid;
		get_attr_types();
		/* optionally create the object and set various features */
		if (hg && (which == Phg || which == Pws))
			die1(2, "Cannot set hostgroup for hostgroup or workstation");
		if (lb && (which != Pws))
			die1(2, "Can only set lab for a workstation");
		if ((host||have_set||have_add||have_sub) && (which != Pws))
			die1(2, "Can only set description or host for a workstation");
		if (reason && (which != Pws))
			die1(2, "Can only set reason for a workstation");
		if (hg)
		{
			/* try to get hostgroup id */
			hgid = get_mappingint(hg, M_HOSTGROUP);
			if (hgid < 0) die2(2, "Unknown hostgroup", hg);
		}
		if (host)
		{
			if (host[0] == '\0' || strcmp(host, "none")==0)
				hostid = -1;
			else
			{
				hostid = get_mappingint(host, M_HOST);
				if (hostid < 0) die2(2, "Unknown host", host);
			}
		}
		while (getnextobj(map, argv))
		{
			item key;
			char k[20];
			hostinfo hdata;
			hostgroup_info hgdata;
			workstation_state wsdata;
			int st;

			switch(which)
			{
			case Pbad: break;
			case Phg:		/* there are no direct operations on hostgroups, so just print it */
				memset(&hgdata, 0, sizeof(hgdata));
				key = key_int(k, C_HOSTGROUPS, objid);
				if ((st=db_read(key, &hgdata, (xdrproc_t)xdr_hostgroup_info, CONFIG)) != R_RESOK && st != R_NOMATCH)
				{
					printf("%s: cannot get hostgroup info for %s\n", command, nm);
				}
				else
				{
					char *sep = "";
					int e;
					printf("HostGroup: %s\n", nm);
					printf("  hosts:\n    ");
					for (e=0 ; e<hgdata.hosts.entitylist_len ; e++)
					{
						char *hnm = get_mappingchar(hgdata.hosts.entitylist_val[e], M_HOST);
						if (hnm)
						{
							printf("%s%s", sep, hnm);
							if (numbers) printf("(%d)", hgdata.hosts.entitylist_val[e]);
						}
						else
							printf("%s#%d", sep, hgdata.hosts.entitylist_val[e]);
						if (hnm) free(hnm);
						sep=", ";
					}
					printf("\n  labs:\n    ");
					sep="";
					for (e=0 ; e<hgdata.labs.entitylist_len ; e++)
					{
						char *hnm = get_mappingchar(hgdata.labs.entitylist_val[e], M_ATTRIBUTE);
						if (hnm)
							printf("%s%s", sep, hnm);
						else
							printf("%s #%d", sep, hgdata.labs.entitylist_val[e]);
						sep=", ";
					}
					printf("\n");
				}
				xdr_free((xdrproc_t)xdr_hostinfo, (char*) &hgdata);
				break;

			case Plab:
			case Phost: /* set hostgroup then print */
				if (hg && hgid >= 0)
				{
					book_change ch;
					if (which == Plab)
					{
						ch.chng = CHANGE_LAB;
						ch.book_change_u.newlab.entity = objid;
						ch.book_change_u.newlab.clump = hgid;
					}
					else
					{
						ch.chng = CHANGE_HOST;
						ch.book_change_u.newhost.entity = objid;
						ch.book_change_u.newhost.clump = hgid;
					}
					if (send_change(&ch)!= 0)
					{
						fprintf(stderr,"%s: failed to set host group!!\n", command);
					}
				}
				/* now print out the host information */
				memset(&hdata, 0, sizeof(hostinfo));
				key = key_int(k, which==Phost?C_HOSTS:C_LABS, objid);
				if (db_read(key, &hdata, (xdrproc_t)xdr_hostinfo, CONFIG)!= R_RESOK)
				{
					printf("%s: cannot get info for %s\n", command, nm);
				}
				else
				{
					char *sep = "";
					int e;
					char *nm2;
					printf("%s: %-15s in HostGroup:", which==Phost?"Host":"Lab ", nm);
					nm2 = get_mappingchar(hdata.clump, M_HOSTGROUP);
					if (nm2)
						printf(" %s\n", nm2);
					else printf(" #%d\n", hdata.clump);
					free(nm2);
					printf("  workstations:\n    ");
					for (e=0 ; e<hdata.workstations.entitylist_len ; e++)
					{
						entityid wsid = hdata.workstations.entitylist_val[e];
						char *wnm = get_mappingchar(wsid, M_WORKSTATION);
						if (wnm)
						{
							printf("%s%s", sep, wnm), free(wnm);
							if (numbers) printf("(%d)", wsid);
						}
						else
							printf("%s#%d", sep , wsid);
						sep = ", ";
					}
					printf("\n");
				}
				xdr_free((xdrproc_t)xdr_hostinfo, (char*) &hdata);
				break;
			case Pws:
				/* set lab, description, reason, and then print */
				/* if lab is not set, or have_add or have_sub, then
				 * must be able to get current structure
				 */
				memset(&wsdata, 0, sizeof(wsdata));
				key = key_int(k, C_WORKSTATIONS, objid);
				switch (db_read(key, &wsdata, (xdrproc_t)xdr_workstation_state, CONFIG))
				{
				case R_RESOK: /* fine */
					break;
				case R_NOMATCH: /* doesn't exist */
					if (lb == NULL)
					{
						fprintf(stderr, "ws: must set lab when creating workstation\n");
						continue;
					}
#if 0
					if (!have_set)
					{
						fprintf(stderr ,"ws: must set description when creating workstation\n");
						continue;
					}
#endif
					wsdata.state = new_desc();
					wsdata.why = strdup("");
					wsdata.time = 0;
					wsdata.host = -1;
					set_a_bit(&wsdata.state, attr_types.available);
					break;
				default: /* error */
					fprintf(stderr,"ws: cannot get info record for %s\n", nm);
					continue;
				}
				if (lb)
				{
					desc_sub(wsdata.state, attr_types.lab);
					set_a_bit(&wsdata.state, labid);
				}
				if (have_set)
				{
					item nlab = invert_desc(&attr_types.lab);
					del_a_bit(&nlab, attr_types.available);
					desc_sub(wsdata.state, nlab);
					free(nlab.item_val);
					desc_add(wsdata.state, set_desc);
				}
				if (have_add)
					desc_add(wsdata.state, add_desc);
				if (have_sub)
					desc_sub(wsdata.state, sub_desc);
				if (reason)
				{
					free(wsdata.why);
					wsdata.why = strdup(reason);
					wsdata.time = time(0);
					if (reason[0])
						del_a_bit(&wsdata.state, attr_types.available);
					else
						set_a_bit(&wsdata.state, attr_types.available);
				}
				if (host)
					wsdata.host = hostid;
				print_wsdata(&wsdata, nm);
				if (host || reason || have_set||have_add||have_sub||lb)
				{
					book_change ch;
					ch.chng = CHANGE_WS;
					ch.book_change_u.newws.state = wsdata;
					ch.book_change_u.newws.ws = objid;
					if (send_change(&ch)!= 0)
						fprintf(stderr,"ws: failed to set workstation info for %s\n", nm);
				}
			}
			free(nm);
		}
	}
	exit(0);
}
