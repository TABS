
ifeq ($(WINDOWS),)
 unix=y
 windows=n
 ObjDir=obj.$(ARCH)
else
 unix=n
 windows=y
 ObjDir=obj.windows
endif

dirs-$(unix) += messaged/
dirs-$(windows) += 

dirs-y += lib/ database/ db_client/ manager/ lablist/ tools/ book/ interfaces/ login/ 
#doc/ 

include $(S)MakeRules
