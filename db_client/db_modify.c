
#include	"db_client.h"
#include	<unistd.h>
#include	<stdlib.h>
#include	<time.h>

book_changeres send_change(book_change *ch)
{
    book_changeent che;
    book_changeres *result;
    book_changeres ret;
    
    if (db_client || refresh_db_client())
    {
	che.time = time(0);
	che.machine = "";
	che.changenum = 0;
	che.thechange = *ch;
	result = change_db_2(&che, db_client);
	update_config_time();
	if (result == NULL)
	{
	    if (refresh_db_client())
		result = change_db_2(&che, db_client);
	}
	if (result == NULL) {
	    ret = -1;
	} else if (*result == C_OK) {
	    ret = 0;
	} else ret = *result;
    } else ret = -1;
    return ret;
}

book_changeres make_mapping(int key, char *val, nmapping_type type)
{
    book_change ch;

    ch.chng = SET_NAMENUM;
    ch.book_change_u.map.type = type;
    ch.book_change_u.map.key = key;
    ch.book_change_u.map.value = val;
    return send_change(&ch);
}

book_changeres make_config(char *key, char *val)
{
    book_change ch;

    ch.chng = CONFIGDATA;
    ch.book_change_u.configent.key = key;
    ch.book_change_u.configent.value = val;
    return send_change(&ch);
}

book_changeres do_removal(book_key key)
{
    book_change ch;
    ch.chng = REMOVE_BOOKING;
    ch.book_change_u.removal = key;
    return send_change(&ch);
}

    
