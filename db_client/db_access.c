
/*
 * routines to access the book database from a client
 * Provides global data (if asked for with get_*):
 *   attr_types
 *   impl_list
 *
 * Requires db_client handle and refresh_db_client()-> success
 *
 * provides db_read(item key, void *data, bool_t xdr_fn, db_number)
 *  if db_number is CONFIG, replies are cached for quick re-access.
 *  periodically check if CONFIG has changed and flush cache if it has
 *
 *
 */

#include	"db_client.h"
#include	<stdlib.h>
#include	<unistd.h>
#include	<time.h>
#include	"../lib/skip.h"
typedef struct cache_ent
{
	item	key;
	item	content;
	time_t	db_time;
} *cache_ent;

CLIENT *db_client = NULL;

static int cache_cmp(cache_ent a, cache_ent b, item *k)
{
	int rv;

	if (b != NULL)
		k = & b->key;
    
	rv = memcmp(a->key.item_val, k->item_val,
		    a->key.item_len<k->item_len ? a->key.item_len:k->item_len);
	if (rv) return rv;
	else return k->item_len - a->key.item_len;
}

static void cache_free(cache_ent a)
{
	free(a->key.item_val);
	free(a->content.item_val);
	free(a);
}

struct
{
	cache_ent	*data;
	time_t	db_time;	/* last modify time of db */
	time_t	check_time;	/* last time we checked the modify time */
	time_t	attr_time;
	time_t	impl_time;
	int 	book_level;
	int		min_class_uid;
} config_cache = {0, 0, 0, 0, 0};

attr_type_desc attr_types;
impls	impl_list;

void update_config_time(void)
{
	config_cache.db_time++;
}


void check_config_time(void)
{
	time_t now = time(0);
	reply_res result;
	db_updaterec db_status;

	if (config_cache.check_time + 20*60 > now)
		return;
	/* time to get the config info again */

	if (db_client == NULL
	    && refresh_db_client() == 0)
		return; /* cannot get db client */

	result = db_read_direct(str_key(STATUS_KEY), &db_status,
				(xdrproc_t)xdr_db_updaterec, CONFIG);
	if (result == R_NOMATCH)
		db_status.update_time = 1;
	else if (result != R_RESOK)
		return;
	if (db_status.update_time > config_cache.db_time)
	{
		char *bl;
		config_cache.db_time = db_status.update_time;
		bl = get_configchar("book_level");
		config_cache.book_level = 0;
		if (bl)
		{
			if (strcmp(bl,"off")==0)
				config_cache.book_level = 0;
			else if (strcmp(bl, "halfon")==0)
				config_cache.book_level = 1;
			else if (strcmp(bl, "on")==0)
				config_cache.book_level = 1;
			else if (strcmp(bl, "fullon")==0)
				config_cache.book_level = 2;
			free(bl);
		}
		config_cache.min_class_uid = get_configint("min_class_uid");
		if (config_cache.min_class_uid <= 0)
			config_cache.min_class_uid = 0x4000000;
	}
	config_cache.check_time = now;
}

int get_book_level(void)
{
	check_config_time();
	return config_cache.book_level;
}

int get_class_min(void)
{
	check_config_time();
	return config_cache.min_class_uid;
}

char *get_exempt_group(void)
{
	/* find the name of the group in which users who are exempt
	   from the booking system (ie not logged out by the booking
	   system) are placed.
	*/
  
	return get_configchar("exempt_group");

}


int get_attr_types(void)
{
	check_config_time();
	if (config_cache.attr_time < config_cache.db_time)
	{
		/* need to refresh this info */
		item dbkey;

		dbkey = str_key(ATTR_TYPE_KEY);
		if (db_read(dbkey, &attr_types, (xdrproc_t)xdr_attr_type_desc, CONFIG)== R_RESOK)
			config_cache.attr_time = config_cache.db_time;
	}
	if (config_cache.attr_time)
		return 1;
	else
		return 0;
}

int get_impl_list(void)
{
	check_config_time();
	if (config_cache.impl_time < config_cache.db_time)
	{
		/* need to refresh this info */
		item dbkey;
		impls new;
		impls one;
		char key[20];

		memset(&one, 0, sizeof(one));
		dbkey = key_int(key, C_ATTR_DEF, 0);
		if (db_read(dbkey, &one, (xdrproc_t)xdr_impls, CONFIG)== R_RESOK)
		{
			/* keep reading */
			new.first = 0;
			new.total = one.total;
			new.exlist.exlist_len = one.total;
			new.exlist.exlist_val = (implication *)malloc(sizeof(implication)*one.total);

			while (new.first < new.total)
			{
				int i;
				for (i=0 ; i<one.exlist.exlist_len ; i++)
				{
					new.exlist.exlist_val[new.first+i] =
						one.exlist.exlist_val[i];
				}
				new.first += one.exlist.exlist_len;
				free(one.exlist.exlist_val);
				one.exlist.exlist_val = NULL;
				if (new.first < new.total)
				{
					dbkey = key_int(key, C_ATTR_DEF, new.first);
					if (db_read(dbkey, &one, (xdrproc_t)xdr_impls, CONFIG)!= R_RESOK)
						new.total = -1;
				}
			}
		}
		else new.total = -1;

		if (new.total >= 0)
		{
			xdr_free((xdrproc_t)xdr_impls, (char*) &impl_list);
			impl_list = new;
			config_cache.impl_time = config_cache.db_time;
		}
	}
	if (config_cache.impl_time)
		return 1;
	else
		return 0;
}

static time_t want_tcp = 0;

reply_res db_read(item key, void *data, xdrproc_t xdr_fn, db_number db)
{

	cache_ent *cep, ce;
	if (db != CONFIG)
		return db_read_direct(key, data, xdr_fn, db);

	if (config_cache.data == NULL)
		config_cache.data = skip_new(cache_cmp, cache_free, NULL);

	check_config_time();
    
	cep = skip_search(config_cache.data, &key);
	if (cep == NULL)
	{
		ce = (cache_ent)malloc(sizeof(struct cache_ent));
		ce->key.item_val = memdup(key.item_val, key.item_len);
		ce->key.item_len = key.item_len;
		ce->content.item_len = 0;
		ce->content.item_val = NULL;
		ce->db_time = 0;
		skip_insert(config_cache.data, ce);
	}
	else
		ce = *cep;

	if (ce->db_time < config_cache.db_time && db_client != NULL)
	{
		/* must refresh info from database */
		query_req req;
		query_reply *res;

		req.key = key;
		req.type = M_MATCH;
		req.database = db;

		res = query_db_2(&req, db_client);
		if (res == NULL)
		{
			/* try for a tcp client FIXME can I tell if reply was toooo big?? */
			want_tcp = time(0);
			refresh_db_client();
			if (db_client != NULL)
				res = query_db_2(&req, db_client);
		}	    

		if (res != NULL)
		{
			if (res->error == R_RESOK)
			{
				if (ce->content.item_val)
					free(ce->content.item_val);
				ce->content.item_val = memdup(res->query_reply_u.data.value.item_val,
							      res->query_reply_u.data.value.item_len);
				ce->content.item_len = res->query_reply_u.data.value.item_len;
				ce->db_time = config_cache.db_time;
			}
			else if (res->error == R_NOMATCH)
			{
				if (ce->content.item_val)
					free(ce->content.item_val);
				ce->content.item_val = NULL;
				ce->content.item_len = 0;
				ce->db_time = config_cache.db_time;
			}
			xdr_free((xdrproc_t)xdr_query_reply, (char*) res);
		}
	}

	if (ce->db_time > 0)
	{
		/* the data in the cache is valid */
		if (ce->content.item_val == NULL)
			return R_NOMATCH;
		item_decode(&ce->content, data, xdr_fn);
		return R_RESOK;
	}
	else
		return R_NOSERVER;
}

reply_res db_read_direct(item key, void *data, xdrproc_t xdr_fn, db_number db)
{

	query_req req;
	query_reply *res;

	req.key = key;
	req.type = M_MATCH;
	req.database = db;

	if (db_client == NULL)
		refresh_db_client();
	if (db_client == NULL)
		return R_NOSERVER;
	
	res = query_db_2(&req, db_client);

	if (res == NULL)
	{
		/* try for a tcp client FIXME can I tell if reply was toooo big?? */
		want_tcp = time(0);
		refresh_db_client();
		if (db_client == NULL)
			return R_NOSERVER;
		res = query_db_2(&req, db_client);
	}	    
	if (res == NULL)
		return R_NOSERVER;

	if (res->error == R_RESOK)
		item_decode(&res->query_reply_u.data.value, data, xdr_fn);
	xdr_free((xdrproc_t)xdr_query_reply, (char*) res);
	return res->error;
}

void open_db_client(char *server, char *pr)
{
	char *proto = pr?pr:"udp";
	if (db_client)
	{
		clnt_destroy(db_client);
		db_client = NULL;
	}
	if (want_tcp > 0 && want_tcp > time(0)-10*60)
		proto = "tcp";
	if (server)
		db_client = clnt_create(server, BOOK_DATABASE, BOOKVERS, proto);
	else {
		if (pr)
			want_tcp = time(0);
		refresh_db_client();
	}
}
