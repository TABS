

#include	"../db_client/db_client.h"
#include	<unistd.h>
#include	<stdlib.h>
#include	<time.h>

struct timebl { /* booklist wirg times */
    int poy;
    int lab;
    booklist bl;
};
static int blcmp(const void *av, const void *bv)
{
	const struct timebl *a = av;
	const struct timebl *b = bv;
	return a->poy - b->poy;
}

int is_defaulter(int who)
{
	/* determine if user is a defaulter
	 *  Get the users record
	 *  take the pastbookings list and discard:
	 *		any booking in the future
	 *		any booking more than X in the past
	 *
	 *  Map_bookings to get actual bookings
	 *   Of bookings which were ALLOCATED,RETURNED,FREEBIE
	 *    sort by time (So consecutives may be found)
	 *
	 * heuristic 1:
	 *    for first in a consecutive list
	 *		count good if loggedin before grace and in last 14 days
	 *		count bad if not loggedin before grace and in last 14 days
	 *   if good+bad>=3 && good <= 2*bad,
	 *		is_defaulter
	 * heuristic 2:
	 *   find most recent defaulter booking more than 2 weeks old and only consider bookings since then.
	 *   If most recent two bookings were "good", then not is_defaulter
	 *   otherwise use heuristic 1
	 */
	char k[20];
	static users urec;
	int good,bad;
	int i;
	booklist bk, bp;
	struct timebl *blist;
	userbookinglist *prev, *next, bn;
	int memory_length = 14; /* how long we remember defaults */
	int grace = get_configint("grace");
	time_t now;
	int cnt;
	int consec_good;
	int ndoy, npod;
	if (db_read(user_key(k, who), &urec, (xdrproc_t)xdr_users, BOOKINGS) != R_RESOK)
		return 0;

	now=time(0);
	get_doypod(&now, &ndoy, &npod);
	for (prev = &urec.pastbookings; *prev ; prev = next)
	{
		int pod, doy;
		bn = *prev;
		next = &bn->next;
		book_key_bits(&bn->slot, &doy, &pod, NULL);
		if (doy>ndoy
		    || (doy==ndoy && pod >= npod)
			)
		{
			*prev = *next;
			*next = NULL;
			next = prev;
			xdr_free((xdrproc_t)xdr_userbookinglist, (char*)&bn);
		}
	}
	bk = getfullbookings(urec.pastbookings, who);

	for (cnt=0,bp=bk ; bp ; bp=bp->next)
		if (bp->status == B_ALLOCATED || bp->status == B_RETURNED || bp->status == B_FREEBIE)
			cnt++;
	blist = (struct timebl *)malloc(cnt*sizeof(struct timebl));
	for (cnt=0,bp=bk, bn=urec.pastbookings ; bp ; bp=bp->next, bn=bn->next)
		if (bp->status == B_ALLOCATED || bp->status == B_RETURNED || bp->status == B_FREEBIE)
		{
			int doy, pod, lab;
			book_key_bits(&bn->slot, &doy, &pod, &lab);
			blist[cnt].bl = bp;
			blist[cnt].poy = doy*48+pod;
			blist[cnt].lab = lab;
			cnt++;
		}
	qsort(blist, cnt, sizeof(blist[0]), blcmp);
	/* now list is a sorted list of allocated bookings */
	good = bad = 0;
	consec_good=0;
	for (i=0; i<cnt ; i++)
		if (i==0 || blist[i-1].poy != blist[i].poy-1 || blist[i-1].lab != blist[i].lab)
		{
			booklist b = blist[i].bl;
			int isgood=0, isbad=0;
			/* this is a first of consecutive list */
			if ((b->claimed&DID_LOGIN) &&
			    ((b->claimed&LOGIN_ON_ALLOC) || !(b->claimed&DID_DEFAULT))  &&
			    ((b->claimed>>4)-30*60)< grace)
				isgood++;
			else if (b->claimed&DID_DEFAULT)
				isbad++;
			if (isbad && blist[i].poy/48 < ndoy-memory_length) /* old bad booking */
				isbad = good = bad = 0;
			if (isbad) consec_good=0;
			if (isgood) consec_good++;
			if (blist[i].poy/48 > ndoy-memory_length)
			{
				good+= isgood;
				bad += isbad;
			}
		}
	free(blist);
	xdr_free((xdrproc_t)xdr_booklist, (char*)&bk);

	/*
	 * Now:
	 *   'good' is the number of allocations that were taken up within the grace period,
	 *   'bad' is the number of bookings which defaulted and were not taken up
	 *   'consec_good' is the number of consecutive good allocations since the last bad one.
	 *
	 * The person is a defaulter if there are at least 2 'bad' allocations, and there have
	 * not been 2 consecutive good allocations, and 2*bad > good
	 *
	 */
	return (bad >= 2 && 2*bad > good && consec_good < 2);
	/*
	  if (consec_good >= 2 || consec_good >= bad)
	  return 0;
	  else
	  return (good+bad>=3 && good <= 2*bad);
	*/
}

