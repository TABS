
#include	"db_client.h"


int get_mappingint(char * tname, nmapping_type type)
{
    int result;
    item key;
    extern char * value_key();
    
    key = key_string(C_NUMBERS+type, tname);
    switch (db_read(key, &result, (xdrproc_t)xdr_int, CONFIG))
    {
    case R_RESOK:
	break;
    case R_NOMATCH:
	result = -1;
	break;
    default: result = -2;
	break;
    }
    free(key.item_val);
    return result;
}

char * get_mappingchar(int num, nmapping_type type)
{
    reply_res result;
    item key;
    char k[20];
    char * retval = NULL;

    key = key_int(k, C_NAMES+ type, num);
    
    result = db_read(key, &retval, (xdrproc_t)xdr_map_value, CONFIG);

    if (result != R_RESOK)
	return NULL;
    
    return (retval);
}

char *get_configchar(char *name)
{
    reply_res result;
    item key;
    char * retval= NULL;

    key = key_string(C_GENERAL, name);
    result = db_read(key, &retval, (xdrproc_t)xdr_map_value, CONFIG);
    free(key.item_val);
    if (result != R_RESOK)
	return NULL;

    return retval;
}

int get_configint(char *name)
{
    reply_res result;
    item key;
    char * retval = NULL;
    int rv;

    key = key_string(C_GENERAL, name);
    result = db_read(key, &retval, (xdrproc_t)xdr_map_value, CONFIG);
    free(key.item_val);
    if (result != R_RESOK)
	return -1;

    rv = atoi(retval);
    free(retval);
    return rv;
}
