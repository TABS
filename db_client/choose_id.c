
/*
 * choose_id(int min, nmapping_type type)
 * find a free id number in the mapping min or more.
 *
 * starting at min, search upwards in steps of 1,2,4,8 ...
 * until a free space is found.
 * then divide and concour until we find two consecutive
 * indexes, first in use, second not in use
 *
 */

#include	"db_client.h"

int choose_id(int min, nmapping_type type)
{
    int step = 1;
    int max;
    char *n;

    n = get_mappingchar(min, type);
    if (n == NULL)
	return min;

    while (n != NULL)
    {
	free(n);
	step *= 2;
	n = get_mappingchar(min+step, type);
    }
    
    max = min+step;
    while (min+1 < max)
    {
	int mid = (max+min)/2;
	n = get_mappingchar(mid, type);
	if (n)
	{
	    min = mid;
	    free(n);
	}
	else
	    max = mid;
    }
    return min+1;
}
