
#include	"../db_client/db_client.h"
#include	"../lib/skip.h"
#include        "../manager/status_client.h"
#include	<unistd.h>
#include	<stdlib.h>
#include	<time.h>
#include	<sys/socket.h>
#include	<string.h>
#include	<stdio.h>
#include	<pwd.h>
#include	<netdb.h>
#ifndef __CYGWIN__
#include	<rpcsvc/ypclnt.h>
#endif


static char *domain = NULL;/* be careful with this on suns */

extern char * strtok();


char *ypmatch(char * key, char * map)
{
	char * result;
	int resultlen;
	int nb;
    
	if (domain == NULL)
	{
		int res;
		if ((res =yp_get_default_domain(&domain)))
		{
			printf("failed here, reason %d\n",res);
			return NULL;
		}
	
	}
	if (!(nb=yp_match(domain, map, key, strlen(key),&result, &resultlen)))
	{
		result[resultlen] = '\0';
printf("YPMATCH %s %s -> %s\n", key, map, result);
		return result;
	}
/*    else if (nb == 5)
      return strdup("");
*/
	return NULL;
}


static int * add_int(int * tab, int new)
{
	int * result ;
	int i;

	if (tab == NULL)
	{
		result = (int *) malloc(2 * sizeof(int));
		result[0] =1;
	}
    
	else
	{
		result = (int *) malloc((tab[0] +2) * sizeof(int));
		for (i=0;i <=tab[0];i++)
			result[i] = tab[i];
		result[0]++;
		free(tab);
	}
    
	result[result[0]] = new;
	return result;
    
}

    
int find_userid(char * userstring)
{
	struct passwd * pwent;
	if ( (pwent = getpwnam(userstring)) != NULL)
		return (pwent->pw_uid);
	else
		return(get_mappingint(userstring, M_CLASS));
}

typedef struct unc {
	time_t when;
	int uid;
	char *uname;
} *unc;
static int unc_cmp(unc a, unc b, int *k)
{
	if (b) k= &b->uid;
	return a->uid - *k;
}
static void unc_free(unc a)
{
	free(a->uname);
	free(a);
}
static unc uncl = NULL;
static void unc_add(int uid, char *name)
{
	unc a;
	if (uncl)
	{
		unc *ap = skip_search(uncl, &uid);
		if (ap)
		{
			a = *ap;
			free(a->uname);
			a->uname = strdup(name);
			a->when = time(0);
			return;
		}
	}
	a = (unc)malloc(sizeof(struct unc));
	a->uid = uid;
	a->uname = strdup(name);
	a->when = time(0);
	if (uncl==NULL)
		uncl = skip_new(unc_cmp, unc_free, NULL);
	skip_insert(uncl, a);
}

static char *unc_find(int uid)
{
	unc *a;
	if (uncl == NULL) return NULL;
	a = skip_search(uncl, &uid);
	if (a && (*a)->when + 40*60 < time(0)) return (*a)->uname;
	else return NULL;
}

char * find_username(int userid)
{
	struct passwd * pwent;
	if (userid < get_class_min())
	{
		char *n = unc_find(userid);
		if (n == NULL)
		{
			if ((pwent = getpwuid(userid)) != NULL)
				unc_add(userid, n=pwent->pw_name);
		}
		return strdup(n);
	}
	else
		return get_mappingchar(userid, M_CLASS);
}
#if 0
int * user_classes(int uid)
{
	char * res;
	char * username;
	char * t;
	int *result = NULL;
	struct passwd * pwent;
    

	if ( (pwent = getpwuid(uid)) != NULL)
		username = strdup(pwent->pw_name);
	else
		return NULL;

	/* always the first one */
	result = add_int(result, uid);
	/* ypmatch username netgroup.byuser */
	res = ypmatch(username, "netgroup.byuser");

	/* this is as NIS+ on the suns will not allow "." in map names!!! */
	if (res == NULL)
		res = ypmatch(username, "netgroup_byuser");
    
	if (res == NULL)
	{
		free(username);
		return(result);
	}
    
	free(username);
	/* map each classname to classid */


	for (t = strtok(res, ",") ; t ; t = strtok(NULL, ","))
	{
		int clid;
		clid = get_mappingint(t, M_CLASS);
		if (clid > 0)
			result = add_int(result,clid);
	}
	free(res);
	return(result);
}
#else

/* find all classes that 'uid' is a member of.
 * We first get a list of valid classes from the CONFIG db, and
 * then use innetgr to check each of these
 */
int * user_classes(int uid)
{
	char * username;
	int *classes = NULL;
	struct passwd * pwent;
	query_req request;
	static time_t listtime = 0; /* age of list of classes */
	static struct cl {
		char *name;
		int id;
		struct cl *next;
	} *list = NULL, *t;
    
	if ( (pwent = getpwuid(uid)) != NULL)
		username = strdup(pwent->pw_name);
	else
		return NULL;
	classes = add_int(classes, uid); /* always in their own class */

	if (listtime + 10*60 < time(0L)) {

		listtime = time(0L);
		while (list) {
			t = list;
			list = list->next;
			free(t->name);
			free(t);
		}

		refresh_db_client();
		request.key.item_val = strdup("206_");
		request.key.item_len = 4;
		request.type = M_NEXT_PREFIX;
		request.database = CONFIG;

		while (db_client) {
			query_reply *result = query_db_2(&request, db_client);

			free(request.key.item_val);

			if (result == NULL)
				break;

			if (result->error != R_RESOK ||
			    result->query_reply_u.data.key.item_len == 0 ||
			    result->query_reply_u.data.key.item_val == NULL)
				break;

			request.key.item_len = result->query_reply_u.data.key.item_len;
			request.key.item_val =
				memdup(result->query_reply_u.data.key.item_val,
				       request.key.item_len+1);
			request.key.item_val[request.key.item_len] = 0;

			if (strncmp(request.key.item_val, "206_", 4) == 0 &&
			    strspn(request.key.item_val+4, "0123456789")
			    == request.key.item_len-4) {
				/* Ok, looks like a class number/name */
				char *name = NULL;
				int clid = atoi(request.key.item_val+4);
				item_decode(&result->query_reply_u.data.value,
					    &name,
					    (xdrproc_t)xdr_map_value);
				
				t = malloc(sizeof(*t));
				t->name = name;
				t->id = clid;
				t->next = list;
				list = t;
			}

			xdr_free((xdrproc_t)xdr_query_reply, (char*)result);
		}
	}
	for (t=list ; t ; t=t->next) {
		if (innetgr(t->name, NULL, username, NULL))
			classes = add_int(classes, t->id);
	}

	return classes;
}
#endif

typedef struct ngc {
    time_t when;
    char *user;
    char *groups;
} * ngc;

#if 0
static int ngc_cmp(ngc a, ngc b, char *k)
{
	if (b) k=b->user;
	return strcmp(a->user, k);
}
static void ngc_free(ngc a)
{
	free(a->user);
	free(a->groups);
	free(a);
}
static ngc ngcl = NULL;
static void ngc_add(char *user, char *grps)
{
	ngc a, *ap;
	if (ngcl)
	{
		ap = skip_search(ngcl, user);
		if (ap)
		{
			a=*ap;
			free(a->groups);
			a->groups = strdup(grps);
			time(&a->when);
			return;
		}
	}
	a = (ngc)malloc(sizeof(struct ngc));
	a->user = strdup(user);
	a->groups = strdup(grps);
	time(&a->when);
	if (ngcl == NULL)
		ngcl = skip_new(ngc_cmp, ngc_free, NULL);
	skip_insert(ngcl, a);
}

static char *ngc_find(char *user)
{
	ngc *a;
	if (ngcl == NULL) return NULL;
	a = skip_search(ngcl, user);
	if (a && (*a)->when + 40*60 > time(0))
		return (*a)->groups;
	else
		return NULL;
}
#endif

#if 0
#ifdef INNETGR_BY_USER
int my_innetgr(char *class, char *user)
{
	char * res;
	char * t;
	int rv = 0;
    

	/* ypmatch username netgroup.byuser */
	res = ngc_find(user);
	if (res)
		res = strdup(res);
	else
	{
		res = ypmatch(user, "netgroup.byuser");

		/* this is as NIS+ on the suns will not allow "." in map names!!! */
		if (res == NULL)
			res = ypmatch(user, "netgroup_byuser");
    
		if (res == NULL)
		{
			return -1;
		}

		ngc_add(user, res);
	}
	/* map each classname to classid */


	for (t = strtok(res, ",") ; t ; t = strtok(NULL, ","))
		if (strccmp(class, t)==0)
			rv = 1;
	free(res);
	return rv;
}
#else
int my_innetgr(char *class, char *user)
{
	/* First lookup innetgr:group,host,user,dom.
	 * If thse finds something, let it say yes or no.
	 * Otherwise fall back to normal innetgr
	 */
	char *ng = malloc(8+strlen(class)+1+0+1+strlen(user)+1+0+1);
	if (ng) {
		sprintf(ng, "innetgr:%s,,%s,", class, user);
		if (setnetgrent(ng)) {
			char *u, *h, *d;
			if (getnetgrent(&h, &u, &d)) {
				/* Successful lookup */
				int rv = 0;
				if (strcasecmp(user, u)==0)
					rv = 1;
				endnetgrent();
				free(ng);
				return rv;
			}
			endnetgrent();
		}
		free(ng);
	}
	return innetgr(class, NULL, user, NULL);
}	
#endif
#else
int my_innetgr(char *class, char *user)
{
	return innetgr(class, NULL, user, NULL);
}
#endif
int in_class(int uid, int clid)
{
	char *uname, *cname;
	int rv;
    
	if (uid == clid) return 1;
	if (clid < get_class_min()) return 0;
	if (uid < 0 || clid < 0) return 0;

	uname = find_username(uid);
	cname = get_mappingchar(clid, M_CLASS);

	if (uname == NULL || cname == NULL)
		rv = -1;
	else
		rv = in_net_group(cname, uname);
	if (uname) free(uname);
	if (cname) free(cname);
	return rv;
}


int in_net_group(char *group, char *user)
{
/*    return innetgr(group, NULL, user, NULL); */
	return my_innetgr(group, user);
}

int in_exempt(int uid)
{
	/* gets the name of the exempt group, checks
	   if user is in that group */

	char *name;
	char *group;
	int rv=0;

	name = find_username(uid);
	group = get_exempt_group(); 
	if (name && group)
		rv = in_net_group(group, name);
	if (name) free(name);
	if (group) free(group);
	return rv;

}


    
int in_neverclose(int uid, char *labname)
{
	char clname[200];
	char *name = find_username(uid);
	int rv;
    
	if (name && labname)
	{
		strcpy(clname, "book.neverclose.");
		strcat(clname, labname);
		rv = in_net_group(clname, name);
		free(name);
	}
	else
		rv = 0;
	return rv;
}


char *user_surname(char * name)
{
	char * result;
	char * eos;
	struct passwd * pwent;
	if ( (pwent = getpwnam(name)) != NULL)
	{
	
		result = strdup(pwent->pw_gecos);
		eos = strchr(result, ',');
		if (eos) *eos = '\0';
		eos = strrchr(result, ' ');
		if (eos && eos > result && eos[1]) strcpy(result, eos+1);
		return result;
	}
	else
		return NULL;
}


