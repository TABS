
#include	"../db_client/db_client.h"

static bool_t match_value(int val, int min, int max)
{
/*     printf("match %d (%d-%d)\n", val, min, max);  */
    
    if (val == -1)
	return TRUE;
    
    return((val >= min) && (val <= max)) ;
}

/* CHECK_CLUSIONS: check the included and excluded attributes */
static bool_t check_clusions(description * d1, description * d2, bool_t included)
{
    bool_t result;
    description inverted;

    
    if (included)
	result = required_bits(d1,d2);
    else {
	inverted = invert_desc(d2);
	result = required_bits(d1,&inverted);
	free(inverted.item_val);
    }
/*     printf("RESULT =%d\n",result);   */
    return result;
    
}

/* PROCESS_EXCLUSIONS: takes a pod, doy, machine_id and description */
/* and uses to the exclusion table to decide which attributes should */
/* be set */

/* maybe need to decide if the supplied description should be used as */
/* the output as well.  This could be another flag */
/* the desc can be null, this means no exclusion.  If any of the */
/* others are -1, this is the null value!, and these values will not */
/* be included in the comparisons.  Some means of determining */
/* defaults has to be devised */

description * process_exclusions(int pod, int doy, description * desc)
{
    description *result;
    int i;

    get_attr_types();
    get_impl_list();
    
    result = desc;
    
    for (i=0 ; i < impl_list.total ; i++)
    {
	implication *imp = &impl_list.exlist.exlist_val[i];
	/* first compare the day of the year */
	if ( match_value(doy, imp->startday, imp->endday)
	    && match_value(doy_2_dow(doy), imp->startdow, imp->enddow)
	    && match_value(pod, imp->starttime, imp->endtime)
	    && check_clusions(&imp->included, result, TRUE)
	    && check_clusions(&imp->excluded, result, FALSE)
	    )
	    set_a_bit(result, imp->attribute);
    }
    return (result);
}

