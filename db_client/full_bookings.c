
#include	"db_client.h"

/* GETFULLBOOKINGS: given a list of slots and times for a user, gets */
 /* the full booking record (token name, request, count, etc) from the */
 /* bookings database.  The slot and time the booking was made are the */
 /* key to uniqly identify a booking.
    Have to be carefull not to ask for too many bookings at one time,
    as there is an 8K limit on the size of a UDP packet */
booklist getfullbookings(userbookinglist bl, bookuid_t user)
{
    usermap_req req;
    usermap_res *res;
    booklist nbl = NULL;
    booklist *nblp = &nbl;

    while (bl)
    {
	userbookinglist bl_end, bl_rest;
	int cnt;
	req.user = user;
	for (bl_end = bl, cnt=20 ; bl_end && cnt ; bl_end=bl_end->next, cnt--);
	if (bl_end)
	{
	    bl_rest=bl_end->next;
	    bl_end->next = NULL;
	}
	else
	    bl_rest = NULL;
	req.blist = bl;
	res = map_bookings_2(&req, db_client);
	if (res == NULL || res->res != C_OK)
	    return NULL;
	*nblp = res->blist;
	res->blist = NULL;
	while (*nblp)
	    nblp = & (*nblp)->next;
	if (bl_end)
	    bl_end->next = bl_rest;
	bl = bl_rest;
    }
    return nbl;
}
