
/*
 * parse_desc - convert a dot sep list of attributes in to a description
 *
 */
#include	<string.h>
#include	"db_client.h"

char *parse_desc(char *attrs, item *desc)
{
	item d =  new_desc();
	char *cp, *ep;

	for (cp=attrs; *cp ; cp = (*ep)?(ep+1):ep)
	{
		char *a;
		int anum;
		ep = strchr(cp, '.');
		if (ep == NULL) ep = cp+strlen(cp);
		a = strndup(cp, ep-cp);
		anum = get_mappingint(a, M_ATTRIBUTE);
		if (anum < 0)
		{
			free(d.item_val);
			return a;
		}
		set_a_bit(&d, anum);
		free(a);
	}
	*desc = d;
	return NULL;
}
