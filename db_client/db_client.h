
#include	"../database/db_header.h"
#include	"../manager/status_client.h"


extern CLIENT *db_client;
extern impls impl_list;
extern attr_type_desc attr_types;

#include	"../database/common.h"

/* from db_access.c */
void update_config_time(void);
void check_config_time(void);
int get_book_level(void);
int get_class_min(void);
int get_attr_types(void);
int get_impl_list(void);
reply_res db_read(item key, void *data, xdrproc_t xdr_fn, db_number db);
reply_res db_read_direct(item key, void *data, xdrproc_t xdr_fn, db_number db);
void open_db_client(char *server, char *pr);

/* from user_utils.c */
int find_userid(char * userstring);
char * find_username(int userid);
int * user_classes(int uid);
int my_innetgr(char *class, char *user);
int in_class(int uid, int clid);
int in_net_group(char *group, char *user);
int in_neverclose(int uid, char *labname);
char *user_surname(char * name);
char *get_exempt_group(void);
int in_exempt(int uid);

/* from mapping_utils.c */
int get_mappingint(char * tname, nmapping_type type);
char * get_mappingchar(int num, nmapping_type type);
char *get_configchar(char *name);
int get_configint(char *name);

/* from db_modify.c */
book_changeres send_change(book_change *ch);
book_changeres make_mapping(int key, char *val, nmapping_type type);
book_changeres make_config(char *key, char *val);
book_changeres do_removal(book_key key);

/* from full_bookings.c */
booklist getfullbookings(userbookinglist bl, bookuid_t user);

/* from ../tools/show_booking.c */
void show_booking(booklist bl, book_key slot);
int char2bstatus(char *s);

/* from choose_id.c */
int choose_id(int min, nmapping_type type);

/* from get_tokens.c */
intpairlist get_alloc_tokenlist(bookuid_t user);

/* from parse_desc.c */
char *parse_desc(char *attrs, item *desc);

/* from isdefaulter.c */
int is_defaulter(int who);
