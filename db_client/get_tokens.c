
#include	"db_client.h"


/* GETTOKENLIST: for a normal user returns a list of all the tokens */
 /* user is allocated.  This is calculated by getting the allocations */
 /* for all classes the user is a member of and then subtracting the */
 /* tokens the user has consumed.
    
    For a class (uid > min_class_uid) all tokens are valid, so the token list
    is calculated from the list of all valid tokens
    */
intpairlist get_alloc_tokenlist(bookuid_t user)
{
	int classsize;
	int i;
	int isclass = (user >= get_class_min());
	int * class_list;
	int r[3];
	intpairlist toks = NULL;
	intpairlist intres = NULL;
    
	/* if the user is a class, all tokens are valid, so pretend to be root */
	if (isclass)
	{
		r[0] = 1;
		r[1] = 0;
		class_list = r;
	}
	else
		class_list = user_classes(user);

	if (class_list == NULL)
	{
		fprintf(stderr,"No classes\n");
		return NULL;
	}
	/* the first element of array is number of users */
	classsize = *class_list;


	/* get allocations for all classes, skip the first */
	for (i= 1; i <= classsize;i++)
	{
		char k[20];
                int res;

		if ((res = db_read(key_int(k, C_ALLOTMENTS, class_list[i]), &toks, (xdrproc_t)xdr_intpairlist, CONFIG))
		    != R_RESOK) {
//                    printf("db_read returned %d\n", res);
                }
                else
		{
//                    printf("db_read also returned %d\n", res);
                    
                           merge_intlists(&intres, &toks);
			xdr_free((xdrproc_t)xdr_intpairlist, (char*) &toks);
		}
	}
	if (class_list != r)
		free(class_list);
	if (isclass)
	{
		intpairlist ip = intres;
		while (ip)
		{
			ip->num = 1000;
			ip  = ip->next;
		}
	}
	return intres;
    
}
