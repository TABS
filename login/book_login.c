
/*
 * book_login: validate and record userlogin with the booking system
 *
 * 1/ determine userid and workstation
 * 2/ check if can login
 * 2a/ ifnot, report message and exit
 * 3/ record controling pid
 * 4/ tell booking system of login
 * 5/ write to log file
 *
 * Usage: book_login -w workstation -d display -u user -p pid
 */

#include	<stdlib.h>
#include	<unistd.h>
#include	"../interfaces/valinterface.h"
#include        "../database/database.h"
#include	<pwd.h>
#ifndef ultrix
#include	<sys/fcntl.h>
#else
#include	<sys/file.h>
#endif
#include	<getopt.h>

char control_pid_dir[] = "/var/book/xdm-pid/"; /*  /var/X11/xdm/xdm-pids */
static void lusage(char *m)
{
	if (m)
		fprintf(stderr, "book_login: %s\n", m);
	fprintf(stderr, "Usage: book_login -w workstation -d display"
		" -u user -p pid\n");
	exit(1);
}


int main(int argc, char *argv[])
{
    
	char *ws = NULL;
	bookuid_t who = -1;
	char *username = NULL;
	pid_t control = 0;
	int arg;
	val_usermrec info;
	struct passwd *pw;
	while((arg=getopt(argc,argv, "w:d:u:p:"))!= EOF)
		switch(arg) {
		case 'w':
		case 'd':
			if (ws != NULL)
				lusage("Only one `-w' or `-d' may be given");
			if (arg == 'd')
				ws = display2ws(optarg);
			else
				ws = optarg;
			break;
		case 'u':
			pw = getpwnam(optarg);
			username = optarg;
			if (pw == NULL || pw->pw_uid < 0)
				fprintf(stderr, "book_login: unknown user %s...\n", optarg);
			else
				who = pw->pw_uid;
			break;
		case 'p':
			control = atoi(optarg);
			break;
		default:
			lusage("unknown option");
			break;
		}

	if (who==0) exit(0); /* root, just say yes */

	canlogin(&info, who, ws, NULL);

	if (info.bookable == 0 ||
	    info.status == CANLOGIN) {
		if (who < 0) {
			printf("Ok to login\n");
			exit(0);
		}
		/* login is OK, try to record things */
		if (control>1 && ws != NULL) {
			char pidfile[200];
			int fd;
			strcpy(pidfile, control_pid_dir);
			strcat(pidfile, ws);
			fd = open(pidfile, O_WRONLY|O_CREAT|O_TRUNC, 0600);
			if (fd) {
				char n[20];
				sprintf(n, "%d\n", control);
				write(fd, n, strlen(n));
				close(fd);
			}
		}
	
		hasloggedin(who, ws, NULL);
	
#if 0
		log_login('+', 0, ws, NULL, username);
#endif
		exit(0);
	} else {
		/* print a message and fail */
		printf("Login denied - ");
		switch(info.status) {
		case OUTOFSERVICE:
			printf("this workstation is NOT IN SERVICE");
			break;
		case CLOSED:
			printf("this lab is currently CLOSED");
			break;
		case CLASSRESERVED:
			if (info.resname== NULL)
				printf("this workstation is currently reserved for a class of which you are not a member");
			else
				printf("this workstation is currently reserved for a member of %s", info.resname);
			break;
		case RESERVED:
			printf("this workstation is currently reserved for %s",
			       info.resname?info.resname:"another user");
			break;
		default:
			printf("no reason is given - please see the booking system administrator");
			break;
		}
		printf("\n");
		exit(1);
	}	
	return 0;
}
