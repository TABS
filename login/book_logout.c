
/*
 * book_logout: tell the booking system that a user has logged out
 *
 * 1/ determine username and workstation
 * 2/ tell booking system the nobody just logged on
 * 3/ write to log file
 * 4/ optionally kill all processes owned by the user
 *
 * Usage: book_logout -u user -w workstation -d display -K
 */

#include	<getopt.h>
#include	"../interfaces/valinterface.h"
#include	<pwd.h>
#include        <stdio.h>
#include        <stdlib.h>

static void lusage(char *m)
{
    if (m)
    fprintf(stderr, "book_logout: %s\n", m);
    fprintf(stderr, "Usage: book_logout -u user -w workstation -d display -K\n");
    exit(1);
}

int main(int argc, char *argv[])
{
    char *ws = NULL;
    bookuid_t who = -1;
    char *username;
    int arg;
    struct passwd *pw;
    int killoff = 0;
    extern char *optarg;
    
    while ((arg = getopt(argc, argv, "u:w:d:K"))!= EOF)
	switch(arg)
	{
	case 'w':
	case 'd':
	    if (ws != NULL)
		lusage("Only one `-w' or `-d' may be given");
	    if (arg == 'd')
		ws = display2ws(optarg);
	    else
		ws = optarg;
	    break;
	case 'u':
	    pw = getpwnam(optarg);
	    username = optarg;
	    if (pw == NULL)
		fprintf(stderr, "book_logout: unknown user %s...\n", optarg);
	    else
		who = pw->pw_uid;
	    break;
	case 'K':
	    killoff = 1;
	    break;
	default:
	    lusage("unknown option");
	    break;
	}

    hasloggedin(NOONE, ws, NULL);
#if 0
    if (username)
	log_login('-', 0, ws, NULL, username);
#endif
    if (killoff)
	killuser(who);
    exit(0);
}
