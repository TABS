
/* map display name to workstation name.
 *
 * split off host part: if it empty or "unix", set to hostname
 * split off display part: discard screen number
 * try host:display - is it fails retry with less of the hostname
 * if host disappears, then giveup
 *
 */

#include	<sys/types.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	<string.h>
#include	"../db_client/db_client.h"

char *display2ws(char *display)
{
    char host[256];
    char disp[100];
    char *colon;
    int len;
    int wsnum = -1;

    colon = strchr(display, ':');
    if (colon==NULL) return NULL;

    strcpy(host, display);
    host[colon-display] = '\0';
    strcpy(disp, colon);
    
    if (host[0]=='\0' || strcmp(host,"unix")==0)
    {
	gethostname(host,256);
    }

    colon = strchr(disp, '.');
    if (colon) *colon='\0';
    
    len = strlen(host);
    while (wsnum== -1 && len > 0)
    {
	strcpy(host+len, disp);
	wsnum = get_mappingint(host, M_WORKSTATION);
	len--;
	while (len>0 && host[len]!= '.') len--;
    }

    if (wsnum < 0)
	return NULL;
    return get_mappingchar(wsnum, M_WORKSTATION);
}
