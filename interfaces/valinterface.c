#include	"valinterface.h"
#include	<time.h>

/* results from can login:
    NOBOOKSYSTEM
    OUTOFSERVICE
    CLOSED
    RESERVED username
    CLASSRESERVED classname
    NOTBOOKED
    CANLOGIN

    plus hosttotry
    plus bookable
    */
    
void canlogin(val_usermrec * rec, bookuid_t user, char * wsname, char * host)
{
	int mywsid;
	int mylabid;
	int bstatus;
	int firm_alloc = 0;
	time_t alloctime;
	table tab;
	int i;
	int whohere;

	rec->bookable = FALSE;

	open_status_client((host != NULL) ? host : "localhost");
	if (status_client == NULL)
	{
		rec->status = NOBOOKSYSTEM;
		return;
	}

	if (wsname == NULL)
	{
		rec->status = NOBOOKSYSTEM;
		return;
	}
    
	mywsid = get_mappingint(wsname, M_WORKSTATION);
	if (mywsid == -2)
		mywsid = get_mappingint_cache(wsname, M_WORKSTATION);
	if (mywsid == -1)
	{
		rec->status = NOBOOKSYSTEM;
		return;
	}

	if ((collect_tab(&tab, status_client) == FALSE) ||
	    ( tab.table_len	== 0))
	{
		rec->status = NOBOOKSYSTEM;
		return;
	}
    

	/* assume table sorted alloccluster then all ws clusters in that lab */
	for (i=0;i < tab.table_len;i++)
	{
		if (Cluster_type(&tab,i) == ALLOCCLUSTER)
		{
			mylabid = Cluster_id(&tab, i);
/*	    labstatus = Alloc_status(&tab,i);  */
			alloctime = Cluster_modtime(&tab, i);
		}
		if (Cluster_type(&tab, i) == WSRESCLUSTER 
		    && Cluster_id(&tab,i)== mywsid)
		{
			whohere = Res_user(&tab,i);
			bstatus = Res_status(&tab,i);
			if (tab.table_val[i].data.data_len >= 3)
				firm_alloc = Res_status2(&tab, i);
			alloctime = Cluster_modtime(&tab,i);
			break;
		}
	}
	if (i == tab.table_len)
	{
		/* workstation not found!! */
		rec->status = NOBOOKSYSTEM;
		return;
	}
	rec->whereami = NULL;
	if (user >= 0)
		for (i=0 ; i<tab.table_len;i++)
		{
			if (Cluster_type(&tab,i) == WSRESCLUSTER
			    && Res_status(&tab,i) == NODEALLOCATED
			    && Res_user(&tab,i) == user)
				rec->whereami =
					get_mappingchar_cache(Cluster_id(&tab,i), M_WORKSTATION);
		}
	switch(bstatus)
	{
	case NODEOUTOFSERVICE:
		rec->status = OUTOFSERVICE;
		rec->bookable = TRUE;
		break;
	case NODEBROKEN:
	case NODENOTALLOC:
	case NODETENTATIVE:
		/* anyone can log on here */
		rec->status = CANLOGIN;
		break;
	
	case NODENOTBOOKABLE:
		/* anyone can log on here too */
		rec->status = CANLOGIN;
		rec->bookable = FALSE;
		break;
	
	case NODEALLOCATED:
		/* can only log in if in class, or if allocation is old */
		rec->bookable = TRUE;
		if (whohere == -2)
		{
			/* lab closed, unless in neverclose */
			char *labname = get_mappingchar_cache(mylabid, M_ATTRIBUTE);
			if (user >= 0 && labname && in_neverclose(user, labname)!= 0)
				rec->status = CANLOGIN;
			else
				rec->status = CLOSED;
		}
		else
		{
			long age = time(0) - alloctime;
			int grace =  get_configint("grace");
			if (grace <= 0)
				grace = 7*60;
			if (time(0)<alloctime) age=0;
			if (age > 40*60 || (!firm_alloc && age > grace))
			{
				/* an old allocation */
				rec->status = CANLOGIN;
			}
			else if (user >= 0 && in_class(user, whohere) != 0)
				rec->status = CANLOGIN;
			else
			{
				if (whohere >= get_class_min())
					rec->status = CLASSRESERVED;
				else
					rec->status = RESERVED;
				rec->resname = find_username(whohere);
			}
		}
		break;
	}
	if (get_book_level()<=0) rec->bookable = 0;
	if (user >=0 && (rec->status == CLOSED || rec->status == CLASSRESERVED || rec->status == RESERVED) && in_exempt(user)!= 0)
	        rec->status = CANLOGIN;
	    
	xdr_free((xdrproc_t)xdr_table, (char*) &tab);
}


int hasloggedin(bookuid_t user, char * wsname, char * host)
{
	static ws_user_pair arg;
	bool_t * result;
	char *hostname = host;

	if (wsname == NULL) return 0;
	set_whoison(wsname, user);
    
	if (hostname == NULL)
		hostname = get_myhostname();

	arg.userid = user;
	arg.hostid = get_mappingint(hostname, M_HOST);
	if (arg.hostid == -2)
		arg.hostid = get_mappingint_cache(hostname, M_HOST);
	arg.wsid = get_mappingint(wsname, M_WORKSTATION);
	if (arg.wsid == -2)
		arg.wsid = get_mappingint_cache(wsname, M_WORKSTATION);

	if (arg.wsid < 0 || arg.hostid < 0)
		return 0;
    
	if (status_client == NULL ||
	    (result = new_user_2(&arg, status_client)) == NULL)
	{
		open_status_client((host != NULL) ? host : "localhost");
		if (status_client == NULL ||
		    (result = new_user_2(&arg, status_client)) == NULL)
			return 0;
	}
	/* record login in database */
	if (user != NOONE)
	{
		time_t now, then;
		book_set_claim *cp;
		book_change ch;
		int doy, pod;
		static workstation_state st;
		int lab;
		char key[20];

		get_attr_types();
		ch.chng = CLAIM_BOOKING;
		cp = & ch.book_change_u.aclaim;
		now = time(0);
		then = now + 7*60; /* a time in the "current" period FIXME use #define */
		get_doypod(&then, &doy, &pod);
		if (db_read(key_int(key, C_WORKSTATIONS, arg.wsid), &st, (xdrproc_t)xdr_workstation_state, CONFIG)== R_RESOK)
		{
			lab = desc_2_labind(&st.state);
			cp->slot = make_bookkey(doy, pod, lab);
			cp->user = user;
			cp->when = 0;
			cp->claimtime = ((now-then+30*60)<<4) | DID_LOGIN;
			cp->where = arg.wsid;
			send_change(&ch);
		}
	}
	return 1;
}
