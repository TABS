#include	"bookinterface.h"

#include	<unistd.h>
#include	<stdlib.h>
#include	<time.h>
#include	<stdio.h>

    

static users u_rec;
static intpairlist u_tokens;
static int user_id = -1;

void init_interface()
{
	if (!get_attr_types()
	    || !get_impl_list())
	{
		printf("Cannot communicate with booking system\n");
		exit(1);
	}
}

static reply_res set_user(bookuid_t  user)
{
	char k[20];

	user_id = user;
	xdr_free((xdrproc_t)xdr_users, (char*) &u_rec);

	return db_read(user_key(k, user), &u_rec, (xdrproc_t)xdr_users, BOOKINGS);
    
}

userbookinglist getbookings( bookuid_t user)
{
	if (user_id != user)
	{
		if (set_user(user) != R_RESOK)
			return NULL;
	}
	return u_rec.bookings;
}

userbookinglist getpastbookings( bookuid_t user)
{
	if (user_id != user)
	{
		if (set_user(user) != R_RESOK)
			return NULL;
	}
	return u_rec.pastbookings;
}

utilizationlist freeslots(tokenname tname, int doy, int pod)
{
	slotlist sl = NULL;
	slotgroup *s;
	static token tok;
	descpairlist all_descriptions;
	description *tok_desc;
	utilizationlist *ul;
	description timebits;
	descpairlist ptr;
	time_t now;
	struct tm *nowtm;
	char key[20];
	int adv_time;
	int attr;
	int openlabs;
	int token_id;
    
	memset(&tok, 0, sizeof(token));
	memset(&all_descriptions, 0, sizeof(descpairlist));
    
    
	/* get the token decsription */
	token_id = get_mappingint(tname, M_TOKEN);
	if (token_id == -1) {
		printf("Bad Token id for token %s\n", tname);
		return NULL;
	}
	if (db_read(key_int(key, C_TOKENS, token_id), &tok, (xdrproc_t)xdr_token, CONFIG)
	    != R_RESOK)
		return NULL;

	time(&now);
	nowtm = localtime(&now);

	adv_time = get_configint("book_advance");

	if (adv_time <= 0)
		adv_time = 4;
	if (nowtm->tm_yday + adv_time < doy)
		tok_desc = &tok.advance;
	else
		/* late booking */
		tok_desc = &tok.late;

	/* find out whether any of the labs are "open" now */
	openlabs=0;
	for (attr=0 ; attr<BITINDMAX ; attr++)
		if (query_bit(&attr_types.lab, attr) && query_bit(tok_desc, attr))
		{
			timebits = new_desc();
			set_a_bit(&timebits, attr);
			process_exclusions(pod, doy, &timebits);

			if (query_bit(&timebits, attr_types.open)
			    && mandatory_attrs(&attr_types.mandatory,  &timebits, tok_desc) == TRUE)
			{
				openlabs++;
			}
			free(timebits.item_val);
		}

	if (openlabs == 0) return NULL;
/*
  printf("Token description %s:",key);
  show_token(&tab, &tok);
*/

	if (db_read(str_key(HOST_SETS), &all_descriptions, (xdrproc_t)xdr_descpairlist, CONFIG)
	    != R_RESOK)
		return(NULL);

	ptr = all_descriptions;

	while (ptr != NULL)
	{
		if (desc_member(tok_desc, &ptr->data))
		{
			/* check lab is open */
			desc_cpy(&timebits, &ptr->data);
			process_exclusions(pod, doy, &timebits);

			if (mandatory_attrs(&attr_types.mandatory,  &timebits, tok_desc) != FALSE)
			{
				slotlist sl2;
				s = (slotgroup *)malloc(sizeof(slotgroup));
				sl2= (slotlist)malloc(sizeof(slotnode));
				sl2->data = s;
				sl2->next = sl;
				sl = sl2;
	    
				s->doy = doy;
				s->pod = pod;
				desc_cpy(&s->what, &ptr->data);
			}
			free(timebits.item_val);
		}
	
		ptr = ptr -> next;
	}
	xdr_free((xdrproc_t)xdr_descpairlist, (char*) &all_descriptions);
	ul = free_slots_2(&sl, db_client);
	if (ul)
		return *ul;
	else
		return NULL;
}

/* for ordinary user assume that their request is the same as the */
 /* token for the moment.  Also assume that they can only make one */
 /* booking at a time for any slot */

bool_t makebook2(bookuid_t user, slotgroup * slotname, tokenname tname, time_t
		 when, int count, bookuid_t who_by, int currtok)
{
	book_change arg;
	token tok;
	book_changeres result;
	time_t now;
	struct tm *nowtm;
	int tnum;
	char tkey[20];


	arg.chng = ADD_BOOKING;
	memset(&tok, 0, sizeof(tok));
    
	if ((tnum = get_mappingint(tname, M_TOKEN)) == -1) {
		shout("Bad token\n");
		return(FALSE);
	
	}
	arg.book_change_u.booking.slot = *slotname;

	if (db_read(key_int(tkey, C_TOKENS, tnum), &tok, (xdrproc_t)xdr_token, CONFIG)
	    != R_RESOK)
		return(FALSE);

	time(&now); nowtm = localtime(&now);
	if (nowtm->tm_yday + 4 < slotname->doy)
		arg.book_change_u.booking.mreq = tok.advance;
	else
		arg.book_change_u.booking.mreq = tok.late;
    
    
	arg.book_change_u.booking.when = when;
	arg.book_change_u.booking.who_for = user;
	arg.book_change_u.booking.who_by = who_by;
	arg.book_change_u.booking.tnum = tnum;
	if (currtok == -1)
		arg.book_change_u.booking.tok_cnt = currtok;
	else {
		intpairlist cp;
		cp = findintkey(u_rec.tokens, tnum);
		if (cp) currtok = cp->num;
		else currtok = 0;
		arg.book_change_u.booking.tok_cnt = currtok;
	}
	arg.book_change_u.booking.number = count;

	result = send_change(&arg);

	if (result != 0)
		printf("Error %d from server\n",result);
    
	return (result == 0)? TRUE : FALSE;
}

bool_t unbook(bookuid_t user, int when, book_key which)
{
	book_change arg;
	book_ch_ent buents[2];

	arg.chng = CHANGE_BOOKING;
	arg.book_change_u.changes.slot = which;
	arg.book_change_u.changes.chlist.book_ch_list_len = 1;
	arg.book_change_u.changes.chlist.book_ch_list_val = buents;
	buents[0].user = user;
	buents[0].when = when;
	buents[0].status = B_CANCELLED;
	buents[0].priv_refund = 0;
	buents[0].tentative_alloc = 0;
	buents[0].allocations.entitylist_len = 0;
	buents[0].allocations.entitylist_val = NULL;
    

	if (send_change(&arg)==0)
		return TRUE;
	else
		return FALSE;
}

bool_t refund(bookuid_t user, int when, book_key which)
{
	book_change arg;
	book_ch_ent buents[2];

	arg.chng = CHANGE_BOOKING;
	arg.book_change_u.changes.slot = which;
	arg.book_change_u.changes.chlist.book_ch_list_len = 1;
	arg.book_change_u.changes.chlist.book_ch_list_val = buents;
	buents[0].user = user;
	buents[0].when = when;
	buents[0].status = B_REFUNDED;
	buents[0].priv_refund = 1;
	buents[0].tentative_alloc = 0;

	buents[0].allocations.entitylist_len = 0;
	buents[0].allocations.entitylist_val = NULL;

	if (send_change(&arg)==0)
		return TRUE;
	else
		return FALSE;
}

/* TOKEN_PERIOD: returns a list of pairs of days of the year that */
 /* define the weeks the token is valid for.  Returns NULL if no token */
 /* found */
period_spec * token_period(char * tname)
{
	token tok;
	int i;
	int cnt;
	period_spec * result;
	char key[20];
	int tnum;

	tnum = get_mappingint(tname, M_TOKEN);
	memset(&tok, 0, sizeof(tok));

	if (db_read(key_int(key, C_TOKENS, tnum), &tok, (xdrproc_t)xdr_token, CONFIG)
	    != R_RESOK)
		return NULL;

	/* should select between advance spec and late */

	/* make some space for the result, this is overkill */
	result = (period_spec*)malloc(sizeof(period_spec) * impl_list.exlist.exlist_len);

	/* look at each index definied in the token, get info about it */
	/* from the exclusions table.  Want to know the day of the year */
	/* that the week starts and finishes */
	cnt = 0;
	for (i = 0; i < impl_list.total ; i++)
	{
		int a = impl_list.exlist.exlist_val[i].attribute;
#ifdef RET_PERIOD_NOT_VALID
		/* if in mand and NOT in adv and not other restrictions */
		if (!query_bit(&tok.advance, a) &&
		    query_bit(&attr_types.mandatory, a))
		{
			implication *im = &impl_list.exlist.exlist_val[i];
			if (im->startdow > 0
			    || im->enddow < 6
			    || im->starttime>0
			    || im->endtime < 24*60
			    || !null_desc(&im->included)
			    || !null_desc(&im->excluded))
				;
			else
			{
				result[cnt].start = im->startday;
				result[cnt].end = im->endday;
				cnt++;
			}
		}
#else
		if (query_bit(&tok.advance, a) &&
		    query_bit(&attr_types.mandatory, a))
		{
			int st, en;
			st = impl_list.exlist.exlist_val[i].startday;
			en = impl_list.exlist.exlist_val[i].endday;
			if (st > 0 && en > 0)
			{
				result[cnt].start = st;
				result[cnt].end = en;
				cnt++;
			}
		}
#endif
	}
	xdr_free((xdrproc_t)xdr_token, (char*) &tok);




	/* terminate the list */
	result[cnt].start = -1;
	return result;
}


/* FIND_USER_RES: find the user's reservation in the table */
int find_user_res(table * tab, int userid)
{
	int i;
	for (i=0;i < tab->table_len;i++)
		if ((Cluster_type(tab,i) == WSRESCLUSTER) &&
		    (Ws_user(tab,i) == userid)) 
			return i;
    
	return -1;
}

/* RECLAIM: attempts to reclaim a tentative booking in a lab for the */
 /* user */
reclaim_result reclaim(char * labname, int userid)
{
	CLIENT * labhandle;
	table labtab;
	hostinfo labdata;
	hostgroup_info hg;
	entityid lab_num;
	int index;
	char key[20];
	reclaim_result res;
	int partner_index;
    
	res.state = RECNO;
	res.host = NULL;

	memset(&labdata, 0, sizeof(labdata));
	memset(&hg, 0, sizeof(hg));
    
	/* try to talk to the lab */
	lab_num = get_mappingint(labname, M_ATTRIBUTE);
	if (lab_num < 0)
		return res;
	if (db_read(key_int(key, C_LABS, lab_num), &labdata, (xdrproc_t)xdr_hostinfo, CONFIG)
	    != R_RESOK)
		return res;
	if (db_read(key_int(key, C_HOSTGROUPS, labdata.clump), &hg, (xdrproc_t)xdr_hostgroup_info, CONFIG)
	    != R_RESOK)
	{
		xdr_free((xdrproc_t)xdr_hostinfo, (char*) &labdata);
		return res;
	}
	labhandle = up_host(&hg, FALSE);
	xdr_free((xdrproc_t)xdr_hostinfo, (char*) &labdata);
	xdr_free((xdrproc_t)xdr_hostgroup_info, (char*) &hg);
    
	if (labhandle == NULL)
		return res;

	/* get the allocation data from the lab */
	collect_tab(&labtab, labhandle);
    
	index = find_user_res(&labtab, userid);
	if (index == -1)
	{
		res.state = RECUNASSIGNED;
	}
	else
	{
		partner_index = lookup_table(&labtab, WSSTATUSCLUSTER,
					     Cluster_id(&labtab,index));

		res.host = get_mappingchar(Cluster_id(&labtab,index), M_WORKSTATION); 

		if (Ws_status(&labtab,partner_index) == NODEBROKEN)
		{
			res.state = RECDOWN;
		}
		else if (Ws_user(&labtab,partner_index) == -1)
		{
			res.state = RECUNUSED;
		}
		else if (Ws_user(&labtab, partner_index) == userid)
		{
			res.state = RECSELF;
		}
		else
			res.state = RECOK;
    
		if (res.state != RECNO)
		{
			/* contact the node to make the update */

			CLIENT * reclaim_node;
			static resinfo data ;
			char * host;
			bool_t * result;

	
			data.wsid = Cluster_id(&labtab,index);
			data.userid = userid;
			data.when = time(0)+5*60;
			if (Cluster_modtime(&labtab,index) > data.when)
				data.when = Cluster_modtime(&labtab,index)+1;

			res.when = data.when;
			result = res_user_time_2(&data, labhandle);
			if (result == NULL || (*result == FALSE))
				if (res.state == RECOK)
					res.state = RECNO;

			/* now maybe talk to the actualy host */
			host = get_mappingchar(Ws_whereon(&labtab,partner_index), M_HOST);
			if (host != NULL)
			{
				reclaim_node = clnt_create(host, BOOK_STATUS, BOOKVERS, "udp");
				if (reclaim_node != NULL)
				{
					result = res_user_time_2(&data, reclaim_node);
					if (result != NULL && (*result != FALSE))
						if (res.state == RECNO)
							res.state = RECOK;
					clnt_destroy(reclaim_node);
				}
				free(host);
			}
		}
	}
	xdr_free((xdrproc_t)xdr_table, (char*) &labtab);

	clnt_destroy(labhandle);
	return res;
}
/* INT_2_CHARPAIRLIST: converts a charpairlist into an int pair list */
void int_2_charpairlist(charpairlist * cl, intpairlist il, nmapping_type type)
{
	while (il  != NULL) {
		char * charval;
		charval = get_mappingchar(il->data, type);
		if (charval != NULL)
			inckey(cl, charval, il->num);
		il = il->next;
	}
}
static void free_tokens();
charpairlist gettokenlist(bookuid_t user)
{
	charpairlist result = NULL;
	reply_res res;

	/* get the user record always, as the token list may have changed*/
	res = set_user(user);
	if ( (res != R_RESOK) && (res != R_NOMATCH))
	{
		fprintf(stderr, "Bad status of user\n");
		return (NULL);
	}
	if (u_tokens)
		free_tokens();
	u_tokens = get_alloc_tokenlist(user);
	if (user < get_class_min())
		del_intlist_nodel(&u_tokens, &u_rec.tokens);
	int_2_charpairlist(&result, u_tokens, M_TOKEN);
	return result;
}

int priv_tok_avail(char *tokname)
{
	/* find out how many privileged versions of this token are available */
	int tnum;
	intpairlist ip;
	tnum = get_mappingint(tokname, M_TOKEN);
	if (tnum < 0)
		return 0;
	for (ip= u_rec.priv_tokens ; ip ; ip=ip->next)
		if (ip->data == tnum)
			return ip->num;
	return 0;
}

int token_is_reusable(char *tokname)
{
	int tnum = get_mappingint(tokname, M_TOKEN);
	char key[20];
	static token tok;
	if (tnum < 0)
		return 0;
               
	memset(&tok, 0, sizeof(token));
	if (db_read(key_int(key, C_TOKENS, tnum), &tok, (xdrproc_t)xdr_token, CONFIG)
	    != R_RESOK)
		return 0;

	if (query_bit(&tok.advance, attr_types.reusable))
		return 1;
	else
		return 0;
}

int *tokname2labs(char *tokname)
{
	/* returns a list of lab indexes, terminated by -1 */
	int token_id;
	static token tok;
	char key[20];
	description *tok_desc;
	int i, cnt;
	int *rv;
            
	memset(&tok, 0, sizeof(token));
    
    
	/* get the token decsription */
	token_id = get_mappingint(tokname, M_TOKEN);
	if (token_id == -1) {
		printf("Bad Token id for token %s\n", tokname);
		return(NULL);
	}
	if (db_read(key_int(key, C_TOKENS, token_id), &tok, (xdrproc_t)xdr_token, CONFIG)
	    != R_RESOK)
		return NULL;

	tok_desc = &tok.advance;

	cnt=0;
	for (i=0; i< BITINDMAX ; i++)
	{
		if (query_bit(&attr_types.lab, i)
		    && query_bit(tok_desc, i))
			cnt++;
	}
	rv = (int*)malloc((cnt+1)*sizeof(int));
	cnt=0;
	for (i=0; i< BITINDMAX ; i++)
	{
		if (query_bit(&attr_types.lab, i)
		    && query_bit(tok_desc, i))
			rv[cnt++] = i;
	}
	rv[cnt] = -1;
    
	return rv;
}


int *get_all_labs()
{
	int i, cnt;
	int *rv;
            
	cnt=0;
	for (i=0; i< BITINDMAX ; i++)
	{
		if (query_bit(&attr_types.lab, i))
			cnt++;
	}
	rv = (int*)malloc((cnt+1)*sizeof(int));
	cnt=0;
	for (i=0; i< BITINDMAX ; i++)
	{
		if (query_bit(&attr_types.lab, i))
			rv[cnt++] = i;
	}
	rv[cnt] = -1;
    
	return rv;
    
}

static token *u_tokeninfo;
static void get_tokens()
{
	/* make a u_tokeninfo array the same size as u_tokens list
	 * and populate it
	 */
	int c;
	intpairlist p;
    
	if (u_tokeninfo) free_tokens();
	if (u_tokens == NULL)
		u_tokens = get_alloc_tokenlist(user_id);
	for (c=0, p=u_tokens ; p ; c++, p=p->next)
		;
	u_tokeninfo =(token*)malloc(c*sizeof(token));
	memset(u_tokeninfo, 0, c*sizeof(token));
	for (c=0, p=u_tokens ; p ; c++, p=p->next)
	{
		char key[20];
		db_read(key_int(key, C_TOKENS, p->data), &u_tokeninfo[c], (xdrproc_t)xdr_token, CONFIG);
	}
}

static void free_tokens()
{
	int c;
	intpairlist p;

	if (u_tokeninfo)
	{
		for (c=0, p=u_tokens ; p ; c++, p=p->next)
		{
			xdr_free((xdrproc_t)xdr_token, (char*)&u_tokeninfo[c]);
		}
		free(u_tokeninfo);
	}
	u_tokeninfo = NULL;
	xdr_free((xdrproc_t)xdr_intpairlist, (char*)&u_tokens);
	u_tokens = NULL;
}



int check_bookable(int doy, int pod, int where)
{
	description timebits;
	int found=0;
	int c;
	intpairlist p;
    
	if (u_tokeninfo == NULL)
		get_tokens();
	timebits = new_desc();
	set_a_bit(&timebits, where);
	process_exclusions(pod, doy, &timebits);

	for (c=0, p=u_tokens ; !found && p ; c++, p=p->next)
	{
		/* fixme decide on advance or late... */
		if (mandatory_attrs(&attr_types.mandatory, &timebits, &u_tokeninfo[c].advance))
			found++;
	}
	return found;
}

int lab_excluded(int lab)
{
	/* returns true if lab is a mandatory attribute
	 * and is not in any token
	 */
	int c;
	intpairlist p;

	if (query_bit(&attr_types.mandatory, lab)==0)
		return 0;

	if (u_tokeninfo == NULL)
		get_tokens();
	for (c=0, p=u_tokens ; p ; c++, p=p->next)
	{
		/* fixme decide on advance or late... */
		if (query_bit(&u_tokeninfo[c].advance, lab)
		    ||query_bit(&u_tokeninfo[c].late, lab))
			return 0;
	}
	return 1;
}
