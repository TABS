
#include	"../db_client/db_client.h"

typedef char *tokenname;

#define RET_PERIOD_NOT_VALID

typedef struct charpairnode
{
    char *data;
    int num;
    struct charpairnode *next;
} charpairnode, *charpairlist;

charpairlist findkey(charpairlist, char*);

void init_interface(void);
userbookinglist getbookings(bookuid_t user);
userbookinglist getpastbookings(bookuid_t user);
charpairlist gettokenlist(bookuid_t user);
utilizationlist freeslots(tokenname tname, int doy, int pod);
bool_t makebook(bookuid_t user, slotgroup *slotname, tokenname tname, time_t
		when, int count, bookuid_t whoby);
bool_t makebook2(bookuid_t user, slotgroup * slotname, tokenname tname, time_t
		 when, int count, bookuid_t who_by, int currtok);
bool_t unbook(bookuid_t user, int when, book_key which);
bool_t refund(bookuid_t user, int when, book_key which);

typedef struct period_spec
{
    int start;
    int end;
} period_spec;

period_spec * token_period(char *);

typedef enum reclaim_state {
    RECNO= 1,  /* network problem */
    RECUNASSIGNED=2, /* No reservation for the current period in lab */
    RECDOWN=3, /* the node is down.. */
    RECUNUSED =4, /* the node is not being used... go get it */
    RECSELF =5, /* the node is being used by you YOU CLOT!! */
    RECOK =10 /* booking status changed from tentative to 'hard' */
	      /* succesfully */
} reclaim_state;

typedef struct  reclaim_result {
    reclaim_state state;
    char * host;
    time_t when;
} reclaim_result;

reclaim_result reclaim(char * lab, int userid);
int* tokname2labs(char* tokname);

book_changeres inckey(charpairlist *ch, char * key, int diff);
int priv_tok_avail(char *tokname);
int token_is_reusable(char *tokname);

int *get_all_labs(void);
int check_bookable(int doy, int pod, int where);
int lab_excluded(int lab);
