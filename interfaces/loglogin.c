
#include	<sys/types.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	<stdio.h>
#include	<sys/stat.h>
#include	<time.h>
#include	<fcntl.h>

static char LogFile[] = "/var/book/logins";

void log_login(char type, time_t when, char *workstation, char *host, char *user)
{
    char *buf;
    struct tm *tm;
    static int sfd = -1;
    int fd = sfd;
    char hname[256];

    if (user == NULL)
	user = "(nobody)";
    if (workstation == NULL)
	workstation = "(console)";
    if (fd == -1)
    {
	fd = open(LogFile, O_WRONLY|O_CREAT|O_APPEND, 0640);
	fcntl(fd, F_SETFD, 1);
    }
    else fd = dup(fd);
    if (type == 0)
    {
	sfd = fd;
	return; /* just opening file */
    }

    if (host == NULL)
    {
	host = hname;
	gethostname(host, 256);
    }
    if (when == 0)
	time(&when);
    
    buf = malloc( 1+1+13+1+strlen(workstation)+1+strlen(host)+5+strlen(user)+1+10);
    tm = localtime(&when);
    sprintf(buf, "%c %02d%02d%02d.%02d%02d%02d %s %s %s\n",
	    type, tm->tm_year, tm->tm_mon+1, tm->tm_mday,
	    tm->tm_hour, tm->tm_min, tm->tm_sec,
	    workstation, host, user);

    if (fd >= 0)
    {
	fchmod(fd, 0640);
	fchown(fd, 0, 0);
	write(fd, buf, strlen(buf));
    }
    free(buf);
    close(fd);
}
