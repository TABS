
/*
 * kill off all processes owned by a given user.
 *
 * on most (posix) OSes, fork, setuid, and send signal to proc -1
 * on ultrix, setuid, do a "ps" and signal every process.
 *
 */

#include	<unistd.h>
#include	<stdlib.h>
#include	<sys/wait.h>
#include	<sys/types.h>
#include	<signal.h>
#include	<errno.h>
#include	<stdio.h>

#ifdef ultrix
#define KILLALLFAILS
#endif

void killuser(int uid)
{
    pid_t pid;
    int st;
    if (uid <= 0) return;
#ifndef KILLALLFAILS
    switch(pid = fork())
    {
    case 0:
	setuid(uid);
	if (kill(-1, SIGHUP) != 0 && errno == ESRCH) exit(0);
	sleep(5);
	if (kill(-1, SIGTERM) != 0 && errno == ESRCH) exit(0);
	sleep(15);
	if (kill(-1, SIGKILL) != 0 && errno == ESRCH) exit(0);
	exit(0);
    case -1:
	break;
    default:
	setuid(0); /* make sure I don't get killed */
	while(wait(&st) != -1)
	    ;
	break;
    }
#else
#ifdef ultrix
    /* fork a ps to find processes to kill */
{
    int pfd[2];
    FILE *psf;
    char buf[200];
    int pids[500];
    int pcnt = 0;
    int i;
    pipe(pfd);
    switch(pid = fork())
    {
    case 0:
	close(1);
	close(pfd[0]);
	dup2(pfd[1], 1);
	close(pfd[1]);
	execl("/bin/ps", "ps", "axgl", 0);
	exit(100);
    case -1:
	break;
    default:
	setreuid(0, uid);
	close(pfd[1]);
	psf = fdopen(pfd[0], "r");
	while (fgets(buf, sizeof(buf), psf)!= NULL)
	{
/*
01234567890123456789   
      F UID   PID  PPID CP PRI NI ADDR  SZ  RSS WCHAN STAT  TT  TIME COMMAND
      3   0     0     0  0 -25  0  2fb   0    0 90a1d D     ?   0:01  swapper
10000001   0     1     0  0   5  0  9b2 252  212 4b3a8 I     ?   0:04 /etc/init
      3   0     2     0  0 -24  0  6a74096    0 4b4e0 D     ?   0:00  pagedaemo
      3   0     3     0  0 -25  0  6ab   0    0       R     ?   0:00  idleproc
1008001   0    45     1  0   1  0  757 100   56 90a5c S     ?   6:23 /etc/route
1000001   0    72     1  1   1  0  860  96   60 90a5c S     ?   9:31 /etc/portm
110080011443 27012     1  0  15  0 1550 416  140 fc000 S     p3  0:03 netstat -I
110080011443 27013     1  0  15  0 1615 416  132 fc000 S     p3  0:03 netstat -I
110080011443 27014     1  0  15  0  f89 400  140 fc000 S     p3  0:09 vmstat 2
110080011443 27015     1  1  15  0 1731 388  172 fc000 S     p3  5:03 iostat 2
01234567890123456789
Don't bother getting uid, just try to kill everything 
*/
	    char *st;
	    int p;
	    if (!isdigit(buf[6])) continue; /* proabably the header line */
	    st = buf + 11;
	    while (*st != ' ' && st < buf+17) st++;
	    if (*st != ' ') continue;
	    while (*st == ' ') st++;
	    if (st > buf+17) continue;
	    p = atoi(st);
	    if (p <= 1 || p == getpid() || p == pid)
		continue;
	    if (kill(p, SIGHUP)== 0)
	    {
		if (pcnt < 499) pids[pcnt++] = p;
		else kill(p, SIGKILL);
	    }
	    
	    
	}
	fclose(psf);
	wait(0);
	for (i=0; i<pcnt ; i++)
	    kill(pids[i], SIGTERM);
	for (i=0; i<pcnt ; i++)
	    kill(pids[i], SIGKILL);
	
	break;
    }
    setreuid(0, 0);
}
#else
  */ --- CANNOT KILL ALL PROCESSES!!! 
#endif /* ultrix */

#endif
}

