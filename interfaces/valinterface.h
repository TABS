
#include	"../db_client/db_client.h"


typedef enum val_result {
    NOBOOKSYSTEM,
    OUTOFSERVICE,
    CLOSED,
    RESERVED,
    CLASSRESERVED,
    NOTBOOKED,
    CANLOGIN
} val_result;

typedef struct val_umrec{
    val_result	status;
    bool_t	bookable;		/* is machine bookable? */
    char 	*resname;		/* who reserved for */
    char	*whereami;		/* workstation requesting user allocated to */
} val_usermrec;

#define NOONE (-1)

extern void canlogin(val_usermrec * rec, bookuid_t user, char * wsname, char * host);
extern int hasloggedin(bookuid_t user, char * wsname, char * host);
extern char *display2ws(char*);

void log_login(char type, time_t when, char *workstation, char *host, char *user);
void killuser(uid_t uid);
