/* some routines for adding and deleting from lists of strings and numbers */

#include	"../interfaces/bookinterface.h"

book_changeres add_to_charpairlist(charpairlist * ch, char * new, int num)
{
    charpairlist new_el;

    /* maybe check if present */

    new_el = (charpairlist) malloc (sizeof(charpairnode));

    new_el -> data = strdup(new);
    new_el -> num = num;
    new_el -> next = *ch;
    *ch  = new_el;
    return C_OK;
}

void free_charpairlist(charpairlist l)
{
    if (l->next)
	free_charpairlist(l->next);
    free(l->data);
    free(l);
}

book_changeres del_from_charpairlist(charpairlist * ch, char *name)
{
    charpairlist ptr = *ch;
    charpairlist trailer = ptr;  /* this points to the prev entry in the */
			     /* list to do deletions, easier than */
			     /* doubly linked list */
    book_changeres result = C_NOMATCH;


    while (ptr != NULL) {
	if (( strcmp(ptr -> data, name) == 0)) {

	    /* delete !! */
	    if (ptr == trailer) {  /* first node */
		if (ptr -> next == NULL) /* only one node in list*/
		    *ch = NULL;
		else
		    *ch = ptr -> next;
	    }
	    else
		trailer->next = ptr-> next;
	    /* cut the node off */
	    ptr -> next = NULL;
	    free_charpairlist(ptr);
	    result = C_OK;
	}
	else { /* go to the next element in the list */
	    trailer = ptr;
	    ptr = ptr->next;
	}
    }
    return(result);
}

charpairlist findkey(charpairlist ch, char * key)
{
    charpairlist ptr = ch;
    while ((ptr != NULL) && (strcmp(ptr->data,key) != 0) ) {
	ptr = ptr->next;
    }
    return(ptr);
}

/* if the key is present in the list inc it's value, else add it to */
 /* the list with a value of 1 operation should always suceed*/
book_changeres inckey(charpairlist *ch, char * key, int diff)
{
    charpairlist val;
    if ((val = findkey(*ch,key)) == NULL) /* add new element to list */
	return(add_to_charpairlist(ch,key,diff));
    else 
	val->num += diff;
#if 0
Dont do this as we are interested in tokens that are all used up    
    if (val->num == 0) /* if this key is empty remove it */
	return( del_from_charpairlist(ch,key));
#endif
    return(C_OK);
}


void merge_lists(charpairlist * tot, charpairlist * others)
{
    charpairlist ptr = *others;

    while (ptr != NULL) {
	inckey(tot, ptr->data, ptr->num);
	ptr = ptr ->next;
    }

}

void del_list(charpairlist * tot, charpairlist * subs)
{
    charpairlist ptr = *subs;
    while (ptr != NULL) {
	inckey(tot, strdup(ptr->data), -ptr->num);
	ptr = ptr->next;
    }
}
