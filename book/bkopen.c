/*
 * Read the system book period spec file, and print out the system opening
 *	and closing times for the next 7 days.
 * This program was based on boo.c - the book interface main program, and
 *	includes many of the modules used in that program.
 * Note:	If compiled with DEBUG defined,
 *		will allow user to interactively define, exclude, and inspect periods
 *		before printing opening and closing times.
 */

#include	<stdio.h>
#include	<ctype.h>
#include	<time.h>
#include	"boo.h"

extern	struct tm	tnow;	/* current time - tm format */
extern	time_t		now;	/* current time - seconds */
extern	int	nexttoken(FILE *fptr);			/* blex.c */
extern	command_type	*getcommand(FILE *fptr);	/* bparse.c */
extern	void	rfree(period *pp);			/* bparse.c */
extern	void	b_define(command_type *comm);		/* bperiod.c */
extern	void	b_undefine(command_type *comm);		/* bperiod.c */
extern	period	empty_abs[];				/* bperiod.c */
extern	time_t	currtime();				/* bperiod.c */
extern	void	initperiods();				/* bperiod.c */
extern	period	*newperiod(int type, time_t start, time_t end, period *next);
extern	time_t	construct(int year, int month, int day, int hour, int minute);
extern	period	*intersect(period *p1, period *p2);	/* bparse.c */
extern	period		*valid_abs, *valid_hour, *valid_day;

/********** global variables **************/

char	*infile;		/* the name of the file currently being read */
#ifdef DEBUG
    char	*sys_cfile =	"periods";
#else
    char	*sys_cfile =	"/usr/local/book/periods";
#endif
int	interrupt = 0;
int	parse_err = 0;		/* set of [ FATAL, RECOVER ]  - NOT CURRENTLY USED */
int	c_inline = 1;		/* the current unprocessed character */

/**** dummy routines to replace the real token routines in bcomm.c */

int mark_token(char *tokname)
{
    return 0;
}

int mark_all_tokens(int include_expired)
{
}

strlist *intokenlist(char *tokname, strlist *strings)
/* return the entry in the strings list matching tokname */
{
    while (strings != NULL && strcmp(strings->str_val, tokname))
	strings = strings->next;
    return (strings);
}

void printcommand(command_type *cp)
/* a debug routine to print out commands, tokens, periods */
{
#ifdef DEBUG
    strlist	*sp;
    if (cp != NULLC)
    {
	printf("Command:\n\t%s\n", typevaltostr(COMND, cp->command));

	puts("Options:");
	for (opt = cp->options; (opt != NULL); opt = opt->next)
	    printf("\t%d = %d\n", opt->option, opt->value);

	puts("Strings:");
	if (cp->strlist != NULL)
	    for (sp = cp->strlist; (sp != NULL); sp = sp->next)
		printf("\t%s\n", sp->str_val);

	puts("Periods:");
	if (cp->period != NULLP)
	{
	    putchar('\t');
	    printperiod(cp->period, "\t");
	}
	else puts("\tNo period passed");
    }
    else puts("Null command");
#else
    fprintf(stderr, "Ignore command: %s\n", typevaltostr(COMND, cp->command));
#endif
}

int readcommands(char *fname)
/* The main loop to read book commands.
 * Used to read setup files and interactive commands (hence fname).
 */
{
    FILE *fptr;
    char *prompt = "BOPEN > ";
    command_type	*cp;
    int quit = 0;

    if (fname != NULL)
    {
	if ((fptr = fopen(fname, "r")) == NULL) return 0;
    }
    else fptr = stdin;

    infile = fname;
    c_inline = 1;
    if (fptr == stdin) printf(prompt);	/* only prompt interactive input */
    while (! quit && nexttoken(fptr) != END)
    {
	if (! interrupt)
	{
	    parse_err = 0;
	    cp = getcommand(fptr);
	    if (cp != NULLC)
	    {
		if (cp->period == empty_abs)
		    puts("\tPeriod spec maps to an empty or invalid period");
		else
		{
		    switch (cp->command)
		    {
		    case DEFINE: b_define(cp); break;
		    case UNDEF:	b_undefine(cp); break;
		    default:	printcommand(cp); break;
		    }
		    if (cp->command != DEFINE) rfree(cp->period);
		}
	    }
	}
	if (fptr == stdin && ! quit) printf(prompt); /* only prompt interactive input */
	if (interrupt) interrupt = 0;
    }
    if (fptr != stdin) fclose(fptr);
    return 1;
}

char	*dayofweek[] = { "su", "mo", "tu", "we", "th", "fr", "sa" };

void printdaytime(time_t timeofday, time_t startofday)
{
    time_t	secofday = timeofday - startofday;
    printf("%02d%02d", secofday/SECINHOUR, (secofday%SECINHOUR)/SECINMIN);
}

void printstartend(period *periods, time_t startofday)
{
    period *pp;
    if (periods != NULL)
    {
	printdaytime(periods->start, startofday);
	for (pp = periods; (pp->next != NULL); pp = pp->next);
	putchar('-');
	printdaytime(pp->end, startofday);
    }
    else puts("0000-0000");
}

void print_opening_times()
{
    int		day;
    time_t	start;
    period	*dayperiod, *pp;
    struct tm	*tp;
    
    start = construct(1900+tnow.tm_year, 0, tnow.tm_yday, 0, 0);
    dayperiod = newperiod(ABSOLUTE, start, start+SECINDAY, NULL);
    for (day = 1; (day <= 7); day++)
    {
	if (day != 1) putchar(',');
	tp = gmtime(&start);
	printf("%s", dayofweek[tp->tm_wday]);
	pp = intersect(valid_abs,
		       intersect(valid_day,
				 intersect(valid_hour, dayperiod)));
	printstartend(pp, start);
	rfree(pp);
	start += SECINDAY;
	dayperiod->start = start;
	dayperiod->end = start + SECINDAY;
    }
    putchar('\n');
}

main(int argc, char **argv)
{
    currtime();
    initperiods();

    /* read in the period specification files */
    if (! readcommands(sys_cfile)) perror(sys_cfile);

#ifdef DEBUG
    readcommands((char *) NULL);
    puts("");
#endif
    
    /* now print out the next seven days */
    print_opening_times();
}
