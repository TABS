
/*
 * Book interface module:
 *	Basic Time related routines
 *	Period manipulating routines
 *	Parsing routines (recursive descent)
 */

#include	<stdio.h>
#include	<time.h>
#include	<stdlib.h>
#include	<rpc/rpc.h>
#include	"../interfaces/bookinterface.h"
#define BOOK_INTERFACE
#include	"boo.h"

/* from boo.c - main */
extern	struct tm	tnow;	/* current time - tm format */
extern	time_t		now;	/* current time - seconds */
extern	int		parse_err;	/* set of [ FATAL | RECOVER ] */
extern	int		c_inline;	/* input line number */
extern	char		*infile;	/* input file name */
extern	plist		*predefined;
extern	period		*valid_abs, *valid_hour, *valid_day;
extern	period		empty_abs[];

#ifdef DEBUG
extern	int		debug;
#endif

#include	"blex.h"
extern	tok	*in_token, *lookahead;			/* blex.c */
extern	void	printperiod(period *pp, char *sep);	/* bperiod.c */
extern	void	mark_all_tokens(int alltokens);		/* bcomm.c */

/******* Globals ********/

int	level = 0;		/* parenthetical nest level */
int	command_no = 0;		/* current command no. - used to mark tokens */
static int daysinmonth[12] =
{
	31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

/********* debug / error routines ********/

void printerror(int err, char *msg)
{
	if (infile != NULL)
		fprintf(stderr, "%s -  Line %d: ",
			infile, (in_token->tok_type == EOLN ? c_inline-1 : c_inline));
	fprintf(stderr, "%s '%s'\n", msg, in_token->tok_str?in_token->tok_str:"???"); /* FIXME zain ! this can violate segnments */
	
	parse_err |= err;
}

/********** basic time related routines **********/

int leapyear(int y)
{
	return (((y % 4) == 0 && (y % 100) != 0) || (y % 400 == 0)) ? 1 : 0;
}

int checkdate(int year, int month, int day)
/* check that day month year are legal */
{
	int		dim, ret;

	if ((ret = (year >= 1970 || year <= 2038)))
	{
		dim = daysinmonth[month] + (month == 1 ? leapyear(year) : 0);
		ret = (day <= dim);
		if (! ret)
			fprintf(stderr, "Month day out of range. (> %d)\n", dim);
	}
	else fprintf(stderr,"Year out of range: %d\n", year);
	return ret;
}

int checktime(int hour, int minute)
/* check that hour minute are legal */
{
	int	ret;
	if (! (ret = ((hour <= 23 && minute <= 59) || (hour == 24 && minute == 0))))
		fprintf(stderr,"Illegal time: %02d:%02d\n", hour, minute);
	return ret;
}

time_t construct(int year, int month, int day, int hour, int minute)
/* On UNIX, by convention, time = seconds since 00:00GMT Thursday, 1 Jan 1970 */
{
	register int		i, days;

	days = 0;
	for(i = 1970; i < year; i++)
		days += 365 + leapyear(i);
	for(i = 0; i < month; i++)
		days += daysinmonth[i] + (i == 1 ? leapyear(year) : 0);
	days += day;		/* days since 1 Jan 1970 */
	return (time_t) days * SECINDAY + hour * SECINHOUR + minute * SECINMIN;
}

time_t weekstart(time_t t)
/* Get the start of the week containing time t.
 * The week starts Monday, and we depend on 1.1.70 being a Thursday
 */
{
	return  t - (t + (THU-MON)*SECINDAY) % SECINWEEK;
} /* weekstart */

time_t time_2_dp(time_t t, int *dayofyear, int *periodofday)
/* return start of period that contains (gm)time t,
 * also return (thro param) day of year and period of that day of this period.
 * Note: Period must be in current year.
 */
{
	struct tm	*tp;
	time_t	ret, secofday;
	tp = gmtime(&t);
	if (tp->tm_year == tnow.tm_year)
	{
		secofday = tp->tm_hour * SECINHOUR + tp->tm_min * SECINMIN + tp->tm_sec;
		*periodofday = secofday / SECINPERIOD;
		*dayofyear = tp->tm_yday;
		ret = t - (secofday - *periodofday * SECINPERIOD);
	}
	else ret = 0;
	return ret;
}

/************ basic period operations **************/

void rfree(period *pp)
/* recursively free period list pp */
{
	period	*tp;
	while (pp != NULLP)
	{
		tp = pp;
		pp = pp->next;
		free(tp);
	}
}

period *newperiod(int type, time_t start, time_t end, period *next)
/* malloc and return a new period with values assigned */
{
	period *pp;
	if ((pp = MALLOC(period)) != NULL)
	{
		pp->type = type;
		pp->start = start;
		pp->end = end;
		pp->next = next;
	}
	return pp;
} /* newperiod */

period *copyperiods(period *periods)
/* return a copy of the period list passed in periods */
{
	period *pp, *start, *end, *newp;

	start = NULLP;
	for (pp = periods; (pp != NULLP); pp = pp->next)
	{
		if ((newp = newperiod(pp->type, pp->start, pp->end, NULL)) != NULL)
		{
			if (start == NULLP)
				start = newp;
			else end->next = newp;
			end = newp;
		}
	}
	return start;
}

period *mergeperiods(period *p1, period *p2)
/*
 * merge both ordered period lists p1 and p2 into one ordered list
 * lists are ordered in increasing time order
 * destroys p1 and p2 in the process.
 */
{
	period	*pp, *lo, *mid, *hi;

	if (p1 == NULLP)
		pp = p2;
	else if (p2 == NULLP)
		pp = p1;
	else
	{

		if (p1->start < p2->start)
		{
			pp = lo = p1;
			hi = p2;
		}
		else
		{
			pp = lo = p2;
			hi = p1;
		}
		mid = lo->next;

		/* invariant lo(s,e) <= mid(s,e) && lo(s) <= hi(s) */
		while (mid != NULLP && hi != NULLP)
		{
			if (mid->start <= hi->start)
				lo = mid;
			else
			{
				if (hi->start-1 <= lo->end)
				{
					if (lo->end < hi->end)
					{
						lo->end = hi->end;
						lo->next = hi->next;
					}
					else mid = hi->next;
					free(hi);
				}
				else
				{
					lo->next = hi;
					lo = hi;
				}
				hi = mid;
			}
			mid = lo->next;
		}
		if (mid == NULLP && hi != NULLP) {
			if (hi->start-1 <= lo->end)
			{
				if (lo->end < hi->end)
					lo->end = hi->end;
				lo->next = hi->next;
				free(hi);
			}
			else lo->next = hi;
		}
	}

	return pp;
} /* mergeperiods */

period *range(period *p1, period *p2)
/* A period which is the range of two other periods is the period starting
 * when the first period starts, and ending either before the second period
 * starts, or when the second period ends (depending on the type of period).
 * We must ensure that both periods are singular, and of the same type.
 * destroys p1 and p2.
 */
{
	period	*pp;

	if (p1 == NULLP || p2 == NULLP)
		pp = NULLP;
	else if (p1->next != NULLP || p2->next != NULLP)
	{
		fputs("Range must be from only one time to only one other\n", stderr);
		pp = NULLP;
	}
	else if (p1->type != p2->type)
	{
		fputs("Range must be between equivalent types\n", stderr);
		pp = NULLP;
	}
	else
	{
		pp = p1;
#if 0
		if (pp->type == HOUR)
			pp->end = p2->start-1;	/* one second before start of p2 */
		else
#endif
			pp->end = p2->end;
		if (pp->start > pp->end)
			switch (pp->type)
			{
			case HOUR: pp->end += SECINDAY; break;
			case DAY: pp->end += SECINWEEK; break;
			case ABSOLUTE:
				fputs("End of period is before Start of period\n", stderr);
				pp = NULLP; break;
			}
	}
	if (p1 != NULLP && pp != p1) rfree(p1);
	if (p2 != NULLP) rfree(p2);

#ifdef DEBUG
	if (debug & DB_CALL) puts("range(p1,p2)\n");
	if (debug & DB_PERIOD) printperiod(pp, "");
#endif
	return pp;
} /* range */

period *periodunion(period *p1, period *p2)
/* Periods must be of the same type */
{
	period	*pp;

	if (p1 != NULLP && p2 != NULLP)
	{
		if (p1->type != p2->type)
		{
			printerror(FATAL, "Union must be between equivalent types");
			fputs("Union must be between equivalent types\n", stderr);
			rfree(p1); rfree(p2);
			p1 = p2 = NULLP;
		}
	}

	pp = mergeperiods(p1, p2);
	/*
	  if (p1 != NULLP)
	  {
	  for (pp = p1; (p1->next != NULLP); p1 = p1->next);
	  p1->next = p2;
	  }
	  else pp = p2;
	*/
#ifdef DEBUG
	if (debug & DB_CALL) printf("periodunion(p1,p2)\n");
	if (debug & DB_PERIOD) printperiod(pp, "");
#endif
	return pp;
} /* periodunion */

period *common(period *p1, period *p2)
/*
 * Precondition: p1->type == p2->type;
 * return the common period (intersection) of p1 with p2.
 */
{
	period	*pp = NULLP;
	if (p1->start < p2->start)
	{
		if (p2->start < p1->end)
			pp = newperiod(p1->type, p2->start,
				       (p1->end < p2->end) ?  p1->end : p2->end, NULLP);
	}
	else /* p2->start <= p1->start */
		if (p1->start < p2->end)
			pp = newperiod(p1->type, p1->start,
				       (p1->end < p2->end) ?  p1->end : p2->end, NULLP);
	return pp;
} /* common */

period *rcommon(period	*rp, period *p)
/*
 * Precondition: rp is a relative period to be applied to the period p
 */
{
	int		rtype;
	time_t	tstart, tend, periodicity;
	period	*pp;

	tstart = rp->start;	/* save values of rp */
	tend = rp->end;
	rtype = rp->type;

	/* convert rp into period of type: p->type */
	if (rtype == HOUR)
	{
		rp->start = (p->start / SECINDAY) * SECINDAY;
		periodicity = SECINDAY;
	}
	else if (rtype == DAY)
	{
		rp->start = weekstart(p->start);
		periodicity = SECINWEEK;
	}
	else
	{
		fputs("Snark: bad relative period type\n", stderr);
		return NULLP;
	}

	rp->end = rp->start + tend;
	rp->start += tstart;
	rp->type = p->type;

	pp = NULLP;
	while (rp->start < p->end)
	{
		pp = mergeperiods(pp, common(rp, p));
		rp->start += periodicity;
		rp->end += periodicity;
	}

	rp->type = rtype;	/* restore rp */
	rp->start = tstart;
	rp->end = tend;

	return pp;
} /* rcommon */

period *intersection(period *p1, period *p2)
/*
 * Precondition: lists p1 and p2 are not empty.
 * Only intersect the first element of p1 with the first of p2.
 * This routine deals with intersecting elements of different types
 * by converting the lesser type to the greater one (HOUR << DAY << ABSOLUTE)
 * Try to maintain list (time) ordering (add new elements to end - not front
 * which would be easier).
 * This is done so that times remain in increasing order.
 * (But will they, and is this important?)
 */
{
	period	*pp = NULLP;

	if (p1->type == ABSOLUTE && p2->type == ABSOLUTE)
		pp = common(p1,p2);
	else if (p1->type != ABSOLUTE && p2->type != ABSOLUTE)
	{
		if (p1->type == p2->type)
			pp = common(p1,p2);
		else
		{
			/* Make p1->type <= p2->type (ie: smaller periodicity).
			 * Currently, with only two relative period types, this implies:
			 * p1->type == HOUR && p2->type == DAY
			 */
			if (p1->type > p2->type) { pp = p1; p1 = p2; p2 = pp; }

			if (p1->type == HOUR && p2->type == DAY)
				pp = rcommon(p1,p2);
			else fputs("Snark!: Relative period types >= 3\n", stderr);
		}
	}
	else /* (p1->type != ABSOLUTE exor p2->type != ABSOLUTE) */
	{
		if (p1->type == ABSOLUTE)
			pp = rcommon(p2, p1);
		else pp = rcommon(p1, p2);
	}
	return pp;
} /* intersection */

period *intersect(period *p1, period *p2)
/*
 * intersects list of periods p1 and p2 and returns resultant period list
 * does not change lists p1 or p2
 */
{
	period	*pp, *pp1, *pp2, *newp, *start, *end;

	pp = NULLP;
	pp1 = p1;
	while (pp1 != NULLP)
	{
		pp2 = p2;
		start = NULLP;
		while (pp2 != NULLP)
		{
			newp = intersection(pp1, pp2);
			/* add the new period list to the end of the current list */
			if (start == NULLP)
				start = end = newp;
			else end->next = newp;
			if (end != NULLP) while (end->next != NULLP) end = end->next;
			pp2 = pp2->next;
		}
		pp = mergeperiods(pp, start);
		pp1 = pp1->next;
	}
#ifdef DEBUG
	if (debug & DB_CALL) printf("intersect(p1,p2)\n");
	if (debug & DB_PERIOD) printperiod(pp, "");
#endif
	return pp;
} /* intersect */

period *fintersect(period *p1, period *p2)
/*
 * intersects p1 and p2 and returns resultant period list
 * destroys p1 and p2
 */
{
	period	*pp;
	pp = intersect(p1,p2);
	rfree(p1); rfree(p2);
	return pp;
}

period *notperiod(period *p1)
/*
 * returns the inverse of period p1
 * destroys p1 in the process
 */
{
	period	*pp, *start, *prev;
	time_t	t, min, max;
	int		ptype;

	if (p1 == NULL) return NULLP;
	switch (ptype = p1->type)
	{
	case ABSOLUTE:
		min = construct(1900+tnow.tm_year, 0, 0, 0, 0);
		max = construct(1900+tnow.tm_year+1, 0, 0, 0, 0) - 1;
		break;
	case HOUR:
		min = 0;
		max = SECINDAY - 1;
		break;
	case DAY:
		min = 0;
		max = SECINWEEK - 1;
		break;
	}

	prev = NULL;
	start = pp = p1;
	while (min < max && pp != NULL)
	{
		if (pp->end < pp->start) /* when negating a moment ... */
			pp->end = pp->start; /* ... netgate the next second */
		t = pp->end + 1;
		if (min < pp->start - 1)
		{
			pp->end = pp->start - 1;
			pp->start = min;
			min = t;
			prev = pp;
		}
		else
		{
			/* skip this irrelevant period */
			if (min < t) min = t;
			if (prev == NULL)
				start = pp->next;
			else prev->next = pp->next;
			/* free(pp); not always passed a malloc'ed period */
		}
		pp = ((prev != NULL) ? prev->next : start);
	}

	if (min < max)
		if ((pp = newperiod(ptype, min, max, NULL)) != NULLP) {
			if (prev == NULL)
				if (start == NULL)
					start = pp;
				else start->next = pp;
			else prev->next = pp;
		}
	return start;
}

period *exclude_past(period *pp)
/* Return the period pp excluding any periods from the past */
{
	int doy, pod;	/* for passing to time_2_dp() only */
	return fintersect(newperiod(ABSOLUTE, time_2_dp(now, &doy, &pod) + SECINPERIOD,
				    construct(tnow.tm_year+1900+1,0,0,0,0)-1, NULLP), pp);
}

period *absperiod(period *pp)
/*
 * Return a valid, instanciated period corresponding to period pp.
 * An instanciated period is a period mapped to real dates.
 * A valid period does not contain any excluded periods.
 */
{
	int pod, doy;
	time_t start;
	period *newp;

	/* first, convert any moments to extend to the end of the day */
	if (pp != NULLP)
	{
	
		if (pp->type != ABSOLUTE)
		{
			/* intersect the period with the relevant day/week absolute period
			 * Note: relative periods start from next absolute period
			 */
			start = time_2_dp(now, &doy, &pod) + SECINPERIOD;
			newp = newperiod(ABSOLUTE, start, (pp->type == HOUR) ?
					 start+SECINDAY-1 : start+SECINWEEK-1, NULLP);
			pp = fintersect(pp, newp);
		}

#if 0
		/* extend all moments to the end of the day */
		for (newp = pp ; newp ; newp = newp->next)
			if (newp->start > newp->end)
			{
				start = time_2_dp(newp->start, &doy, &pod);
				newp->end = start + (SECINDAY - pod*SECINPERIOD) - 1;
			}
#else
		/* extend all moments to last for one period */
		for (newp = pp ; newp ; newp = newp->next)
			if (newp->start > newp->end)
			{
				newp->end = newp->start + SECINPERIOD -1;
			}
#endif

		/* exclude invalid periods by intersecting with valid periods */
		pp = intersect(valid_abs, intersect(valid_day, intersect(valid_hour, pp)));
	}
	return pp;
} /* absperiod */

/*********** recursive descent routines ************/

period *finddate(FILE *fptr)
/*
 * date = num '/' num ['/' num]
 *	| num month    neilb removed [num]
 */
{
	int	dayofmonth, month, year;
	period	*pp;
	time_t	start;
    
	month = 0;
	year = tnow.tm_year + 1900;
	dayofmonth = in_token->tok_val;
	if (lookahead->tok_type == SLASH)
	{
		if (nexttoken(fptr) != NUMBER)
			printerror(FATAL, "Missing Month before");
		else
		{
			month = in_token->tok_val-1;
			if (nexttoken(fptr) == SLASH)
			{
				if (nexttoken(fptr) != NUMBER)
				{
					year = 1900 + tnow.tm_year;
					printerror(RECOVER, "Missing Year before");
				}
				else
				{
					year = in_token->tok_val;
					if (year < 1900) year += 1900;
					nexttoken(fptr);
				}
			}
			else year = 1900 + tnow.tm_year;
		}
	}
	else if (lookahead->tok_type == MONTH)
	{
		month = lookahead->tok_val;
		if (nexttoken(fptr) == NUMBER)
		{
#if 0
			neilb removed thing because  23 may 10:00 didn't' work...
				year = in_token->tok_val;
			nexttoken(fptr);
#endif
		}
	}
	else printerror(FATAL, "Missing Month before");	/* should not happen! */
    
	if (checkdate(year,month,dayofmonth))
	{
		start = construct(year,month,dayofmonth-1,0,0);
		pp = newperiod(ABSOLUTE, start, start+SECINDAY-1, NULLP);
	}
	else pp = NULLP;
#ifdef DEBUG
	if (debug & DB_CALL)
		printf("finddate() -> %d/%d/%d\n", dayofmonth, month, year);
#endif
	return pp;
} /* finddate */

period *book_mktime(int hour,int minute)
{
	period *pp;
	time_t start;
	if (checktime(hour, minute))
	{
		start = hour * SECINHOUR + minute * SECINMIN;
		pp = newperiod(HOUR, start, start-1, NULLP);
	}
	else pp = NULLP;
	return pp;
}

period *gettime(FILE *fptr)
/*
 * time	= num [':' num] ["am" | "pm"]
 */
{
	int	minute, hour;
	period	*pp;
    
	hour = in_token->tok_val;
	minute = 0;
	if (lookahead->tok_type == COLON)
	{
		if (nexttoken(fptr) != NUMBER)
			printerror(RECOVER, "Missing minute before");
		else
		{
			minute = in_token->tok_val;
			nexttoken(fptr);
		}
	}
	else swaptok();
	if (in_token->tok_type == APM)
	{
		if (hour == 12) hour = 0;	/* 12am --> 0:00; 12pm --> 12:00 */
		hour += in_token->tok_val;
		nexttoken(fptr);
	}
	pp = book_mktime(hour, minute);
    
#ifdef DEBUG
	if (debug & DB_CALL)
		printf("gettime() -> %d:%d\n", hour, minute);
#endif
	return pp;
} /* gettime */

period *getday(FILE *fptr)
/*
 * day	= "monday" | ... | "sunday" | "today" | "tomorrow"
 */
{
	int		day;
	period	*pp;
	int		type;
	time_t	start;
    
	if ((day = in_token->tok_val) <= SUN)
	{
		type = DAY;
		start = day * SECINDAY;
	}
	else
	{
		type = ABSOLUTE;
		start = construct(1900+tnow.tm_year, 0, tnow.tm_yday, 0, 0);
		if (day == TOMOR) start += SECINDAY;
	}
	pp = newperiod(type, start, start + SECINDAY - 1, NULLP);
	nexttoken(fptr);
#ifdef DEBUG
	if (debug & DB_CALL)
		printf("getday() -> Day: %d\n", day);
#endif
	return pp;
} /* getday */

period *getpredef(FILE *fptr)
/*
 * predef	= periodid ['.' num]
 */
{
	int		week;
	period	*defp, *newp, *pp;
	time_t	tstart, tend;
	char	*s;
    
	defp = in_token->tok_period;
	s = in_token->tok_str;
#ifdef DEBUG
	if (debug & DB_CALL)
		printf("getpredef() -> %s\n", s);
#endif
	if (nexttoken(fptr) == FSTOP)
	{
		if (nexttoken(fptr) != NUMBER)
		{
			pp = NULLP;
			printerror(FATAL, "Expected week number before");
		}
		else
		{
			if (defp->type != ABSOLUTE)
			{
				fprintf(stderr,"Ignored '.%d' after relative period '%s'\n",
					in_token->tok_val, s);
				pp = copyperiods(defp);
			}
			else
			{
				if (defp != NULLP)
				{
					pp = copyperiods(defp);
					tstart = weekstart(defp->start);
					tend = tstart + SECINWEEK - 1;
					week = in_token->tok_val;	/* week 0 == week 1 */
					while (week-- > 1 && pp != NULLP)
					{
						tstart += SECINWEEK;
						tend += SECINWEEK;
						while (pp != NULLP && pp->end < tstart)
							pp = pp->next;
						if (pp != NULLP)
							while (tend < pp->start)
							{
								tstart += SECINWEEK;
								tend += SECINWEEK;
							}
					}
					if (pp != NULLP &&
					    (newp = newperiod(ABSOLUTE, tstart, tend, NULLP)) != NULL)
						pp = fintersect(pp, newp);
					else
					{
						rfree(pp);
						pp = NULLP;
					}
				}
				else
				{
					fprintf(stderr, "Predefined period '%s' undefined\n", s);
					pp = NULLP;
				}
			}
			nexttoken(fptr);
		}
	}
	else pp = copyperiods(defp);
	return pp;
} /* getpredef */

period *getmonth(FILE *fptr)
/*
 * month	= "january" | ... | "december"
 */
{
	period	*pp;
	int		month, year;
	time_t	start;
    
	month = in_token->tok_val;
	nexttoken(fptr);
	start = construct(tnow.tm_year+1900, month, 0, 0, 0);
	pp = newperiod(ABSOLUTE, start, start + SECINDAY*(daysinmonth[month] + (month == 1 ? leapyear(1900+year) : 0)) - 1, NULLP);
#ifdef DEBUG
	if (debug & DB_CALL)
		printf("getmonth() -> %d\n", month);
#endif
	return pp;
} /* getmonth */

static period *getintersect();

static period *getperiod(FILE *fptr)
/*
 * period	= '(' expr ')'
 *		| time | day | week | date | month
 */
{
	int	type;
	period	*pp;
    
	switch (in_token->tok_type)
	{
	case LPR:
		level++;
		nexttoken(fptr);
		pp = getintersect(fptr);
		if (in_token->tok_type != RPR)
		{
			printerror(FATAL, "Inserted Missing ')' before");
		}
		else nexttoken(fptr);
		level--;
		break;
	case NUMBER:
		swaptok();
		nexttoken(fptr);
		swaptok();
		if ((type = lookahead->tok_type) == SLASH || type == MONTH)
			pp = finddate(fptr);
		else pp = gettime(fptr);
		break;
	case HOUR:	 pp = book_mktime(in_token->tok_val, 0); nexttoken(fptr); break;
	case DAY:	 pp = getday(fptr); break;
	case PID:	 pp = getpredef(fptr); break;
	case MONTH:	 pp = getmonth(fptr); break;
	default:
		printerror(FATAL, "Expected time, day, week, or date before");
		pp = NULLP; break;
	}
#ifdef DEBUG
	if (debug & DB_PERIOD) printperiod(pp, "");
#endif
	return pp;
} /* getperiod */

static period *getrange(FILE *fptr)
/*
 * range = ['not' | '~'] period ['-' period]
 */
{
	period	*pp;
	int		not = 0;
    
	if ((not = (in_token->tok_type == NOT))) nexttoken(fptr);
	pp = getperiod(fptr);
	if (in_token->tok_type == DASH)
	{
		nexttoken(fptr);
		pp = range(pp, getperiod(fptr));
	}
	if (not) pp = notperiod(pp);
	return pp;
} /* getrange */

static period *getunion(FILE *fptr)
/*
 * union = range {',' range}
 */
{
	period	*pp;
    
	pp = getrange(fptr);
	while (in_token->tok_type == COMMA)
	{
		nexttoken(fptr);
		pp = periodunion(pp, getrange(fptr));
	}
	return pp;
} /* getunion */

static period *getintersect(FILE *fptr)
/*
 * intersect = union {union}
 */
{
	int	type;
	period	*pp;
    
	pp = getunion(fptr);
	while ((type = in_token->tok_type) == LPR || type == NUMBER || type == DAY ||
	       type == PID || type == MONTH || type == RPR || type == NOT)
	{
		if (type == RPR) {
			if (level == 0)
			{
				nexttoken(fptr);
				printerror(RECOVER, "Deleted unmatched ')' before");
			}
			else break;
		}
		pp = fintersect(pp, getunion(fptr));
	}
	return pp;
} /* getintersect */

void skiptoeoln(FILE *fptr)
{
	char	ch;
#ifdef DEBUG
	if (infile != NULL) fprintf(stderr, "%s -  Line %d: ", infile, c_inline);
	fprintf(stderr, "Skipping :");
	while (fputc((ch = getc(fptr)), stderr) != EOF && ch != '\n');
#else
	while ((ch = getc(fptr)) != EOF && ch != '\n');
#endif    
	c_inline++;
}

void addstrlist(strlist	**head, char *value)
/* add the new string value to the strlist ponted to by *head,
 * being carefull to maintain string order in list
 */
{
	static strlist	*end;	/* used to insert at end of list */
	strlist	*sp;
	if ((sp = MALLOC(strlist)) != NULL &&
	    (sp->str_val = (char *) malloc(strlen(value)+1)) != NULL)
	{
		sp->next = NULL;
		strcpy(sp->str_val, value);
		if (*head == NULL)
			*head = sp;
		else end->next = sp;
		end = sp;
	}
}

void addoption(optlist **head, int option, int value)
/* add an option and its value to the list of options */
{
	optlist	*newopt;
	if ((newopt = MALLOC(optlist)) != NULL)
	{
		newopt->option = option;
		newopt->value = value;
		newopt->next = *head;
		*head = newopt;
	}
}

void getoptval(FILE *fptr, optlist **head, int option)
/* [option] "=" . value */
{
	nexttoken(fptr);
	if (in_token->tok_type == NUMBER)
	{
		addoption(head, option, in_token->tok_val);
		nexttoken(fptr);
	}
	else printerror(FATAL, "Expected a number between '=', and");
}

void getoption(FILE *fptr, optlist **head, int option)
/* option . "=" value */
{
	nexttoken(fptr);
	if (in_token->tok_type == EQUAL)
		getoptval(fptr, head, option);
	else printerror(FATAL, "Expected '= number' between option, and");
}

int option_value(optlist *head, int option)
{
	while (head != NULL && head->option != option) head = head->next;
	return (head != NULL ? head->value : 0);
}

command_type *getcommand(FILE *fptr)
/*
 * command = [command] [period] {token_string} <eoln>
 */
{
	static command_type	command;
	int		tokens_parsed, gotperiod, goodoption;
	int		strcommand, conseccommand, optioncommand;
    
	/* prepare to parse a new command */
	command_no++;
	command.period = empty_abs;
	command.strlist = NULL;
	command.options = NULL;
	command.ntokpassed = 0;
	command.labs.item_len = 0;
	tokens_parsed = gotperiod = parse_err = 0;

	/* default for O_CONFIRM is 1 ... */
	addoption(&command.options, O_CONFIRM, 1);
    
	/* default command is help */
	if (in_token->tok_type == COMND)
	{
		tokens_parsed++;
		command.command = in_token->tok_val;
		nexttoken(fptr);
	}
	else command.command = HELP;
    
	/* check the type of command we have (this affects command syntax/semantics) */
	conseccommand = (command.command == AVAIL || command.command == BOOK);
	optioncommand = (command.command == CHANGE || conseccommand);
	strcommand = (command.command == DEFINE || command.command == UNDEF ||
		      command.command == ECHO);

	/* parse the command (which ends with an EOLN or END) */
	while (in_token->tok_type != EOLN &&
	       in_token->tok_type != END &&
	       (parse_err & FATAL) == 0)
	{
		tokens_parsed++;
		switch (in_token->tok_type)
		{
		case TOKNAME:
			if (! strcommand)
				command.ntokpassed++;
			else printerror(RECOVER, "Ignoring token name");
			nexttoken(fptr);
			break;
		case LABNAME:
			if (! strcommand)
			{
				if (command.labs.item_len == 0)
					command.labs = new_desc();
				set_a_bit(&command.labs, in_token->tok_val);
			}
			else printerror(RECOVER, "Ignoring lab name");
			nexttoken(fptr);
			break;
		case STRING:
			if (strcommand)
				addstrlist(&(command.strlist), in_token->tok_str);
			else printerror(FATAL, "Undefined string");
			nexttoken(fptr);
			break;
		case COMND:
		case TOPIC:
			if (command.command == HELP)
			{
				addoption(&command.options, O_HELP_TYPE, in_token->tok_type);
				addoption(&command.options, O_HELP_VALUE, in_token->tok_val);
				nexttoken(fptr);
			}
			else printerror(FATAL, "Unexpected second command/topic");
			break;
		case EQUAL:
			if (optioncommand)
				if (conseccommand)
					getoptval(fptr, &command.options, O_CONSEC);
				else getoptval(fptr, &command.options, O_MACHINES);
			else printerror(FATAL, "Invalid option");
			break;
		case OPTION:
			switch (in_token->tok_val)
			{
			case O_ALL_TOKENS:
				if ((goodoption = (command.command == TOKEN || command.command == AVAIL)))
				{
					addoption(&command.options, O_ALL_TOKENS, 1);
					nexttoken(fptr);
				}
				break;
			case O_CONSEC:
				if ((goodoption = conseccommand))
					getoption(fptr, &command.options, in_token->tok_val);
				break;
			case O_MACHINES:
				if ((goodoption = optioncommand))
					getoption(fptr, &command.options, in_token->tok_val);
				break;
			case O_CONFIRM:
				goodoption=1;
				getoption(fptr, &command.options, in_token->tok_val);
				break;
			case O_COUNTS:	
				goodoption=1;
				getoption(fptr, &command.options, in_token->tok_val);
				break;
			}
			if (! goodoption) printerror(FATAL, "Invalid option");
			break;
		default:
			if (! gotperiod++)
			{
				command.period = getintersect(fptr);
				if (command.command != DEFINE) command.period = absperiod(command.period);
			}
			else printerror(FATAL, "Unexpected second period specification starting at");
			break;
		}
	}
	if (in_token->tok_type != EOLN) skiptoeoln(fptr);

	if ((parse_err & FATAL) == 0 && tokens_parsed)
	{
		/* A fudge to allow us to detect the difference between not giving
		 * a period specification, and having a period specification reduce to
		 * an empty (NULL) period.
		 * We should have all period routines return one of the empty periods
		 * and not NULLP, but NULLP is easier, was developed first, and I am not
		 * bothered to change all neccessary places.
		 */
		if (command.period == empty_abs)
			command.period = NULLP;
		else if (command.period == NULLP)
			command.period = empty_abs;
	
		if (! strcommand)
		{
			int alltokens;
			/* mark all tokens if the command uses them, but has none */
			if ((alltokens = option_value(command.options, O_ALL_TOKENS)))
				command.ntokpassed = 0;	/* ignore any tokens explicitly passed */
			if (command.ntokpassed == 0) mark_all_tokens(alltokens);
		}
	
		return &command;
	}
	else return NULLC;
} /* getcommand */
