#define	MALLOC(X)	(X *) malloc(sizeof(X))

#define	NULLP	(period *) NULL
#define NULLPL	(plist *) NULL
#define NULLC	(command_type *) NULL

/* fundemental time constants (all time is measured in seconds) */
#define SECINWEEK	604800L
#define SECINDAY	 86400L
#define SECINHOUR	  3600L
#define SECINMIN	    60L

#define	SECINPERIOD	  1800L
#define MINPERIOD	  1800L		/* half hour minimum booking period */

/* error states */
#define FATAL	1
#define	RECOVER	2

/* debug states */
#define DB_TOKEN	1
#define DB_PERIOD	2
#define	DB_CALL		4

/* define day ordinals */
#define	MON	0
#define	TUE	1
#define	WED	2
#define	THU	3
#define	FRI	4
#define	SAT	5
#define	SUN	6
#define	TODAY	7
#define	TOMOR	8

/* defines the token type - assigned to token.tok_type */
/* following token types used in period definitions */
/* there should not be a token type == 0 */
#define COMMA	1
#define	DASH	2
#define LPR	3
#define RPR	4
#define COLON	5
#define FSTOP	6
#define SLASH	7
#define APM	8
#define PID	9
#define	NUMBER	10
#define	HOUR	11
#define DAY	12
#define MONTH	13
#define NOT	14
#define	EQUAL	15

/* following token types used for command parsing */
#define	COMND	50
#define	TOPIC	51
#define	STRING	52
#define TOKNAME	53
#define	OPTION	54
#define	LABNAME	55	/* tok_val is lab number */

/* following token types used for general lexical parsing */
#define	SKIP	97
#define EOLN	98
#define END	99
#define ERROR	999

/* the types of commands */
#define	HELP	1
#define	BOOK	2
#define	UNBOOK	3
#define	AVAIL	4
#define	LIST	5
#define	TOKEN	6
#define	DEFINE	7
#define	UNDEF	8
#define ECHO	9
#define	REFUND	10
#define	PAST	11
#define CHANGE	12
#define	RECLAIM	13
#define	SHELL	14
#define	TIMES	15
#define	LABS	16
#define	QUIT	99

/* the types of topics */
#define	PERIOD	1
#define	PREDEF	2
#define	SYNTAX	3
#define	CFILE	4
#define	DEFAULTER 5
#define	SUMMARY 7
/* also uses COMND(50) and TOKEN(6) */

/* the types of options */
#define	O_ALL_TOKENS	1
#define	O_HELP_TYPE	2
#define	O_HELP_VALUE	3
#define	O_CONSEC	4
#define	O_MACHINES	5
#define	O_CONFIRM	6
#define	O_COUNTS	7

/* values assigned to period.repetition - also use HOUR & DAY defined above */
#define ABSOLUTE	0

typedef struct periodrec period;
struct periodrec {
	int	type;		/* ABSOLUTE, HOUR, DAY */
	time_t	start, end;	/* seconds from start of type of period */
	period	*next;
};

typedef struct periodlist {
    char	*id;		/* the name of this period list */
    period	*period;	/* the period list */
    struct periodlist	*next;
} plist;

typedef struct str_list strlist;	/* stores a list of arbitrary strings */
struct str_list {
    char	*str_val;
    strlist	*next;
};

typedef struct option_list {
    int	option;			/* the option type being passed (see O_.. above) */
    int	value;			/* the value of the option */
    struct option_list	*next;
} optlist;

typedef struct {
    int		command;	/* one of HELP..QUIT */
    int		ntokpassed;	/* number of tokens explicitly passed to command */
    description labs;		/* labs (an other attributes) passed on command line */
    optlist	*options;	/* the options passed to the command */
    period	*period;	/* the period applying to the command */
    strlist	*strlist;	/* list of strings passed to the command */
} command_type;

#ifndef BOOK_INTERFACE
typedef int bookuid_t;
#endif

/* from bcomm.c */
void b_echo(command_type *comm);
void b_shell(command_type *comm);
void b_help(command_type *comm);

/* book routines and variables from other modules */


extern	plist	*predefined;
extern	int	c_inline;
extern	int	mark_token(char *tokname);	

extern	bookuid_t	realuser;
extern	bookuid_t	user;
extern	char		*helpfile;
extern	char		*username;
extern	char	*book_version;				/* bversion.c */
extern	char	*typevaltostr(int typ, int val);	/* blex.c */
extern	command_type	*getcommand(FILE *fptr);	/* bparse.c */
extern	int		command_no;		/* used to mark tokens and bookings */
extern	int		interrupt;
extern	int		privilaged;
extern	int		user_is_class;
extern	int	nexttoken(FILE *fptr);			/* blex.c */
extern	time_t	construct(int year, int month, int day, int hour, int minute);
extern	time_t	currtime();				/* bperiod.c */
extern	time_t	time_2_dp(time_t t, int *doy, int *pod); 	/* bparse.c */
extern	period		empty_abs[];		/* bparse.c */
extern	period	*absperiod(period *p);			/* bparse.c */
extern	period	*copyperiods(period *p);		/* bparse.c */
extern	period	*exclude_past(period *p);		/* bparse.c */
extern	period	*fintersect(period *p1, period *p2);	/* bparse.c */
extern	period	*intersect(period *p1, period *p2);	/* bparse.c */
extern	period	*mergeperiods(period *p1, period *p2);	/* bparse.c */
extern	period	*newperiod(int type, time_t start, time_t end, period *next); /* bparse.c */
extern	period	*notperiod(period *p);			/* bparse.c */
extern	period	*periodunion(period *p1, period *p2);	/* bparse.c */
extern	period	empty_abs[];				/* bperiod.c */
extern	struct tm	tnow;
extern	time_t		enow;
extern	time_t		now;
extern	void	b_avail(command_type *comm);		/* bcomm.c */
extern	void	b_book(command_type *comm);		/* bcomm.c */
extern	void	b_define(command_type *comm);		/* bperiod.c */
extern	void	b_help(command_type *comm);		/* bcomm.c */
extern	void	b_showbook(command_type *comm);		/* bcomm.c */
extern	void	b_showfree(command_type *comm);		/* bcomm.c */
extern	void	b_showpastbook(command_type *comm);	/* bcomm.c */
extern	void	b_token(command_type *comm);		/* bcomm.c */
extern	void	b_labs(command_type *comm);		/* bcomm.c */
extern	void	b_times(command_type *comm);		/* bcomm.c */
extern	void	b_undefine(command_type *comm);		/* bperiod.c */
extern	void	initbookings();				/* bcomm.c */
extern	void	initperiods();				/* bperiod.c */
extern	void	printcommand(command_type *cp);		/* bcomm.c */
extern	void	printperiod(period *pp, char *sep);	/* bperiod.c */
extern	void	printtokens(int n, int inc_expired);	/* bcomm.c */
extern	void	rfree(period *pp);			/* bparse.c */
extern	int	option_value(optlist *head, int option); /* bparse.c */
extern	void	printpredefs();			/* bperiod.c */
extern	void swaptok(); /* blex.c */
extern	period *book_mktime(int hour,int minute);
extern	period *range(period *p1, period *p2);
