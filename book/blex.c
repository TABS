/*
 * Lexical analyser for book interface
 */

#include	<sys/types.h>
#include	<stdio.h>
#include	<rpc/rpc.h>
#include	"../interfaces/bookinterface.h"
#define BOOK_INTERFACE
#include	"boo.h"
#include	"blex.h"
#include	<ctype.h>
#include	<string.h>
#include	<sys/errno.h>


char	strbuff[MAXTOKSTR];	/* input buffer */
char	*inchar = " ";		/* input buffer */
int	instring = 0;		/* indicates that we are in middle if string */

#ifdef DEBUG
extern	int	debug;
#endif

/* Used by nexttoken() to identify strings.
 * Minmatch is used to choose between ambiguous string prefixes.
 * viz:
 *	if S(n): set of strings with minmatch <= n;
 *	then for all i,j members of S(n): prefix(i, n) <> prefix(j, n)
 * If not set up like this then nexttoken() will choose first of 
 * conflicting alternatives.
 */
struct slist slist[] =
    {

	{	"not",		NOT,	0,	3	},

	{	"help",		COMND,	HELP,	1	},
	{	"book",		COMND,	BOOK,	1	},
	{	"unbook",	COMND,	UNBOOK,	1	},
        {	"change",	COMND,	CHANGE,	1	},
	{	"reclaim",	COMND,	RECLAIM, 3	},
	{	"claim",	COMND,	RECLAIM, 2	},
	{	"available",	COMND,	AVAIL,	1	},
	{	"test",		COMND,	AVAIL,	2	},
	{	"show",		COMND,	LIST,	1	},
	{	"list",		COMND,	LIST,	1	},
	{	"token",	COMND,	TOKEN,	1	},
	{	"define",	COMND,	DEFINE,	1	},
	{	"undefine",	COMND,	UNDEF,	3	},
	{	"echo",		COMND,	ECHO,	1	},
	{	"refund",	COMND,	REFUND,	3	},
	{	"past",		COMND,	PAST,	1	},
	{	"showpast",	COMND,	PAST,	5	},
	{	"times",	COMND,	TIMES,	3	},
	{	"labs",		COMND,	LABS,	3	},
	{	"quit",		COMND,	QUIT,	1	},
	{	"exit",		COMND,	QUIT,	2	},
	{	"shell",	COMND,	SHELL,	3	},

	{	"period",	TOPIC,	PERIOD,	2	},
	{	"predef",	TOPIC,	PREDEF,	2	},
	{	"syntax",	TOPIC,	SYNTAX,	2	},
	{	"command",	TOPIC,	COMND,	2	},
	{	"cfile",	TOPIC,	CFILE,	2	},
        {	"tokens",	TOPIC,	TOKEN,	6	},
	{	"defaulter",	TOPIC,	DEFAULTER, 4	},
	{	"summary",	TOPIC,	SUMMARY, 4	},

        {	"all",		OPTION,	O_ALL_TOKENS,	3	},
        {	"consecutive",	OPTION,	O_CONSEC,	6	},
        {	"machines",	OPTION,	O_MACHINES,	4	},
	{	"confirm",	OPTION, O_CONFIRM,	7	},
	{	"counts",	OPTION, O_COUNTS,	6	},
	
	{	"am",		APM,	0,	2	},
	{	"pm",		APM,	12,	2	},

	{	"midday",	HOUR,	12,	4	},
	{	"midnight",	HOUR,	0,	4	},

	{	"monday",	DAY,	MON,	1	},
	{	"tuesday",	DAY,	TUE,	2	},
	{	"wednesday",	DAY,	WED,	1	},
	{	"thursday",	DAY,	THU,	2	},
	{	"friday",	DAY,	FRI,	1	},
	{	"saturday",	DAY,	SAT,	2	},
	{	"sunday",	DAY,	SUN,	2	},
	{	"today",	DAY,	TODAY,	3	},
	{	"tomorrow",	DAY,	TOMOR,	3	},

	{	"january",	MONTH,	0,	2	},
	{	"february",	MONTH,	1,	2	},
	{	"march",	MONTH,	2,	3	},
	{	"april",	MONTH,	3,	2	},
	{	"may",		MONTH,	4,	3	},
	{	"june",		MONTH,	5,	3	},
	{	"july",		MONTH,	6,	3	},
	{	"august",	MONTH,	7,	2	},
	{	"september",	MONTH,	8,	2	},
	{	"october",	MONTH,	9,	1	},
	{	"november",	MONTH,	10,	1	},
	{	"december",	MONTH,	11,	3	},

	{	"",	0,	0	}
    };

tok	tok0, tok1;		/* token buffers */
tok	*in_token = &tok0;	/* current token */
tok	*lookahead = &tok1;	/* current lookahead */

char *typevaltostr(int typ, int val)
{
    struct slist *sp;
    for (sp = slist;
	 (*sp->string != '\0' && (sp->tok_type != typ || sp->tok_val != val));
	 sp++);
    return(sp->string);
}

/******** debug / error routines ********/

#ifdef DEBUG
void printtoken()
{
    if (debug & DB_TOKEN)
    {
	printf("%d\t", c_inline);
	switch (in_token->tok_type)
	{
	    case COMMA:	puts("COMMA");	break;
	    case DASH:	puts("DASH");	break;
	    case LPR:	puts("LPR");	break;
	    case RPR:	puts("RPR");	break;
	    case COLON:	puts("COLON");	break;
	    case FSTOP:	puts("FSTOP");	break;
	    case SLASH:	puts("SLASH");	break;
	    case PID:	puts("PID");	break;
	    case EOLN:	puts("EOLN");	break;
	    case END:	puts("END");	break;
	    case ERROR:	puts("ERROR");	break;
	    case NOT:	puts("NOT");	break;
	    case EQUAL:	puts("EQUAL");	break;
	    case APM:	printf("APM %d\n", in_token->tok_val);	break;
	    case DAY:	printf("DAY %d\n", in_token->tok_val);	break;
	    case MONTH:	printf("MONTH %d\n", in_token->tok_val);	break;
	    case COMND:	printf("COMMAND %d\n", in_token->tok_val);	break;
	    case TOPIC:	printf("TOPIC %d\n", in_token->tok_val);	break;
	    case TOKNAME: printf("TOKNAME '%s'\n", in_token->tok_str);	break;
	    case STRING: printf("STRING '%s'\n", in_token->tok_str);	break;
	    case NUMBER: printf("NUMBER %d\n", in_token->tok_val);	break;
	    default:	printf("Unknown token %d\n", in_token->tok_type); break;
	}
    }
}
#endif

/********** basic lexical routines **********/

void swaptok()
{
tok	*tmp;
	tmp = in_token;
	in_token = lookahead;
	lookahead = tmp;
}

char *getstring(FILE *fptr)
{
    int		ch, len;
    char	*s;

    len = 0;
    s = strbuff;
    while (((ch = getc(fptr)) != EOF && ch != '"') && len++ < MAXTOKSTR-1)
	*s++ = ch;
    *s = '\0';
    if (ch == '"')
	instring = 0;
    else ungetc(ch, fptr);
    return strbuff;
}
			    

int nexttoken(FILE *fptr)
/*
 * Read tokens from input file.
 * Never returns current token with type ERROR.
 */
{
    int		ch;
    int		len, type, val;
    char	*s;
    struct slist	*slistp, *sfound;
    struct periodlist	*plistp;

    if (instring)
    {
	type = STRING;
	in_token->tok_type = type;
	in_token->tok_val = 0;
	in_token->tok_str = getstring(fptr);
    }
    else
    {
	int eofcnt = 0;
	ch = getc(fptr);	/* initialisation */
	while (ch == EOF && !feof(fptr) && !interrupt)
	{
	    /* presumably an alarm */
	    eofcnt++;
	    if (eofcnt > 10) break;
	    clearerr(fptr);
	    ch = getc(fptr);
	}
	do {
	    while (isspace(ch) && ch != '\n')
		ch = getc(fptr);
	    if (isalpha(ch))
	    {
		/* get the alphanumeric string */
		s = strbuff;
		len = 0;
		while ((isalpha(ch) || isdigit(ch) || ch == '_')
		&& len++ < MAXTOKSTR-1)
		{
		    if (isalpha(ch) && isupper(ch)) ch = tolower(ch);
		    *s++ = ch;
		    ch = getc(fptr);
		}
		ungetc(ch, fptr);
		*s = '\0';
		in_token->tok_str = strbuff;

		/* look for the string in predefined list */
		for (slistp = slist, sfound = '\0'; (*slistp->string != '\0');
		     slistp++)
		{
		    if (strncmp(slistp->string, strbuff,
			(slistp->minmatch > len) ? slistp->minmatch : len) == 0)
		    {
			if (sfound)
			{
			    /* this should not happen if minmatch has been
			     * set properly for each string
			     */
			    fprintf(stderr, "'%s' ambiguous: chose %s not %s\n",
			    strbuff, slistp->string, sfound->string);
			}
			else sfound = slistp;
		    }
		}
		if (sfound)
		{
		    /* type == APM or DAY or MONTH */
		    type = sfound->tok_type;
		    in_token->tok_val = sfound->tok_val;
		}
		else if ((in_token->tok_val = get_mappingint(strbuff, M_ATTRIBUTE)) >= 0
			 && query_bit(&attr_types.lab, in_token->tok_val)
			 )
		{
		    type = LABNAME;
		}
		else
		{
		    /* look for the string in (system+user) defined strings */
		    /* these strings must match exactly */
		    for (plistp = predefined;
			(plistp != NULL && strcmp(plistp->id, strbuff) != 0);
			plistp = plistp->next);
		    if (plistp != NULL)
		    {
			type = PID;
			in_token->tok_period = plistp->period;
		    }
		    else if (mark_token(strbuff))
			/* this is the only way we can identify tokens */
			type = TOKNAME;
		    else type = STRING;
		}
	    }
	    else
	    {
		if (isdigit(ch))
		{
		    s = strbuff;
		    type = NUMBER;
		    len = val = 0;
		    while (isdigit(ch) && len++ < MAXTOKSTR-1)
		    {
			*s++ = ch;
			val = val * 10 + ch - '0';
			ch = getc(fptr);
		    }
		    ungetc(ch, fptr);
		    *s = '\0';
		    in_token->tok_str = strbuff;
		}
		else
		{
		    val = 0;
		    if (ch == '\\')
		    {
			if ((ch = getc(fptr)) == '\n')
			{
			    ch = getc(fptr);
			    c_inline++;
			}
			type = SKIP;
		    }
		    else
		    {
			s = NULL;
			switch (ch)
			{
			case '~': type = NOT;	break;
			case ',': type = COMMA;	break;
			case '-': type = DASH;	break;
			case '(': type = LPR;	break;
			case ')': type = RPR;	break;
			case ':': type = COLON;	break;
			case '.': type = FSTOP;	break;
			case '/': type = SLASH;	break;
			case '=': type = EQUAL;	break;
			case '?': type = COMND;	val = HELP; break;
			case '"':
			    type = STRING;
			    instring++;
			    s = getstring(fptr);
			    break;
			case '#': /* handle comments */
			    while ((ch = getc(fptr)) != EOF && ch != '\n');
			case '\n': /* flow on from last case */
			    c_inline++;
			    s = "End of Line";
			    type = EOLN;	break;
			case EOF:
			    if (interrupt)
			    {
				s = "interrupt";
				type = EOLN;
				clearerr(stdin);
			    }
			    else
			    {
				s = "End of File";
				type = END;
			    }
			    break;
			default:
			    type = ERROR;
			    s = "ERROR";
			    fprintf(stderr, "Line %d - Skipping bad char: '%c'\n",
			    c_inline, ch);
			    ch = fgetc(fptr);
			    break;
			}
			in_token->tok_str = s;
		    }
		}
		in_token->tok_val = val;
	    }
	} while ((type == ERROR || type == SKIP) && ! interrupt);
	in_token->tok_type = type;
    }
#ifdef DEBUG
    printtoken();
#endif
    return type;
} /* nexttoken */
