/*
 * Book interface module:
 *	Routines that perform the book commands:
 */

#include	<unistd.h>
#include	<stdlib.h>
#include	<sys/wait.h>
#include	<signal.h>
#include	<stdio.h>
#include	<time.h>
#include	<rpc/rpc.h>
#include	<errno.h>
#include	<grp.h>

#ifdef NOTYET
#include	<udb.h>
#endif
#include	"../interfaces/bookinterface.h"
#define BOOK_INTERFACE
#include	<string.h>
#include	<ctype.h>
#include	<pwd.h>
#include	"boo.h"
#include	"bcomm.h"

static void removebooking(bookuid_t user, booked *bp);

booked	*currbooked = NULL;	/* list of current bookings */
booked	*pastbooked = NULL;	/* list of past bookings */
currtokentype *currtokens;	/* list of current tokens */

char unknown[] = "Unknown";

/***** time and slot conversion routines *****/

time_t dp_2_time(int dayofyear, int periodofday)
/* return the zulu time corresponding to parameters */
{
	int	min, hour;
	min = periodofday * (SECINPERIOD / 60);
	hour = min / 60;
	min -= hour * 60;
	return construct(1900 + tnow.tm_year, 0, dayofyear, hour, min);
}

period *t_within_period(time_t start, period *periods)
/* A simple way of determining whether a booking period is within a series
 * of periods (more efficient than using intersect()).
 * We assume that the periods are in increasing time order.
 * Return the simple period containing start
 */
{
	period	*pp;
	time_t	end = start + SECINPERIOD - 1;
	for (pp = periods; (pp != NULLP && pp->end < end); pp = pp->next);
	/* pp == NULL || pp->end >= end */
	return ((pp != NULLP && pp->start <= start) ? pp : NULLP);
}

int t_lap_period(time_t start, period *periods)
/* A simple way of determining whether a booking period overlaps one of a
 * series of periods (more efficient than using intersect()).
 * We assume that the periods are in increasing time order.
 */
{
	period	*pp;
	time_t	end = start + SECINPERIOD - 1;
	for (pp = periods; (pp != NULLP && pp->end < start); pp = pp->next);
	/* pp == NULL || pp->end >= start */
	return (pp != NULLP && pp->start <= end);
}

time_t endofperiods(period *periods)
/* return the end time of the last period in periods */
{
	period *pp;
	if (periods != NULLP)
	{
		for (pp = periods; (pp->next != NULLP); pp = pp->next);
		return pp->end;
	}
	else return 0;
}

/************ input routines ************/

int getresponse(char *prompt, int *choice, int min)
/* Interactively prompt for a response from the user.
 * Responses are Yes: 'y'; No: ('n', <return>); Quit: ('q', <EOF>, <interrupt>).
 * User may respond with an integer min <= i <= *choice in which case return Yes,
 * and the number in choice.
 */
{
	char	ch;
	char	*badresp = "Invalid response\nRespond with 'y', 'n', 'q', <Return>";
	int		neg, max, val, ret;

	max = *choice;	/* the maximum choice */
	ret = R_BAD;
	val = 0;
	do {
		neg = 0;
		printf("%s", prompt);
		if (max > min) printf(" [%d..%d]", min, max);
		printf (" ?");
		if (!isatty(0)) printf("\n");
		fflush(stdout);
		if ((ch = getchar()) == EOF || interrupt)
		{
			puts("");
			ret = R_QUIT;
		}
		else
			switch (ch)
			{
			case 'y':
			case 'Y':	ret = R_YES;	break;
			case '\n':
			case 'n':
			case 'N':	ret = R_NO;	break;
			case 'q':
			case 'Q':	ret = R_QUIT;	break;
			case '+':
				neg = 0;
				goto digit;
			case '-':
				neg++;
			digit:
				ch = getchar();	/* fall into next condition */
			default:
				if (isdigit(ch))
				{
					val = 0;
					while (isdigit(ch))
					{
						val = val * 10 + ch - '0';
						ch = getchar();
					}
					if (neg) val *= -1;
					if (min <= val && val <= max) ret = R_YES;
				}
				if (ret == R_BAD)
				{
					fputs(badresp, stderr);
					if (max > min)
						fprintf(stderr, ", or integer: [%d..%d]\n", min, max);
					else fputs(".", stderr);
				}
				break;
			}
		while (ch != '\n' && ch != EOF) ch = getchar();
	} while (ret == R_BAD);
	*choice = val;
	return ret;
} /* getresponse */

/********* output routines *********/

void printslot(time_t start, int nperiods)
/* print the period starting start, lasting nperiods periods */
{
	char *s;
	struct tm	*tp;

	tp = gmtime(&start);
	s = asctime(tp);
	printf("%5.5s", s+11);
	if (nperiods > 0)
	{
		printf(" - ");
		start += nperiods*SECINPERIOD - 1;
		tp = gmtime(&start);
		s = asctime(tp);
		printf("%5.5s ", s+11);
	}
	printf(" %3.3s %2.2s %3.3s", s, s+8, s+4);
}

#ifdef NOTYET
void update_tokens(udb_uidt user);	/* defined later */
#else
void update_tokens(bookuid_t user);	/* defined later */
#endif
void printtokens(int ntokpassed, int include_expired)
/* print the details of the tokens passed, or if none passed, all tokens
 * available */
{
	currtokentype	*cp;
	update_tokens(user);
	if (currtokens != NULL)
	{
		if (ntokpassed == 0)
			printf("\tAll tokens (%scluding expired tokens):\n",
			       (include_expired ? "in" : "ex"));
		puts("\tToken\t\t\tNumber\tPeriod Available");
		for (cp = currtokens; (cp != NULL); cp = cp->next)
			if (cp->active == command_no)
			{
				printf("\t%-16s\t%d\t", cp->name, cp->number);
				printperiod(cp->period, "\t\t\t\t\t");
			}
	}
	else puts("\tNo tokens allocated");
}

void printcommand(command_type *cp)
/* a debug routine to print out commands, tokens, periods */
{
	strlist	*sp;
	optlist	*opt;
	if (cp != NULLC)
	{
		printf("Command:\n\t%s\n", typevaltostr(COMND, cp->command));

		puts("Options:");
		for (opt = cp->options; (opt != NULL); opt = opt->next)
			printf("\t%d = %d\n", opt->option, opt->value);

		puts("Strings:");
		if (cp->strlist != NULL)
			for (sp = cp->strlist; (sp != NULL); sp = sp->next)
				printf("\t%s\n", sp->str_val);

		printf("Tokens passed: (%d)\n", cp->ntokpassed);
		if (cp->ntokpassed > 0)
			printtokens(cp->ntokpassed, 0);

		puts("Periods:");
		if (cp->period != NULLP)
		{
			putchar('\t');
			printperiod(cp->period, "\t");
		}
		else puts("\tNo period passed");
	}
	else puts("Null command");
}

/***** help output routines ******/

int findhelp(FILE *fp, char *topic, int toprint)
/*
 * Help info is found in the file pointed to by fp.
 * Help sections generally start with a "#topic" line and end with a "#" line.
 * This routine prints or skips (depending on toprint), all lines
 * up to the line matching "#topic". topic may be the null string.
 */
{
	char ch, *s;
	int	found = 0;
	char *term = getenv("TERM");
	int linecnt = 0;
	while (! found && (ch = fgetc(fp)) != EOF)
	{
		if (ch == '#')
		{
			s = topic;
			while ((ch = fgetc(fp)) != EOF && ch != '\n' && *s && ch == *s++);
			if (ch == '\n')
				found = (*s == '\0');
			else while ((ch = fgetc(fp)) != EOF && ch != '\n');
		}
		else
		{
			while (ch != '\n' && ch != EOF)
			{
				if (toprint) putchar(ch);
				ch = fgetc(fp);
			}
			if (toprint)
			{
				putchar('\n');
				if (++linecnt >= 23 && (term == NULL || strcmp(term, "xterm") != 0))
				{
					int c, more;
					fputs("Press return for more: ", stdout);
					fflush(stdout);
					more = ((c = fgetc(stdin)) == '\n');
					while (c != '\n' && c != EOF) c = fgetc(stdin);
					if (more)
						linecnt = 0;
					else return found;
				}
			}
		}
	}
	return found;
}

void printhelp(char *topic)
/* print the help section indexed by topic (see findhelp() for more details) */
{
	FILE	*fp;
	if ((fp = fopen(helpfile, "r")) != NULL)
	{
		if (findhelp(fp, topic, 0))
			findhelp(fp, "", 1);
		fclose(fp);
	}
	else perror(helpfile);
}

/******* token cache routines ***********/

currtokentype *currtokenentry(char *name)
/* find and return the token in currtokens matching name */
{
	currtokentype *cp;
	for (cp = currtokens; (cp != NULL && strcmp(cp->name, name)); cp = cp->next);
	return cp;
}

int token_active(char *name)
{
	currtokentype *cp;
	if ((cp = currtokenentry(name)) != NULL)
		return (cp->active == command_no);
	else return 0;
}

int tokens_book_lab(char *labname)
{
	/* check to see if any of the currently active tokens
	 * can book the lab labname
	 */
	/* FIXME why is this here -= neilb 20may96 */
	return 0;
}

period *token_periods(char *tname)
/* return the periods during which the token called tname is available.
 */
{
	period *pp, *pfinal;
	period_spec *pspecp;

	pfinal = NULLP;

#ifdef RET_PERIOD_NOT_VALID
	/*
	 * token_period returns an array of periods when the token
	 * named tname is *NOT* valid.
	 * The array is NULL if the token is invalid.
	 * The array is terminated by a sentinal period (with start == -1).
	 * The array contains only the sentinal if the token is always
	 * valid.
	 */
	if ((pspecp = token_period(tname)) != NULL)
	{
		while (pspecp->start != -1)
		{
			pp = newperiod(ABSOLUTE,
				       construct(tnow.tm_year+1900,0,pspecp->start, 0, 0),
				       construct(tnow.tm_year+1900,0,pspecp->end+1, 0, 0) - 1,
				       NULLP);
			pfinal = mergeperiods(pfinal, pp);
			pspecp++;
		}
		if (pfinal == NULLP)
		{
			/* By default, the token is valid for the current year only.
			 */
			pfinal = newperiod(ABSOLUTE,
					   construct(1900+tnow.tm_year  ,0,0,0,0),
					   construct(1900+tnow.tm_year+1,0,0,0,0)-1,
					   NULLP);
		}
		else
			pfinal = notperiod(pfinal);

	}
#else
	/* token_period returns an array of periods when the token
	 * named tname is valid.
	 * The array is NULL if the token is invalid.
	 * The array is terminated by a sentinal period (with start == -1).
	 */
	for (pspecp = token_period(tname); (pspecp != NULL && pspecp->start != -1); pspecp++)
	{
		pp = newperiod(ABSOLUTE,
			       construct(tnow.tm_year+1900,0,pspecp->start, 0, 0),
			       construct(tnow.tm_year+1900,0,pspecp->end+1, 0, 0) - 1,
			       NULLP);
		pfinal = mergeperiods(pfinal, pp);
	}
#endif
	return pfinal;
}

#ifdef NOTYET
void update_tokens(udb_uidt user)
#else
void update_tokens(bookuid_t user)
#endif
/* get the current list of tokens from the db.
 * update the current tokenlist "currtokens".
 * do not deactivate any active tokens.
 */
{
	charpairlist	charpair;
	currtokentype	*currtok;

	/* reset the number of all tokens in currtok */
	for (currtok = currtokens; (currtok != NULL); currtok = currtok->next)
		currtok->number = 0;

	/* get the user's current tokens from the db */
	charpair = gettokenlist(user);
#ifdef DEBUG
	printf("gettokenlist(%d) returns %snull\n", user, (charpair != NULL) ? "non-" : "");
#endif

	/* update the tokens in currtok; add new ones if not there */
	while (charpair != NULL)
	{
		if ((currtok = currtokenentry(charpair->data)) == NULL)
		{
			if ((currtok = MALLOC(currtokentype)) != NULL)
			{
				currtok->name = charpair->data;
				currtok->period = token_periods(currtok->name);  /* period of token */
				currtok->next = currtokens;
				currtok->active = 0;
				currtokens = currtok;
			}
		}
		if (currtok != NULL) currtok->number = charpair->num;
		charpair = charpair->next;
	}
} /* update_tokens */

int count_active_tokens()
/* return the total number of positive active tokens */
{
	currtokentype *currtok = currtokens;
	int	total = 0;
	while (currtok != NULL)
	{
		if (currtok->active == command_no && currtok->number > 0)
			total += currtok->number;
		currtok = currtok->next;
	}
	return total;
}

int mark_token(char *tokname)
/* if the token tokname is in currtokens, mark the entry and return true */
{
	currtokentype *cp;
	if ((cp = currtokenentry(tokname)) != NULL)
		cp->active = command_no;
	return (cp != NULL);
}

void mark_all_tokens(int include_expired)
/* mark all current, unexpired tokens as active.
 * if (include_expired) then mark the expired tokens as well
 */
{
	currtokentype	*cp;
	period		*pp, *ptemp;
    
	if (! include_expired)	/* mark only those tokens that are not yet expired */
		pp = newperiod(ABSOLUTE, now, construct(tnow.tm_year+1900+1,0,0,0,0) - 1, NULLP);
	else pp = ptemp = NULLP;

	for (cp = currtokens; (cp != NULL); cp = cp->next)
	{
		if (include_expired || (ptemp = intersect(pp, cp->period)) != NULL)
			cp->active = command_no;
		rfree(ptemp);
	}
	rfree(pp);
}

period *union_tokenperiods()
/* return the union of the periods of all the active tokens */
{
	period	*pp = NULL;
	currtokentype	*cp;
	for (cp = currtokens; (cp != NULL); cp = cp->next)  {
		if (cp->active == command_no) {
			if (pp != NULL)
				pp = periodunion(pp, copyperiods(cp->period));
			else pp = copyperiods(cp->period);
		}
	}
	return pp;
} /* union_tokenperiods */

/********** booking cache routines **********/

int cmpbooked(const void *b1v, const void *b2v)
/* function used by qsort to compare two bookings according to start time  */
{
	const booked *b1=b1v, *b2=b2v;
	time_t diff;
	diff = b1->start - b2->start;
	return ((diff < 0) ? -1 : (diff > 0) ? 1 : 0);
}

booked *alreadybooked(time_t start, bookuid_t user, char *lab)
/* return the booked period starting start, owned by user, in lab
 * ignore user == -1 and lab == NULL
 */
{
	booked	*bp;
	for (bp = currbooked;
	     (bp != NULL &&
	      (bp->start != start || (user != -1 && bp->owner != user)
	       || (lab != NULL && strcmp(lab, bp->labname) != 0)));
	     bp = bp->next);
	return(bp);
}

void insertbookelement(booked **head, booked *new)
/* insert into place the new booked element into the list pointed to be head */
{
	booked	*bp, *prev;
	for (prev = NULL, bp = *head;
	     (bp != NULL && bp->start < new->start); bp = bp->next)
		prev = bp;
	new->next = bp;
	if (prev != NULL)
		prev->next = new;
	else *head = new;
}

void addbooking(slotgroup *sp, int whenmade,
		char *tokname, time_t start, char *labname, int nbooked)
/* add new (PENDING) booked record to list of current bookings */
{
	booked	*newbooked;
	book_key	slot;
    
	slot = make_bookkey(sp->doy, sp->pod, desc_2_labind(&sp->what)); 
	if ((newbooked = MALLOC(booked)) != NULL)
	{
		newbooked->mdesc = sp->what;
		newbooked->slot = slot;
		newbooked->whenmade = whenmade;
		newbooked->tokname = tokname;
		newbooked->start = start;
		newbooked->labname = labname;
		newbooked->owner = user;
		newbooked->ownername = username;
		newbooked->nbooked = nbooked;
		newbooked->status = B_PENDING;		/* can only add pending bookings */
	
		/* insert the newbooked element into the current (PENDING) booked list */
		insertbookelement(&currbooked, newbooked);
	}
}

void delbooking(booked *todelete)
/* delete unbooked record from list of current bookings
 * and add it to list of past bookings
 */
{
	booked	*prev, *bp;
	for (prev = NULL, bp = currbooked;
	     (bp != NULL && bp != todelete); bp = bp->next) prev = bp;
	if (bp == todelete)
	{
		if (prev != NULL)
			prev->next = bp->next;
		else currbooked = bp->next;
		todelete->status = B_CANCELLED;
		insertbookelement(&pastbooked, todelete);
	}
	else
	{
		puts("Cannot find slot to delete: ");
		printslot(todelete->start, 1);
		puts("");
	}
}

void addbookings(bookuid_t uid, booked **currbookings, int *nbookings, int pastbookings)
{
	userbookinglist	userbooklist, ublp;
	booklist		blist, bp;
	booked		*bookedp;
	int			n, doy, pod, labind;
	char		*uidname;
	if (uid != user)
	{
		if ((uidname = find_username(uid)) ==  NULL)
			uidname = unknown;	

	}
	else uidname = username;
	/* get the bookings for the user/class nominated */
#ifdef DEBUG
	printf("adding %s bookings for %d (%s)\n",
	       pastbookings ? "past" : "current", uid, uidname);
#endif
	n = 0;
	if (pastbookings)
		userbooklist = getpastbookings(uid);	/* bookings with status != PENDING */
	else userbooklist = getbookings(uid);	/* bookings with status == PENDING */
	if (userbooklist != NULL)
	{
		for (ublp = userbooklist; (ublp != NULL); ublp = ublp->next) n++;
		if (n > 0)
		{
#ifdef DEBUG
			printf("\tfound %d bookings\n", n);
#endif
			n += *nbookings;
			if (*currbookings == NULL)
				*currbookings = (booked *) malloc(n*sizeof(booked));
			else *currbookings = (booked *) realloc(*currbookings, n*sizeof(booked));
			if (*currbookings != NULL)
			{
				blist = getfullbookings(userbooklist, uid);
				for (bp=blist, ublp=userbooklist, bookedp = *currbookings + *nbookings;
				     (ublp != NULL);
				     (bp = bp==NULL?NULL:bp->next), ublp = ublp->next, bookedp++)
				{
					if (bp) bookedp->mdesc = bp->charged;
					bookedp->slot = ublp->slot;
					bookedp->whenmade = ublp->when;
					bookedp->tokname = bp?get_mappingchar(bp->tnum, M_TOKEN): unknown;
					book_key_bits(&(ublp->slot), &doy, &pod, &labind);
					bookedp->start = dp_2_time(doy, pod);
					bookedp->labname = get_mappingchar(labind, M_ATTRIBUTE);
					bookedp->owner = uid;
					bookedp->ownername = uidname;
					bookedp->nbooked = bp?bp->number:1;
					bookedp->status = bp?bp->status:B_PENDING;
					if (bookedp->status != B_ALLOCATED
					    && bookedp->status != B_RETURNED
					    && bookedp->status != B_FREEBIE)
						bookedp->defaulted = 0;
					else
					{
						if ((bp->claimed & DID_DEFAULT)
						    && !(
							    (bp->claimed&DID_LOGIN)
							    && (bp->claimed&LOGIN_ON_ALLOC)
							    && (bp->claimed>>4)-30*60 < 7*60) /* FIXME */
							)
							bookedp->defaulted = 1;
						else
							bookedp->defaulted = 0;
					}
					bookedp->printed = 0;
					bookedp->next = NULL;	/* corrected in initbookings() */
				}
				*nbookings = n;
			}
			else *nbookings = 0;
		}
	}
}

void initbookings()
/*
 * Set the list of the user's current bookings and tokens.
 * Bookings are sorted in order of start time,
 * tokens left in the order that they were returned from database.
 */
{
	int			n, i;
	booked		*bookedp, *nextbp;
	bookuid_t		*class_udb_ptr;
    
	currbooked = pastbooked = NULL; n = 0;
	addbookings(user, &currbooked, &n, 0);
    
	/* add the bookings for classes that user belongs to */
	if ((class_udb_ptr = (bookuid_t*)user_classes(user)) != NULL)
	{
		/* class_udb_ptr-> (nentries, uid {, classid}) */
		for (i = *class_udb_ptr++; (i>1); i--)
			addbookings(*++class_udb_ptr, &currbooked, &n, 0);
	}
    
	/* sort the bookings in date order, and create a linked list of bookings.
	 * (linked list is used so that deletions and insertions of bookings are easy)
	 */
	if (n > 0)
	{
		qsort(currbooked, n, sizeof(booked), cmpbooked);
		for (bookedp = currbooked, i = 1; (i < n); bookedp++, i++) bookedp->next = bookedp + 1;
		bookedp->next = NULL;
	}
    
	/* now get the past bookings for this user (but not their classes) */
	n = 0;
	addbookings(user, &pastbooked, &n, 1);
	if (n > 0)
	{
		qsort(pastbooked, n, sizeof(booked), cmpbooked);
		for (bookedp = pastbooked, i = 1; (i < n); bookedp++, i++) bookedp->next = bookedp + 1;
		bookedp->next = NULL;
	}

	/* cancel pending bookings owned by the user that are for the past */
	for (bookedp = currbooked; (bookedp != NULL); bookedp = nextbp)
	{
		nextbp = bookedp->next;
		if (bookedp->status == B_PENDING
		    && bookedp->start < now
		    && bookedp->owner == user)
			removebooking(user, bookedp);
	}
    
	currtokens = NULL;
	update_tokens(user);
} /* initbookings */

/******** main booking functions **********/

int minconsecfree(consecfree *cfp);
int maxconsectotal(consecfree *cfp);

int printtokavail(int nconsec, time_t start, tokused *tokavail, int makebooking, int counts)
{
	/* if 'counts' in set, we provide extra information about have much space in
	 * available in each lab.
	 * if counts==2, print labname(freeworkstations)
	 * if counts==1, print:
	 *	labname  if > warn_percent free
	 *	labname? if <= warn_percent free, but not full
	 *	labname! if full, but still included in list (i.e. no overbook_percent)
	 */
	tokused	*tp;
	labavail	*lp;
	int		ntoken, nlabs;
	int		warn_percent;

	warn_percent = get_configint("warn_percent");
	if (warn_percent < 0) warn_percent = RESERVE_PC;
    
	ntoken = 0;
	for (tp = tokavail; (tp != NULL); tp = tp->next)
	{
		if (tp->nlabs > 0)
		{
			nlabs = 0;
			for (lp = tp->labs; (lp != NULL); lp = lp->next)
			{
				if (lp->laststart == start && lp->nperiods >= nconsec)
				{
					int free = minconsecfree(lp->consecfree);
					int size = maxconsectotal(lp->consecfree);
					if (nlabs++ == 0)
					{
						if (ntoken++ == 0)
						{
							if (makebooking)
								fputs("Choice", stdout);
							puts("\tToken\t\t\tLabs");
						}
						if (makebooking)
							fprintf(stdout, "(%d)", ntoken);
						fprintf(stdout, "\t%-16s", tp->tokname);
					}
					if (counts==2) fprintf(stdout, "\t%s(%d)", lp->labname, free);
					else if (counts==1 && free > 0 && free < (size*warn_percent/100+1))
						fprintf(stdout,"\t[%s]", lp->labname);
					else if (free <= 0)
						fprintf(stdout,"\t(%s)", lp->labname);
					else
						fprintf(stdout, "\t%s", lp->labname);
				}
			}
			if (nlabs) putchar('\n');
		}
	}
	return ntoken;
} /* printtokavail */

static int choosetoken(int nconsec, time_t start, tokused *tokavail,
		tokused **tokchoice, int ntoken)
/*
 * return an indicator of what the user wants to do,
 * and (via parameter) a pointer to the available token chosen (if any).
 */
{
	int		i, n, response;
	tokused	*tp;
    
	do {
		printtokavail(nconsec,start,tokavail,TRUE, FALSE);
		n = ntoken;	/* the number of tokens available in tokavail */
		response = getresponse("Choose Token", &n, 1);
		if ((i = (response == R_YES && n == 0)))
			puts("Cannot respond with 'y'\n");
	} while (i);
    
	if (response == R_YES)
	{
		for (tp = tokavail, i = 0;
		     (tp != NULL && (tp->nlabs <= 0 || ++i < n));
		     tp = tp->next);
		if (tp == NULL)
		{
			fprintf(stderr, "Cannot find the token choice %d !\n", n);
			response = R_NO;
		}
		else *tokchoice = tp;
	}
    
	return response;
} /* choosetoken */

void printlab(labavail *lab)
/* print the number of available terminals in this lab for all
 * consecutive free slots
 */
{
	consecfree	*cp = lab->consecfree;	/* cp is a circular linked list */
	printf("\t%-8s", lab->labname);
	do {
		printf("\t%d", cp->nfree);
	} while ((cp = cp->next) != lab->consecfree);
	putchar('\n');
}

int minconsecfree(consecfree *cfp)
/* Return the minimum no of free machines in the consecutive slots in cfp */
{
	consecfree	*cp = cfp;	/* cfp is a circular linked list */
	int		min = cp->nfree;
	while ((cp = cp->next) != cfp) if (cp->nfree < min) min = cp->nfree;
	return min;
}

int maxconsectotal(consecfree *cfp)
/* Return the maximum total machines over the consecutive slots in cfp
 * This should not change between the consecutive slots, but we shouldn't
 * assume anything.
 */
{
	consecfree	*cp = cfp;	/* cfp is a circular linked list */
	int		max = cp->ntotal;
	while ((cp = cp->next) != cfp) if (cp->ntotal < max) max = cp->ntotal;
	return max;
}

static int chooselab(int nconsec, time_t start, tokused *tokavail,
labavail **labchoice, bookuid_t user, int labgiven)
/*
 * return an indicator of what the user wants to do,
 * and (via parameter) a pointer to the machine description chosen (if any).
 * Do not allow the user to choose a lab that has already been booked
 * (this applies particulary to class bookings which are allowed to book more
 * than one lab at the same time; normal users do not even get to choose another
 * booking if they already have one for the same time).
 */
{
	labavail	*maxminlp, *lp;
	int		nlabs, i, n, minfree, maxmin, maxminindex;
	int		response;

#ifdef OVERBOOK
	int 	obindex;
	labavail	*oblp;
	int		obpercent, obavail = 0;

	obpercent = get_configint("over_book_percent");
	if (obpercent <= 0) obpercent = 0;
#endif    

	/* print the labs to be chosen from */
	puts("Choice\tLab\t\tNo of machines free per slot");
	nlabs = maxmin = 0;
	for (lp = tokavail->labs; (lp != NULL); lp = lp->next)
	{
		if (lp->laststart == start && lp->nperiods >= nconsec
		    && alreadybooked(start, user, lp->labname) == NULL)
		{
			printf("(%d)", ++nlabs);
			printlab(lp);
			minfree = minconsecfree(lp->consecfree);
			if (maxmin < minfree)
			{
				maxmin = minfree;
				maxminlp = lp;
				maxminindex = nlabs;
			}
#ifdef OVERBOOK
			/* see if this lab is a candidate for over-booking */
			if (minfree < 0 && obpercent) {
				int t = maxconsectotal(lp->consecfree);
				t = (t * obpercent) / 100;
				if (t+minfree > obavail) {
					obavail = t+minfree;
					oblp = lp;
					obindex = nlabs;
				}
			}
#endif
		}
	}
	n = nlabs;
	if ((n = nlabs) == 1 && user_is_class)
	{
		/* there is only one choice of lab, pick it if the user is a class
		 * as class users are asked to choose a number of machines to book
		 * later anyway.
		 */
		*labchoice = maxminlp;
		return R_YES;
	}
	if (n == 1 && labgiven) response = R_YES;
	else
		response = getresponse((n == 1) ? "Book Lab" : "Choose Lab", &n, 1);
    
	/* find the lab chosen */
	if (response == R_YES)
	{
		if (n > 0)
		{
			if (n != maxminindex)
			{
				for (lp = tokavail->labs, i = 0;
				     (lp != NULL &&
				      (lp->laststart != start || lp->nperiods < nconsec ||
				       alreadybooked(start, user, lp->labname) != NULL ||
				       ++i < n));
				     lp = lp->next);
				if (lp == NULL)
				{
					/* should not be possible! */
					printf("Cannot find the lab choice %d !\n", n);
					response = R_NO;
				}
				else maxmin = minconsecfree(lp->consecfree);
			}
			else lp = maxminlp;
		}
		else
		{
			lp = maxminlp;
			printf("Picked choice (%d) - %s\n", maxminindex, lp->labname);
		}
	}
    
	if (response == R_YES)
	{
		int warn_percent = get_configint("warn_percent");
		if (warn_percent < 0) warn_percent = RESERVE_PC;
		*labchoice = lp;
		/* issue warning if needed */
		i = maxconsectotal(lp->consecfree);
		if (maxmin < (i * warn_percent / 100) + WARNING)
		{
			printf("Warning: You have %s one of the last %d terminals available in %s\n",
			       ((n<=0 && nlabs>1) ? "been given" : "chosen"),
			       (i * warn_percent / 100) +WARNING, lp->labname);
			puts("\tDepending on the state of the lab at allocation time,");
			puts("\tyour booking MIGHT NOT be able to be honoured,");
			puts("\tin which case your token will be refunded.");
		}
	}
    
	return response;
} /* chooselab */

void utillist_add(utilizationlist *head, utilizationlist item)
/* insert a copy of the new item into the list.
 * list is in decreasing order of free machines
 */
{
	utilizationlist new, prev, curr;
	if ((new = MALLOC(utilizationnode)) != NULL)
	{
		*new = *item;		/* copy data */
		curr = *head;
		prev = NULL;
		while (curr != NULL && curr->free > new->free)
		{
			prev = curr;
			curr = curr->next;
		}
		if (prev == NULL)
			*head = new;
		else prev->next = new;
		new->next = curr;
	}
}

void utillist_free(utilizationlist *head)
/* free all entries in the list head */
{
	utilizationlist up1, up2;
	for (up1 = *head; (up1 != NULL); up1 = up2)
	{
		up2 = up1->next;
		free(up1);
	}
	*head = NULL;
}

int mergebooking(tokused **tokavail, time_t start, char *tok,
		 int nmach, int nconsec, int minpcfree, description labs)
/*
 * tokavail:	The list of available (tokens * labs * free) collected so far.
 * start:	Period to be booked.
 * tok:		The name of the token to be used.
 * nmach:	The number of machines required to be booked at once.
 * nconsec:	The number of consecutive free slots required.
 * minpcfree:	A lab is not free unless % free > minpcfree
 * labs:	a bitset indicating allowable labs
 *
 * Get the list of free labs at start using tokname, and add them to tokavail.
 */
{
	utilizationlist	freelist, flp;
	tokused		*tp;
	labavail		*lp;
	consecfree		*cp;
	char		*labname;
	int			labindex;
	int			i, dayofyear, periodofday;
    
#ifdef DEBUG
	printf("Mergebooking(%d, '%s', ", nconsec, tok);
	printslot(start, 1);
	puts(")");
#endif
	/* get the token entry */
	for (tp= *tokavail; (tp != NULL && strcmp(tp->tokname, tok)); tp=tp->next);
	if (tp == NULL)
	{
		if ((tp = MALLOC(tokused)) != NULL)
		{
			tp->tokname = tok;
			tp->labs = NULL;
			tp->next = *tokavail;
			*tokavail = tp;
		}
		else
		{
			fputs("Malloc error\n", stderr);
			return 0;
		}
	}

	/* get the list of free slots using token tok */
	time_2_dp(start, &dayofyear, &periodofday);
	freelist = freeslots(tok, dayofyear, periodofday);
#ifdef DEBUG
	printf("freeslots(%s, %d, %d) returns %snull\n",
	       tok, dayofyear, periodofday, (free == NULL) ? "" : "non-");
#endif
    
	/* collate the labs returned */
	for (flp = freelist; (flp != NULL); flp = flp->next)
	{
		labindex =  desc_2_labind(&(flp->machine_set));
		if (labs.item_len == 0 ||
		    query_bit(&labs, labindex))
		{
			labname = get_mappingchar(labindex, M_ATTRIBUTE);
#ifdef DEBUG
			printf("add to lab: '%s' free: %d\n", labname, flp->free);
#endif
			for (lp=tp->labs;
			     (lp != NULL && strcmp(lp->labname,labname));
			     lp=lp->next);
	
			if (lp == NULL)
			{
				/* add a new lab to this token's availablility */
				if ((lp = MALLOC(labavail)) != NULL)
				{
					lp->labname = labname;
					lp->nperiods = 0;
		
					/* add a circular list of nconsec free periods */
					lp->consecfree = NULL;
					for (i=0; (i < nconsec); i++)
					{
						if ((cp = MALLOC(consecfree)) != NULL)
						{
							cp->nfree = cp->ntotal = 0;
							/* cp->periodofday = cp->dayofyear = 0; not necessary */
							cp->speclist = NULL;
							if (i == 0)
							{
								cp->next = cp;
								lp->consecfree = cp;
							}
							else
							{
								/* consecfree is a circular linked list */
								cp->next = lp->consecfree->next;
								lp->consecfree->next = cp;
							}
						}
						else
						{
							fputs("Malloc error\n", stderr);
							return 0;
						}
					}
		
					lp->next = tp->labs;
					tp->labs = lp;
				}
				else
				{
					fputs("Malloc error\n", stderr);
					return 0;
				}
			}
			else if (start - lp->laststart > SECINPERIOD) lp->nperiods = 0;
	
			cp = lp->consecfree;
			if (lp->laststart != start)
			{
				/* Have not marked this lab off yet.
				 * Note: it is possible to have more than one machine type in the
				 * same lab. Each machine type may be returned in free */
				lp->laststart = start;
				cp->periodofday = periodofday;
				cp->dayofyear = dayofyear;
				cp->start = start; /* a bit redundant when nconsec == 1 */
				cp->ntotal = cp->nfree = 0;
				utillist_free(&(cp->speclist));
			}
			cp->nfree += flp->free;
			cp->ntotal += flp->total;
	
			/* If there is more than one machine type free in the same lab,
			 * keep track of the machine type with the most free so that this machine
			 * type will be booked if this lab is selected.
			 * Note: Users are given choices between labs, but not between machine
			 * types within the same lab. We always book the machine type that is most free.
			 */
			utillist_add(&(cp->speclist), flp);
		}
	}

	/* neilb 16feb95
	 * if the token is available in privileged form atleast consecfree
	 * times, then set minpcfree to 0
	 */
	if (minpcfree>0 && (nconsec * nmach) <= priv_tok_avail(tok))
		minpcfree = 0;

    
	/* We have collated the number of all available terminals in each lab.
	 * Now check whether each lab meets the minimum free requirements.
	 */
	tp->nlabs = 0;		/* the number of labs available */
	for (lp = tp->labs; (lp != NULL); lp = lp->next)
	{
#ifdef DEBUG
		printslot(lp->laststart, 1);
		printf(": lab(%s) avail = %d", lp->labname, lp->consecfree->nfree);
#endif
		if (lp->laststart == start)
		{
			cp = lp->consecfree;
			if (cp->nfree >= nmach && cp->ntotal * minpcfree < cp->nfree * 100)
			{
				lp->consecfree = cp->next;	/* next available consecfree */
				if (lp->nperiods < nconsec)
					lp->nperiods++;
				if (lp->nperiods >= nconsec) tp->nlabs++;
			}
			else lp->nperiods = 0;
		}
		else lp->nperiods = 0;
#ifdef DEBUG
		printf(", nconsec = %d\n", lp->nperiods);
#endif
	}
    
	/* active labs are those with laststart == start && nperiods > 0
	 * available labs are those with laststart == start && nperiods == nconsec
	 */
	return tp->nlabs;
} /* mergebooking */

void resetconsecperiods(tokused *tokavail)
/* simply resets number of consecutive free periods available for
 * all tokens, and all labs under all tokens. Called after sucessfully
 * making a booking of required number of consecutive periods.
 */
{
	tokused	*tp;
	labavail	*lp;
	for (tp = tokavail; (tp != NULL); tp = tp->next)
		for (lp = tp->labs; (lp != NULL); lp = lp->next) lp->nperiods = 0;
}

static void removebooking(bookuid_t user, booked *bp)
{
	if (unbook(user, bp->whenmade, bp->slot))
	{
		delbooking(bp);
		puts("Unbook successfull");
	}
	else fprintf(stderr, "Unbook failed!\n");
}

int makechange(booked *bkp, int nmachines, char *newtokname, int *nbkp)
/*
 * Change the current booking:
 *	from (bkp->tokname, bkp->nbooked)
 *	to (newtokname, nmachines).
 * Can only be used for class bookings.
 * if (nmachines == 0) prompt user for new number of machines.
 * return the number of booked machines in nbkp.
 */
{
	slotgroup	slottobook;
	int		maxfreeinlab, resp, nchange;
	utilizationlist	free, fp;

	*nbkp = 0;
    
	time_2_dp(bkp->start, &(slottobook.doy), &(slottobook.pod));
	slottobook.what = bkp->mdesc;

	/* find the maximum additional bookings that can be made */
	maxfreeinlab = 0;
	free = freeslots(bkp->tokname, slottobook.doy, slottobook.pod);
	for (fp = free; (fp != NULL); fp = fp->next)
	{
		/* compare free lab with lab booked */
		if (strcmp(bkp->labname,
			   get_mappingchar(desc_2_labind(&(fp->machine_set)),M_ATTRIBUTE))
		    == 0)
			maxfreeinlab += fp->free;
	}

	if (nmachines == 0)
	{
		nmachines = maxfreeinlab + bkp->nbooked;
		resp = getresponse("New number of bookings", &nmachines, 0);
		if (resp != R_YES)
			return resp;
		else if (nmachines == 0 &&
			 (resp = getresponse("This will delete the current booking, are you sure", &nmachines, 1)) != R_YES)
			return resp;
	}

	if (nmachines == 0)
	{
		/* remove the class booking */
		removebooking(user, bkp);
		return R_YES;
	}
	else nchange = nmachines - bkp->nbooked;

	if (nchange != 0 || strcmp(bkp->tokname, newtokname) != 0)
		/* actually make the change */
		if (makebook2(user, &slottobook, newtokname, bkp->whenmade, nchange, user, -1))
		{
			printf("Changed booking (%s = %d) to (%s = %d)\n",
			       bkp->tokname, bkp->nbooked,
			       newtokname, bkp->nbooked + nchange);
			bkp->nbooked = nmachines;
			bkp->tokname = newtokname;
			*nbkp = nmachines;
		}
		else
		{
			puts("*** Failed to change booking ***");
			resp = R_NO;
		}
	else
	{
		resp = R_YES;
		*nbkp = nmachines;
	}
	return resp;
}


int try_change_token(booked *bkp, int nmachines, int *nbookedp)
{
	/*
	 * If there exists an active token that books the same lab as
	 * is already booked by bkp->tokname, then change the booking bkp
	 * to use the first such token.
	 */
	currtokentype *tokenp;
	int	*labindex;
	int	ret, found;

	for (tokenp = currtokens; (tokenp != NULL); tokenp = tokenp->next)
	{
		if (tokenp->active == command_no)
		{
			found = 0;
			labindex = tokname2labs(tokenp->name);
			while (*labindex != -1 &&
			       ! (found = (strcmp(get_mappingchar(*labindex, M_ATTRIBUTE), bkp->labname) == 0)))
				labindex++;
			if (found) break;
		}
	}
	if (found)
		ret = makechange(bkp, nmachines, tokenp->name, nbookedp);
	else
	{
		*nbookedp = 0;
		ret = R_NO;
	}
	return ret;
}

int check_class_changes(booked *bkp, int nmachines, int *nbookedp)
{
	/* Try to change bkp into booking nmachines using one of
	 * the currently active tokens that books the same lab as
	 * is already booked by bkp->tokname.
	 * If possible, use the same token as is already used.
	 */
	int ret;
	if (token_active(bkp->tokname))
		ret = makechange(bkp, nmachines, bkp->tokname, nbookedp);
	else ret = try_change_token(bkp, nmachines, nbookedp);
	return ret;
}

void b_showfree(command_type *comm)
{
	/* Called for commands: "book" and "available"
	 * Using the token(s) passed, and over the period(s) specified,
	 * find those labs that are still available, print them out,
	 * and if required allow the user to choose to make bookings.
	 */
	booked	*bookedp, *bkp;
	int		nconsec;	/* number of consecutive periods being booked */
	int		nmachreq;	/* number of machines requested at a time */
	int		minmachreq;	/* min no of machines to be booked at a time */
	int		nbooked;	/* the number of machines already booked */
	int		makebooking, totaltokens;
	time_t	start, end, reserve_end, delay;
	int		tofirst, ntoken, nfound, minpcfree;
	int		response, i, j;
	int		ignore_tokcnt=0; /* if "all" option is given and this is "Avail" command, we ignore token limitations */
	tokused	*tokavail;
	slotgroup	slottobook;
	currtokentype	*tokenp;
    
	makebooking = (comm->command == BOOK);
	if (!makebooking && option_value(comm->options, O_ALL_TOKENS)>0) ignore_tokcnt = 1;
	if ((nconsec = option_value(comm->options, O_CONSEC)) == 0) nconsec = 1;
	if (user_is_class)
		/* note: (nmachreq == 0) ==> (prompt user for nmachreq) */
		nmachreq = option_value(comm->options, O_MACHINES);
	else nmachreq = 1;
    
	/* get the set of tokens to be used for bookings */
/*    update_tokens(user); */

	/* find out the delay time that people can book in, ie the time
	   limit before a period that they can book for the next period */
	delay=0;
	delay = get_configint("book_delay");
	if (delay <= 0 ) 
		delay = 600; /* 10 minutes by default */
    
	/* can only book periods from now onwards */
	start = time_2_dp(currtime() + delay, &i, &j);
	/* ordinary user cannot book the current, or next period */
	if (! privilaged) start += MINPERIODAHEAD * SECINPERIOD;
    
	/* default period is all non-excluded periods */
	if ((tofirst = (comm->period == NULL)))
		comm->period = absperiod(notperiod(copyperiods(empty_abs)));
    
	/* can only book next period onwards - exclude past periods */
	if ((comm->period = exclude_past(comm->period)) == NULL)
	{
		fputs("\tCan only book or check future periods\n", stdout);
		return;		/**** return ****/
	}
#ifdef DEBUG
	puts("command period :");
	printperiod(comm->period, "");
	puts("token periods :");
	printperiod(union_tokenperiods(), "");
#endif
    
	/* intersect the requested period(s) with the token period(s) */
	if ((comm->period = fintersect(union_tokenperiods(), comm->period)) != NULL)
	{
		if (comm->period->start > start)
			start = time_2_dp(comm->period->start, &i, &j);	/* ignore i,j */
		end = endofperiods(comm->period);
	}
#ifdef DEBUG
	puts("intersect(command, token) :");
	printperiod(comm->period, "");
#endif
    
	if (comm->period == NULL || start > end)
	{
		fputs("\tTokens expired", stdout);
		if (! tofirst) fputs(" or unavailable in specified period", stdout);
		puts(".");
		return;		/**** return ****/
	}
    
	if ((totaltokens = count_active_tokens()) <= 0 && !ignore_tokcnt)
	{
		fputs("\tNo tokens available", stdout);
		if (comm->ntokpassed != 0) fputs(" from tokens specified", stdout);
		puts(".");
		return;		/**** return ****/
	}
    
	nfound = 0;
	reserve_end = construct(1900+tnow.tm_year,0,tnow.tm_yday+MINDAYRESERVE,0,0);
	tokavail = NULL;
	for (; (start < end && ( totaltokens > 0 || ignore_tokcnt) && ! interrupt);
	     start += SECINPERIOD)
	{
		if (t_within_period(start, comm->period) != NULLP)
		{
			nbooked = 0;
			if ((bookedp = alreadybooked(start, -1, NULL)) != NULL)
			{
				booked	*nextbkp;
				/* there may be more than one booking for this time */
				for (bkp = bookedp; (bkp != NULL && bkp->start == start); bkp = nextbkp)
				{
					/* check_class_changes() might delete bkp from list */
					nextbkp = bkp->next;
		    
					fputs("Period: ", stdout);
					printslot(bkp->start, 1);
					printf(" - already booked '%s'", bkp->labname);
		    
					if (bkp->owner == user)
					{
						if (user_is_class) printf(" (%d)", bkp->nbooked);
						printf(" with token '%s'\n", bkp->tokname);

						if (makebooking && user_is_class
						    && ((nmachreq == 0 && nbooked == 0)
							|| (nmachreq > nbooked)))
						{
							int nbkd;
							if (check_class_changes(bkp, nmachreq-nbooked, &nbkd) == R_QUIT)
							{
								totaltokens = 0;
								break;
							}
							nbooked += nbkd;
						}
					}
					else printf(" for class <%s>\n", bkp->ownername);
				}
			}
			/*
			 * if (more tokens
			 *    && (want to book more)
			 *    && (not already booked or user is class)
			 */ 
			if ((totaltokens != 0 || ignore_tokcnt)
			    && ((nmachreq > nbooked) || (nmachreq == 0 && nbooked == 0))
			    && (user_is_class || alreadybooked(start, user, NULL) == NULL))
			{
				if (start < reserve_end)
					minpcfree = 0;
				else {
					minpcfree = get_configint("warn_percent");
					if (minpcfree < 0)
						minpcfree = RESERVE_PC;
				}

				/* the minimum number of machines required at a time */
				if (user_is_class && nmachreq != 0)
					minmachreq = (nmachreq - nbooked);
				else minmachreq = 1;

				/* collate the freeslots for all specified tokens */
				ntoken = 0;
				for (tokenp = currtokens;
				     (tokenp != NULL);
				     tokenp = tokenp->next)
				{
					if (tokenp->active == command_no
					    && t_within_period(start, tokenp->period) != NULL
					    && (tokenp->number >= (nconsec * minmachreq) || ignore_tokcnt))
					{
						if (mergebooking(&tokavail, start, tokenp->name,
								 minmachreq, nconsec, minpcfree,
								 comm->labs))
							ntoken++;
					}
				}
		
				if (ntoken)
				{
					/* we have specified at least one token that can be used
					 * to book nconsec periods in a lab.
					 */
					fputs("Period: ", stdout);
					printslot(start-((nconsec-1)*SECINPERIOD), nconsec);
					if (nconsec > 1)
						printf(" (%d consecutive slots", nconsec);
					else printf(" (1 slot");
					if (minmachreq > 1)
						printf(", %d machines", minmachreq);
					puts(")");
		    
					if (! makebooking)
						printtokavail(nconsec, start, tokavail, makebooking, option_value(comm->options, O_COUNTS));
					else
					{
						tokused *tp;
			
						if (ntoken == 1)
						{
							/* find and print the one token */
							for (tp = tokavail;
							     (tp != NULL && tp->nlabs == 0);
							     tp = tp->next);
							if (tp != NULL)
							{
								response = R_YES;
								fprintf(stdout,"Token: %s\n", tp->tokname);
							}
							else response = R_NO;	/* should not happen */
			    
						}
						else response = choosetoken(nconsec, start, tokavail, &tp, ntoken);
			
						switch (response)
						{
							labavail	*labs;
							consecfree	*cp;
			    
						case R_YES:
							switch (chooselab(nconsec, start, tp, &labs, user, (comm->labs.item_len != 0)))
							{
							case R_YES:
								for (tokenp = currtokens;
								     (tokenp != NULL && strcmp(tokenp->name ,tp->tokname));
								     tokenp = tokenp->next);
								if (tokenp != NULL)
								{
									utilizationlist	up;
									if (user_is_class)
									{
										int max;
										/* classes can book more than one machine */
										cp = labs->consecfree;
										max = cp->nfree;
										while ((cp=cp->next) != labs->consecfree)
											if (cp->nfree < max) max = cp->nfree;
										if (nmachreq == 0)
										{
											minmachreq = max;
											switch (response = getresponse("How many machines", &minmachreq, 1))
											{
											case R_YES:
												if (minmachreq == 0)
													minmachreq = max;
												break;
											case R_QUIT:
												minmachreq = totaltokens = 0;
												break;
											case R_NO:
												minmachreq = 0;
												break;
											}
										}
										/* else minmachreq == (nmachreq-nbooked)) */
									}
				    
									/* for every consecutive slot chosen,
									 * book the number of machines required,
									 * from those groups of machines available.
									 */
									cp = labs->consecfree;
									/* for every consecutive slot: */
									do {
										int	tobook, nbook;
										slottobook.doy = cp->dayofyear;
										slottobook.pod = cp->periodofday;
										currtime();	/* change value of now */
										tobook = minmachreq;
										/* Note: speclist is the list of
										 * machine groups that make up the lab,
										 * ordered from most free to least free.
										 */
										for (up = cp->speclist;
										     (up != NULL && tobook > 0);
										     up = up->next)
										{
											if (tobook > up->free)
												nbook = up->free;
											else nbook = tobook;
											slottobook.what = up->machine_set;
											if (makebook2(user,&slottobook,tokenp->name,enow,nbook, user, tokenp->number))
											{
												addbooking(&slottobook, enow, tokenp->name,
													   cp->start, labs->labname, nbook);
												update_tokens(user);
												totaltokens = (tofirst) ? 0 : count_active_tokens();
												tobook -= nbook;
												printf("Booked");
											}
											else printf("Failed to book");
											if (nbook > 1)
												printf(" %d machines,", nbook);
											printf(" period: ");
											printslot(cp->start, 1);
											putchar('\n');
										}
										if (tobook > 0 && minmachreq > 1)
											printf("Could not book %d out of %d machines\n", tobook, minmachreq);
									} while ((cp=cp->next) != labs->consecfree);
									resetconsecperiods(tokavail);
								}
								break;
							case R_QUIT:	totaltokens = 0; break;
							case R_NO:		break;
							}
							break;
						case R_QUIT:	totaltokens = 0; break;
						case R_NO:	break;
						}
					}
					nfound += ntoken;
				}
			} /* period unbooked */
		} /* start in selected period */
	} /* for periods */
    
	if (nfound == 0)
	{
		fputs("\tNo future free slots found", stdout);
		if (comm->ntokpassed) fputs(" using tokens specified", stdout);
		if (interrupt || tofirst)
		{
			fputs(" up to ", stdout);
			printslot(start, 1);
		}
		else fputs(" in period specified", stdout);
		puts("");
	}
	if (interrupt || tofirst)
	{
		fputs("\tSearched up to ", stdout);
		printslot(start, 0);
		puts("");
	}
} /* b_showfree */

char *status2str(int status)
{
	char *s;
	switch (status)
	{
	case B_PENDING:	s = "Pending";	break;
	case B_TENTATIVE:	s = "Tentative"; break;
	case B_ALLOCATED:	s = "Alloc'd";	break;
	case B_RETURNED:	s = "Returned"; break;
	case B_FREEBIE:	s = "Free";	break;
	case B_REFUNDED:	s = "Refund";	break;
	case B_CANCELLED:	s = "Cancel";	break;
	default:		s = "Unknown";	break;
	}
	return s;
}

void printperiodheader(int showstatus)
{
	printf("Lab\t\tToken or <Class>\t%s%sPeriod\n",
	       (showstatus ? "Status\t" : ""),
	       (user_is_class ? " N   " : ""));
}

void printbookperiod(booked *bookedp, int nconsec, int showstatus)
{
	int fieldlen;
	printf("%-8s\t", bookedp->labname);
	fieldlen = 24;
	if (bookedp->owner == user)
	{
		printf("%s", bookedp->tokname);
		fieldlen -= strlen(bookedp->tokname);
	}
	else
	{
		printf("<%s>", bookedp->ownername);
		fieldlen -= (strlen(bookedp->ownername) + 2);
	}
	if (fieldlen > 0) printf("%*s", fieldlen, " ");
	if (showstatus) printf("%s%s\t", status2str(bookedp->status), bookedp->defaulted?"(D)":"");
	if (user_is_class) printf("%-4d ", bookedp->nbooked);
	printslot(bookedp->start, nconsec);
	fputs("\n", stdout);
}

void listbooking(booked *start, int showstatus)
{
	/* Print the booking start if it has not already been printed.
	 * Include as many consecutive bookings matching (and following) start
	 * as possible.
	 */
	booked *nxt, *curr;
	int nconsec;
	if (start->printed != command_no)
	{
		nxt = curr = start;
		nconsec = 1;
		while (curr == nxt)
		{
			curr->printed = command_no;
			/* skip same times */
			for (nxt = curr->next;
			     (nxt != NULL && nxt->start == curr->start);
			     nxt = nxt->next);
			/* get matching next period */
			while (nxt != NULL
			       && nxt->start == curr->start + SECINPERIOD
			       && (nxt->owner != curr->owner
				   || nxt->nbooked != curr->nbooked
				   || nxt->status != curr->status
				   || strcmp(nxt->tokname, curr->tokname)
				   || strcmp(nxt->labname, curr->labname)
				       )) nxt = nxt->next;
			if (nxt != NULL && nxt->start == curr->start + SECINPERIOD)
			{
				nconsec++;
				curr = nxt;
			}
		}
		printbookperiod(start, nconsec, showstatus);
	}
}

void b_showbook(command_type *comm)
/*
 * This routine deals with bookings made within the periods specified using the
 * tokens specified.
 * Called with commands: "list" , "unbook", and "change".
 * Restrictions:
 * List:   None.
 * Change: Only classes can invoke this.
 * Unbook:
 * The user will not be allowed to cancel a slot less than MINDAYPREUNBOOK
 * days before the slot is to be taken up unless the booking was made less
 * than UNBOOKGRACE seconds before the cancellation and the booking was
 * more than MINPERIODAHEAD periods ahead (ie: not the current or next slot).
 */
{
	booked	*bp, *next;
	int		command, n, resp, nfound, nconsec, nmachreq;
	time_t	start, minstart;

	command = comm->command;
	if (command == CHANGE)
	{
		if (user_is_class)
			nmachreq = option_value(comm->options, O_MACHINES);
		else
		{
			puts("Only classes may use the 'change' command - Other users must use 'book' or 'unbook'");
			return;
		}
	}

	if (currbooked != NULL)
	{
		if (command == UNBOOK)
		{
			/* set up unbook restrictions */
			start = currtime();
			start = time_2_dp(start, &n, &resp); /* n, resp ignored */
			start += MINPERIODAHEAD * SECINPERIOD;
			minstart = construct(1900+tnow.tm_year, 0, tnow.tm_yday+MINDAYPREUNBOOK,
					     0, 0) - 1;
		}
		nfound = nconsec = 0;
		printperiodheader(0);
	
		for (bp = currbooked; (bp != NULL); bp = next)
		{
			int pod,doy,lab;
			book_key_bits(&bp->slot, &doy, &pod, &lab);
			/* check_class_changes() or removebooking() might delete bp from list */
			next = bp->next;
	    
			if ((comm->period == NULLP || t_lap_period(bp->start, comm->period))
			    && (comm->labs.item_len == 0 || query_bit(&comm->labs, lab))
				)
			{
				if (command == CHANGE)
				{
					if (bp->owner == user)
					{
						int n;		/* we ignore the number booked */
						nfound++;
						printbookperiod(bp, 1, 0);
						if (check_class_changes(bp, nmachreq, &n) == R_QUIT)
							break;
					}
				}
				else if (comm->ntokpassed == 0 || token_active(bp->tokname))
				{
					if (command == LIST)
					{
						nfound++;
						listbooking(bp, 0);
					}
					else /* command == UNBOOK */
						if (bp->owner == user &&
						    (privilaged || bp->start > minstart ||
						     (bp->start >= start &&
						      (bp->whenmade >= enow - UNBOOKGRACE || token_is_reusable(bp->tokname))
							     )))
						{
							nfound++;
							if (option_value(comm->options, O_CONFIRM))
							{
								printbookperiod(bp, 1, 0);
								n = 0;
								resp = getresponse("Unbook", &n, 1);
							}
							else resp = R_YES;
							if (resp == R_QUIT)
								break;
							else if (resp == R_YES)
							{
								removebooking(user, bp);
								/* unbook only the first if no period specified */
								if (comm->period == NULLP) break;
							}
						}
				}
			}
		}
		if (nfound == 0)
		{
			fputs("\tNo bookings", stdout);
			if (comm->ntokpassed != 0) fputs(" using tokens specified",stdout);
			if (comm->period != NULLP) fputs(" in period specified", stdout);
			switch (command)
			{
			case LIST:		puts(" were found"); break;
			case UNBOOK:	puts(" could be unbooked"); break;
			case CHANGE:	puts(" could be changed"); break;
			}
		}
	}
	else puts("\tNo bookings recorded");
} /* b_showbook */

void b_showpastbook(command_type *comm)
/* This routine shows past bookings made within the periods specified using the
 * tokens specified, and if refunding is true, will allow the user to
 * interactively have these bookings refunded, depending on the status of the
 * booking.
 * There are a couple of restrictions on refunding:
 *	Only privilaged people may make refunds.
 *	Only bookings that have been ALLOCATED may be refunded.
 */
{
	booked	*bp;
	int		command, nconsec, resp, n, nfound, doy, pod;
	time_t	start;
	reclaim_result	recres;

	switch (command = comm->command)
	{
	case REFUND:
		if (! privilaged)
		{
			puts("\tOnly privilaged users may attempt refunds");
			return;
		}
		else break;
	case RECLAIM:
		/* create a dummy current period */
		start = currtime();
		start = time_2_dp(start, &doy, &pod);	/* ignore doy, pod */
		comm->period = newperiod(ABSOLUTE, start , start + SECINPERIOD , NULLP);
		comm->ntokpassed = 0;	/* ignore any tokens passed */
		break;
	}
    
	if (pastbooked != NULL)
	{
		nfound = nconsec = 0;
		if (command != RECLAIM) printperiodheader(command == PAST);
		for (bp = pastbooked; (bp != NULL); bp = bp->next)
		{
			if ((comm->period == NULLP || t_lap_period(bp->start, comm->period))
			    && (comm->ntokpassed == 0 || token_active(bp->tokname)))
			{
				char *cp;
				switch (command)
				{
				case PAST:
					nfound++;
					listbooking(bp, 1);
					break;
				case REFUND:
					/* can only refund Allocated, returned and freebie bookings */
					if (bp->status == B_ALLOCATED || B_FREEBIE || B_RETURNED)
					{
						nfound++;
						printbookperiod(bp, 1, 0);
						n = 0;
						resp = getresponse("Refund", &n, 1);
						if (resp == R_YES)
						{
							if (refund(user, bp->whenmade, bp->slot))
							{
								bp->status = B_REFUNDED;
								puts("Refund successfull");
							}
							else fprintf(stderr, "Refund failed!\n");
						}
						else if (resp == R_QUIT)
						{
							bp = NULL ; /* fall out for for loop as well as switch */
							break;
						}
					}
					break;
				case RECLAIM:
					/* would be faster to simply reclaim the most recent past booking */
					recres = reclaim(bp->labname, user);
					switch (recres.state)
					{
					case RECOK:
						printf("Machine '%s' has been reclaimed\n", recres.host);
						cp = ctime(&recres.when);
						if (cp)
							printf("You should be able to log in at %5.5s\n",cp+11);
						nfound++;
						break;
					case RECNO:
						fprintf(stderr, "Reclaim failed - Network Problem.\n");
						break;
					case RECUNASSIGNED:
						fprintf(stderr, "No current booking for lab '%s'\n", bp->labname);
						break;
					case RECDOWN:
						fputs("Allocated to a terminal that is down - cannot reclaim it\n", stderr);
						break;
					case RECUNUSED:
						printf("Your allocated machine '%s' is not being used. Go get it\n",
						       recres.host);
						break;
					case RECSELF:
						fputs("You cannot reclaim a booking you are already using.\n",
						      stderr);
						break;
					}
					break;
				}
			}
		}
		if (nfound == 0)
		{
			fputs("\tNo bookings", stdout);
			if (comm->ntokpassed != 0) fputs(" using tokens specified",stdout);
			if (comm->period != NULLP) fputs(" in period specified", stdout);
			fputs(" could be ", stdout);
			switch (command)
			{
			case REFUND: puts("refunded"); break;
			case PAST:	puts("found"); break;
			case RECLAIM: puts("reclaimed"); break;
			}
		}
	}
	else puts("\tNo past bookings recorded");
} /* b_showpastbook */

/************ the main booking commmand routines **************/
/*** note: b_define and b_undefine are defined in bperiod.c ***/

void b_token(command_type *comm)
{
	printtokens(comm->ntokpassed, option_value(comm->options, O_ALL_TOKENS));
	/* ignore period if passed to token command */
}

void b_labs(command_type *comm)
{
	/* for each lab, see if it is bookable at any time
	 * in the given range
	 */
	int *lablist;
	int *openlist;
	int l;
	time_t aperiod;
	lablist = get_all_labs();
	for (l=0; lablist[l]!= -1 ; l++);
	openlist=(int*)malloc(sizeof(int)*(l+1));

	if (comm->period == NULL)
		comm->period = absperiod(range(book_mktime(0,0), book_mktime(23,59)));
	comm->period = exclude_past(comm->period);
	if (comm->period != NULL)
	{
		for (l=0; lablist[l]!= -1 ; l++)
			if (!lab_excluded(lablist[l]))
			{
				openlist[l]=0;
				for (aperiod=comm->period->start;
				     aperiod <= comm->period->end && openlist[l]==0;
				     aperiod+= SECINPERIOD)
				{
					int doy,pod;
					time_2_dp(aperiod, &doy, &pod);

					if (check_bookable(doy, pod, lablist[l]))
						openlist[l]=1;
				}
			}
			else openlist[l]=0;
		for (l=0 ; lablist[l]!= -1; l++)
			if (openlist[l])
			{
				char *n = get_mappingchar(lablist[l], M_ATTRIBUTE);
				if (n) printf(" %s", n);
				if (n) free(n);
			}
	}
	printf("\n");
	free(openlist);
	free(lablist);
}

void b_times(command_type *comm)
{
	/* for each bookperiod in the time given, check
	 * to see if it might be bookable in any lab with any token
	 */
	int *lablist;
	int periodlist[SECINDAY/SECINPERIOD];
	time_t aperiod;
	int p;
	int l;
	int inrange;

	lablist = get_all_labs();
	if (comm->period == NULL)
		comm->period = absperiod(range(book_mktime(0,0), book_mktime(23,59)));
	comm->period = exclude_past(comm->period);
	memset(periodlist, 0, sizeof(periodlist));
	for (l=0 ; lablist[l] != -1 ; l++)
		if (lab_excluded(lablist[l])) lablist[l] = -2;
	for (aperiod = comm->period->start;
	     aperiod <= comm->period->end;
	     aperiod += SECINPERIOD)
	{
		int dperiod = (aperiod % SECINDAY)/SECINPERIOD;
		int l;
		for (l=0; lablist[l]!= -1 && periodlist[dperiod]==0;
		     l++)
			if (lablist[l]>=0)
			{
				int doy,pod;
				time_2_dp(aperiod, &doy, &pod);

				if (check_bookable(doy, pod, lablist[l]))
					periodlist[dperiod]=1;
			}
	}
	free(lablist);
	inrange=0;
	for (p=0; p<=SECINDAY/SECINPERIOD ; p++)
	{
		if (p<SECINDAY/SECINPERIOD && periodlist[p])
		{
			if (!inrange)
				printf(" %02ld:%02ld", p*SECINPERIOD/3600, ((p*SECINPERIOD)%3600)/60);
			inrange++;
		}
		else
		{
			if (inrange > 1)
				printf("-%02ld:%02ld", (p*SECINPERIOD-60)/3600, (((p*SECINPERIOD-60))%3600/60));
			inrange=0;
		}
	}
	printf("\n");
    
}

void b_help(command_type *comm)
{
	int help_type, help_value;

	help_type = option_value(comm->options, O_HELP_TYPE);
	help_value = option_value(comm->options, O_HELP_VALUE);

	if (help_type != 0 || (comm->period == NULLP && comm->ntokpassed == 0))
		printhelp(typevaltostr(help_type, help_value));

	if (help_type == TOPIC && help_value == PREDEF)
		printpredefs();

	if (comm->ntokpassed > 0 || (help_value == TOKEN 
				     && (help_type == COMND || help_type == TOPIC)))
		printtokens(comm->ntokpassed, 0);

	if (comm->period != NULLP)
	{
		putchar('\t');
		printperiod(comm->period, "\t");
	}
}

void b_echo(command_type *comm)
{
	strlist *sp;
	for (sp = comm->strlist; (sp != NULL); sp = sp->next)
		fputs(sp->str_val, stdout);
	puts("");
}


void b_shell(command_type *comm)
{
	/* fork, become user, and execute users shell as a login shell
	 * parent waits for 4 minutes and then (warning at 3 minutes)
	 * kills the child and continues
	 */
	extern int ntimeoutwarnings;
	int childid = 0, pid;
	struct passwd *pwe;
	char *sh;
	char shellbase[100];
	char *sl;
	int sig;

	if (realuser != 0 && realuser != user)
	{
		fputs("book: cannot run a shell unless you are booking for your self\n", stderr);
		return;
	}
    
	if (user_is_class)
	{
		fputs("book: cannot run shell for a class!!\n", stderr);
		return;
	}
#if 0
#ifdef TIOCGPGRP
	ioctl(0, TIOCGPGRP, &termpgrp);
#else
	termpgrp = tcgetpgrp(0);
#endif
#endif
	switch(childid = fork())
	{
	default: /* parent */
		break;
	case -1:
		perror("fork");
		fputs("book: Cannot fork a new shell, sorry!\n", stderr);
		return ;
	case 0: /* child */
		setuid(0);
		pwe = getpwuid(user);
		if (pwe == NULL)
		{
			fputs("book: cannot find user database entry, sorry.\n", stderr);
			exit(1);
		}
		initgroups(pwe->pw_name, pwe->pw_gid);
		setgid(pwe->pw_gid);
		setuid(user);
		if (chdir(pwe->pw_dir)!= 0)
		{
			perror("book: chdir");
			fprintf(stderr, "book: cannot change to HOME directory %s\n", pwe->pw_dir);
			exit(1);
		}
		sh = pwe->pw_shell;
		if (sh == NULL || sh[0] == '\0')
			sh = "/bin/sh";
#if 0
		if (strncmp(sh, "/usr/local/etc/logins/", 22)==0)
		{
			strcpy(shellname, "/usr/local/bin/");
			strcat(shellname, sh+22);
			sh=  shellname;
		}
#endif
		sl = strrchr(sh, '/');
		shellbase[0] = '-';
		if (sl)
			strcpy(shellbase+1, sl+1);
		else
			strcpy(shellbase+1, sh);
		fputs(" ....... Running a Shell. You have five minutes........\n", stderr);
		putenv("LNAME=root");
		execl(sh, shellbase, NULL);
		perror(shellbase);
		fputs("Failed to run your shell.\n", stderr);
		exit(120);
	}
	ntimeoutwarnings = -1;
	alarm(4*60);
	pid = wait(0);
	sig = 0;
	while (pid != childid && (pid >=0 || errno != ECHILD))
	{
		switch(sig)
		{
		case 0:
			sig = 15;
			fputs("\n------- Shell will be killed in one minute, please finish up ------ \n" , stderr);
			alarm(60);
			break;
		case 15:
			fputs("\n======= Time expired, killing shell and exitting ======\n", stderr);
			kill(childid, sig);
			sig = 9;
			alarm(3);
			break;
		case 9:
			kill(childid, sig);
			alarm(3);
		}
		pid = wait(0);
	}
	if (sig == 9) exit(0);
	alarm(0);
#if 0
#ifdef TIOCSPGRP
	ioctl(0, TIOCSPGRP, &termpgrp);
#else
	tcsetpgrp(0, termpgrp);
#endif
#endif
}
