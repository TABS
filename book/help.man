.TH BOOK 1 "18 September 1992" "UNSW"
.SH NAME
book \- terminal booking interface.
.SH SYNOPSIS
.B book
.RB [ -h ]
.RI [ uid | login ]
.SH DESCRIPTION
.I Book
is used to make, inspect, or cancel terminal bookings interactively.
Terminal bookings are reservations of terminals in particular labs at
particular times for particular users or classes of users.
The
.I book
command provides users with an interface to the booking management system
which forms one half of the Terminal Allocation and Booking
System (TABS) at UNSW. The terminal allocation system
is not dealt with here, except where allocations affect bookings.
.PP
There are a number of simple book commands which
may be entered interactively in response to the prompt: \fB"BOOK >"\fP.
Book commands may also be stored in a command file which is read at
invocation (see Command Files below).
.SH OPTIONS
.TP
.RI [ uid | login ]
Changes the current user to the new user with the given uid or login name.
This flag can only be used by root or members of the group wheel,
and is provided as a means of simulating and fixing simple user problems.
.PP
.SH COMMANDS
.SS General Syntax
Most book commands follow the general form defined below.
.nf
.ta 1i 2i 3i
\fIcommand =\fB	[\fIcommand_name\fB] {\fItokens\fB} [\fIperiod\fB]\fP
\fIcommand_name =\fR
	"book" \fB|\fP "unbook" \fB|\fP "available" \fB|\fP "show" \fB|\fP
	"refund" \fB|\fP "past" \fB|\fP "change" \fB|\fP
	"define" \fB|\fP "undefine" \fB|\fP "token" \fB|\fP "echo" \fB|\fP
	"help" \fB|\fP "quit" \fB|\fP "exit" .
\fItokens =\fP	booking token name.
\fIperiod =\fP	time specification.
.fi
Each command is described in more detail under the section
"COMMAND DESCRIPTIONS".
Tokens and periods are described in more detail under the section
"CONCEPTS".
.SS General Defaults
If no command is given, but a period or tokens are specified,
then the help command is assumed.
.br
If no period is given, then the next appropriate period from now is
usually assumed. However, this will depend upon the actual
command invoked.
.br
If no tokens are given, then all available or appropriate tokens
will usually be assumed. However, this will depend upon the actual
command invoked.
.SS Abbreviations
Most commands may be abbreviated to their first character with the following
exceptions:
.br
	"undefine" - abbreviation: "und" (to distinguish it from "unbook"
- abbrev: "u").
.br
	"exit" - abbreviation: "ex" (to distinguish it from "echo" -
abbrev: "e").
.br
In fact any predefined string may generally be abbreviated to at least
its unique prefix (see Periods in CONCEPTS below for more predefined strings).
.SS Case Sensitivity
No predefined string is case sensitive, including command names. (ie:
"Book", "book", "BOOK" are equivalent).
.bp
.SH COMMAND DESCRIPTIONS
.TP
\fBBook { \fItokens\fB } [ \fIperiod\fB ] { \fIoptions\fB }\fR
.RS
.TP .6i
where:
.nf
\fIoptions\fP =	[ "consecutive" ] "=" \fIC\fP
	| "machines" "=" \fIM\fP .
\fIC\fP | \fIM\fP =	"0".."9" { "0".."9" } .
.fi
.TP .6i
Synopsis:
Interactively make a booking:
for the current user;
within the period(s) specified;
using one or more of the tokens specified;
for a sequence of C consecutive periods at a time;
booking M machines at a time (Class bookings only).
.TP .6i
Defaults:
If no period is specified,
then stop after the first successful booking.
.br
If no tokens are specified,
then all tokens available to the current user will be used.
.br
If not specified, C and M are both 1.
.TP .6i
Operation:
Basically, for every sequence of available slots that meets the
criteria, the user will be asked to choose from a list of tokens
that can be used, and then from a list of labs that are available.
If there is not a number of tokens or labs to choose from, then the
user will be shown the one token or lab available and asked if they
want to book that.
.RS
.PP
In general, when prompted, the user may respond with:
.TP 5
.I "y"
Make the booking.
.TP 5
.I "n"
Don't book this period - try the next one.
.TP 5
.I "q"
Quit - Return to the BOOK prompt.
.PP
When prompted with a choice of tokens or labs, the user may also
respond with:
.TP 5
.I number
The corresponding item from the list will be used;
.TP 5
.I "y"
Pick the lab with the most free machines.
(This response is only valid when choosing labs)
.PP
After making a booking, the program will search for the next sequence
of slots to book within the period specified, and the user is prompted
once again. If no period was specified, book returns to the command
prompt.
.RE
.TP .6i
Restrictions:
.RS
.TP 5
\(bu
The user is not allowed to book
any slot in the past, the current slot, or the next slot.
.TP 5
\(bu
Consecutive slots will be booked using tokens of the same type.
To book consecutive slots using different token types, the user must resort
to using the book command more than once, specifying (or choosing) a different
token type for each successive slot.
.TP 5
\(bu
The user will not be allowed to book a slot that has already been
booked for that user, regardless of whether the booking was made by the user
himself/herself, or booked by the administrator of a class of which
the user is a member.
.RE
.TP .6i
Note:
A terminal booking does not imply a terminal allocation.
This is due to the fact that bookings are based on the number
of physical terminals in a lab, while allocations are based on the number of
these terminals which are up (working) and available to the network
at the time of allocation.

Thus, if the number of bookings exceeds the number of terminals
actually available at allocation, then these excess bookings cannot be
honoured. As bookings are allocated in the chronological order in which
they were made, it is the last few bookings made for a particular lab and slot
that may not be honoured.

Consequently, if a user books one of the last available slots in a lab, then
the user will be warned that the booking may not be honoured.
.TP .6i
Class Bookings:
.RS
If the user is a class, the user is required to specify the number of
machines to be booked for each period satisfying the specs.
If this number has not been passed as an option, the user will be
prompted for it for every period found.
.PP
In general, a class booking will be always be made without prompting
if there are no unknown parameters and no choices to be made.
Ordinary users however, have to at least confirm that they want to make
the booking.
.RE
.RE

.TP
\fBUnbook { \fItokens\fB } [ \fIperiod\fB ]\fR
.RS
.TP .6i
Synopsis:
Interactively cancel all bookings made:
by the current user;
using any of the tokens specified;
for any booking slot within the periods specified;
.TP .6i
Defaults:
If no period is specified,
then the next booked period made with any of the specified
tokens will be cancelled.
.br
If no tokens are specified,
then all bookings made by the user in the period(s) specified
will be cancelled, regardless of what token was used to make
the booking.
.TP .6i
Operation:
When a booking is found matching the criteria,
the user is prompted with:
.RS
.TP 5
\(bu
The booking slot taken;
.TP 5
\(bu
The token used to make the booking.
.PP
The user may respond with:
.TP 5
.I y
The booking is cancelled, and the token credited to the user.
.TP 5
.I n
The booking is not cancelled, and the program searches for
  the next booking meeting the criteria.
.TP 5
.I q
Quit - The program returns to the command prompt "BOOK >"
.RE
.TP .6i
Restrictions:
The user will not be allowed to cancel a slot on or after the day
of the slot unless the booking was made less than an hour before the
cancellation and the booking was not for the current or next slot.
.RE

.TP
\fBChange { \fIname\fB } [ \fIperiod\fB ] [[\fR"machine"\fB]\fR "=" \fIN\fB ]\fR
.RS
.TP .6i
where:
\fIN\fP =  "0".."9" { "0".."9" } .
.TP .6i
Synopsis:
Can only be used by a user who is a class.
Interactively increase or decrease the number of machines booked
by the current class;
using any of the tokens specified;
for any booking slot within the periods specified;
to the number of machines specified;
.TP .6i
Defaults:
.RS
If the total number of machines booked per period is not specified,
then the user will be prompted for the machine booking changes
for each booking matching the criteria.
.PP
If no period is specified,
then the next class booking made with any of the specified
tokens will be cancelled.
.PP
If no tokens are specified,
then all bookings made by the class in the period(s) specified
will be cancelled, regardless of what token was used to make
the booking.
.RE
.TP .6i
Operation:
.RS
When a booking is found matching the criteria,
the user is shown:
.TP 5
\(bu
The booking slot taken;
.TP 5
\(bu
The token used to make the booking;
.TP 5
\(bu
The number of machines booked.
.PP
If the number of machines is already specified with the "mach ="
option, then the booking is changed to that number of machines,
otherwise the user is prompted for the change to be made and the
user may respond with:
.TP 5
\(bu
A positive or negative number (the change to be made);
.TP 5
"n"
The booking is not changed, and the program searches for
the next booking meeting the criteria.
.TP 5
"q"
Quit - The program returns to the command prompt "BOOK >"
.RE
.TP .6i
Notes:
.RS
.PP
If the change removes more machine bookings than were made, the
booking is removed altogether (same as "unbook").
.PP
The number of machines may only be increased by as many machines
as are currently unbooked.
.PP
The option "machines" may be shortened to "mach" or left out altogether.
.RE
.TP .6i
Restrictions:
Only users who are classes may use this command.
.RE

.TP
\fBAvailable { \fItokens\fB } [ \fIperiod\fB ] [[\fR"consecutive"\fB]\fR "=" \fIN\fB ]\fR
.RS
.TP .6i
where:
\fIN\fP =  "0".."9" { "0".."9" } .
.TP .6i
Synopsis:
List those periods which may be booked:
by the current user;
within the period(s) specified;
using one or more of the tokens specified;
for a sequence of N consecutive slots at a time.
.TP .6i
Defaults:
If no period is specified, then the next available period is found.
.br
If no tokens are specified,
then any of the tokens available to the current user may be used.
.br
If not specified, N is 1.
.TP .6i
Restrictions:
.RS
.TP 5
\(bu
Neither the next slot, the current slot, nor any slot in the past,
is available to the user.
.TP 5
\(bu
Consecutive slots are only available using tokens of the same type.
.RE
.TP .6i
Note:
This command will not make or cancel any bookings.
.br
The option "consecutive" may be shortened to "consec", or left out altogether.
.RE

.TP
\fBShow { \fItokens\fB } [ \fIperiod\fB ]\fR
.RS
.TP .6i
Synopsis:
List the bookings that have been made:
for the current user;
within the periods specified;
using any of the tokens specifed.
.TP .6i
Output:
.RS
Two kinds of bookings may be shown:
.TP .6i
1)
Bookings made for the user by the user.
.TP .6i
2)
Bookings made for a class that the user is a member of, by the class
administrator.
.PP
Each booking is identified by:
the lab containing the booked terminal;
the period of the booking.
.PP
If the booking is one made by the user,
then the token used to make the booking is shown,
otherwise the class booked is shown in angle brackets instead.
.RE
.TP .6i
Defaults:
If no period is specified, then all bookings are shown.
.br
If no tokens are specified, then bookings made with any token are shown.
.TP .6i
Note:
This command will not make or cancel any bookings.
.RE

.TP
\fBPast { \fItokens\fB } [ \fIperiod\fB ]\fR
.RS
.TP .6i
Synopsis:
.RS
List the expired bookings:
for the current user;
that were booked within the periods specified;
that used (or tried to use) any of the tokens specifed.
.PP
Expired bookings are those that have been cancelled, or those that
have already been allocated a terminal, ie: any booking that
is not pending.
.RE
.TP .6i
Output:
.RS
Each expired booking is identified by:
.TP 5
\(bu
the lab containing the booked terminal;
.TP 5
\(bu
the token used to make the booking;
.TP 5
\(bu
the period of the booking;
.TP 5
\(bu
the status of the booking.
.PP
The booking status will be one of the following:
.PP
.nf
.ta 1i 2i 3i
Status	 Meaning
Alloc'd	The booking was successfully allocated a terminal.
Refund	The booking was not successfully allocated a terminal, and the token
	used to make the booking was refunded.
Cancel	The booking was cancelled by the user before it could be allocated
	a terminal.
Free	The booking was successfully allocated a terminal, but as the lab was
	underutilised at the time, the user was refunded the token that he/she
	had used to make the booking.
.fi
.RE
.TP .6i
Defaults:
If no period is specified, then all expired bookings are shown.
.br
If no tokens are specified, then expired bookings made with any
token are shown.
.RE

.TP
\fBRefund { \fItokens\fB } [ \fIperiod\fB ]\fR
.RS
.TP .6i
Synopsis:
.RS
Refund the tokens spent in making allocated bookings:
for the current user;
that were booked within the periods specified;
that used any of the tokens specifed.
.PP
Allocated bookings are those that have been successfully allocated
a terminal in the past, and for which one token has been spent.
.RE
.TP .6i
Output:
.RS
When an allocated booking is found meeting the criteria, the user
is prompted with:
.TP 5
\(bu
the lab containing the booked terminal;
.TP 5
\(bu
the token used to make the booking;
.TP 5
\(bu
the period of the booking;
.TP 5
\(bu
the status of the booking (which will always be Alloc'd);
.TP 5
\(bu
the prompt string: "refund ?".
.PP
The user may respond with:
.TP 5
"y"
The token used to make the booking is refunded to the user,
and the booking status turned into "Refund".
.TP 5
"n"
The token is not refunded, and the program searches for
the next expired booking meeting the criteria.
.TP 5
"q"
Quit - The program returns to the command prompt "BOOK >"
.RE
.TP .6i
Restrictions:
This command may only be used by a privilaged user.
.RE

.TP
\fBDefine { \fIname\fB } [ \fIperiod\fB ]\fR
.RS
.TP .6i
Synopsis:
Add a new predefined period (predef) to the existing list of predefs.
The new period may subsequently be refered to in a period specification
by any of the names given.
.TP .6i
Defaults:
If no period or names are given, then the name of all predefs are printed.
.br
If no new predef names are given, then the period (or predef) is
displayed as defined, not as instanciated.
eg: compare "define ah" with "help ah".
.br
(for the difference between period definition and instanciation,
see Periods in CONCEPTS below).
.TP .6i
Notes:
The period expression passed to define may of course include any predefs
already defined. (eg: ah (after hours) is defined by reference to bh (business
hours) by the command: "define ah not bh")
.RS
.PP
To change an existing predef definition, the user must first undefine
the predef, and then redefine it.
.PP
If the name given to a period is the same as a token name, the user
will not be able to explicitly refer to that token after the define.
.RE
.TP .6i
See Also:
For more information on predefs, see Predefined Periods in CONCEPTS below.
.RE

.TP
\fBUndefine \fIquotename\fB { \fIquotename\fB } \fR
.RS
.TP .6i
where:
\fIquotename\fP = `" name "'
.br
ie: predef name to be undefined is in lower case and enclosed in double quotes.
.TP .6i
Synopsis:
Remove from the current set of predefined periods (predefs), the
predefs with the names listed.
.TP .6i
Restrictions:
Cannot remove excluded predefs ("exclude").
.TP .6i
See Also:
For more information on predefs, see Predefined Periods in CONCEPTS below.
.RE

.TP
\fBEcho { `" \fIstring\fB "' }
.RS
.TP .6i
Synopsis:
Print the string given within double quotes on standard output.
.TP .6i
Notes:
The string MUST start with and be terminated by a double quote.
.RS
.PP
The double quotes are not printed.
.PP
The string may extend over more than one line in which case the
intervening newlines are included as part of the string.
.PP
Consecutive quoted strings are concatenated.
.PP
This command is particularly useful in command files
.RE
.TP .6i
See Also:
For information on command files, see Command Files in CONCEPTS below.
.RE

.TP
\fBHelp [ \fIcommand\fP  |  \fItopic\fP ] { \fItokens\fP } [ \fIperiod\fP ]
.RS
.TP .6i
where:
.nf
\fIcommand\fP =	"book" | "unbook" | "available" | "show" | "define"
	| "undefine" | "token"  | "echo" | "help" | "quit" | "exit" .
\fPtopic\fP =	"period" | "predef" | "syntax" | "cfile" | "command" .
.fi
.TP .6i
Synopsis:
Displays help information on the command, topic, period, or tokens
specified.
.TP .6i
Defaults:
If none of the above are specified,
then an overall introduction to the book interface is displayed.
.br
If a period specification is given,
then the actual time periods mapped by the specification are displayed as
instanciated. (For the definition of period instanciation,
see Periods in CONCEPTS below).
.br
If the name of a token is given,
then the number of these tokens assigned to the user is displayed.
.TP .6i
Note:
Since the help command is the default command, the user will usually
not have to type "help" (or "h") at all, just the arguements to the
help command on their own. The only exception to this is when the
arguement to the help command is itself the name of a command (obviously!).
.RE

.TP
\fBToken ( [\fP "all" \fB ] | { \fItokens\fB } )
.RS
.TP .6i
Synopsis:
Show the number of tokens currently assigned to the user.
.TP .6i
Defaults:
If no tokens are named,
then list all unexpired tokens currently assigned to the user.
If "all" is named, show all tokens regardless of expiry date
.TP .6i
Notes:
.RS
Tokens are assigned to users of the booking system, and used by them to
make terminal bookings. Users refer to tokens by name, and may pass
these token names to various book interface commands.
.PP
Tokens can be used to book a restricted set of periods only.
Tokens of the same name have the same set of restricted periods.
.PP
Token names cannot be abbreviated (unlike commands, which can be).
.RE
.TP .6i
See Also:
For more details on what each command does
with the list of token names passed to it,
refer to the specific command entry.
.RE

.TP
\fBQuit | Exit | <Control-D>\fP
.RS
.TP .6i
Synopsis:
Exit from the book program.
.RE

.SH CONCEPTS
.TP
.B Periods
The simplest period is that defined by a start time and an end time.
(The end time has to be later than the start time, or the period does not
exist). A period expression involves the union and/or intersection of
such simple periods. Such expressions usually lead to a series of
non-intersecting periods, each starting after the previous has ended.
Most book commands will accept a period expression which allows the
users to specify the specific periods that they would like to book, inspect, or
cancel. The Syntax of such an expression is as follows:
.RS
.TP .6i
Syntax:
.nf
\fIperiod\fP =	\fIintersection\fP .
\fIintersection\fP =	\fIunion\fP {\fIunion\fP} .
\fIunion\fP =	\fIrange\fP {"," \fIrange\fP} .
\fIrange\fP =	["not" | "~"] \fIstarttime\fP {"-" \fIendtime\fP} .
\fIendtime\fP =	\fIperiodtime\fP .
\fIstarttime\fP =	\fIperiodtime\fP .
\fIperiodtime\fP =	"(" \fIintersection\fP ")"
	| \fItime\fP | \fIday\fP | \fIdate\fP | \fImonth\fP | \fIpredef\fP .
\fItime\fP =	\fIhour\fP [":" \fImin\fP] ["am" | "pm"] .
\fIdate\fP =	\fIdayofmonth\fP "/" \fImonthofyear\fP ["/" \fIyear\fP] .
\fIday\fP =	"monday"  | ... | "sunday" | "today" | "tomorrow" .
\fImonth\fP =	"january" | ... | "december" .
\fIpredef\fP =	\fIpredef_id\fP ["." \fIweek\fP]
\fIpredef_id\fP =	The name of one of the predefined periods (see Predefs below).
\fIhour\fP = \fImin\fP = \fIweek\fP = \fIdayofmonth\fP = \fImonthofyear\fP = \fIyear\fP = "0".."9"{"0".."9"}
.fi
.TP .6i
Notes:
.RS
.TP 5
\(bu
All of the predefined strings (eg: "monday", "february", ...), may be
abbreviated to their shortest unique prefix (eg: "mo", "fe", .).
.TP 5
\(bu
Predefined strings are not case sensitive (eg: "Mon", "mon", "MON" are
equivalent).
.TP 5
\(bu
Users may experiment with period specifications using the \fBhelp\fP
and \fBdefine\fP commands.
.RS
.PP
The \fBhelp\fP command will list the actual
(instanciated) periods matched by a period expression. These periods are
the ones mapped to an actual date, and are the periods that are passed
to all commands except \fBdefine\fP.
.PP
The \fBdefine\fP command
will list the evaluation of the period expression before instanciation
takes place. (eg: compare: "define 10-12 Tue" with "help 10-12 Tue" or
"define evening" with "help evening").
.RE
.TP 5
\(bu
The special predefined period \fI"excluded"\fP, defines those periods that
will never be instanciated (see Predefined Periods below).
.RE
.TP .6i
Defaults:
A booking period currently has a minimum duration of half an hour.
If a user specifies a start time for the period, but not an end time
then the end time will depend on the type of start time specified.
.nf
Start time	Default end time
--------------------------------------------
Time of day	time of day + 30 minutes
Day of Week	end of that day of week
Predef.week	end of that predefined week.

eg:
Start time	Default end time
--------------------------------------------
10am	10:29:59
12:30 tue	12:59:59 Tue
Tue	23:59:59 Tue
s1.2	Sunday of second week of session 1
.fi
.TP .6i
Period Examples:
.nf
10am	The next (10:00 to 10:29) period.
	Today if the current time is before 10am, otherwise Tommorrow.
10-12	The next (10:00 to 11:59) period.
	Today or tommorrow depending on current time.
10-12,2pm Tue	The next ((10:00 to 11:59, and 14:00 to 14:29) of Tuesday)
	If today is Tuesday
	and the current time is later than the specified period,
	then the times will refer to next Tuesday's periods,
10 tue-thur	next 10:00 to 10:29 on Tuesday, Wednesday, and Thursday
2pm tue october	14:00 to 14:29 on all Tuesdays in October of this year
Thur,Tue s1.4	Tuesday and Thursday of the fourth week of the predefined
	period s1 (s1 will normally be defined to be session 1 of the current
	year (see Predefined Periods below).
(11,2pm-3pm wed),(10:30 thur) s1.2-s1.4
	The various periods on Wednesdays and Thursdays
	of the second week of session 1
	to the fourth week (inclusive) of session 1
.fi
.RE

.TP
.B Predefined Periods (Predef)
.RS
.TP .6i
Synopsis:
A predefined period or predef is a period that has been given a name.
This period may be used in other period expressions by refering to its
name (in full) in the period expression.
.RS
.PP
A particular week of the predefined period may be refered to by
.br
	"name.week"
.br
where "name" is the name of the predefined period, and "week" is the number
of the week desired.
.PP
A new period is added to the list of existing predefs by using the
command "define", and removed from the list using "undefine".
.RE
.TP .6i
Special Predefs:
The predef named "excluded" is special in a number of ways:
.RS
.TP 5
(1)
This predef defines those periods which will never be instanciated.
These periods usually refer to public holidays when students will not
generally be given access to the workstations.
.TP 5
(2)
There may be more than one period named "excluded". These
periods are merged into the one predef.
.TP 5
(3)
Once a period has been excluded, it cannot be unexcluded.
ie: excluded periods cannot be undefined.
.RE
.TP .6i
Notes:
.RS
Periods start from week 1 (week 0 and week 1 are synonamous).
.br
To see what predefs have been defined, type: "define"
.br
To see what a predef has been defined as, type: "define" predef_name.
.br
To see what periods have been excluded, type: "define excluded"
.PP
While predefs may be defined interactively, they are more usually
defined in command files (see Command Files below).
.RE
.RE

.TP
.B Command Files
.RS
.TP .6i
Synopsis:
A book interface command file contains booking commands that the book
interface reads and executes before accepting interactive commands from
the user.
These command files will usually contain non-interactive booking
commands - most commonly the "define" command.
.TP .6i
Files:
There are two command files that the book interface will read:
.RS
.TP
.B /usr/local/lib/book/periods
System command file which define
general periods and predefs that are useful
for the current year.
.TP
.B $HOME/.periods
User's command file. This file is read after the system file.
.RE
.TP .6i
File format:
Commands are entered as they would be interactively.
.br
Commands may be extended onto the next line if the line does not
contain a comment, and the line ends with a "\\".
.br
Comments start with a "#" and extend to the end of line.
.br
Blank lines are ignored.
.TP .6i
Notes:
The user's command file is primarily intended to contain those defines
of predefs that the user will find personally useful.
For example, the user may wish to add a define command to their
".period" file which defines a predef that maps all the user's
weekly free timetable periods.
.RS
.TP 5
eg:
define myfree (10-12,13-14 Mon), \\
.br
	  (3pm-5pm Tue-Wed), (thur afternoon)
.PP
This predef could later be used (for instance) to find all available
free periods on Tuesday of the fourth week of session 1 using:
.br
	available myfree tue s1.4
.PP
The user may also wish to print a list of tokens, bookings, and
available free periods just before getting the interactive prompt from
the book interface, by putting the following few lines at the end
of their ".period" file.
.TP 5
eg:
.nf
token	# this lists all available tokens
echo "    Current Bookings"
show	# this shows all current bookings
echo "    Free periods available today:"
avail myfree today	# this lists all my free periods available today
	# (assuming predef "myfree" has already been defined)
.fi
.RE
.RE

.SH SYNTAX CONVENTIONS
Command and Period syntax are described in this document using the following (
.I EBNF
) conventions:
.PP
.nf
Convention	Meaning
____________________________________________________________
"abc"	the literal string "abc" (entered without the quotes)
\fIabc\fP	the construct \fIabc\fP, whose value is given by the assignment of
	literals and/or constructs to it by the assignment statement.
.fi
.PP
In what follows, \fBX\fP,\fBY\fP,\fBZ\fP refer to expressions of literals and/or constructs.
.nf
Expression	Meaning
( \fBX\fP )	The same as \fBX\fP.
[ \fBX\fP ]	\fBX\fP may occur once or not at all. (\fBX\fP is optional).
{ \fBX\fP }	\fBX\fP may occur zero or more times.
\fBX\fP | \fBY\fP	An occurence of \fBX\fP or \fBY\fP (but not both).
\fBX\fP \fBY\fP	\fBX\fP is followed by \fBY\fP, optionally separated by blanks or tabs.

\fIabc\fP = \fBX\fP .	The assignment statement.
	The construct \fIabc\fP has the value of the expression \fBX\fP.
.fi
.TP
eg:
The syntax of the book command is: "book" { \fItokens\fP } [ \fIperiod\fP ] .
This means that you type the literal string "book" followed
by zero or more \fItoken\fP constructs, and followed optionally
by the \fIperiod\fP construct.
.SH FILES
/usr/local/lib/book/periods	System command file.
.br
$HOME/.periods		User's command file.
.SH BUGS
Probably
