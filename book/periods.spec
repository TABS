expr	= isect

isect	= union {union}

union	= range {',' range}

range	= period {'-' period}		# types must be the same

period	= '(' expr ')
	| time
	| day
	| week
	| date

time	= num [':' num] ["am" | "pm"]	# type = repeated;daily

day	= "monday" | ... | "friday"	# type = repeated;weekly

week	= periodid ['.' num]		# type = absolute

date	= num '/' num ['/' num]		# type = absolute
	| num month [num]

month	= "january" | ... | "december"

num	= '0'..'9' {'0'..'9'}


neilb thoughts 23may96

make "time" become a moment - 10:00 -> 10:00-9:59 (!)
ranges are then always a-b == first of a to last of b
not 10:00 ??? want to exclude that second, and (anything which contains it)