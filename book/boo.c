/*
 * The book user interface.
 */

#include	<sys/types.h>
#include	<sys/param.h>		/* for NGROUPS used by getgroups */
#include	<unistd.h>
#include	<stdlib.h>
#include	<grp.h>		/* for getgrnam() */
#include	<stdio.h>
#include	<getopt.h>
#include	<ctype.h>
#include	<signal.h>
#include	<pwd.h>
#include	<rpc/rpc.h>
#include	"../interfaces/bookinterface.h"
#define BOOK_INTERFACE
#include	"boo.h"
#ifdef sun
#include	<unistd.h>
#include	<limits.h>
#define NGROUPS NGROUPS_MAX
#endif

#define TIMEOUT		120 
#define WARNTIMEOUT	30
#define	NWARNINGS	1

/********* externals **********/
/* system calls and subroutines */
#ifdef ultrix
extern	int	getgroups(int len, int *array);
#endif
extern int optind;



/********** global variables **************/

char	*infile;		/* the name of the file currently being read */
char	*cfile = "/.periods";	/* appended to home directory */
#ifdef DEBUG
    char	*helpfile =	"help";
    char	*sys_cfile =	"periods";
    char	*motd_file =	"motd";
#else
#ifdef apollo
    char	*helpfile =	"/usr/local/book/help";
    char	*sys_cfile =	"/usr/local/book/periods";
    char	*motd_file =	"/usr/local/book/motd";
#else
    char	*helpfile =	"/var/book/help";
    char	*sys_cfile =	"/var/book/periods";
    char	*motd_file =	"/var/book/motd";
#endif
#endif

#ifdef NOTYET
UDB	udb_handle;		/* used to make udb requests */
#endif
int	interrupt = 0;
int	ntimeoutwarnings = -1;
int	privilaged = 0;
int	user_is_class = 0;
char	*username;
bookuid_t	realuser = 0;
bookuid_t	user = 0;
int	parse_err = 0;		/* set of [ FATAL, RECOVER ]  - NOT CURRENTLY USED */
int	c_inline = 1;		/* the current unprocessed character */

/********** debug / error routines **********/

#ifdef DEBUG
int	debug;			/* set to DB_TOKEN | DB_PERIOD */
#endif

int readcommands(char *fname)
/* The main loop to read book commands.
 * Used to read setup files and interactive commands (hence fname).
 */
{
	FILE *fptr;
	char *prompt = "BOOK > ";
	command_type	*cp;
	int quit = 0;

	if (fname != NULL)
	{
		if ((fptr = fopen(fname, "r")) == NULL) return 0;
	}
	else fptr = stdin;

	infile = fname;
	c_inline = 1;
	if (fptr == stdin)
	{
		/* if stdin, prompt.
		 * but if it isn't a tty, a dd anewline and don't timeout
		 */
		printf(prompt);		/* only prompt interactive input */
		if (isatty(0))
		{
			alarm(TIMEOUT);
			ntimeoutwarnings = NWARNINGS;
		}
		else printf("\n");
		fflush(stdout);
	}
	while (! quit && nexttoken(fptr) != END)
	{
		if (fptr == stdin) alarm(0);	/* cancel the alarm request */
		if (! interrupt)
		{
			parse_err = 0;
			cp = getcommand(fptr);
			if (cp != NULLC)
			{
				if (cp->period == empty_abs)
					puts("\tPeriod spec maps to an empty or invalid period");
				else
				{
					switch (cp->command)
					{
					case HELP:	b_help(cp); break;
					case AVAIL:
					case BOOK:	b_showfree(cp); break;
					case CHANGE:
					case UNBOOK:
					case LIST:	b_showbook(cp); break;
					case RECLAIM:
					case REFUND:
					case PAST:	b_showpastbook(cp); break;
					case LABS:	b_labs(cp); break;
					case TIMES: b_times(cp); break;
					case TOKEN:	b_token(cp); break;
					case UNDEF:	b_undefine(cp); break;
					case DEFINE: b_define(cp); break;
					case ECHO:	b_echo(cp); break;
					case SHELL: b_shell(cp); break;
					case QUIT:	quit++; break;
					default:	printcommand(cp); break;
					}
					if (cp->command != DEFINE) rfree(cp->period);
				}
			}
		}
		if (fptr == stdin && ! quit)
		{
			printf(prompt);	/* only prompt interactive input */
			if (isatty(0))
			{
				alarm(TIMEOUT);
				ntimeoutwarnings = NWARNINGS;
			}
			else printf("\n");
			fflush(stdout);
		}
		if (interrupt) interrupt = 0;
	}
	if (fptr != stdin) fclose(fptr);
	return 1;
}

void timeout()
{
	if (ntimeoutwarnings < 0)
		return; /* someone else is monitoring timeouts */
	if (ntimeoutwarnings <= 0)
	{
		puts("\07TIME OUT\07");
		exit(0);
	}
	else
	{
		printf("\07Timeout in %d seconds\07\n", ntimeoutwarnings-- * WARNTIMEOUT);
		alarm(WARNTIMEOUT);
	}
}

void setinterrupt()
{
#ifdef USE_SIGACTION
	signal(SIGINT,setinterrupt);
#endif
    
	puts("Interrupt\07");
	interrupt++;
}

int ingroup(int gid)
/* return true if the current user is in the group wheel (group 0) */
{
	int	n;
#ifdef ultrix
	int *ip, groups[NGROUPS];
#else
	gid_t *ip, groups[NGROUPS];
#endif
    
	n = getgroups(NGROUPS, groups);
	for (ip = groups; (n>0 && *ip++ != gid); n--);
	return (n>0);
}

int main(int argc, char **argv)
{
	FILE	*f;
	struct passwd	*pwe;

	char	ch, *command;
	int		err = 0;
	char	*home_cfile;
#ifdef USE_SIGACTION
	static struct sigaction sigalrmact;
#else
	static struct sigvec sigintp[] = { setinterrupt, NULL, SV_INTERRUPT };
	static struct sigvec sigalrmp[] = { timeout, NULL, 0 };
#endif
	char	*options = "hvD:";
#ifdef DEBUG
	char	*usage = "Usage: %s [-v] [-D debug_level] [uid | login_name | class_name]\n";
#else
	char	*usage = "Usage: %s [-v] [uid | login_name | class_name]\n";
#endif
	struct group	* supr_grp;
#ifdef  USE_SIGACTION
	sigalrmact.sa_handler = timeout;
	sigalrmact.sa_flags = 0/*SA_RESTART*/;
#endif    
	command = *argv;
	while ((ch = getopt(argc, argv, options)) != EOF)
		switch (ch)
		{
		case 'h': fprintf(stderr, usage, command); exit(0);
		case 'v': puts(book_version); exit(0);
		case 'D':
#ifdef DEBUG
			if (sscanf(optarg, "%d", &debug) != 1) err++;
#else
			fputs("Debug not compiled in this version\n", stderr);
#endif
			break;
		default: err++; break;
		}

	/** sort out our identity **/

	realuser = user = getuid();		/* set up user */
	username = NULL;
	privilaged = (realuser == 0
		      || ingroup(0) ||  (((supr_grp = getgrnam("supervisor")) != 
					  NULL) && ingroup(supr_grp->gr_gid)));
	if (optind < argc)
	{
		if (privilaged)
			if (! isdigit(*argv[optind]))
			{
				username = argv[optind];
				user = find_userid(username);
				if (user < 0)
					fprintf(stderr, "No such user or class: '%s'\n", username);
			}
			else user = atoi(argv[optind]);
		else
		{
			fprintf(stderr, "%s: only group wheel or supervisor may change user\n", command);
			exit(1);
		}
	}
	/* sort out our name if we haven't got it yet */
	if (username == NULL)
		username = find_username(user);
	if (username == NULL)
	{
		fprintf(stderr, "No such uid or classid: '%d'\n", (int)user);
		username = "Who are you?";
		err++;
	}
	if (user >= get_class_min())
		user_is_class = 1;

	if (err)
	{
		fprintf(stderr, usage, command);
		exit(2);
	}

	currtime();
	init_interface();
	initperiods();
	initbookings();
#ifdef USE_SIGACTION
	signal(SIGINT,setinterrupt);
/*    sigset(SIGALRM,timeout); */
	sigaction(SIGALRM, &sigalrmact, NULL);
#else
	sigvec(SIGINT, sigintp, NULL);
	sigvec(SIGALRM, sigalrmp, NULL); 
#endif
	/***** print message of the day file */
	if ((f = fopen(motd_file, "r")) != NULL)
	{
		while ((ch = fgetc(f)) != EOF) putchar(ch);
		fclose(f);
	}

	/***** read in the period specification files *****/
	if (! readcommands(sys_cfile))
	{
		/* the system periods file has to exist */
		perror(sys_cfile);
		exit(1);
	}
	if (! user_is_class)
	{
		pwe = getpwuid(user);
		if ((home_cfile = (char *) malloc(strlen(pwe->pw_dir)+strlen(cfile)+1)) != NULL)
		{
			strcat(strcpy(home_cfile, pwe->pw_dir), cfile);
			readcommands(home_cfile); /* ignore failure */
		}
	}

	if (!user_is_class && is_defaulter(user))
	{
		printf("WARNING: you are currently deemed to be a defaulter.\n");
		printf("   see \"help defaulter\" for more details.\n");
	}
	/***** the interactive bit *****/
	readcommands((char *) NULL);
	puts("");
	exit(0);
} /* main */

