/*
 * Book interface module:
 *	Routines that deal with defining and undefining periods only.
 *	This subset of the full book interface routines may be used to
 *	parse the system period definition file.
 */

#include	<stdio.h>
#include	<string.h>
#include	<stdlib.h>
#include	<time.h>
#include	<rpc/rpc.h>
#include	"../interfaces/bookinterface.h"
#define BOOK_INTERFACE
#include	"boo.h"


/****** global variables used for periods and predefs *****/

struct tm	tnow;	/* current time - tm format */
time_t		now;	/* current time - seconds */
time_t		enow;	/* current time - seconds , not adjusted for timezone*/
plist	*predefined;	/* predefined periods */
period	*valid_abs;	/* non-excluded periods of type ABSOLUTE */
period	*valid_day;	/* non-excluded periods of type DAY */
period	*valid_hour;	/* non-excluded periods of type HOUR */
period	empty_abs[] =	{ { ABSOLUTE, 0, -1, NULLP } };
period	empty_day[] =	{ { DAY, 0, -1, NULLP } };
period	empty_hour[] =	{ { HOUR, 0, -1, NULLP } };

time_t currtime()
/* set the global time variables now and tnow to current time */
{
    time(&enow);
    tnow = *localtime(&enow);
    now = construct(1900+tnow.tm_year,0,tnow.tm_yday,tnow.tm_hour,tnow.tm_min);
    return now;
}

void initperiods()
/* initialise global valid periods and predefs */
{
    predefined = NULLPL;
    valid_abs = notperiod(empty_abs);
    valid_day = notperiod(empty_day);
    valid_hour = notperiod(empty_hour);
    currtime();
}
/********* period printing routines ******/

void printtime(int type, time_t t, int showyear)
/* showyear: include year when printing ABSOLUTE type */
{
    char	*s;
    struct tm	*tp;

    tp = gmtime(&t);
    s = asctime(tp);
    switch (type)
    {
    case ABSOLUTE:
	printf("%5.5s %3.3s %2.2s %3.3s", s+11, s, s+8, s+4);	/* HH:MM Day Date Mnth */
	if (showyear)
	    printf(" %d", tp->tm_year+1900);
	break;
    case HOUR:
	printf("%5.5s", s+11);	/* Hour:Min */
	break;
    case DAY:
	t += 4 * SECINDAY;	/* 1 Jan 1970 was a Thursday */
	tp = gmtime(&t);
	s = asctime(tp);
	printf("%5.5s %3.3s", s+11, s);	/* Hour:Min Day */
	break;
    }
}

void print_one_period(period *pp)
{
    printtime(pp->type, pp->start, 0);
    fputs(" - ", stdout);
    printtime(pp->type, pp->end, 1);
}

void printperiod(period *pp, char *separator)
/* print each period in the list of periods pp */
{
    if (pp != NULLP)
    {
	print_one_period(pp);
	while ((pp = pp->next) != NULLP)
	{
	    printf("\n%s", separator);
	    print_one_period(pp);
	}
	fputs("\n", stdout);
    }
    else fputs("NULL period\n", stdout);
}

/********* predef routines **********/

void printpredefs()
/* print out a list of all the current predefined periods.
 * list items are columnated over output lines of 80 charactes, using tabs.
 */
{
    plist *pl;
    int	col;

    puts("    Predefs currently defined:");
    col = 0;
    for (pl = predefined; (pl != NULL); pl = pl->next)
    {
	col = ((col+8) / 8) * 8 + strlen(pl->id);
	if (col > 80)
	{
	    puts("");
	    col = strlen(pl->id) + 8;
	}
	printf("\t%s", pl->id);
    }
    puts("");
}

void addpredefs(strlist *strings, period *periods)
/*
 * if the period is not to be excluded,
 * add it to the predefined period list "predefined", otherwise
 * add the excluded period to one of: valid_abs, valid_hour, or valid_day.
 */
{
    strlist	*sp;
    plist	*new;
    period	**predeflist;

    for (sp = strings; (sp != NULL); sp = sp->next)
    {
	if (strcmp(sp->str_val, "exclude"))
	{
	    if ((new = MALLOC(plist)) != NULLPL)
	    {
		new->id = sp->str_val;
		new->period = periods;
		new->next = predefined;
		predefined = new;
	    }
	}
	else
	{
	    switch(periods->type)
	    {
	    case HOUR:	predeflist = &valid_hour;	break;
	    case DAY:	predeflist = &valid_day;	break;
	    case ABSOLUTE: predeflist = &valid_abs;	break;
	    }
	    *predeflist = fintersect(*predeflist, notperiod(copyperiods(periods)));
	}
    }
}

int delpredef(char *name)
/* remove the predefined period called name.
 * free the period list iff the list is not shared with another predef.
 */
{
    struct periodlist	*pdp, *prev;

    for (pdp = predefined, prev = NULL;
	(pdp != NULL && strcmp(name, pdp->id));
	prev = pdp, pdp = pdp->next);
    if (pdp != NULL)
    {
	if (prev == NULL)
	    predefined = pdp->next;
	else prev->next = pdp->next;

	for (prev = predefined; (prev != NULL && prev->period != pdp->period);
	    prev = prev->next);
	if (prev != NULL) rfree(pdp->period);	/* no predef shares periods */
	free(pdp->id);
	free(pdp);
    }
    return (pdp != NULL);
}

/************ miscellaneous routines ****************/

strlist *instrlist(char *name, strlist *strings)
/* return the entry in the strings list matching tokname */
{
    while (strings != NULL && strcmp(strings->str_val, name))
	strings = strings->next;
    return (strings);
}

/****** main period/predef commands *****/

void b_define(command_type *comm)
{
    if (comm->strlist != NULL)
    {
	if (comm->period == NULL)
	{
	    if (instrlist("exclude", comm->strlist))
	    {
		/* The only way we can print excluded predefs.
		 * Ignore the other (undefined) predef names
		 */
		puts("The following periods are excluded:");
		fputs("Absolute Periods:\n\t", stdout);
	        printperiod(notperiod(copyperiods(valid_abs)), "\t");
		fputs("Weekly periods:\n\t", stdout);
		printperiod(notperiod(copyperiods(valid_day)), "\t");
		fputs("Daily periods:\n\t", stdout);
		printperiod(notperiod(copyperiods(valid_hour)), "\t");
	    }
	    else fputs("Cannot define an empty predef\n", stderr);
	}
	else addpredefs(comm->strlist, comm->period);
    }
    else if (comm->period != NULLP)
    {
	putchar('\t');
	printperiod(comm->period, "\t");
    }
    else printpredefs();
}

void b_undefine(command_type *comm)
{
    strlist *sp;
    if (comm->strlist != NULL)
    {
	for (sp = comm->strlist; (sp != NULL); sp = sp->next)
	    if (! delpredef(sp->str_val))
	    fprintf(stderr, "Predef '%s' does not exist\n", sp->str_val);
    }
    else fputs("Nothing to undefine\n", stderr);
}
