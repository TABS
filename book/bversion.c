/* Defines the current version of book (the user interface) */

char *book_version = "Book Version 2.1.4 (16 Apr 1996)";

/*
 v.2.1.4 16 Apr 1996:
	. Change token_periods [bcomm.c] to allow for change in
	  token_period(token_name) [bookinterface.c] now returning
	  periods when the token is *not* valid.
 v.2.1.3 13 Apr 1995:
	. Fix minor bug when non-privilaged user uses "book" or
	  "available" without specifying a period (defaulting to
	  current time).
 v.2.1.2 7 Apr 1995:
	. Fix error in cancelling past or pending bookings.
	. Not printing status of past bookings properly.
 v.2.1.1 24 March 1995:
	. Fix bug when specifying a period not starting on the
	  hour/half hour.
 v.2.1	22 March 1995:
	. Include version (this module and associated flags).
	. Cancell bookings (status==B_PENDING) that start in the past.
	. Allow classes to change the token used in bookings.
	. Improve the listing of current bookings - particularly when
	  printing out consecutive class bookings for the same time but
	  for different labs.
	. Help is now paged for all TERM setting except xterm.
 v.2.0	Date Unknown (1992?)
	. First developed this interface to the booking system (2.0).
	. Booking system (1.*) had a completely different user
	  interface (along with everything else!)

 */
