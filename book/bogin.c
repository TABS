/* bogin: Booking login for booking terminals
 * Run book for those accounts with passwords
 * For those accounts without passwords, run their shell program.
 */

#include	<stdio.h>
#include	<pwd.h>
#include	<sys/param.h>		/* for getgroups() */
#include	<sys/wait.h>
#include	<grp.h>		/* for getgrnam() */
#if defined (SYS5) && SYS5 >= 4
#include	<shadow.h>
#endif
#include	<string.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	<limits.h>
#ifndef NGROUPS
#define NGROUPS NGROUPS_MAX
#endif

#include	<sys/types.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<crypt.h>

#define MAXLOGIN	20

#ifndef DEC
    /* no getusershell on DEC */
extern	char *getusershell();
#endif

char	*login_prompt =	"\nFor a shell, type 'shell' at the BOOK> prompt\nBook login: "; /* \07 not liked by aidan */
char	*pass_prompt =	"Password: ";

char	*E_toolong =	"Login too long";
char	*E_badpass =	"Password incorrect";
char	*E_nouser =	"No such user";
char	*E_baduser =	"Invalid user";

char	*book =		"/usr/local/bin/book";
char	*rootshell =	"/bin/sh";

char	*systype =	"SYSTYPE=bsd4.3";
char	*homevar =	"HOME=";
char	*shellvar =	"SHELL=";

struct passwd *findlogin(char *name)
{
	int		ch;
	char	*s, login[MAXLOGIN];
	char	*password, *guess;
	int		i, badpass;
	struct passwd	*pwe;

	if (name != NULL) {
		if (strlen(name) >= MAXLOGIN)
		{
			*login = '\0';
			fputs(E_toolong, stderr);
		}
		else strcpy(login, name);
	}
	else
		*login = '\0';
	do {
		while (*login == '\0')
		{
			fputs(login_prompt, stdout);
			s = login;
			i = 0;
			while ((ch = getchar()) != EOF && ch != '\n')
				if (++i < MAXLOGIN) *s++ = ch;
			if (i >= MAXLOGIN)
			{
				puts(E_toolong);
				*login = '\0';
			}
			else *s = '\0'; 
		}

		if ((pwe = getpwnam(login)) != NULL)
		{
			if (*(pwe->pw_passwd) != '\0')
			{
				password = getpass(pass_prompt);
				guess = crypt(password, pwe->pw_passwd);
				if ((badpass = strcmp(guess, pwe->pw_passwd)))
				{
#if defined (SYS5) && SYS5 >= 4
					struct spwd *pwe2;
					pwe2 = getspnam(login);
					if (pwe2 != NULL)
						badpass = strcmp(crypt(password, pwe2->sp_pwdp), pwe2->sp_pwdp);
					if (badpass)
#endif
						puts(E_badpass);
				}
			}
			else badpass = 0;
		}
		else puts(E_nouser);
		*login = '\0';
	} while (pwe == NULL || badpass);
	setgid(pwe->pw_gid);
	initgroups(pwe->pw_name, pwe->pw_gid);
	setuid(pwe->pw_uid);
	return pwe;
}

char *isshell(char *usershell)
{
	char	*shell;
	/* no getusershell on DEC */
#ifndef DEC
	while ((shell = getusershell()) != NULL && strcmp(shell, usershell));
	return shell;
#else
	return(usershell);
    
#endif    
}

int ingroup(int gid)
/* return true if the current user is in the group wheel (group 0) */
{
	int	n;
#ifdef ultrix
	int
#else
		gid_t
#endif
		*ip, groups[NGROUPS];

	n = getgroups(NGROUPS, groups);
	for (ip = groups; (n>0 && *ip++ != gid); n--);
	return (n>0);
}

void usage(void)
{
	fprintf( stderr, "Usage: bogin [ -d terminal_dev ] [-c stty-command] [username]\n" );
	exit( 2 );
}

int new_sid( void )
{
	pid_t pid;

	/* the case where we don't currently have a ctty */
	if( setsid() != -1 )
		return( 1 );

	/*
	 *  first, fork in the case where we already have a pgrp/ctty
	 */
	pid = fork();
	if( pid == 0 ) /* child */
	{
		/* Lose our controlling terminal */
		if( setsid() < 0 )
		{
			perror( "bogin: setsid() failed: " );
			return( 0 );
		}
		else
		{
			return( 1 );
		}
	}
	else  /* parent */
	{
		wait(0);
		exit(0);
	}
}

void grab_terminal( char *terminal_dev )
{
	int   fd, maxfds=64;
	char  errbuff[100];

	/* get a new controlling terminal */
	new_sid();

	/* Lose any open file descriptors */
	for( fd=0; fd<maxfds; fd++ )
		close( fd );

	/* point 0,1,2 at terminal_dev */
	if( open( terminal_dev, O_RDWR) == 0 ) 
	{
		/* open() has returned stdin (0) */
		dup(0);    /* stdout */
		dup(0);    /* stderr */
	}
	else
	{
		sprintf( errbuff, "bogin: cannot open %s: ", terminal_dev );
		perror(errbuff);
		exit(1);
	}
}

int main(int argc, char **argv)
{
	struct passwd	*pwe;
	char		*arg0, *command, *name;
	char		*newargv[2], *newenvp[4];
	int			i;
	struct group	* supr_grp;
	char                * terminal_dev = NULL;
	char		* init_cmd = NULL;
    
	newenvp[0] = arg0 = NULL;

	i = 1;
	if( argv[i] && strcmp(argv[i], "-d")==0 )
	{
		i++;
		if( argv[i] )
			terminal_dev = argv[i];
		else
			usage();
		i++;
	}

	if (argv[i] && strcmp(argv[i], "-c")==0)
	{
		i++;
		if (argv[i])
			init_cmd = argv[i];
		else
			usage();
		i++;
	}

/* aidan
   if (1 <= --argc && argc <= 2)
   name = argv[argc];
*/
	/*   else name = ""; */
	/* changed by adam 05/05/94 */

	if (argv[i] && argv[i][0] == '-')
		name = NULL;
	else
		name = argv[i] ;

	if (terminal_dev)
		grab_terminal( terminal_dev );

	if (init_cmd) system(init_cmd);
	pwe = findlogin(name);

	newenvp[0] = systype;
	if ((newenvp[1] = (char *) malloc(strlen(homevar)+strlen(pwe->pw_dir)+1)) != NULL)
	{
		strcat(strcpy(newenvp[1], homevar), pwe->pw_dir);
		if ((newenvp[2] = (char *) malloc(strlen(shellvar)+strlen(pwe->pw_shell)+1)) != NULL)
		{
			strcat(strcpy(newenvp[2], shellvar), pwe->pw_shell);
			newenvp[3] = NULL;
		}
	}

	if (pwe->pw_uid == 0)
		command = rootshell;		/* root gets its shell */
	else if (*(pwe->pw_passwd) == '\0')
		if (isshell(pwe->pw_shell))
		{
			puts(E_baduser);
			exit(1);
		}
		else command = pwe->pw_shell;	/* newacc, etc */
	else if (! isshell(pwe->pw_shell))
	{
		puts(E_baduser);
		exit(1);
	}
	else if (  ingroup(0) || (((supr_grp = getgrnam("supervisor")) != 
				   NULL) && ingroup(supr_grp->gr_gid)) )
	{
		/* wheels and supervisors get their shell.
		 * set up SYSTYPE, SHELL, and HOME in their environment
		 */
		arg0 = "-";
		command = pwe->pw_shell;
	}
	else command = book;		/* ordinary user gets book */

	if (arg0 == NULL) arg0 = command;
	newargv[0] = arg0;
	newargv[1] = NULL;
#ifdef DEBUG
	printf("execve(%s, (%s, NULL), (", command, arg0);
	for (i=0; (newenvp[i] != NULL); i++) printf("%s%s", (i==0 ? "" : ", "), newenvp[i]);
	printf("%sNULL))\n", (i==0 ? "" : ", "));
#else
	execve(command, newargv, newenvp);
#endif
	perror(command);
	sleep(2);		/* give error message time to be seen */
	exit(0);
}

