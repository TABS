* This file contains stanzas of help information used by book.
* Each stanza is started by a line of the form: "#stanza_name",
* where stanza_name is the name used by book to index the information;
* and each stanza is ended by a line containing only a "#".
* Any lines between the "#stanza_name" line and the "#" line that also
* start with a "#" will be skipped.

* The following stanza must be the first in this file - It gives
* the default help stanza (produced by the help command with no arguments).
* It is also a good idea to put the stanzas into the file in order of descreasing
* access.
#
    Synopsis:
	Book is used to make, inspect, and cancel terminal bookings.
	At the prompt ("BOOK > ") enter a command specification with the
	syntax shown below.
#command
    Syntax:
	[command] {tokens} [period]
    where:
	command	= "book"   | "unbook"
		| "show"   | "available"
		| "refund" | "reclaim"   | "past"
		| "define" | "undefine"
		| "token"  | "echo"
		| "labs"   | "times"
		| "quit"   | "exit"
		| "change" | "help"
		| "shell" .
	tokens	= booking token name.
	period	= time specification.
    Note:
	The user may abbreviate the commands to the first character.
	Exceptions are:
	    "exit" (abbrev:"ex") to distinguish it from "echo" (abbr:"e").
	    "undefine" (abbr:"und") to distinguish it from "unbook" (abbr:"u").
	    "refund" (abbr:"ref") to distinguish it from "reclaim" (abbr:"rec").
    Defaults:
	If no command is given, but a period or tokens are specified,
	    then the help command is assumed.
	If no period is given, then the next appropriate period from now
	    is usually assumed. However, this will depend upon the actual
	    command invoked.
	If no tokens are given, then all available or appropriate tokens will
	    usually be assumed. However, this will depend upon the actual
	    command invoked.
    See Also:
        "help summary"	For a summary of the commands available.
	"help" command	For detailed information on the specified command.
			(Where command is any one of the commands defined above).
	"help period"	For information on period syntax.
	"help token"	For information on the token command and on
			  tokens in general.
	"help syntax"	For help on the notation used in syntax definitions.
	"help defaulter" For information about defaulters.
#

#summary
   Summary of commands:
	available	Find available periods that may be booked.
	book		Make a terminal booking.
	change		Change a class booking.
	claim		Reclaim a booking that was not taken up at allocation time.
	define		Give a name to a period - create a predefined period.
	echo		Print a message to the screen.
	exit		Exit from the program.
	help		Print help on a command, topic, period, or token.
	labs		List all labs that might be bookable during a given time range.
	past		Show past bookings and cancellations.
	quit		Exit from the program.
	reclaim		Reclaim a booking that was not taken up at allocation time.
	refund		Refund a token spent on a past booking.
	show		List the pending bookings.
	times		List all timeslot which may be bookable on a given day.
	token		Show the number of tokens and their periods. 
	unbook		Cancell a pending booking.
	undefine	Remove a predefined period.
	shell		Run a shell for 5 minutes.
			This is for printing and giving from booking terminal.

   Summary of Topics:
   	cfile		Information on book command files.
	command		General information on book commands.
	period		Info on specifying periods to commands.
	predef		Info on predefined periods and their use.
	summary		This summary page.
	syntax		Info on the syntax specification used in the help documents.
	tokens		General information about tokens.
	defaulter	Explaination of when a user is considered a "defaulter"

    See Also:
        "help" command		For detailed information on any command listed above.
	"help" topic		For detailed information on any topic listed above.
#

#book
    Syntax:
	"book" {tokens} [period] {options}
    where:
	options	= ["consecutive"] "=" C
		| "machines" "=" M
	C | M	= "0".."9" {"0".."9"}
    Synopsis:
	Interactively make a booking
	    for the current user;
	    within the period(s) specified;
	    using one or more of the tokens specified;
	    for a sequence of C consecutive periods at a time;
	    booking M machines at a time (Class bookings only).
    Defaults:
	If no period is specified,
	    then stop after the first successful booking.
	If no tokens are specified,
	    then any token available to the current user may be used.
	If not specified, C and M are both 1.
    Operation:
	Basically, for every sequence of available slots that meets the
	criteria, the user will be asked to choose from a list of available
	tokens, and then from a list of available labs.
	If there is not a choice of tokens or labs to choose from, then the
	user will be shown the one token or lab available, and asked if they
	want to book that.
	In general, when prompted, the user may respond with:
	    "y"		Make the booking.
	    "n"		Don't book this period - try the next one.
	    "q"		Quit - Return to the BOOK prompt.
	When prompted with a choice of tokens or labs, the user may also
	respond with:
	    number	The corresponding item from the list will be used;
	    "y"		Pick the lab with the most free machines.
			(This response is only valid when choosing labs)

	After making a booking, the program will search for the next sequence
	of slots to book within the period specified, and the user is prompted
	once again. If no period was specified, book returns to the command
	prompt.
    Restrictions:
	1) The user is not allowed to book the next slot, the current slot, or
	   any slot in the past.
	2) Consecutive slots will be booked using tokens of the same type.
	   To book consecutive slots using different token types, the user must
	   resort to using the book command more than once, specifying (or
	   choosing) a different token type for each successive slot.
	3) The user will not be allowed to book a slot that has already been
	   booked by that user. If there already exists a class booking for
	   for that time which is available to the user, the user will be warned
	   about it, but will still be allowed to make the booking.
	4) Only classes may specify the number of machines to be booked (M).
    Note:
	A terminal booking does not imply a terminal allocation.  This is due
	to the fact that bookings are based on the number of physical terminals
	in a lab, while allocations are based on the number of these terminals
	which are up (working) and available to the network at the time of
	allocation.

	Thus, if the number of bookings exceeds the number of terminals
	actually available at allocation, then these excess bookings cannot be
	honoured. As bookings are allocated in the chronological order in which
	they were made, it is the last few bookings made for a particular lab
	and slot that may not be honoured.

	Consequently, if a user books one of the last available slots in a lab,
	then the user will be warned that the booking may not be honoured.

   Class Bookings:
	If the user is a class, the user is required to specify the number of
	machines to be booked for each period satisfying the specs.
	If this number has not been passed as an option, the user will be
	prompted for it for every available period found.

	In general, a class booking will be always be made without prompting
	if there are no unknown parameters and no choices to be made.
	Ordinary users however, have to at least confirm that they want to make
	the booking.

	If a class booking exists within the specified period, that:
	a) Uses one of the specified tokens, or
	b) Is made for a lab that one of the specified tokens could book,
	then the existing booking is changed to be for the new specified
	number of machines, using the alternate token.
	In this way, the "book" command subsumes the functionality of the
	"change" command.
   See Also:
	"unbook":	For information on unbooking terminal bookings.
	"change":	For information on changing terminal bookings.
#

#unbook
    Syntax:
	"unbook" {tokens} [period]
    Synopsis:
	Interactively cancel all bookings made
	    by the current user;
	    using any of the tokens specified;
	    for any booking slot within the periods specified;
    Defaults:
	If no period is specified,
	    then the next booked period made with any of the specified
	    tokens will be cancelled.
	If no tokens are specified,
	    then all bookings made by the user in the period(s) specified
	    will be cancelled, regardless of what token was used to make
	    the booking.
    Operation:
	When a booking is found matching the criteria,
	the user is prompted with:
	    The booking slot taken;
	    The token used to make the booking.
	The user may respond with:
	    "y"	The booking is cancelled, and the token credited to the user.
	    "n"	The booking is not cancelled, and the program searches for
		  the next booking meeting the criteria.
	    "q"	Quit - The program returns to the command prompt "BOOK >"
    Restrictions:
	The user will not be allowed to cancel a slot on or after the day
	of the slot unless the booking was made less than an hour before the
	cancellation and the booking was not for the current or next slot.
#

#change
    Syntax:
	"change" [period] {tokens} [["machines"] "=" N]
    Where:
	N	=	"0".."9" { "0".."9" }
    Synopsis:
	Interactively change any class bookings made within the periods specified,
	such that:
	    (1) If the booking was made with one of the specified tokens,
		then change the number of bookings to N (if possible).
	    (2)	If the booking was made for a lab which could also be booked by one
	        of the specified tokens, then change the token used to
		the first such token specified, and the number of bookings to N
		(if possible).
    Restrictions:
	Only users who are classes may use this command.
    Defaults:
	If N is not specified (or zero), the user will be asked to specify
	    the change to be applied to each matching booking.
	If no period is specified,
	    then the next class booking made with any of the specified
	    tokens will be cancelled.
	If no tokens are specified,
	    then all bookings made by the class in the period(s) specified
	    will be changed, regardless of the token used.
    Operation:
	When a booking is found matching the criteria, the user is shown:
	    The matching booking slot;
	    The token used to make the booking;
	    The number of machines booked.
	If the number of machines (N) is already specified,
	then the booking is changed to that number of machines,
	otherwise the user is prompted for the change to be made and the
	user may respond with:
	    A positive or negative number (the change to be made);
	   "n"	The booking is not changed, and the program searches for
		the next booking meeting the criteria.
	   "q"	Quit - The program returns to the command prompt "BOOK >"
    Notes:
	If the change removes more machine bookings than were made, the
	    booking is removed altogether (same as "unbook").
	The number of machines can only be increased by as many machines
	    as currently remain unbooked.
	The option "machines" may be shortened to "mach".
#

#available
    Syntax:
	"available" {tokens} [period] [["consecutive"] "=" N]
    where:
	N = "0".."9" {"0".."9"}
    Synopsis:
	List those periods which may be booked:
	    by the current user;
	    within the period(s) specified;
	    using one or more of the tokens specified;
	    for a sequence of N consecutive slots at a time.
    Defaults:
	If no period is specified, then the next available period is shown.
	If no tokens are specified,
	    then any of the tokens available to the current user may be used.
	If not specified, N is 1.
    Restrictions:
	1) Neither the next slot, the current slot, nor any slot in the past,
	   is available to the user.
	2) Consecutive slots are only available using tokens of the same type.
    Note:
	This command will not make or cancel any bookings.
	The option "consecutive" may be shortened to "consec", or left out
	    altogether.
#

#show
    Syntax:
	"show" {tokens} [period]
    Synopsis:
	List the pending bookings that have been made:
	    for the current user;
	    within the periods specified;
	    using any of the tokens specifed.
    Output:
	Two kinds of pending bookings may be shown:
	    1) Bookings made for the user by the user.
	    2) Bookings made by the administrator of a class of which the user
	       is a member.
	Each booking is identified by:
	    the lab containing the booked terminal;
	    the period of the booking.
	If the booking is one made by the user,
	    then the token used to make the booking is shown;
	otherwise the class booked is shown in angle brackets instead.
    Defaults:
	If no period is specified, then all bookings are shown.
	If no tokens are specified, then bookings made with any token are shown.
    Note:
	This command will not make or cancel any bookings.
#

#past
    Syntax:
	"past" {tokens} [period]
    Synopsis:
	List the expired bookings:
	    for the current user;
	    that were booked within the periods specified;
	    that used (or tried to use) any of the tokens specifed.
	Expired bookings are those that have been cancelled, or those that
	have already been allocated a terminal, ie: any booking that
	is not pending.
    Output:
	Each expired booking is identified by:
	    the lab containing the booked terminal;
	    the token used to make the booking;
	    the period of the booking;
	    the status of the booking.
	The booking status will be one of the following:
	     Status	 Meaning
	    Alloc'd	The booking was successfully allocated a terminal.
	    Refund	The booking was not successfully allocated a terminal,
			and the token used to make the booking was refunded.
	    Cancel	The booking was cancelled by the user before it could
			be allocated a terminal.
	    Free	The booking was successfully allocated a terminal,
			but as the lab was underutilised at the time, the
			user was refunded the token that he/she had used to
			make the booking.
	    Tentative	The booking was not claimed at allocation time, but
			tentatively retained in case the user reclaimed it.
    Defaults:
	If no period is specified, then all expired bookings are shown.
	If no tokens are specified, then expired bookings made with any
	token are shown.
    See Also:
	"refund"	for refunding expired bookings.
	"reclaim"	for reclaiming the current tentative booking.
#

#refund
    Syntax:
	"refund" {tokens} [period]
    Synopsis:
	Refund the tokens spent in making allocated bookings:
	    for the current user;
	    that were booked within the periods specified;
	    that used any of the tokens specifed.
	Allocated bookings are those that have been successfully allocated
	a terminal in the past, and for which one token has been spent.
    Output:
	When an allocated booking is found meeting the criteria, the user
	is prompted with:
	    the lab containing the booked terminal;
	    the token used to make the booking;
	    the period of the booking;
	    the status of the booking (which will always be Alloc'd);
	    the prompt string: "refund ?".
	The user may respond with:
	    "y"	The token used to make the booking is refunded to the user,
		  and the booking status turned into "Refund".
	    "n"	The token is not refunded, and the program searches for
		  the next expired booking meeting the criteria.
	    "q"	Quit - The program returns to the command prompt "BOOK >"
    Restrictions:
	This command may only be used by a privilaged user.
    See Also:
	"past"	for more details on expired bookings and their statuses.
#

#reclaim
#claim
    Syntax:
	"reclaim" | "claim"
    Synopsis:
	Reclaim a booking that was not taken up at allocation time.
    Notes:
	Once a booking has been allocated to a terminal, the user making the
	booking has 7 minutes to log in. If he/she does not log in within this
	time, the terminal is made available to other users.
	The only way the user can subsequently take up the booking once the
	allocated terminal has been taken by another, is by using this
	command.
	Also, if a user has repeatedly not turned up to claim bookings,
	the booking system will allow anyone to use a terminal that has been
	allocated to that user. In this case the user will only be able to
	claim their booking by using this command.

	If the command is successful, the other (non-booked) user is given
	5 minutes and a series of warnings, to log off.
#

#define
    Syntax:
	"define" {name} [period]
    Synopsis:
	Add a new predefined period (predef) to the existing list of predefs.
	The new period may subsequently be refered to in a period specification
	by any of the names given.
    Defaults:
	If no period or names are given,
	    then the name of all predefs are printed.
	If no new predef names are given, then the period (or predef) is
	    displayed as defined (not as instanciated).
	    eg: compare "define ah" with "help ah".
    Note:
	To change an existing predef definition, the user must first undefine
	    the predef, and then redefine it.
	If the name given to a period is the same as a token name, the user
	    will not be able to explicitly refer to that token after the define.
    See Also:
	"help predef"	for more information on predefs.
	"help undefine"	for how to undefine predefs interactively.
	"help token"	for more information on tokens.
#

#undefine
    Syntax:
	"undefine" quotename {quotename}
    where:
	quotename = '"' name '"'
	ie: the predef names to be undefined are enclosed in double quotes.
    Synopsis:
	Remove from the current set of predefined periods (predefs), the
	predefs with the names listed.
    Restrictions:
	Cannot remove excluded predefs ("exclude").
    See Also:
	"help predef"	for more information on predefs.
	"help define"	for how to define predefs interactively.
#

#labs
    Syntax:
	labs [period]
    Synopsis:
	Finds and displays a list of all labs which are potentially bookable
	with any current tokens during the given period.
	Actualy availability in these labs in not checked.
    Output:
	One line containing a list of space separated labnames.
    Note:
	This command is provided to support the "tkbook" interface to "book".

#

#times
    Syntax:
	times [period]
    Synopsis:
	Finds a displays a list of all bookable periods during the given period
	for which booking might possibly be made.
	This essentially determines lab opening hours.
    Output:
	One line containing an ordered list of periods or period ranges. e.g.
		10:00   would mean the booking period which starts at 10am
		9:30-17:30 would mean the periods from the one which starts at
			9:30am to the one which start at 5:30pm
    Note:
	This command is provided to support the "tkbook" interface to "book".
#

#echo
    Syntax:
	"echo" { '"' string '"' }
    Synopsis:
	Print the string given within double quotes on standard output.
    Notes:
	The string MUST start with and be terminated by a double quote.
	The double quotes are not printed.
	The string may extend over more than one line.
	Consecutive quoted strings are concatenated.
	This command is particularly useful in command files.
    See Also:
	"help cfile"	For help on command files.
#

#shell
    Syntax:
	"shell"
    Synopsis:
	Runs a login shell for at most five minutes
    Notes:
	If the shell does not exit within four minutes of starting,
	a warning message is printed. If it still does not exit
	after a further minute, it is killed and book will exit.
#
#help
#?
    Syntax:
	"help" [command | topic] {tokens} [period]
    where:
	command	= "book"   | "unbook"
		| "show"   | "available"
		| "define" | "undefine"
		| "token"  | "echo"
		| "exit"   | "quit"
		| "help" .
	topic	= "period" | "predef"
		| "syntax" | "cfile"
		| "topics" | "command" .
    Synopsis:
	Displays help information on the command, topic, period, or tokens
	specified.
    Defaults:
	If none of the above are specified,
	    then an overall introduction to the book interface is displayed.
	If a period specification is given,
	    then the time periods mapped by the specification are explicitly
	    instanciated.
	If the name of a token is given,
	    then the number of these tokens assigned to the user is displayed.
    Note:
	Help command strings are always presented in the "See Also" section.
	Since the help command is the default command, the user will usually
	not have to type "help" (or "h") at all, just the arguements to the
	help command on their own. The only exception to this is when the help
	arguement is itself the name of a command (obviously!).
#

#exit
#quit
    Syntax:
	"quit" | "exit" | "<CTRL-D>"
    Synopsis:
	Exit from the book program.
#

* the token command includes the tokens concept
#token
    Syntax:
	"token" ( ["all"] | {token_name} )
    Synopsis:
	For each token named, show:
	    the number of such tokens currently	assigned to the user;
	    and the periods that may be booked using the token.
    Defaults:
	If no tokens are named,
	    then list all unexpired tokens assigned to the user.
	If "all" is named, show all tokens regardless of expiry date.
#tokens
    Description:
	Tokens are assigned to users of the booking system, and used by them to
	    make terminal bookings. Users refer to tokens by name, and may pass
	    these token names to various book interface commands.
	Tokens can be used to book a restricted set of periods only.
	    Tokens of the same name have the same set of restricted periods.
	Token names cannot be abbreviated (unlike commands, which can be).
    See Also:
	"help" command_name	For more details on what each command does
				with the list of token names passed to it.

#

#period
    Synopsis:
	Most book commands will accept a period specification which allow the
	users to specify the periods that they would like to book, inspect, or
	cancel.
    Syntax:
	period	= intersection .
	intersection = union {union} .
	union	= range {"," range} .
	range	= ["not" | "~"] starttime {"-" endtime} .
	endtime	= periodtime .
	starttime = periodtime .
	periodtime = "(" intersection ")"
		| time | day | date | month | predef .
	time	= hour [":" min] ["am" | "pm"] .
	date	= dayofmonth "/" monthofyear ["/" year] .
	day	= "monday"  | ... | "sunday" | "today" | "tomorrow" .
	month	= "january" | ... | "december" .
	predef	= predef_id ["." week]
	predef_id = the name of one of the predefined periods.
	hour = min = week = dayofmonth = monthofyear = year = "0".."9"{"0".."9"}
    Examples:
	10am		next (10:00 to 10:29) period.
			    Today if the current time is before 10am, otherwise
			    Tommorrow.
	10-12		next (10:00 to 11:59) period.
			    Today and/or tommorrow depending on current time.
	10-12,2pm Tue	next ((10:00 to 11:59, and 14:00 to 14:29) of Tuesday)
			    If today is Tuesday, then times will refer to next
			    Tuesday's periods if current time exceeds specified
			    time(s).
	10 tue-thur	next 10:00 to 10:29 on Tuesday, Wednesday, and Thursday
	2pm tue october	14:00 to 14:29 on all Tuesdays in October of this year
	Thu,Tue s1.4	Tuesday and Thursday of the fourth week of the
			predefined period s1 (s1 will usually be defined to be
			session 1 of the current year - see "help predef").
	(11,2pm-3pm wed),(10:30 thur) s1.2-s1.4
			The various periods on Wednesdays and Thursdays
			    of the second week of session 1
			    to the fourth week (inclusive) of session 1
    Notes:
	A booking period has a minimum duration of half an hour.
	If a user specifies a start time for the period, but not an end time
	then the end time will depend on the type of start time specified.
	    Start time		Default end time
	    --------------------------------------------
	    Time of day		time of day + 30 minutes
	    Day of Week		end of that day of week
	    Predef.week		end of that predefined week.
	eg:
	    Start time		Default end time
	    --------------------------------------------
	    10am		10:29:59
	    12:30 Tue		12:59:59 Tue
	    Tue			23:59:59 Tue
	    s1.2		Sunday of second week of session 1
    See Also:
	"help predef"	For help on predefined periods.
	"help" period	To list the periods matched by a period specification
			 eg: "help 10-12 Tue" will list the times and dates
			 matched by the period specification "10-12 Tue"
	"help syntax"	For help on the notation used in syntax definitions.
#

#predef
    Synopsis:
	A predef is a predefined period that has been given a name.
	The period may be used in other period expressions by refering to its
	name in the period expression.

	A particular week of the predefined period may be refered to by
	    "name.week"
	where "name" is the name of the predefined period, and "week" is the
	number of the week desired.

	A new period is added to the list of existing predefs by using the
	command "define", and removed from the list using "undefine".

    Special Predefs:
	The predef named "excluded" is special in a couple of ways:
	    1) This predef defines those periods which can never be booked.
		The periods usually refer to holidays when students will not
		generally be given access to the workstations.
	    2) There may be more than one period named "excluded". These
	       periods are merged into the one predef.
    Notes:
	Periods start from week 1 (week 0 and week 1 are synonamous).
	To see what a predef has been defined as, type: "define" predef_name.
	To see what predefs have been defined, type: "define"
	To see what periods have been excluded, type: "define excluded"

	While predefs may be defined interactively, they are more usually
	defined in command files.

    See Also:
	"help define"	For the syntax of the define command
	"help undefine"	For the syntax of the undefine command
	"help period"	For help on period specifications in general.
	"help cfile"	For help on command files.

#

#cfile
    Synopsis:
	A book interface command file contains booking commands that the book
	interface reads and executes before accepting interactive commands from
	the user.
	These command files will usually contain non-interactive booking
	commands - most commonly the "define" command.
    Files:
	There are two command files that the book interface will read:

	/usr/local/lib/book/periods	System command file which define
				general periods and predefs that are useful
				for the current year.

	$HOME/.periods		User's command file.
				This file is read after the system file.
    File format:
	Commands are entered as they would be interactively.
	Commands may be extended onto the next line if the line does not
	    contain a comment, and the line ends with a "\".
	Comments start with a "#" and extend to the end of line.
	Blank lines are ignored.

    Notes:
	The user's command file is primarily intended to contain those defines
	of predefs that the user will find personally useful.
	For example, the user may wish to add a define command to their
	".period" file which defines a predef that maps all the user's
	free timetable periods for the week.
	eg:
	    define myfree (10-12,13-14 Mon), \
			  (3pm-5pm Tue-Wed), (thur afternoon)

	This predef could later be used (for instance) to find all available
	free periods on Tuesday of the fourth week of session 1 using:
	    available myfree tue s1.4

	The user may also wish to print a list of tokens, bookings, and
	available free periods just before getting the interactive prompt from
	the book interface, by putting the following few lines at the end
	of their ".period" file.
	eg:
	    token		# this lists all available tokens
	    show		# this shows all current bookings
	    echo "Periods free today:"
	    avail myfree today	# this lists all my free periods today
				# (assuming predef "myfree" has been defined)
    See Also:
	"help command"	For a description of the command syntax.
	"help" command	For a description of the specific command.
	"help predef"	For a description of predefined periods.
#

#syntax
    Command and Period syntax are described using the following (EBNF)
    conventions:

    Convention	Meaning
    ---------------------------------------------------------------
    "acb"	the literal string "abc" (entered without the quotes)
    abc		the construct abc, whose value is given by the assignment
		of literals and/or constructs to it by the assignment statement

    In what follows, X,Y,Z refer to expressions of literals and/or constructs.

    Expression	Meaning
    ( X )	The same as X.
    [ X ]	X may occur once or not at all. (X is optional).
    { X }	X may occur zero or more times.
    X | Y	An occurence of X or Y (but not both).
    X Y		X is followed by Y, optionally separated by blanks or tabs.

    abc = X .	The assignment statement.
		The construct abc has the value of the expression X.

    eg: The syntax of the book command is: "book" {tokens} [period].
    This means that you type the literal string "book" optionally followed
    by zero or more token constructs (defined in "help token"), and followed 
    by the period construct (whose syntax is defined in "help period").
#

#defaulter
    A "defaulter" is a user of the booking system who has demonstated
    a pattern of making bookings, but not claiming them.

    When a user is considered to be a defautler, bookings made by that
    user will be allocated to a workstation, but the workstation will
    not be freed or reserved for them. This is because it is expected
    that the user will not turn up, so someone else should be allowed
    to use the workstation.

    If the user does turn up to claim their booking, they will have to
    log in to a booking terminal and issue the "claim" command. This
    command will cause the allocated workstation to be freed (if
    necessary) and reserved for the user.

    A user is considered to be a defaulter if they have failed to turn
    up to at least two bookings in the past fortnight including one of
    their two most recent bookings.
    NOTE: this heuristic may change if it doesn't appear to catch the
    right set of people.

    Once a defaulter demonstates that they take bookings seriously by
    turning up to several consecutive bookings, the defaulter status
    will be dropped and bookings will one more cause workstation to be
    freed and reserved.

    "claiming" a booking means logging on to, or already being logged
    on to, the allocated workstation before the grace period (of seven
    minutes) passes. This only applies to the first of a consecutive
    sequence of booked periods.

    So, if you log in after the seven minute mark, or log into a
    workstation other than the one you booked, then you will be
    considered to have defaulted on that booking.

    To find out which bookings the booking system thinks you have 
    defaulted on, use the "past" command. The defaulted bookings are
    marked with a (D).

#
