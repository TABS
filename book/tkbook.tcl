#!/usr/local/bin/wish -file

#
# Things to do:
#
# o read list of pending bookings and allow access to it  DONE
# o allow bookings with multiple tokens and choice of how many of which DONE
# o make periods page resizable - trace window too.
# o behave sensibly if {un,}book command is entered
# o handle prompting in user-entered commands
# o allow query of a particular slot DONE

# tcl/tk front end to "book" program
#
# we present a matrix of days, at least one week,
# mon to sun. each as a button with a date in it
# e.g.
#    mon  tue  wed  thu  fri  sat  sun
#              22   23   24   25   26
#    27   28   29   30   31    1    2
#
# any day which has bookings might get highlighted
# Below this is a matrix of periods by labs for the selected
# day. There is always one selected day, today at first.
# e.g.
#         ball     club    ring    oboe
#  8:30
#  9:00
#  9:30
# 10:00
# ....
# 21:00
# 21:30
#
# each timeslot will display number of bookable workstations, plus
# a 'C' if there is a class booking in the lab
# Booked slots show "Booked"
# slots can be selected with the mouse. any column can be selected
# selected slots may be "booked" or "unbooked" with a button.
#
# there is a pane which lists current bookings in the same format
# that book lists them:
#    lab   token   fr:om - to:.. day month
# double clicking on a booking with cause that booking to be selected in the
# calendar window.
#
# There is a window which traces the output of "book"
# there is also (for testing at least) an input window for typing commands to book.

option add *Dialog.msg.font fixed 
frame .book
pack .book

frame .book.title
label .book.title.name -text ""
pack .book.title -fill x
button .book.title.dismiss -command exit -text "Exit"
button .book.title.help -command DisplayHelp -text "Help"
pack .book.title.dismiss -side right
pack .book.title.help -side right
label .book.title.tlabel -text "Token:"
pack .book.title.tlabel -side left
place .book.title.name -relx 0.5 -rely 0.5 -anchor cent
#pack .book.title.name -side top

frame .book.cal -borderwidth 5 -relief groove
frame .book.periods -borderwidth 5 -relief groove
frame .book.bookings -borderwidth 5 -relief groove
frame .book.io

pack .book.cal -side top
pack .book.periods -side top 

label .book.bookings.label -text "Bookings"
pack .book.bookings.label -side top
listbox .book.bookings.list -relief sunken -yscrollcommand ".book.bookings.scroll set" -height 5 -font fixed -selectmode none -width 60
bind .book.bookings.list <1>  "findbooking  \[.book.bookings.list nearest \[expr \[winfo pointery .book.bookings.list] -  \[winfo rooty .book.bookings.list]]] "
bind .book.bookings.list <2>  "findbooking  \[.book.bookings.list nearest \[expr \[winfo pointery .book.bookings.list] -  \[winfo rooty .book.bookings.list]]] "
scrollbar .book.bookings.scroll -relief sunken -command ".book.bookings.list yview" -width 10
pack .book.bookings.list -side left 
pack .book.bookings.scroll -side right -fill y
pack .book.bookings -side top 


entry .book.io.cmd 
bind .book.io.cmd <Return> { send_book_command [ .book.io.cmd get ] }
frame .book.io.trace 
text .book.io.trace.text -wrap none -width 66 -height 5 -state disabled \
	-yscrollcommand ".book.io.trace.scroll set"
scrollbar .book.io.trace.scroll -relief sunken -command ".book.io.trace.text yview" -width 10
pack .book.io.trace.text -side left
pack .book.io.trace.scroll -side right -fill y

label .book.io.prompt -text "Book> "
pack .book.io.trace -side bottom
pack .book.io.cmd -side right -fill x -expand yes
pack .book.io.prompt -side left
pack .book.io  -side top


proc send_book_command { text } {
    global file
    .book.io.trace.text configure -state normal
    .book.io.trace.text insert  end "BOOK > "
    .book.io.trace.text insert  end [ .book.io.cmd get ]
    .book.io.trace.text insert end "\n"
    .book.io.cmd delete 0 end
    .book.io.trace.text yview -pickplace end
    .book.io.trace.text configure -state disabled
    update
    puts $file $text
    flush $file
    find_prompt ignore 1
}

proc start_book { { who {}} } {
    global file
    .book.title.name configure -text "$who"
    set file [ open "|/usr/local/bin/book $who 2>@stdout" r+ ]
}

proc take_line { proc trace line } {
 if { $trace } {
  .book.io.trace.text configure -state normal
  .book.io.trace.text insert  end $line
  .book.io.trace.text insert end "\n"
  .book.io.cmd delete 0 end
  .book.io.trace.text yview -pickplace end
  .book.io.trace.text configure -state disabled
 }
 if { $proc != "ignore" } {
   $proc $line
 }
}

proc find_prompt_orig { {proc ignore} {trace 0 } } {
 # read lines from  "file" until BOOK > appears
 global file
 set extra ""
 while { "$extra" != "BOOK > "  } {
   set more [ read $file 10 ] 
   while { [ string match "*\n*" $more ]  } {
      regexp "(\[^\n]*)\n(.*)" $more all head tail
      set extra "$extra$head"
      set more $tail
      take_line $proc $trace $extra
      set extra ""
   }
   set extra "$extra$more"
 }
}

proc find_prompt { {proc ignore} {trace 0 } } {
    # read lines from  "file" until BOOK > appears
    global file
    set extra ""
    set found 0
    while { ! $found  } {
	if { [gets $file more] < 0 } {
	    book_error "Book process has died. tkbook will now terminate"
	    exit
	}
#	puts "line is $more"
	if { $more == "BOOK > " } { set found 1 } else {
	    take_line $proc $trace $more
	}
	set extra ""
    }
    #puts "done"
}



proc cal_init {} {
global monthlen months days to_day to_month cal_day cal_month today
# code for the mini-calendar
# the calendar shows two weeks and can step forward or backward
# a week
# We keep to_day and to_month to know when we are
# We keep cal_day and cal_month to show start of calendar
# cal_day/month must never be a week before today
# we display month as e.g. may or may/june

# find start of this week
set cal_day $to_day
set cal_month $to_month
set diff 0
while { [ lindex $days $diff ] != $today } {
  set diff [ expr $diff+1 ]
  set  cal_day [ expr $cal_day - 1 ]
  if { $cal_day < 0 } {
    set cal_day [ expr $cal_day + [ lindex $monthlen $cal_month ] ]
    set cal_month [ expr $cal_month - 1 ]
  }
}
}

proc cal_create {} {
global monthlen months days to_day to_month cal_day cal_month 
 # create the calendar widgits - leave them blank
 # .book.cal is a frame to hold it
 # make .book.cal.month fo month name
 # make .book.cal.{mon..sun}{days,week1,week2} labels buttons, buttons
 # make .book.cal.back and .fore for moving back and fore

 set monthlen { 31 31 29 31 30 31 30 31 31 30 31 30 31 }
 set months { January February March April May June July August September October November December }
 set days { Mon Tue Wed Thu Fri Sat Sun }

 label .book.cal.monthname
 pack .book.cal.monthname -side top
 frame .book.cal.back
 label .book.cal.back.days -text "   " -width 3 -padx 0 -pady 0
 button .book.cal.back.week1 -state disabled -width 2 -padx 0 -pady 0 -relief flat
 button .book.cal.back.week2 -state disabled -width 2 -padx 0 -pady 0 -relief flat
 pack .book.cal.back -side left
 pack .book.cal.back.days .book.cal.back.week1 .book.cal.back.week2 -side top
 foreach day $days {
   frame .book.cal.d$day
   label .book.cal.d$day.days -text $day -width 3 -padx 0 -pady 0
   button .book.cal.d$day.week1 -state disabled -width 2 -justify right -padx 0 -pady 0
   button .book.cal.d$day.week2 -state disabled -width 2 -justify right -padx 0 -pady 0
   pack .book.cal.d$day.days -side top
   pack .book.cal.d$day.week1 -side top
   pack .book.cal.d$day.week2 -side top
   pack .book.cal.d$day -side left
 }
 frame .book.cal.fore
 label .book.cal.fore.days -text "   " -width 3 -padx 0 -pady 0
 button .book.cal.fore.week1 -state disabled -width 2 -padx 0 -pady 0 -relief flat
 button .book.cal.fore.week2 -state disabled -width 2 -padx 0 -pady 0 -relief flat
 pack .book.cal.fore -side top
 pack .book.cal.fore.days .book.cal.fore.week1 .book.cal.fore.week2 -side top
}

proc cal_fill {} {
    global monthlen months days to_day to_month cal_day cal_month  max_month max_day
    global the_day the_month the_date
    # set the dates and month name in the calendar
    # any button before today is inactive, others are normal
    set aday $cal_day
    set amonth $cal_month
    if  { $amonth < $to_month || ( $amonth == $to_month && $aday <= $to_day) } {
	.book.cal.back.week1 configure -state disabled -relief flat -text ""
    } else {
	.book.cal.back.week1 configure -state normal -relief raised -text "<<" -command backweek
    }
    set month1  [ lindex $months $amonth ]
    foreach week { week1 week2 } {
	foreach day $days {
	    if { $amonth < $to_month || ( $amonth == $to_month && $aday < $to_day)  ||  $amonth > $max_month || ($amonth == $max_month && $aday > $max_day) } {
		# before now or after end
		.book.cal.d$day.$week configure -text "" -state disabled -relief flat
	    } else {
		# now or later
		.book.cal.d$day.$week configure -text $aday -state normal -relief raised -command "book_select $day $aday $amonth "
		if { $the_date == $aday && $the_month == $amonth } {
		    .book.cal.d$day.$week configure -relief ridge
		}
	    }
	    if { $week == "week2" && $day == "Sun" } {
		set month2 [ lindex $months $amonth ]
	    }
	    
	    set aday [ expr $aday + 1 ]
	    if { $aday > [ lindex $monthlen [ expr $amonth + 1]] } {
		set amonth [ expr $amonth + 1 ]
		set aday 1
	    }
	}
    }
    if { $month1 == $month2 } {
	.book.cal.monthname configure -text $month1
    } else {
	.book.cal.monthname configure -text $month1/$month2
    }
    
    if { $amonth > $max_month || ($amonth == $max_month && $aday > $max_day) } {
	.book.cal.fore.week2 configure -state disabled -relief flat -text ""
    } else {
	.book.cal.fore.week2 configure -state normal -relief raised -text ">>" -command foreweek
    }
}

proc backweek {} {
   global cal_day cal_month monthlen
  set  cal_day [ expr $cal_day - 7 ]
  if { $cal_day < 0 } {
    set cal_day [ expr $cal_day + [ lindex $monthlen $cal_month ] ]
    set cal_month [ expr $cal_month - 1 ]
  }
  cal_fill
}

proc foreweek {} {
   global cal_day cal_month monthlen
   set cal_day [ expr $cal_day + 7 ]
   if { $cal_day > [ lindex $monthlen [ expr $cal_month + 1]] } {
      set cal_month [ expr $cal_month + 1 ]
      set cal_day [ expr $cal_day - [ lindex $monthlen [ expr $cal_month ]]]
    }
   cal_fill
}
 
set getid NONE
proc book_select { day date month } {
    global the_day the_date the_month
    global firstperiod lastperiod 
    global getid
    global nextperiod finalperiod
    global labs
    global av avtype

    if { $the_date != $date || $the_month != $month } {
	cursorBusy 0
	set the_day $day
	set the_date $date
	set the_month $month

        if { [array exists av] } {
            unset av
        }
        if { [array exists avtype] } {
            unset avtype
        }
	
	cal_fill
	
	if { $getid != "NONE" } { after cancel $getid }
	set oldlabs $labs
	get_labs
	get_times
#	update # only re-enable this is events (e.g. ymoveall) can cope that not all in $labs have frames created.
	foreach lab $oldlabs { destroy .book.periods.l$lab }
	if [winfo exists .book.periods.message] { destroy .book.periods.message}
	
	make_periods redo
	
	set nextperiod $firstperiod
	set finalperiod $lastperiod
	set getid [after idle getnext]
	# cursorUnset
    }
}

proc getnext {} {
    global getid finalperiod nextperiod usecounts
    set getid NONE
    set oldusecounts $usecounts
    get1available $nextperiod
    if { "$oldusecounts" == "$usecounts"  } {
	incr nextperiod
    }
    if { $nextperiod <= $finalperiod } {
	set getid [ after idle getnext ]
    } else {
	cursorUnset
    }
    
}


# now for the period table
# we make this out of listboxen - one listbox for each lab
# there is also a scroll bar.
# the scroll bar will effect ALL list boxes. possibly do this via a cycle of control
# scroll bar modifies first which modifies second ... which modifies scrollbar??
# no, that creates multiple loops which are hard to break.
# instead get scrollbar to control time.list
# time.list then sets all others, and other when moved, reset to match time.list
# There is also a list box for period times - 08:30 09:00 ...
# each lab listbox will contain a number of bookable workstations for that time
# I want the updating of these to be a background task. Initialy fill with
# blanks.
proc make_periods { redo } {
 # expect to have a list of lab names in "labs"
 # and a range of periods in "firstperiod" and "lastperiod" (0..47)
 # need a frame for each lab to hold lab name and listbox
    global firstperiod lastperiod labs
    global the_day the_date the_month months
    global av
    if { $redo != "redo" } {
	frame .book.periods.title
	pack .book.periods.title -side top -fill x
	if { $the_day == "unknown" } {
	    label .book.periods.title.date -text ""
	} else {
	    label .book.periods.title.date -text "$the_day $the_date [lindex $months $the_month]"
	}
	button .book.periods.title.book -text " Book " -command dobook
	button .book.periods.title.unbook -text "Unbook" -command dounbook
	pack .book.periods.title.book -side left
	pack .book.periods.title.unbook -side right
	pack .book.periods.title.date -side bottom
	
	frame .book.periods.time
	label .book.periods.time.label -text "Time"
	listbox .book.periods.time.list -width 5  -selectmode none -font fixed
	pack .book.periods.time.label -side top
	pack .book.periods.time.list -side right
	pack .book.periods.time -side left
    } else {
	.book.periods.time.list delete 0 end
    }

    if { $the_day == "unknown" } {
	.book.periods.title.date configure -text ""
    } else {
	.book.periods.title.date configure  -text "$the_day $the_date [lindex $months $the_month]"
    }
    if { $firstperiod == "unknown" } { set firstp 0 ; set lastp 47 ; } else {
	set firstp $firstperiod ; set lastp $lastperiod  
    }


    for { set p $firstp } {$p <= $lastp } { set p [ expr $p+1 ] } {
	.book.periods.time.list insert end [ format "%02d:%02d" [ expr $p/2] [expr 30*($p%2)] ]
    }


    if { $redo != "redo" } {
	frame .book.periods.scroll
	label .book.periods.scroll.label -text ""
	scrollbar .book.periods.scroll.bar -orient vertical 
	pack .book.periods.scroll.label -side top
	pack .book.periods.scroll.bar -side right -fill y
	pack .book.periods.scroll -side right -fill y
	
    }
    # now fill in the labs...

    if { $firstp > $lastp } {
	message .book.periods.message -text "You have no tokens which can be used to book on this day." -aspect 250
	pack .book.periods.message -side left -expand yes
    } else {
	foreach lab $labs {
	    frame .book.periods.l$lab
	    pack .book.periods.l$lab -side left
	    label .book.periods.l$lab.label -text "$lab"
	    listbox .book.periods.l$lab.list -width 8 -selectmode extended -font fixed
	    bind .book.periods.l$lab.list <1> "explain $lab  \[.book.periods.l$lab.list nearest \[expr \[winfo pointery .book.periods.l$lab.list] -  \[winfo rooty .book.periods.l$lab.list]]] "
	    bind .book.periods.l$lab.list <2> "explain $lab  \[.book.periods.l$lab.list nearest \[expr \[winfo pointery .book.periods.l$lab.list] -  \[winfo rooty .book.periods.l$lab.list]]] "
	    pack .book.periods.l$lab.label -side top
	    pack .book.periods.l$lab.list
	    pack .book.periods.l$lab -side left
	    for { set p $firstp} {$p <= $lastp } { incr p } {
		.book.periods.l$lab.list insert end " ???"
	    }
	    .book.periods.l$lab.list configure -yscrollcommand "ynomove .book.periods.l$lab.list"
	}
    }
    .book.periods.time.list configure -yscrollcommand "ymoveall"
    .book.periods.time.list configure -xscrollcommand "xnomove .book.periods.time.list"
    .book.periods.scroll.bar  configure -command " .book.periods.time.list yview "
    

}

proc explain { lab row } {
    global firstperiod  av labs
    set period [expr $row + $firstperiod ]
    set time [format "%2d:%02d" [expr $period / 2] [expr ($period % 2)*30 ] ]
    set hdr "Explanation of period $time in lab $lab"
    set state [ array get av $period.$lab ]

    set ambooked NO
    foreach lb $labs {
	if { [lindex $av($period.$lb) 1] != {} } {
	    set ambooked $lb
	}   
    }
    if [llength $state] {
	set state [lindex $state 1]
	if {[lindex $state 1] != {} } {
	    #booked with the given token
	    set msg "You are booked with token \"[lindex $state 1]\""
	} elseif { $ambooked != "NO"} {
	    set msg "You are already booked at this time in lab \"$ambooked\""
	} elseif { [lindex $state 0] != {}} {
	    # bookable, maybe there is a class booking
	    set msg ""
	    set ch [choose_tokens 1 0 [lindex $state 0]]
	    if { $ch == "insufficient" } {
		set msg "You do not have any tokens left which can book this period"
	    } else {
		if { [llength [lindex $state 0]] == 1 } {
		    set msg "You can book this slot with the token \"[lindex $state 0]\""
		} else {
		    set msg "You can book this slot with any of the tokens \"[lindex $state 0]\""
		}
		global avtype
		if { [ llength [ array get avtype $period.$lab ] ] } {
		    if { "$avtype($period.$lab)" == "near" } {
			append msg "\n   but the lab is nearly full"
		    }
		    if { "$avtype($period.$lab)" == "full" } {
			append msg "\n   but the lab is full and you will be over-booking"
		    }
		}
	    }
	    # fixme - count tokens
	    if {[lindex $state 2]  != {} } {
		# class booking...
		set msg "$msg\nThere is a class booking for \"[lindex $state 2]\""
	    }
	} else {
	    if {[lindex $state 2] == {} } {
		set msg "The lab is fully booked"
	    } else {
		set msg "The lab is fully booked for class \"[lindex $state 2]\""
	    }
	}
    } else { 
	set msg "The lab state is unknown..."
    }
	

    #    tk_dialog .dialog1 "Explanation" "$hdr\n\n$msg" info 0 OK
    .book.io.trace.text configure -state normal
    .book.io.trace.text insert end "\n$hdr:\n  $msg\n"
    .book.io.trace.text yview -pickplace end
    .book.io.trace.text configure -state disabled
}

proc ynomove { list a b } {
    # ignore a and b and move to match time.list
    set timepos [ .book.periods.time.list yview ]
    set a [lindex $timepos 0]
    $list yview moveto [ expr $a + ($b-$a)/60 ] 
}
proc ymoveall { a b } {
    global labs
    .book.periods.scroll.bar set $a $b
    foreach lab $labs {
	.book.periods.l$lab.list yview moveto  [ expr $a + ($b-$a)/60 ] 
    }
}

proc ymove { list a b } {
 set current [ $list yview ]
 if { [ lindex $current 0 ] != $a } {
   $list yview moveto [ expr $a + ($b-$a)/100 ]
 }
}

proc symove { sb list a b } {
 $sb set $a $b
 set current [ $list yview ]
 if { [ lindex $current 0 ] != $a } {
   $list yview moveto  [ expr $a + ($b-$a)/100 ] 
 }
}

proc xnomove { list a b } {
 if { $a > 0 } {
  $list xview 0 
 }
}

proc periodstr {p} {
    return [format "%d:%02d" [expr $p/2] [expr ($p%2)*30]]
}

# booking and unbooking 
proc dobook {} {
    global ch_start ch_end ch_firstperiod ch_periods ch_lab ch_tokens ch_booked ch_free
    if [get_selection] {
	if { $ch_booked > 0 } {
	    book_error "You are already booked during that time"
	} elseif { $ch_free != $ch_periods } {
	    book_error "Your selection includes a period which is not bookable"
	} elseif { [llength $ch_tokens] < 1 } {
	    book_error {You have no token type which can book all of the selected periods.
	    Please select as shorter range of periods to book}
	} else {
	    # ok, we might be able to book..
	    set ch [choose_tokens $ch_periods 1 $ch_tokens]
	    
	    if { $ch == "insufficient"} {
		tk_dialog .dialog1 "Insufficient Tokens" "You do no have enough tokens to book the selected periods" error 0 OK
	    } elseif { $ch != "abort" } {
		
		global tokens tokencount
		global nextperiod finalperiod getid the_date the_month months
		cursorBusy 0
		set nextp $ch_firstperiod
		foreach pair $ch {
		    set tk [lindex $pair 0]
		    set n [lindex $pair 1]
		    global mesg usecounts ; set mesg ""
		    sendcommand "book $the_date [lindex $months $the_month] [periodstr $nextp]-[periodstr [expr $nextp + $n]] =$n $ch_lab $tk confirm=0" takemesg trace
		    if { [ string length $mesg ] && [ string length $usecounts ] } {
			tk_messageBox -message $mesg  -type ok
		    }
		    incr nextp $n
		}
		get_tok
		update_current
		do_update
		if { $getid != "NONE"} {
		    if { $nextperiod > $ch_firstperiod } { set $nextperiod $ch_firstperiod}
		} else {
		    set nextperiod $ch_firstperiod
		    set finalperiod [expr $ch_firstperiod + $ch_periods-1]]
		    set getid [after idle getnext]
		}
		cursorUnset
	    }
	}
    } else { 
	book_error "Please select a period to book"
    }
}

proc dounbook {} {
    global ch_start ch_periods ch_lab ch_tokens ch_booked ch_free ch_end
    global the_date the_month months
    if [get_selection] {
	if { $ch_booked != $ch_periods } {
	    book_error "Your selection includes some periods for which you are not booked"
	} else {
	    global getid nextperiod ch_firstperiod finalperiod
	    cursorBusy 0
	    sendcommand "unbook $the_date [lindex $months $the_month] $ch_start-$ch_end $ch_lab confirm=0" takeline trace
	    get_tok
	    update_current
	    do_update
	    if { $getid != "NONE"} {
		if { $nextperiod > $ch_firstperiod } { set $nextperiod $ch_firstperiod}
	    } else {
		set nextperiod $ch_firstperiod
		set finalperiod [expr $ch_firstperiod + $ch_periods-1]]
		set getid [after idle getnext]
	    }
	    cursorUnset
	}
    } else { 
	book_error "Please select a period to unbook"
    }
}

proc book_error {m} { 
    after idle {.dialog.msg configure -wraplength 4i}
    set i [tk_dialog .dialog "Error" "$m" error 0 "OK"]
}
proc get_selection {} {
 # find the start, length and lab of current selection
 # determine number of bookings at that time and 
 # list of tokens which may be used on ALL unbooked times
 global ch_start ch_firstperiod ch_end ch_periods ch_lab ch_tokens ch_booked ch_free
 global labs av firstperiod seltoken tokens
 set ch_lab ""
 foreach lab  $labs {
   set sel [ .book.periods.l$lab.list curselection ]
     if { $sel != "" } {
	 set ch_lab $lab
	 set ch_sel $sel
     }
 }
 if { $ch_lab == "" } { return 0 }
 set start [lindex $ch_sel 0]
 set ch_start [.book.periods.time.list get $start ]
 set ch_firstperiod [expr $start + $firstperiod]
 set ch_periods [llength $ch_sel]

 set hr [ string range $ch_start 0 1 ]
 set mn [ string range $ch_start 3 4 ]
 set tm [expr ("1$hr"-100)*2 + $mn/30 ]
 set ch_end [format "%02d:%02d" [expr ($tm+$ch_periods)/2] [expr (($tm+$ch_periods)%2)*30]]
 
 set ch_booked 0
 set ch_free 0
 if { $seltoken == "All-Tokens" } {
     set ch_tokens [ lsort $tokens ]
 } else {
     set ch_tokens $seltoken
 }
 foreach p $ch_sel {
     set p [expr $p + $firstperiod ]
     set e $av($p.$ch_lab)

     if { [lindex $e 1] != {} } { 
	 incr ch_booked 
     } elseif { [lindex $e 0] != {} } {
	 incr ch_free
	 # find common tokens
	 set ch_tokens [lcomm $ch_tokens [lsort [lindex $e 0] ]]
     }
 }
 return 1
}

proc lcomm { a b } {
    # get list of word in both a and b
    set r {}
    while { $a != {} && $b != {} } {
	if { [lindex $a 0] == [lindex $b 0] } {
	    lappend r [lindex $a 0]
	    set a [ lrange $a 1 end ]
	    set b [ lrange $b 1 end ]
	} elseif { [lindex $a 0] < [lindex $b 0] } {
	    set a [lrange $a 1 end ]
	} else {
	    set b [lrange $b 1 end ]
	}
    }
    return $r
}

	
proc takemesg { aline } {
    # If a line starts with "Warning:", take it and subsequent lines
    # that start with a tab, and store them in "mesg"
    global mesg
    if { [ string length $mesg ] } {
	set reg "^\t\(.*\)"
    } else {
	set reg "^Warning: \(.*\)" 
    }
    if { [ regexp $reg $aline all tail ] } {
	append mesg "\n" $tail
    }
}

# talk to book program...
# get todays date
proc takeline { aline } {
    global line
    set line $aline
}
proc sendcommand { command proc args } {
    global file
    if { [llength $args] > 0 } {
	set  trace 1
    } else { set trace 0 }
    if {$trace} {
	.book.io.trace.text configure -state normal
	.book.io.trace.text insert  end "BOOK > "
	.book.io.trace.text insert  end $command
	.book.io.trace.text insert end "\n"
	.book.io.cmd delete 0 end
	.book.io.trace.text yview -pickplace end
	.book.io.trace.text configure -state disabled
	update
    }
#    puts "sending command $command"
    puts $file $command
    flush $file
    find_prompt $proc $trace
}

proc get_today {} {
    # give command "today" to book and extract date from reply
    # e.g. reply:         00:00 Mon 27 May - 23:59 Mon 27 May 1996
    global today to_day to_month line
    global months
    sendcommand "today" takeline trace
    regexp {00:00 (...) ([0-9 ][0-9]) (...) - 23:59.*} $line all today to_day monthname
    for {set i 0 } { $i < 12 } { incr i } {
	if { [ string range [lindex $months $i ] 0 2 ] == $monthname } { set to_month $i }
    }
}

proc get_labs {} {
    # give command "labs date" and read a list of labs..
    global the_date the_month months to_day  labs line
    sendcommand "labs $the_date [lindex $months $the_month]" takeline 
    set labs [ lsort $line ]
}

proc get_times {} {
    global the_date the_month months line firstperiod lastperiod
    sendcommand "times $the_date [lindex $months $the_month]" takeline 
    if [ regexp {^ *0?([1-9]?[0-9]*):0?([1-9]?[0-9]).*[- ]0?([1-9]?[0-9]*):0?([1-9]?[0-9]) *$} $line all sh sm eh em ] {
	set firstperiod [ expr $sh * 2 + ( $sm/30) ]
	set lastperiod [ expr $eh *2 + ( ($em-29)/30) ]
    } else {
	set firstperiod 1
	set lastperiod 0
    }
}

# uncomment this, and you will get a count of the number of free
# workstations in each free slot, if you have a recent version of
# book
set usecounts "counts=1"
#set usecounts ""
proc get_available {} {
    # get availablility for the_day
    global the_date the_month months av labs
    global firstperiod lastperiod
    global usecounts
    set firstperiod 48
    set lastperiod 0
    foreach a [array names av] { array set av [list $a {} ]}
    sendcommand "av all $the_date [lindex $months $the_month] $usecounts" take_avail
    set labs [ lsort $labs ]
}

proc get1available { period } {
    # get availablility for the_day
    global the_date the_month months av labs
    global firstperiod lastperiod currentperiod
    global usecounts

    foreach a $labs { set av($period.$a) {} }
    set sh [ expr $period / 2 ]
    set sm [ expr ($period % 2)*30 ]
    set eh [ expr ($period+1)/2]
    set em [ expr (($period+1)%2)*30]
    set time [ format "%d:%02d-%d:%02d" $sh $sm $eh $em]
    set currentperiod none
    sendcommand "av all $the_date [lindex $months $the_month] $time $usecounts" take_avail

    update_avail $period $period
} 

# av(period.lab) == { {tokens-can-book-with} {tokens-booked-with} {classes-booked-for} }
proc add_class { where which } {
    global av
    set a [lindex $av($where) 0] ; set b [lindex $av($where) 1] ; set c [lindex $av($where) 2];
    if { [ lsearch -exact $c $which] < 0}  { lappend c $which }
    set av($where) [ list $a $b $c ]
}
proc add_booked { where which } {
    global av
    set a [lindex $av($where) 0] ; set b [lindex $av($where) 1] ; set c [lindex $av($where) 2];
    if { [ lsearch -exact $b $which] < 0} { lappend b $which }
    set av($where) [ list $a $b $c ]
}
proc add_bookable { where which } {
    global av
    set a [lindex $av($where) 0] ; set b [lindex $av($where) 1] ; set c [lindex $av($where) 2];
    if { [ lsearch -exact $a $which ] < 0} { lappend a $which }
    set av($where) [ list $a $b $c ]
}
proc take_avail { line } {
    # extract availability info and store:
    # labs is a list of all available labs
    # firstperiod and lastperiod are period numbers: 0..47
    # av is an array(period.lab)->{free,booked,class,full}
    #
    # Period: time - time  day date month (1 slot)  --- adjust periods, remember period
    # Period: time - time  day date month - already booked 'lab' for class < > --- periods/lab/av
    # ....                                                       with token ' ' --- periods/lab/av
    #   Token   Labs    --- skip
    #     tokenname    lab lab lab     --- labs/av
    global labs firstperiod lastperiod av currentperiod
    if [ regexp {^Period: 0?([1-9]?.):(..) - ..:..  ... .. ... (.*)} $line head hour minute rest ] {
	set currentperiod [ expr $hour*2+($minute/30) ]
	if [ regexp -- {- already booked '([^']*)' (.*)} $rest head lab rest2 ] {
	    # now check booking type
	    if { [ lsearch -exact $labs $lab ] >= 0  } {
		if [ regexp {for class <(.*)>} $rest2 rest3 theclass] {
		    add_class $currentperiod.$lab $theclass
		} else {
		    regexp {with token '(.*)'} $rest2 rest3 thetoken
		    add_booked $currentperiod.$lab $thetoken
		}
	    }
	}
    } elseif { [ regexp {^Undefined string} $line ] } {
	global usecounts
	set usecounts ""
    } else {
	if { ! [ regexp {No future free} $line ] && $currentperiod != "none" && ! [ regexp {Token.*Labs} $line ] } {
	    # must be "tokenname   lab lab..." (I hope )
	    # or "token  lab [lab] (lab)"
	    # [lab] are nearly fully booked.  (lab) are fully booked
	    set token [lindex $line 0 ]
	    foreach lab [ lrange $line 1 end ] {
#		puts "lab is <$lab>"
		if { [ regexp {^\((.*)\)$} $lab all thelab ] } {
		    set lab $thelab
		    global avtype
		    set avtype($currentperiod.$lab) full
		} elseif { [ regexp {^\[(.*)\]$} $lab all thelab ] } {
		    set lab $thelab
		    global avtype
		    set avtype($currentperiod.$lab) near
		}
		add_bookable $currentperiod.$lab $token
	    }
	}
    }
}

proc do_update { args } { 
    global firstperiod lastperiod
    update_avail $firstperiod $lastperiod
}

proc update_avail { first last } {
    # update the period display bases\d on possibly changed token choice
    global firstperiod lastperiod labs av seltoken
    for { set p $first  } { $p <= $last } { incr p } {
	set ambooked NO
	foreach lab $labs {
	    if { [lindex $av($p.$lab) 1] != {} } {
		set ambooked $lab
	    }   
	}
	foreach lab $labs {
	    set state [ array get av $p.$lab ]
	    #	    puts "$p.$lab state is $state (sel is $seltoken)"
	    if [llength $state] {
		set state [ lindex $state 1]
		if {[lindex $state 1] != {} }  {
		    # booked...
		    set mesg "BOOKED"
		} elseif {$ambooked != "NO" } {
		    if { $lab < $ambooked } {
			set mesg " -->"
		    } else {
			set mesg " <--"
		    }
		} elseif { [lindex $state 0] != {}} { 
		    if {  $seltoken == "All-Tokens" || [ lsearch -exact [lindex $state 0] $seltoken ] >= 0 } {
			# count possible tokens
			global tokencount
			set atok 0
			if { $seltoken == "All-Tokens" } { set tokset [lindex $state 0]} else { set tokset $seltoken}
			foreach tok $tokset { incr atok $tokencount($tok) }
			# bookable, maybe with class booking...
			if {[lindex $state 2] == {} } {
			    if { $atok > 0} {
				global avtype
				set mesg "  ."
				if { [ llength [array get avtype $p.$lab] ] } {
				    set mesg "  -"
				    if { "$avtype($p.$lab)" ==  "full" } {
					set mesg " full"
				    }
				}
			    } else {
				set mesg " N/T"
			    }
			} else {
			    if { $atok > 0 } {
				set mesg "  C"
			    } else {
				set mesg "Class"
			    }
			}
		    } else {
			set mesg "N/A"
		    }
		} else {
		    # not bookable..
		    if {[lindex $state 2] == {} } {
# this should be FULL
			set mesg " full"
		    } else {
			set mesg "Class"
		    }
		} 
	    } else { set mesg "  ---" }
	    .book.periods.l$lab.list delete [expr $p - $firstperiod ]
	    .book.periods.l$lab.list insert [expr $p - $firstperiod ] " $mesg"
	}
    }
}
# find out what tokens are available....
proc take_tok {line} {
    global tokens tokencount
    if [ regexp "^\tAll tokens" $line ] {
	# skip it
    } elseif [ regexp "^\tToken" $line ] {
	# skip it 
    } elseif [  regexp "^\t(\[^ \t]*) *\t\t*(-?\[0-9]\[0-9]*)\t.* - ..:.. ...  ?(\[0-9]*) (...) \[0-9]\[0-9]\[0-9]\[0-9]$" $line head tok count day mon ] {
	lappend tokens $tok
        if { $count < 0 } { set count 0 }
	set tokencount($tok) $count
	set nmon [monthnumber $mon]
	global max_month max_day
	if { $nmon > $max_month } { set max_month $nmon ; set max_day $day }
	if { $nmon == $max_month && $day > $max_day } { set max_day $day }
    } elseif [  regexp "^\t\t\t.* - ..:.. ...  ?(\[0-9]*) (...) \[0-9]\[0-9]\[0-9]\[0-9]$" $line head  day mon ] {
	set nmon [monthnumber $mon]
	global max_month max_day
	if { $nmon > $max_month } { set max_month $nmon ; set max_day $day }
	if { $nmon == $max_month && $day > $max_day } { set max_day $day }
    }
}

proc monthnumber {mon} {
    global months
    set nmonth 0
    for {set i 0 } { $i < 12 } { incr i } {
	if { [ string range [lindex $months $i ] 0 2 ] == $mon } { set nmonth $i }
    }
    return $nmonth
}
 
proc get_tok {} {
 global tokens tokencount global seltoken
 global max_month max_day to_month to_day
 set oldsel $seltoken
 set tokens {}
 
 set max_month $to_month
 set max_day $to_day
 sendcommand "tok" take_tok
    if [winfo exists .book.title.token] {
	destroy .book.title.token
    }

 set tmenu [eval [concat {tk_optionMenu .book.title.token seltoken "All-Tokens" } $tokens ] ]
 set sum 0
 for {set i 0 } { $i < [llength $tokens]} { incr i} {
   $tmenu entryconfigure [expr 1+ $i] -accelerator $tokencount([lindex $tokens $i])
   set sum [expr $sum+$tokencount([lindex $tokens $i])]
 }
 $tmenu entryconfigure 0 -accelerator $sum
 pack .book.title.token -after .book.title.tlabel -side left
 trace variable seltoken w do_update
 if [lsearch -exact $tokens $oldsel] { set seltoken "$oldsel" }
}

# get current bookings.
# these are stored in a simple list of lists
# {lab token start end mon date} - start and end are period numbers
proc get_current {} {
    global current_bookings
    set current_bookings {}
    sendcommand "show" take_show 
}
proc take_show {line} {
    global current_bookings
    if [regexp {^Lab[ 	]*Token or <Class>} $line match ] {
	# header line, skip it
    } else {
	if [regexp {^([a-zA-Z0-9]*)[ 	]*([^ 	]*)[ 	]*(..):(..) - (..):(..)  (...) (..) (...)} $line all lab token starth startm  endh endm day date mon] {
	    if [regexp {<.*>} $token ] {
		# it is a class booking, skip it
	    } else {
		# got a good booking
		# get rid of space
		set date [ expr $date + 0]
		set start [ expr ("1$starth" -100)*2 + ( "1$startm" -100)/30]
		set end [ expr ("1$endh" -100)*2 + ( "1$endm" -100)/30]
		set booking [ list $lab $token $start $end $mon $date $day ]
		lappend current_bookings $booking
	    }
	}
    }
}

# update the currnet bookings pane
proc update_current {} {
    global current_bookings
    get_current
    .book.bookings.list delete 0 end
    foreach b $current_bookings {
	foreach pair { {lab 0} {token 1} {start 2} {end 3} {mon 4} {date 5} {day 6}} {
	    set [lindex $pair 0] [lindex $b [lindex $pair 1]]
	}
	set line [format "%-16s%-16s%2d:%02d - %2d:%02d  %3s %2d %3s" $lab $token [expr $start / 2] [expr ($start%2)*30 ] [expr $end/2] [expr ($end%2)*30+29] $day $date $mon ]
	.book.bookings.list insert end $line
    }
}
    
proc findbooking {row} {
    global current_bookings firstperiod
    set booking [ lindex $current_bookings $row ]

    book_select [lindex $booking 6] [lindex $booking 5] [monthnumber [lindex $booking 4]]
    .book.periods.time.list yview [expr [lindex $booking 2] - $firstperiod ]
}

# a help window...

proc DisplayHelp {} {

    set w ".bookhelp"
    if [winfo exists $w] {
	destroy $w
    }
    toplevel $w

    wm title $w "Help for tkbook"
    wm minsize $w 1 1
    frame $w.frame -borderwidth 5

    scrollbar $w.frame.yscroll -relief sunken -command "$w.frame.page yview"
    text $w.frame.page -yscrollcommand "$w.frame.yscroll set " \
	    -width 80 -height 20 -relief sunken -wrap word
    pack $w.frame.yscroll -side right -fill y
    pack $w.frame.page -side top -expand 1 -fill both

    set contents {

tkbook - a visual interface to book

If you have any problems with this program that this help does not
sort out for you, please send mail to Neil Brown <neilb@cse.unsw.edu.au>
and I will try to fix the problem.

The tkbook window contains the following buttons and subwindows:

Tokens:
   select the token to use, or All-Tokens
   If a particular token is selected, then bookings will only
   be made with that token. Otherwise bookings will be made
   with whatever token is available. If a booking could be made with
   one of several tokens, you will be asked to choose.

Help:
   display this help message.

Exit:
   Exit from tkbook.

Calendar:
   select day to view in periods window.
   ">>" will move forward one week
   "<<" will move backward one week
   The currently displayed day is highlighted

Periods window:
   A range of times in any lab column may be selected with the left
   mouse button. Once selected, this range may be booked with the "book"
   button (if all the selected periods are bookable) or unbooked with the
   "unbook" button (if all the selected periods are booked).

   The text in the various periods have the following meanings:

   .       This period is bookable.
   C       This period is bookable, but there is also a class booking
           for a class that you are a member off.
   N/A     This period is bookable, but not with selected token.
   N/T     You have used up all your tokens that would be able to book this
           period.

   full    This period is fully booked.
   Class   This period is fully booked, and there is a class booking.
           at this time for a class that you are a member of.

   BOOKED  You have a booking for this period
    -->
    <--    You have a booking at this time in another lab


   Clicking with the left mouse button in any particular period will
   provide an explanation of the state of that period in the output window
   at the bottom.

   If there is a choice of tokens to be used when making a booking, then
   a dislog window will be displayed wherein your choice can be made.
   For each possible token, there will be a slider widget which can be
   manipulated to select the desired number of that token to use.
   Other sliders will be automatically update so that the sum remains
   constant.

Bookings window:
   A list of all current bookings is displayed giving the lab that the book
   is in, the token that was used to book, and the time and date of the booking.
   Selecting a booking with the mouse causes that date to be selected for the
   periods window, which will be scrolled to make the selected booking
   appear.

Book> window:
    Commands for the book program may be entered here.
    If the command asks a question, tkbook will currently hang.

Output window at bottom:
    tkbook works by running "book" and sending commands to it. Some of these
    interactions with book are displayed in this window.
    }
    $w.frame.page insert 0.0 $contents
    $w.frame.page configure -state disabled

    button $w.dismiss -text Dismiss -command "destroy $w"
    pack $w.dismiss -side bottom -fill x
    pack $w.frame -side top -fill both -expand 1
}

# mouse pointer routines, shameless stolen from tkrestore

proc cursorBusy {{up 1}} {
    if {[. cget -cursor]!="watch"} {
        cursorSet watch
        if $up {update idletasks}
    }
}

proc cursorSet {c {w .}} {
    global cursor
    set cursor($w) [lindex [$w configure -cursor] 4]
    $w configure -cursor $c
    foreach child [winfo children $w] {cursorSet $c $child}
}

proc cursorUnset {{w .}} {
    global cursor
    catch {$w configure -cursor $cursor($w)}
    foreach child [winfo children $w] {cursorUnset $child}
}


#
# choose some tokens:
#  If a booking is requested and multiple tokens may apply,
#  a decision needs to be made as to which tokens to use
#  This is done with
#     choose_tokens n ask{yes/no} tokenlist
#  which will return a list of token/count pairs, or nothing
#  if there are not enough tokens
#  if ask is yes, the user is asked to make a discision if there are choices.
#  if ask is no, the first possibility found is given

proc choose_tokens { num ask tlist } {
    global tokencount
    global gnum gtlist
    set gnum $num
    set gtlist {}
    set avail 0
    foreach t $tlist {
	if [ expr $tokencount($t) > 0 ] {
	    incr avail $tokencount($t)
	    lappend gtlist $t
	}
    }
    set tlist $gtlist
    if { $avail < $num } {
	return {insufficient}
    } elseif { $avail == $num || [llength $tlist] == 1 || ! $ask } {
	set need $num
	set ans {}
	foreach t $tlist {
	    if { $need >= $tokencount($t) } {
		lappend ans [ list $t $tokencount($t) ]
		set need [ expr $need - $tokencount($t) ]
	    } elseif { $need > 0 } {
		lappend ans [ list $t $need ]
		set need 0
	    }
	}
	return $ans
    } else {
	global choice
	# need to pop up a window
	# it has one vertical scale for each token
	set w .ch
	toplevel .ch
	wm transient $w [winfo toplevel [winfo parent $w]]
	# fill window ...

	if {$num == 1} {
	    label $w.l -text "Choose $num token"
	} else {
	    label $w.l -text "Choose $num tokens"
	}
	pack $w.l -side top
	
	frame $w.b
	button $w.b.ok -text "ok" -command { done 1 }
	button $w.b.quit -text "abort" -command { done 0 }
	pack $w.b.ok -side left
	pack $w.b.quit -side right
	pack $w.b -side bottom
	
	set need $num
	foreach t $tlist {
	    frame $w.t$t
	    pack $w.t$t -side left -fill y
	    label $w.t$t.l -text $t
	    pack $w.t$t.l -side bottom
	    scale $w.t$t.s -from $tokencount($t) -to 0 -showvalue yes -command "slide_update $t" \
		    -label $tokencount($t) -length [ expr $tokencount($t)*20+40]
	    if { $need > $tokencount($t) } {
		$w.t$t.s set $tokencount($t)
		set need [ expr $need - $tokencount($t) ]
	    } else {
		$w.t$t.s set $need
		set need 0
	    }
	    pack $w.t$t.s -side bottom -fill y
	}
	
	# centre new window
	wm withdraw $w
	update idletasks
	set x  [expr [winfo screenwidth $w]/2 - [winfo reqwidth $w]/2 \
		- [winfo vrootx [winfo parent $w]]]
	set y [expr [winfo screenheight $w]/2 - [winfo reqheight $w]/2 \
		- [winfo vrooty [winfo parent $w]]]
	wm geom $w +$x+$y
	wm deiconify $w
	
	grab $w
	tkwait visibility $w
	
	tkwait variable choice
	destroy $w
	
	return $choice
    }
}

proc slide_update { tok newnum } {
    # count how many we now have, calc how many needed,
    # try to update other sliders to make total the same
    global gnum tokencount gtlist

    set have 0
    foreach t $gtlist {
	incr have [ .ch.t$t.s get]
    }
    set need [expr $gnum - $have]

    set found 0
    foreach t [concat $gtlist $gtlist ] {
	if $found {
	    while { $need > 0 && [.ch.t$t.s get] < $tokencount($t) } {
		incr need -1
		.ch.t$t.s set [expr [.ch.t$t.s get] + 1]
	    }
	    while { $need < 0 && [.ch.t$t.s get] > 0 } {
		incr need 
		.ch.t$t.s set [expr [.ch.t$t.s get] - 1]
	    }
	} else {
	    if { $t == $tok } {
		set found 1
	    }
	}
    }
}

proc done { ok } {
    global choice gtlist
    if { ! $ok } {
	set choice "abort"
    } else {
	set ch {}
	foreach t $gtlist {
	    set n [.ch.t$t.s get ]
	    if $n {
		lappend ch [ list $t $n ]
	    }
	}
	set choice $ch
    }
}



cursorBusy 0

set firstperiod 1
set lastperiod 0
# who for...

if { $argc >= 1 } {
    start_book [lindex $argv 0]
} else {
    start_book
}

cal_create

set the_day unknown
set firstperiod unknown
set labs {}
make_periods first
update
find_prompt ignore 1

get_today

set seltoken "All-Tokens"
get_tok
update
cal_init
set the_month NOMONTH
set the_date NODATE
update

book_select $today $to_day $to_month

update_current

#cursorUnset

