#define MAXTOKSTR	40	/* length of string token */

/* the following structure is used for predefined strings in period syntax */
struct slist {
	char	*string;	/* should be lower case */
	char	tok_type;	/* assigned to token.tok_type */
	int	tok_val;	/* assigned to token.tok_val */
	int	minmatch;	/* minimum length to match */
};

/* the lexical tokens are defined as follows */
typedef struct {
    char *tok_str;	/* the input string producing this lexical token */
    int	tok_type;	/* one of the types of lexical tokens */
    int	tok_val;	/* the value for one of these types of tokens */
    period	*tok_period;	/* only used if tok_type == PID */
} tok;
