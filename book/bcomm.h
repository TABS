/*** basic booking interface constants ***/

#define UNBOOKGRACE	3600	/* seconds grace to unbook */
#define	MINDAYPREUNBOOK	1	/* days before slot before cannot unbook */
#define	MINPERIODAHEAD	1	/* min periods from current before can book */
#define	MINDAYRESERVE	4	/* min days before reserves are considered */
#define	RESERVE_PC	10	/* percent terminals reserved per lab */
#define	WARNING		1	/* non-allocation warning issued */
#define MAXBOOK		200	/* maximum number of class bookings at a time */

/*** user interactive response types ***/

#define	R_BAD	0
#define	R_NO	1
#define	R_YES	2
#define	R_QUIT	3

/*** structures used to keep track of currently available tokens ***/

typedef struct currtokentype {
    char	*name;
    int		number;
    period	*period;
    int		active;		/* == command_no if token was specified in command */
    struct currtokentype *next;
} currtokentype;

/*** structures used to keep track of current bookings ***/

typedef struct bookstruct booked;
struct bookstruct {
	description	mdesc;	/* the machine type booked - used to change booking */
    	book_key	slot;	/* the slot that was booked -  used by unbook() */
    	int	whenmade;	/* when the booking was made - used by unbook() */
    	char	*tokname;	/* name of token */
    	time_t	start;		/* start of period */
    	char	*labname;   	/* the lab that the booking was made for */
    	bookuid_t	owner;	/* the user (or class) owning the booking */
    	char	*ownername;	/* the name of the user owning the booking */
	int	nbooked;	/* the number of bookings; >=1 for class bookings */
	booking_status	status;	/* the status of the booking */
	int	defaulted;	/* non-zero if user defaulted on this booking */
	int	printed;	/* == command_no if this booking has been printed */
    	booked	*next;
};

/*** structures used to keep track of free labs and machines ***/

typedef struct consecfree {
	int	nfree;		/* avail machines in lab using token */
	int	ntotal;		/* total machines in lab using token */
	int	periodofday;	/* passed to makebook() to make bookings */
	int	dayofyear;	/* passed to makebook() to make bookings */
	time_t	start;		/* start of period; stored when period booked */
	utilizationlist speclist;	/* all machine specs for this lab & time */
	struct consecfree	*next;
} consecfree;

typedef struct labavail {
    char	*labname;	/* the name of the lab */
    time_t	laststart;	/* start time of last period in this series */
    int		nperiods;	/* consecutive periods in series */
    consecfree	*consecfree;	/* list of consecutive free specs */
    struct labavail	*next;
} labavail;

typedef struct tokused {
    char		*tokname;	/* the name of the token */
    labavail		*labs;		/* labs available using this token */
    int			nlabs;		/* the number of labs available */
    struct tokused	*next;
} tokused;
