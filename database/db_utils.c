/* some utilities for accesing the databases */

#include	<time.h>
#include	<unistd.h>
#include	"db_header.h"
#include	<stdio.h>
#include	<sys/types.h>
#include	<sys/stat.h>

extern update_status update_st;


/* GENERIC_QUERY: provides the basic query operations (firstkey, */
 /* nextkey and matchkey for any database.  Because all the databases */
 /* are stored in XDR format, the result of the query is sent back in */
 /* this form for the client to interpret.  */
query_reply * generic_query(query_req * query)
{
	static datum key =  {0,0 } ;
	static datum val =  {0,0 } ;
	static query_reply result;
	int plen;

	/* free the gdbm key and data, after the result is returned */
	/* ie at the start of the next call */
	if (key.dptr) free(key.dptr);
	if (val.dptr) free(val.dptr);
	key.dptr = val.dptr = NULL;

	/* check db actually exists */
    
	if  (query->database<0 || query->database >3 || db_file[query->database] == NULL) {
		result.error = R_NODB;
		return(&result);
	}
    
	result.error = R_RESOK;
	switch (query->type  ) {
	case M_FIRST: /* return the first key in the db and the data as */
		/* well */
		key = gdbm_firstkey(db_file[query->database]);
		if (key.dptr
		    && strncmp(key.dptr, "_STATUS_", 8)==0)
		{
			datum k2;
			k2 = gdbm_nextkey(db_file[query->database], key);
			free(key.dptr);
			key= k2;
		}
		if (key.dptr != NULL) {
			result.query_reply_u.data.key.item_len = key.dsize;
			result.query_reply_u.data.key.item_val = key.dptr;
			val = gdbm_fetch(db_file[query->database],key);
		}
		else
			result.error = R_NOFIRST;
		break;
	case M_NEXT: /* given a key return the next key and the data */
		/* associated with it */
		key.dsize = query->key.item_len;
		key.dptr = query->key.item_val;
// printf("1key db=%d is %.*s %d\n", query->database, key.dptr?key.dsize:0, key.dptr?key.dptr:"(null)", key.dsize);
		key = gdbm_nextkey(db_file[query->database],key);
// printf("2key is %.*s %d\n", key.dptr?key.dsize:0, key.dptr?key.dptr:"(null)", key.dsize);
		if (key.dptr && strncmp(key.dptr, "_STATUS_",8)==0)
		{
			datum k2 = gdbm_nextkey(db_file[query->database],key);
// printf("3k2 is %.*s %d\n", k2.dptr?k2.dsize:0, k2.dptr?k2.dptr:"(null)", k2.dsize);
			free(key.dptr);
			key = k2;
// printf("4key is %.*s %d\n", key.dptr?key.dsize:0, key.dptr?key.dptr:"(null)", key.dsize);
		}
// printf("5key is %.*s %d\n", key.dptr?key.dsize:0, key.dptr?key.dptr:"(null)", key.dsize);
		if (key.dptr != NULL)
		{
			result.query_reply_u.data.key.item_len = key.dsize;
			result.query_reply_u.data.key.item_val = key.dptr;
			val = gdbm_fetch(db_file[query->database],key);
//	    printf("5content is %.*s %d\n", val.dptr?val.dsize:0, val.dptr?val.dptr:"(null)", val.dsize);
		}
		else
			result.error = R_NONEXT;
		break;
	case M_MATCH: /* given any key, just return it and its data */
		/* copy this as the arg to service is freed anyway */
		key.dptr = query->key.item_val;
		key.dsize = query->key.item_len;
		result.query_reply_u.data.key.item_val = key.dptr;
		result.query_reply_u.data.key.item_len = key.dsize;
		val = gdbm_fetch(db_file[query->database],key);
		key.dptr = NULL; /* make sure it doesn't get freed */

		break;
	case M_NEXT_PREFIX: /* given a key return the next key with the same prefix and the data */
		/* associated with it.  If first '_' is last char, fint FIRST key*/
		key.dsize = query->key.item_len;
		key.dptr = query->key.item_val;
		if (strchr(key.dptr, '_')== NULL) {
			result.error = R_NONEXT;
			break;
		}
		plen = strchr(key.dptr, '_') - key.dptr +1;
		if (plen == key.dsize)
			key = gdbm_firstkey(db_file[query->database]);
		else
			key = gdbm_nextkey(db_file[query->database],key);
		while (key.dptr &&
		       strncmp(key.dptr, query->key.item_val, plen) != 0) {
			datum k2 = gdbm_nextkey(db_file[query->database],key);
			free(key.dptr);
			key = k2;
		}

		if (key.dptr != NULL)
		{
			result.query_reply_u.data.key.item_len = key.dsize;
			result.query_reply_u.data.key.item_val = key.dptr;
			val = gdbm_fetch(db_file[query->database],key);
		}
		else
			result.error = R_NONEXT;
		break;
	default:
		result.error = R_SYS;
		break;
	}
	if ((val.dptr == NULL) && (result.error ==  R_RESOK))
		result.error = R_NOMATCH;

	result.query_reply_u.data.value.item_val = val.dptr;
	result.query_reply_u.data.value.item_len = val.dsize;

	return(&result);
}

/* DB_OPEN: a generic initialization routine.  given a database id, */
 /* open the database with the appropriate file name. */
int db_open(db_number db, int gdbm_mode)
{
	char buf[1024];

	char * pathname;
    
	pathname= make_pathname(DatabaseName[db]);

	if (gdbm_mode == GDBM_NEWDB)
	{
		struct stat sb;
		if (stat(pathname, &sb)== 0)
			return 0;
	}
	/* open the database file */
	db_file[db] = gdbm_open(pathname, 1024, gdbm_mode, 0600, 0);

	if (db_file[db] == NULL) {
		sprintf(buf, "Cannot open database: %s, reason %d\n",pathname, gdbm_errno);
		shout(buf);
		fprintf(stderr, buf);
		exit(3);
		return 0;
	}
	free(pathname);
	return 1;
}

/* CLOSE_DB: close a given database */
void close_db(db_number db)
{
	if (db_file[db] == NULL) return;
	gdbm_close(db_file[db]);
	db_file[db] = NULL;
}

/* READ_DB: given a key, a pointer to the data, a function to decode */
 /* the database record into the data, and the id number of the */
 /* database, read the key and decode the data into the supplied */
 /* structure. */
/* NOTE: the content field should be a static variable o
   that it can be freed by the next read_db
   */
book_changeres read_db(item key, void * data, int data_size, xdrproc_t xdr_fn,  db_number db)
{
	datum content;
	datum dkey;
	static book_changeres result;

	result = C_SYSERR;
    
	xdr_free(xdr_fn, (char*) data);
	/* make sure all fields are 0 */
	memset(data, 0, data_size);
    
	if (db_file[db] == NULL) {
		result = C_FILE;
		return(result);
	}

	dkey.dptr = key.item_val;
	dkey.dsize = key.item_len;
	/* look for the description */
	content = gdbm_fetch(db_file[db],dkey);

	if (content.dptr != NULL) { /* decode existing list */
		result = (datum_decode(&content, data, xdr_fn) == TRUE) ? C_OK : C_XDR;
		free(content.dptr);
	}
	else
		result = C_NOMATCH;

	if (result == C_SYSERR) shout("Strange SYSERR\n");
	return result;
}

/* BOTTOMLEVEL_WRITE_DB: given a key, some data in a structure, and */
 /* xdr function */ 
 /* and a database id, use the xdr function to encode the data and */
 /* store it in the required database with the right key. */
/* This routine is called by write_db.  The reason it is not called */
 /* directly is the the UPDATE_KEY for the database needs to be */
 /* changed as well.  The simplest way of doing this involves calls to */
 /* BOTTOMLEVEL_WRITE_DB */
static book_changeres bottomlevel_write_db(item key, void * data, xdrproc_t xdr_fn,
					   db_number db)
{
	static book_changeres result;
	datum content;
	datum dkey;

	if (key.item_len == 0) {
		char buf[256];
		sprintf(buf, "PANIC: attempt to corrupt database %d by writting NULL key\n", db);
		bailout(buf,6);
	}

	if (!datum_encode(&content, data, xdr_fn))
		return C_XDR;
	dkey.dptr = key.item_val;
	dkey.dsize = key.item_len;

	/* round the users and bookings databases up to 512 byte boundary. */
	/* This will reduce the amount of freed data in the database */

	if (db == BOOKINGS)
	{
		/* find out how much space the key and data will take up. */
		/* They are both stored together.  Round this up to a 512 byte */
		/* boundary  */
		int total = (((content.dsize + dkey.dsize) / 512 ) +1 ) * 512;

		/* this is the amount for the data */
		content.dsize = total - dkey.dsize;
	}
    
	if (gdbm_store(db_file[db],dkey,content,GDBM_REPLACE) == 0)
		result = C_OK;
	else
		result = C_GDBM;
    
	return result;
}

/* UPDATE_STATUS_KEY: this routine updates the status of the database. */
 /* The fields that can change are the update_time and the */
 /* reorganise_time */
book_changeres update_status_key(db_number db, int update_time, int reorganise_time)
{

	static db_updaterec db_update_status;
	book_changeres result ;
	item key;
	key.item_val = STATUS_KEY;
	key.item_len = strlen(STATUS_KEY);

	/* it doesn't matter  if the key is here or not */
	read_db(key, &db_update_status, sizeof(db_updaterec),
		(xdrproc_t)xdr_db_updaterec, db);
	if (update_time)
		db_update_status.update_time = update_time;

	if (reorganise_time)
		db_update_status.reorganise_time = reorganise_time;

	result = bottomlevel_write_db(key, &db_update_status,
				      (xdrproc_t)xdr_db_updaterec, db);
	return result;
    
}

void get_status_key(db_number db, time_t *update_time, time_t *reorganise_time)
{

	static db_updaterec db_update_status;
	item key;
	key.item_val = STATUS_KEY;
	key.item_len = strlen(STATUS_KEY);

	/* it doesn't matter  if the key is here or not */
	read_db(key, &db_update_status, sizeof(db_updaterec),
		(xdrproc_t)xdr_db_updaterec, db);
	if (update_time)
		*update_time = db_update_status.update_time;

	if (reorganise_time)
		*reorganise_time = db_update_status.reorganise_time;
}

    
/* WRITE_DB: This is just a wrapper aroud BOTTOMLEVEL_WRITE_DB, to */
 /* update the status key as well */    
book_changeres write_db(item key, void * data, xdrproc_t xdr_fn, db_number db)
{
	book_changeres result ;
	if ( (result = bottomlevel_write_db(key, data, xdr_fn, db)) == C_OK)
		result = update_status_key(db, time(0), 0);
	return(result);
   
}

/* This routine reoganizes the database for us.  It has to return */
 /* something otherwise the rpc call times out. */

void * SVC(db_reorganize_2)(void * null , struct svc_req *rpstp)
{
	static char result;
	int i;
	extern GDBM_FILE log_file;
	char *p;

	/* send a reply here so the client doesn't time out and retry */
	svc_sendreply(rpstp->rq_xprt, (xdrproc_t)xdr_char, &result);

	for (i=0 ; i<= MAXDB;i++)
	{
		unlink(p=make_tmp_pathname(DatabaseName[i]));
		free(p);
		gdbm_reorganize(db_file[i]);
		update_status_key(i, 0, time(0));
	}

	unlink(p=make_tmp_pathname(LogFile));
	free(p);
	gdbm_reorganize(log_file);
	/* return nothing, we have already sent back a reply as the */
	/* operation takes some time */
	return(NULL);
    
}

book_changeres delete_from_db(item key, db_number db)
{
	datum dkey;
	dkey.dptr = key.item_val;
	dkey.dsize = key.item_len;
	return((gdbm_delete(db_file[db],dkey) ==0) ? C_OK : C_GDBM);
}
