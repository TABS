#include	"db_header.h"

extern book_changeres read_db();

/* this database maps
   workstationid -> workstation_state (description, disabled?, host)
   description -> int (number of workstations with that description)
   hostgroup ->hostid[], labid[]   (hosts in a hostgroup - table passing set, labs in a hostgroup)
   hostid -> hostinfo  (workstationlist + hostgroup)
   labid -> hostgroup, workstationlist (which hostgroup maintains this lab, and which workstations are in it)

 */
typedef enum change_direction {
    WS_DEL = 1,
    WS_ADD =2
} change_direction;


/* HOST_WS: add/deletes a workstation from a host or lab */
static book_changeres move_ws(config_type whichmap,
			      entityid ent, entityid wsid,
			      change_direction dir)
{
    book_changeres result;
    static hostinfo hinfo;
    char key[20];
    item k;
    bool_t bresult;

    if (tracing)
	printf("move_ws(map %d host %d, ws %d, dir %d)\n", whichmap, ent, wsid, dir);
    
    /* read the host entry */
    if (ent == -1 )
	return C_OK;
    
    result = read_db(k = key_int(key, whichmap, ent),
		     &hinfo, sizeof(hostinfo), (xdrproc_t)xdr_hostinfo,
		     CONFIG);

    if (result == C_NOMATCH)
    {
	hinfo.clump= -1;
	hinfo.workstations.entitylist_len=0;
	hinfo.workstations.entitylist_val=NULL;
    }
    else if (result != C_OK)
	return result;

    /* add/del the workstation */
    if (dir == WS_ADD)
	bresult = add_intarray((int_array*)&hinfo.workstations, wsid);
    else 
	bresult = del_intarray((int_array*)&hinfo.workstations, wsid);

    if (!bresult)
	return C_MISMATCH;

    if (tracing)
    {
	int i;
	char *sep;
	sep = "";
	printf("workstation array: ");
	for (i=0; i<hinfo.workstations.entitylist_len; i++)
	{
	    printf("%s #%d", sep, hinfo.workstations.entitylist_val[i]);
	    sep=", ";
	}
	printf("\n");
    }

    
    /* write the host entry */
    result = write_db(k, &hinfo, (xdrproc_t)xdr_hostinfo, CONFIG);
    return(result);
    
}


static book_changeres change_hostgroup(config_type where, int oldgroup, int newgroup, hostid host)
{
    book_changeres result;
    static hostgroup_info hg;
    item k;
    char key[20];

    if (tracing)
	printf("old %d, new %d, host %d\n",oldgroup, newgroup, host);

    result = C_OK;
    if (oldgroup != newgroup) 
    {
	if (oldgroup != -1)
	{
	    /* remove from old hostlist */

	    result = read_db(k = key_int(key, C_HOSTGROUPS, oldgroup),
			     &hg, sizeof(hostgroup_info), (xdrproc_t)xdr_hostgroup_info,
			     CONFIG);
	    if (result == C_OK)
	    {
		/* remove from list */
		if (where == C_HOSTS)
		    del_intarray((int_array*)&hg.hosts, host);
		else
		    del_intarray((int_array*)&hg.labs, host);
	
		/* remove this hostgroup if it no longer has any
		 * hosts or labs associated with it
		 */
		if (hg.hosts.entitylist_len == 0 &&
		    hg.labs.entitylist_len == 0)
		    result = delete_from_db(k, CONFIG);
		else
		    result =write_db(k, &hg, (xdrproc_t)xdr_hostgroup_info, CONFIG);
	    }
	}

	if (result == C_OK && newgroup != -1)
	{
	    result = read_db(k = key_int(key, C_HOSTGROUPS, newgroup),
			     &hg, sizeof(hostgroup_info), (xdrproc_t)xdr_hostgroup_info,
			     CONFIG);
	    /* no matter if it not there */
	
	    /* add to list */
	    if (where == C_HOSTS)
		add_intarray((int_array*)&hg.hosts, host);
	    else
		add_intarray((int_array*)&hg.labs, host);
	
	    result =write_db(k, &hg, (xdrproc_t)xdr_hostgroup_info, CONFIG);
	}
    }
    return result;
}

static book_changeres change_set(description * set, int diff)
{
    /* We keep a list of the types of workstations (as defined by their state
book_change_u.     * or attributes (available, lab, etc)).
     * This list is used by the booking system to determine (for example)
     * the number of available or broken machines in a lab, etc.
     * If given set doesn't exist, then it is added to the setlist
     * Note that when a machine becomes broken, it is moved out of its old
     * set and into the broken set (for the laboratory).
     */
    static book_changeres result;
    static descpairlist setlist;
    item key;

    if (tracing)
	printf("Change set %d\n", diff);
    
    /* read list of sets */
    result = read_db(key = str_key(HOST_SETS),
		     &setlist, sizeof(descpairlist),
		     (xdrproc_t)xdr_descpairlist, CONFIG);
    /* no matter if fails!! */

    incdesc(&setlist, set, diff);
    
    /* write description */
    result = write_db(key, &setlist, (xdrproc_t)xdr_descpairlist, CONFIG);
    return(result);
}

book_changeres change_ws(workstation_ch * newws)
{
    book_changeres result, res1;
    static workstation_state wstate;
    workstation_state wstate2;
    char key[20];
    item k;
    entityid newlab, oldlab;
    int hostchanged, labchanged;
    
    /* look for existing workstation record */
    result = read_db(k = key_int(key, C_WORKSTATIONS, newws->ws),
		     &wstate, sizeof(wstate),
		     (xdrproc_t)xdr_workstation_state, CONFIG); 

    if (result == C_OK)
    {
	hostchanged = (wstate.host != newws->state.host);
	oldlab = desc_2_labind(&wstate.state);
    }
    else
    {
	hostchanged = 0;
	oldlab = -1;
    }
    newlab = desc_2_labind(&newws->state.state);
    labchanged = (oldlab != newlab);
    
    if (newlab == -1)
    {
	/* we are removing the workstation record */
	if (result == C_OK)
	{
	    /* workstation exists in db */

	    /* remove workstation type from set of available workstations */
	    if ((result = change_set(&wstate.state, -1)) == C_OK)
	    {
		/* remove the workstation from the old host */
		move_ws(C_HOSTS, wstate.host, newws->ws, WS_DEL);
		/* remove workstation from the oldlab */
		move_ws(C_LABS, oldlab, newws->ws, WS_DEL);
		/* remove the workstation record */
		result = delete_from_db(k, CONFIG);
	    }
	}
    }
    else
    {
	if (newws->state.host != -1 && (hostchanged || labchanged))
	{
	    static hostinfo hoststate, labstate;
	    char key1[20];

	    /* check that hostgroup(lab(ws)) == hostgroup(host(ws) */
	    
	    if ((res1 = read_db(key_int(key1, C_HOSTS, newws->state.host),
				&hoststate, sizeof(hoststate),
				(xdrproc_t)xdr_hostinfo, CONFIG)) == C_OK)
	    {
		if ((res1 = read_db(key_int(key1, C_LABS, newlab),
				    &labstate, sizeof(labstate),
				    (xdrproc_t)xdr_hostinfo, CONFIG)) == C_OK)
		{
		    if (hoststate.clump != labstate.clump)
		    {
			if (tracing)
			    printf("hostgroup(lab(ws:%d)) != hostgroup(host(ws:%d))\n", newws->ws, newws->ws);
			res1 = C_BAD;
		    }
		}
		else if (tracing) printf("Could not find lab(ws:%d)\n", newws->ws);
	    }
	    else if (tracing) printf("Could not find host(ws:%d)\n", newws->ws);
	}
	else
	{
	    /* precondition: hostgroup(lab(ws)) == hostgroup(host(ws) */
	    res1 = C_OK;
	}

	if (res1 != C_OK)
	    result = res1;
	else
	{
	    if (result == C_OK)
		/* remove workstation type from set of available workstations */
		res1 = change_set(&wstate.state, -1);

	    if (hostchanged)
		/* remove the workstation from the old host */
		move_ws(C_HOSTS, wstate.host, newws->ws, WS_DEL);

	    if (labchanged)
	    {
		/* remove the workstation from the oldlab */
		move_ws(C_LABS, oldlab, newws->ws, WS_DEL);
		/* add the workstation to the new lab */
		move_ws(C_LABS, newlab, newws->ws, WS_ADD);
	    }

	    if (result == C_NOMATCH || hostchanged)
		/* if the workstation is new or the host has changed
		 * then add the workstation to the new host */
		move_ws(C_HOSTS, newws->state.host, newws->ws, WS_ADD);

	    /* we are changing/rewriting the workstation record */
	    wstate2.why = newws->state.why;
	    wstate2.time = newws->state.time;
	    wstate2.host = newws->state.host;
	    wstate2.state = newws->state.state;

	    result = write_db(k, &wstate2, (xdrproc_t)xdr_workstation_state, CONFIG);
	    if (result == C_OK)
		/* add workstation type to set of available workstations */
		result = change_set(&newws->state.state, 1);
	}
    }
    return result;
}

book_changeres change_hostlab(host_ch * newhl, int which)
{
    book_changeres result;
    static hostinfo linfo;
    char key[20];
    item k;

    memset(&linfo, 0, sizeof(linfo));
    result = read_db(k = key_int(key, which, newhl->entity),
		     &linfo, sizeof(linfo),
		     (xdrproc_t)xdr_hostinfo, CONFIG);

    if (result == C_NOMATCH)
	linfo.clump = -1;

    if (linfo.clump != newhl->clump)
    {
#if 0 /* must make sure errors are reflected back before enforcingthis  */
	if (linfo.workstations.entitylist_len != 0)
	    result = C_BAD; /* lab not empty */
	else
#endif
	    result = change_hostgroup(which, linfo.clump,
				      newhl->clump, newhl->entity);
    }
    if (result == C_OK)
    {
	if (newhl->clump == -1 && linfo.workstations.entitylist_len == 0)
	    result = delete_from_db(k, CONFIG);
	else
	{
	    linfo.clump = newhl->clump;
	    result = write_db(k, &linfo, (xdrproc_t)xdr_hostinfo, CONFIG);
	}
    }
    return result;
}

book_changeres change_lab(host_ch *newlab)
{
    return change_hostlab(newlab, C_LABS);
}
book_changeres change_host(host_ch *newhst)
{
    return change_hostlab(newhst, C_HOSTS);
}
