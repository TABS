/* the tokens database */
#include	"db_header.h"
#include	<stdio.h>

book_changeres add_token(tok_req * addtok)
{
    static book_changeres result;
    static token tok;
    char key[20];
    item k;
    
    result = read_db(k = key_int(key, C_TOKENS, addtok->tnum),
		     &tok,sizeof(tok),
		     (xdrproc_t)xdr_token, CONFIG);
    if ((result != C_OK) && (result != C_NOMATCH))
	return result;

    if(tracing)
	printf(">read ok!!\n");

    if (addtok->tok.advance.item_len == 0
	&& addtok->tok.late.item_len == 0)
    {
	/* delete token */
	if (result == C_OK)
	{
	    delete_from_db(k, CONFIG);
	    result = set_allotment(0, addtok->tnum, 0);
	}
	else result = C_OK;
    }
    else
    {
	/* now save the result */
	result = write_db(k, &addtok->tok, (xdrproc_t)xdr_token, CONFIG); 

	if (result == C_OK)
	    result = set_allotment(0, addtok->tnum, 1);
    }
    return result;
}
