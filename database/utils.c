/* some routines shared across modules */

#include	"db_header.h"
#include	<netdb.h>
#include	<sys/file.h>
#include	<sys/stat.h>
#ifdef sun
#include	<sys/fcntl.h>
#endif

item slotbits2key(int doy, int pod, description *desc)
{
    item key;
    key.item_len = 2+desc->item_len;
    key.item_val = (char*)malloc(key.item_len);
    key.item_val[0] = doy;
    key.item_val[1] = pod;
    memcpy(key.item_val+2, desc->item_val, desc->item_len);
    return key;
}

item slot2key(slotgroup *slot)
{
    return slotbits2key(slot->doy, slot->pod, &slot->what);
}

item bookkey2key(char *ky, book_key k)
{
    /* convert this to network byte order to keep the database happy */
    /* maybe not!!! */
    int key = htonl(k);
    item retval;
    retval.item_val = ky;
    bcopy(&key, retval.item_val, sizeof(book_key));
    retval.item_len = sizeof(book_key);
    return retval;
}

void book_key_bits(book_key *bkey, int *doy, int *pod, int *lab)
{
    int key = *bkey;
    if (pod) *pod = (key >>17) & 255;
    if (doy) *doy = (key >>8) & 511;
    if (lab) *lab = (key) & 255;
}

book_key make_bookkey(int doy, int pod, int lab)
{
    return  (pod<<17) | (doy <<8) | lab;
}

