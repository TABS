#include	"db_header.h"

/* the user database.
   key: user_id, value: [bookings] [tokens] 
   */
/* The list of bookings contains all bookings the user has made.
   The list of tokens contains all tokens the user has consumed.
   */
/* the operations required on this structure,
   add booking
   del booking
   add user ?  maybe status instead!!!
   del user ?
*/

static book_changeres add_to_userbooking(userbookinglist * ch, book_key *slot, int when)
{
	userbookinglist new_el;

	/* maybe check if present */

	new_el = (userbookinglist) malloc (sizeof(userbookingnode));

	new_el->slot = *slot;
	new_el -> when = when;
	new_el -> next = *ch;
	*ch  = new_el;
	return C_OK;
}

static book_changeres del_from_userbooking(userbookinglist * ch, book_key *key,
				    int when)
{
	userbookinglist ptr = *ch;
	userbookinglist trailer = ptr;  /* this points to the prev entry in the */
	/* list to do deletions, easier than */
	/* doubly linked list */
	book_changeres result = C_NOMATCH;


	while (ptr != NULL) {
		if (ptr->slot == *key
		    && ptr->when == when)
		{	    
			/* delete !! */
			if (ptr == trailer) {  /* first node */
				if (ptr -> next == NULL) /* only one node in list*/
					*ch = NULL;
				else
					*ch = ptr -> next;
			}
			else
				trailer->next = ptr-> next;
			/* cut the node off */
			ptr -> next = NULL;
			xdr_free((xdrproc_t)xdr_userbookinglist, (char*) &ptr);  	    
			result = C_OK;
		}
		else { /* go to the next element in the list */
			trailer = ptr;
			ptr = ptr->next;
		}
	}
	return(result);
}

static userbookinglist finduserbook(userbookinglist *ch, book_key *key, int when)
{

	userbookinglist ptr = *ch;
	while ((ptr != NULL) &&
	       !(ptr->slot == *key
		 && ptr->when == when)
		)
		ptr = ptr->next;

	return(ptr);
}


/* if the key is present in the list inc it's value, else add it to */
 /* the list with a value of 1 operation should always suceed*/
static book_changeres incuserbook(userbookinglist *ch, slotgroup * key, int when)
{
	userbookinglist val;
	book_key k;
	slot2bookkey(key, &k);
	if ((val = finduserbook(ch,&k,when)) == NULL) /* add new element to list */
		return(add_to_userbooking(ch,&k,when));
	else
		return(C_OK); /* FIXME ? fail? */
}


/* ADD_USER_BOOKING: this operation assumes that all validataion has been */
 /* performed for the user request of a booking.  Only required */
 /* datastructures to be updated */

book_changeres add_user_booking(book_req * booking, int clientreq)
{
	item k;
	static users u_rec;
	static book_changeres result;
	intpairlist ipl;
	char key[20];

	/* read in record for this user */
	result = read_db(k = user_key(key, booking->who_by),
			 &u_rec, sizeof(users), (xdrproc_t)xdr_users, BOOKINGS);

	if ((result != C_OK) && (result != C_NOMATCH))
		return result;

	/* check that user still has the number of tokens that we expected */
	if (booking->tok_cnt != -1 && clientreq)
	{
		extern intpairlist findintkey();
	
		/* actually do the check */
		intpairlist cp;
		cp = findintkey(u_rec.tokens, booking->tnum);
		if ((cp == NULL && booking->tok_cnt != 0)
		    || (cp != NULL && cp->num != booking->tok_cnt))
		{
			result = C_MISMATCH;
			return result;
		}
	}
    
	incintkey(&u_rec.tokens, booking -> tnum, booking->number);
	ipl = findintkey(u_rec.priv_tokens, booking->tnum);
	if (ipl)
	{
		ipl->num -= booking->number;
		if (ipl->num <= 0)
			del_from_intpairlist(&u_rec.priv_tokens, ipl->data);
	}
	if (booking->who_by != booking->who_for)
	{
		result = write_db(k, &u_rec, (xdrproc_t)xdr_users, BOOKINGS);
		k = user_key(key, booking->who_for);
    
		/* read in record for this user */
		result = read_db(k, &u_rec, sizeof(users), (xdrproc_t)xdr_users, BOOKINGS);
		if ((result != C_OK) && (result != C_NOMATCH))
			return result;
	}	
	/* now add booking to list */
	/* perhaps this should be sorted!!!! */
	result = incuserbook(&u_rec.bookings, &booking->slot, booking->when);

	if (result != C_OK)
		return result;
    
	/* now save the resultant record */
	result = write_db(k, &u_rec, (xdrproc_t)xdr_users, BOOKINGS);
	return result;
    
}

/* For each user,
 * move any bookings that are no-longer pending into pastbookings,
 * and update the appropriate token
 */

book_changeres change_user_booking(book_chng * bchanges, int *refund, int *tokens, int *who_by)
{
	static users u_rec;
	static book_changeres result;
	item k;
	char key[20];
	int i;

	result = C_OK;
	for (i =0 ;i < bchanges->chlist.book_ch_list_len;i++)
	{
		/* read in record for this user */
		k = user_key(key, bchanges->chlist.book_ch_list_val[i].user);
		result = read_db(k, &u_rec, sizeof(users), (xdrproc_t)xdr_users, BOOKINGS);
		if (result != C_OK)
			return result;

		/* first del booking from list */
		result = del_from_userbooking(&u_rec.bookings, &bchanges->slot,
					      bchanges->chlist.book_ch_list_val[i].when);
/*	printf("result of del = %d\n",result);  */

		if (result == C_OK)
		{
			result = add_to_userbooking(&u_rec.pastbookings, &bchanges->slot,
						    bchanges->chlist.book_ch_list_val[i].when);
		}
    
		if (refund[i] && bchanges->chlist.book_ch_list_val[i].user != who_by[i])
		{
			result = write_db(k, &u_rec, (xdrproc_t)xdr_users, BOOKINGS);
			if (result != C_OK)
				return result;

			k = user_key(key, who_by[i]);
	
			result = read_db(k, &u_rec, sizeof(users), (xdrproc_t)xdr_users, BOOKINGS);
			if (result != C_OK)
				return result;

		}
		if (refund[i])
			decintkey(&u_rec.tokens,
				  tokens[i] & ~PRIV_REFUND_FLAG, refund[i]);
		if (refund[i] && (tokens[i] & PRIV_REFUND_FLAG))
			incintkey(&u_rec.priv_tokens,
				  tokens[i] & ~PRIV_REFUND_FLAG, refund[i]);
		      

/* 	printf("result deckey %d... user %s\n",result,key); */
    
		/* now save the resultant record */
		result = write_db(k, &u_rec, (xdrproc_t)xdr_users, BOOKINGS);
		if (result != C_OK)
			return result;
	}
    
	return result;
    
}


/* DEL_USER_BOOKING: deletes a booking from the user list */
book_changeres del_user_booking(bookuid_t user, int when, book_key *bk)
{
	static users u_rec;
	static book_changeres result;
	item k;
	char key[20];

	k = user_key(key, user);

    
	/* read in record for this user */
	result = read_db(k, &u_rec, sizeof(users), (xdrproc_t)xdr_users, BOOKINGS);
	if (result != C_OK)
		return result;

    
	/* first del booking from list,  look at past bookings first */
	result = del_from_userbooking(&u_rec.pastbookings, bk, when);

	/* is this a good idea?? */
	if (result != C_OK) 
		result = del_from_userbooking(&u_rec.bookings, bk, when);
    
	if (result != C_OK)
		return result;

	/* now save the resultant record */
	result = write_db(k, &u_rec, (xdrproc_t)xdr_users, BOOKINGS);
	return result;
}

/* DEL_USER : deletes a user record providing no bookings are attached */
book_changeres del_user(bookuid_t user)
{
	static users u_rec;
	static book_changeres result;
	item k;
	char key[20];

	k = user_key(key, user);
	result = read_db(k, &u_rec, sizeof(users),
			 (xdrproc_t)xdr_users, BOOKINGS);
	if (result != C_OK) return result;
    
	if (u_rec.pastbookings || u_rec.bookings)
	{
		/* there are bookings... just ignore req */
		return C_MISMATCH;
	}
	result = delete_from_db(k, BOOKINGS);
	return result;
}
