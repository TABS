#include	"db_header.h"


item user_key(char *key, int value)
{
    item rv;
    sprintf(key, "%d", value);
    rv.item_val = key;
    rv.item_len = strlen(key);
    return rv;
}

item key_int(char *key, config_type type, int num)
{
    item rv;
    sprintf(key, "%d_%d", type, num);
    rv.item_val = key;
    rv.item_len = strlen(key);
    return rv;
}

item key_string(config_type type, char *str)
{
    item rv;
    char *p;
    rv.item_val = (char*)malloc(6+strlen(str)+1);
    sprintf(rv.item_val, "%d_%s", (int)type, str);
    for (p= rv.item_val ; *p ; p++)
	if (*p >= 'A' && *p <= 'Z')
	    *p += 'a' - 'A';
    rv.item_len = strlen(rv.item_val);
    return rv;
}

    
item str_key(char * str)
{
    item rv;
    rv.item_val = str;
    rv.item_len = strlen(str);
    return rv;
}
