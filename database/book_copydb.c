/* Copies a number of databases from the server supplied on the */
 /* command line */

#include	"db_header.h"
#include	<stdio.h>
#include	<unistd.h>
#ifndef apollo
#include	<netinet/in.h>
#endif
#include	<sys/socket.h>
#include	<netdb.h>
#include	<sys/stat.h>

char errbuff[128];

int rread(int fd, void *buf, int len)
{
	int have = 0;

	while (have < len)
	{
		int n = read(fd, buf+have, len-have);
		if (n > 0)
			have += n;
		else if (n == 0)
			len = have;
		else
			return n;
	}
	return have;
}

/* given the name of a master server and a port number that the master */
 /* is listening on to send a copy  of the database */
int receive_db(char * supplier,short port)
{
	int sock;
	int datasize;
	struct sockaddr_in sin;
	char *buf;
	struct hostent *he;
	int buflen = 1024;
	char keybuf[1024];
	GDBM_FILE newdb= NULL;
	datum key, content;
	char * pathname;


	char *files[20];
	int filecnt = 0;
    
	/* later check that port is privileged FIXME */
	if (port == 0)
		return 1;

	sock =  socket(AF_INET, SOCK_STREAM, 0);
    
	if (sock == -1)
		return 1;

	he = gethostbyname(supplier);
	if (he == NULL) {
		sprintf(errbuff,"Could not get hostname for %s\n",supplier);
		close(sock);
		bailout(errbuff, 10);
	}

	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	bcopy(he->h_addr_list[0], &sin.sin_addr, he->h_length);
	if (connect (sock, (struct sockaddr*)&sin, sizeof(sin)) == -1) {
		sprintf(errbuff,"Could not connect to %s, reason....\n",supplier);
		close(sock);
		bailout(errbuff,11);
	}


	/* set up initial buffer */
	buf = (char *)calloc(buflen, sizeof(char));

	while (1)
	{
		if (rread(sock, &datasize, sizeof(datasize)) != sizeof(datasize))
		{
			datasize = 1;
			printf("datasize read error\n");
			break;
		}
	
		datasize = ntohl(datasize);
		if (datasize == 0)	/* finish signal */
			break;
	
		if (datasize < 0)
		{			/* new database */
			int opt;
			if (newdb != NULL)
				gdbm_close(newdb);
	    
			datasize = -datasize;
	
			if (rread(sock, keybuf, datasize) != datasize)
				break;
			keybuf[datasize] = '\0';
			files[filecnt++] = strdup(keybuf);
			strcat(keybuf, ".new");
			pathname = make_pathname(keybuf);
			newdb = gdbm_open(pathname, 1024, GDBM_NEWDB, 0600,0);
			if (newdb == NULL ) {
				printf("error opening file %s\n",pathname);
				break;
			}
			/* no need to sync */
			opt = 1;
			gdbm_setopt(newdb, GDBM_FASTMODE, &opt, sizeof(opt));
		}
		else
		{
			key.dsize = datasize;
			if (rread (sock, keybuf, datasize) != datasize)
			{
				printf("Error reading key\n");
				break;
			}
	
			key.dptr = keybuf;
			/*
			  printf("got :");
			  print_key(&key);
			*/
			/* now for the data */
	
			if (rread(sock, &datasize, sizeof(datasize)) != sizeof(datasize)) {
				printf("datasize read error\n");
				datasize = 1;
				break;
			}
			/*
			  printf("datasize = %d\n",datasize);
			*/
			datasize = ntohl(datasize);
	
			if (datasize == 0)  /* finish signal */
				break;
			content.dsize = datasize;
	
			if (datasize > buflen) {
				free(buf);
				buflen = datasize + 100;
				buf = (char *)calloc(buflen, sizeof(char));
			}
			datasize = 0;
			while (datasize  < content.dsize ) {
				int n = rread(sock, buf+ datasize, content.dsize - datasize);
				if (n <= 0)
				{
					datasize = 1;
					break;
				}
				datasize += n;
			}

			if (datasize < content.dsize) {
				printf("Data read bad, only %d of %d\n", datasize, content.dsize);
				datasize = 1;
				break;
			}
			content.dptr = buf;
	
			if (newdb == NULL) {
				printf("Protocol error... no file opened\n");
				break;
			}
			/* store it away, remember key must be unique*/
			if (gdbm_store(newdb,key,content, GDBM_INSERT) != 0) 
			{
				printf("store failed: %d\n",gdbm_errno);
				break;
			}
		}
	}
	close (sock);
	if (newdb != NULL)
		gdbm_close(newdb);

	while (filecnt--)
	{
		char *preal = make_pathname(files[filecnt]);
		char *pnew;

		strcpy(keybuf, files[filecnt]);
		strcat(keybuf, ".new");
		pnew = make_pathname(keybuf);
	
	    
		if (datasize != 0)
			unlink(pnew);
		else
		{
			struct stat stbuf;
			if (stat(preal, &stbuf)==0)
			{
				printf("book_copydb: %s already exists!!\n", preal);
				datasize = 1;
				unlink(pnew);
			}
			else
				rename(pnew, preal);
		}
	}
	if (datasize == 0)
		return 0;
	else
	{
		printf("book_copydb: copying of database failed\n");
		return 1;
	}
}

int main (int argc, char *argv[])
{
	CLIENT * supplier_cl;
	transfer_reply * result;
	char *supplier ;
	int arg;


	if (argc != 2) {
		printf("usage: %s server\n",argv[0]);
		exit(3);
	}

	supplier = argv[1];

	supplier_cl = clnt_create(supplier,BOOK_DATABASE,BOOKVERS,"udp");
	if (supplier_cl == NULL) {
		/* no connection to server */
		clnt_pcreateerror(supplier);
		exit(1);
	}

	result = transfer_db_2(&arg,supplier_cl);
	if (result == NULL) {
		printf("error contacting server %s\n",supplier);
		exit(1);
	}
	if (result -> error != R_RESOK) {
		printf("error with server: %d\n",result-> error);
		exit(1);
	}
	/* now get the copy */
	exit(receive_db(supplier,result->port));
}

