#include	"db_header.h"

/* these routines are to un XDR structures.  There are two forms */
 /* provided, a plain for a gdbm data structure, and an opaque for */
 /* data delivered across the network */

bool_t datum_decode (datum * content, void * data, xdrproc_t xdr_fn )
{
	XDR xdrs;
	xdrmem_create(&xdrs, content->dptr,content->dsize, XDR_DECODE);
	return (xdr_fn(&xdrs, data)) ;
}

bool_t item_decode (item * val, void *data, xdrproc_t xdr_fn)
{
	XDR xdrs;
    
	xdrmem_create(&xdrs, val->item_val,val->item_len, XDR_DECODE);
	return (xdr_fn(&xdrs, data));
}

static char *buf = NULL;
static int bufsize = 0;

bool_t item_encode(item * val, void * data, xdrproc_t xdr_fn)
{
	XDR xdrs;
	if (buf == NULL)
	{
		buf = malloc(bufsize = 16384);
	}
	xdrmem_create(&xdrs, buf, bufsize-1024, XDR_ENCODE);
	while (!xdr_fn(&xdrs, data))
	{
		free(buf);
		buf = malloc(bufsize += 8192);
		xdrmem_create(&xdrs, buf, bufsize-1024, XDR_ENCODE);
	}
    
	val->item_val = buf;
	val->item_len = XDR_GETPOS(&xdrs);
	return TRUE;
}

bool_t datum_encode(datum * val, void * data, xdrproc_t xdr_fn)
{
	XDR xdrs;
	if (buf == NULL)
	{
		buf = malloc(bufsize = 16384);
	}
	xdrmem_create(&xdrs, buf, bufsize, XDR_ENCODE);
	while (!xdr_fn(&xdrs, data))
	{
		free(buf);
		buf = malloc(bufsize += 8192);
		xdrmem_create(&xdrs, buf, bufsize, XDR_ENCODE);
	}
    
	val->dptr = buf;
	val->dsize = XDR_GETPOS(&xdrs);
	return TRUE;
}
