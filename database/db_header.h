/* standard include files for all modules associated with the database */
 /* manager  */
#include	<rpc/rpc.h>
#include	<string.h>
#include	<gdbm.h>
#include	"../database/database.h"

#include	"../lib/misc.h"
#ifdef linux
#define SVC(x) x ## _svc
#else
#define SVC(x) x
#endif

/*  this is  the directory containing the database files */


/* some special keys for the database */
#define DOMAIN_KEY "_DOMAIN_"  /* the domain of the database.. not */
			       /* always used */
#define STATUS_KEY "_STATUS_" /* the status of the database.. the */
			      /* update time, reorg time */
#define ATTR_TYPE_KEY "_ATTR_TYPE_1_"
#define HOST_SETS "_HOSTS_SETS_" /* key of the sets in the hosts */
				  /* database */
#define LogFile "db_log"

/* some stuff for bitstrings */
#define BITSTRLEN 16  /* size in characters */
#define BITINDMAX BITSTRLEN * sizeof(char)  *8  /* size in bits */

#define MAXCHANGES 20  /* the maximum number of changes to transfer in */
		       /* one go */

#define PRIV_REFUND_FLAG 0x8000

#ifdef _AM_MAIN_
#include	"db_globals.h"
#else
#include	"db_extern.h"
#endif

#include	<stdio.h>

typedef struct CLIENTHOST {
    CLIENT * client;
    char * host;
} CLIENTHOST;

#define Malloc(ptr,type) ptr=(type *)malloc(sizeof(type));if (ptr == NULL) {printf("no memory\n"); exit(33); }



#include	"common.h"

/* from descpairlist.c */
void add_to_descpairlist(descpairlist * ch, description * new, int num);
book_changeres del_from_descpairlist(descpairlist * ch, description *name);
descpairlist finddesc(descpairlist *ch, description * key);
void incdesc(descpairlist *ch, description * key, int diff);

/* from db_utils.c */
query_reply * generic_query(query_req * query);
int db_open(db_number db, int gdbm_mode);
void close_db(db_number db);
book_changeres read_db(item key, void * data, int data_size, xdrproc_t xdr_fn,  db_number db );
book_changeres update_status_key(db_number db, int update_time, int reorganise_time);
void get_status_key(db_number db, time_t *update_time, time_t *reorganise_time);
book_changeres write_db(item key, void * data, xdrproc_t xdr_fn, db_number db);
book_changeres delete_from_db(item key, db_number db);

/* from config_db.c */
book_changeres change_implication(impls * impl);
void init_attr_types();
book_changeres change_attr_type(attr_type_desc *attrs);
book_changeres change_configdata(configdata * configent);


/* from db_change.c */
void get_replica_header(char *host);
void sync_replica_header();
int change_init(void);
void close_change(void);
book_changeres commit(book_changeent *ch);
void free_changes(change_reply reply);
void change_del_changes(char *replica, int highest);

/* from replica_db.c */
int replica_grow(machine_name machine, replica_no height);
book_changeres replica_add(machine_name server);
book_changeres replica_del(machine_name server);
int replica_init();

/* from db_transfer */
void collect_xfer(void);
void send_database(int sock, db_number db);

/* from users_db.h */
book_changeres add_user_booking(book_req * booking, int clientreq);
book_changeres change_user_booking(book_chng * bchanges, int *refund, int *tokens, int *who_by);
book_changeres del_user_booking(bookuid_t user, int when, book_key *bk);
book_changeres del_user(bookuid_t user);

/* from bookings_db.c */
book_changeres add_booking_to_list(bookhead * blist, book_req *req);
book_changeres incuser(bookhead *bh, book_req *req);
book_changeres read_bookhead(bookhead *bh, book_key *key);
book_changeres write_bookhead(bookhead *bh, book_key *key);
book_changeres add_booking(book_req * booking);
int inc_usage_count(slotgroup *slot, int number);
book_changeres change_booking(book_chng *bchanges, int *refund, int *tokens, int *who_by);
booklist map_bookings(bookuid_t user, userbookinglist list);
book_changeres remove_booking(book_key * removal);

/* from classes_db.c */
book_changeres add_class(class_req * cl_req);
book_changeres set_allotment(int uid, int tok, int cnt);


/* from freeslots.c */
utilizationlist *free_slots(slotlist sl);

/* from main.c */
bool_t open_dbs(int gdbm_stat);
void close_dbs(void);
void init_db(void);
void close_all(void);
void start_helper(void);
void kill_helper(void);
SIGRTN die();
void check_update_status(void);
