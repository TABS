/* some routines for adding and deleting from lists of strings and numbers */

#include	"db_header.h"

static void add_to_utilizationlist(utilizationlist * ch, description * new,
				   int free, int total)
{
    utilizationlist new_el;

    /* maybe check if present */

    new_el = (utilizationlist) malloc (sizeof(utilizationnode));

    desc_cpy(&new_el -> machine_set, new);
    new_el -> free = free;
    new_el -> total = total;
    new_el -> next = *ch;
    *ch  = new_el;
}

utilizationlist *free_slots(slotlist sl)
{
    
    book_changeres state;
    static utilizationlist result;
    static descpairlist setlist;
    descpairlist descptr;
    item key;
    extern descpairlist finddesc();

    /* clear the result*/
    xdr_free((xdrproc_t)xdr_utilizationlist, (char*) &result);
    memset(&result, 0, sizeof(utilizationlist));

    /* get the list of descriptions and counts for each type of */
    /* machine */

    key = str_key(HOST_SETS);
    state = read_db(key, &setlist, sizeof(descpairlist),
		    (xdrproc_t)xdr_descpairlist, CONFIG);

    
    /* for each element in the list:
       - get number of machines
       - subtract number of bookings
       */

    for ( ; sl ; sl=sl->next)
    {
	for (descptr = setlist ; descptr != NULL; descptr = descptr->next)
	{
	    int bookslots;

	    /* make sure the description is for available machines */
	    if (!query_bit(&descptr->data, attr_types.available))
		continue;

	    if (!desc_member(&sl->data->what, &descptr->data))
		continue;
	
	    /* read the bookings db */
	    key = slotbits2key(sl->data->doy, sl->data->pod, &descptr->data);
	    state = read_db(key, &bookslots,
			    sizeof(bookslots), (xdrproc_t)xdr_int, BOOKINGS);
	    if (state != C_OK)
		bookslots = 0;
	    free(key.item_val);
	
/* Read the actual list of bookings to, because we seem to have some
corruption... neilb 12mar2002
This is unfinished .. the corruption disappeared...
*/
#if 0
if (0)
	    {	
		    int lab = desc_2_labind(&descptr->data);
		    book_key bk = make_bookkey(sl->data->doy, sl->data->pod, lab);
		    static bookhead bh;
		    int nbs;
		    book_changeres res;
		    res = read_bookhead(&bh, &bk);
		    if (res != C_OK)
			    nbs = 0;
		    else {
			    booklist bptr = bh.data;
			    
			    /* should we count?? */
		    }
	    }
#endif
	    
	    add_to_utilizationlist(&result, &descptr->data,
				   descptr->num - bookslots, descptr->num);
	}
    }
    return(&result);
}
