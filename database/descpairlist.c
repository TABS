/* some routines for adding and deleting from lists of strings and numbers */

#include	"db_header.h"

void add_to_descpairlist(descpairlist * ch, description * new, int num)
{
    descpairlist new_el;

    /* maybe check if present */

    new_el = (descpairlist) malloc (sizeof(descpairnode));

    desc_cpy(&new_el -> data, new);
    new_el -> num = num;
    new_el -> next = *ch;
    *ch  = new_el;
}

book_changeres del_from_descpairlist(descpairlist * ch, description *name)
{
    descpairlist ptr = *ch;
    descpairlist trailer = ptr;  /* this points to the prev entry in the */
			     /* list to do deletions, easier than */
			     /* doubly linked list */
    book_changeres result = C_NOMATCH;


    while (ptr != NULL) {
	if (( bcmp(ptr -> data.item_val, name->item_val, name->item_len) == 0)) {

	    /* delete !! */
	    if (ptr == trailer) {  /* first node */
		if (ptr -> next == NULL) /* only one node in list*/
		    *ch = NULL;
		else
		    *ch = ptr -> next;
	    }
	    else
		trailer->next = ptr-> next;
	    /* cut the node off */
	    ptr -> next = NULL;
	    xdr_free((xdrproc_t)xdr_descpairlist, (char*) &ptr);  	    
	    result = C_OK;
	}
	else { /* go to the next element in the list */
	    trailer = ptr;
	    ptr = ptr->next;
	}
    }
    return(result);
}

descpairlist finddesc(descpairlist *ch, description * key)
{

    descpairlist ptr = *ch;
    while ((ptr != NULL) && (bcmp(ptr->data.item_val,key->item_val,key->item_len) != 0) ) {
	ptr = ptr->next;
    }
    return(ptr);
}

/* if the key is present in the list inc it's value, else add it to */
 /* the list with a value of 1 operation should always suceed*/
void incdesc(descpairlist *ch, description * key, int diff)
{
    descpairlist val;
    if ((val = finddesc(ch,key)) == NULL) /* add new element to list */
	add_to_descpairlist(ch,key,diff);
    else
    {
	val->num += diff;
	if (val->num == 0)
	    del_from_descpairlist(ch,key);
    }
}



#ifdef WANTED
merge_lists(descpairlist * tot, descpairlist * others)
{
    descpairlist ptr = *others;

    while (ptr != NULL)
    {
RUBBISH	inckey(tot, strdup(ptr->data), ptr->num);
	ptr = ptr ->next;
    }

}

del_list(descpairlist * tot, descpairlist * subs)
{
    descpairlist ptr = *subs;
    while (ptr != NULL) {
RUBBISH	deckey(tot, strdup(ptr->data), ptr->num);
	ptr = ptr->next;
    }
}
#endif





