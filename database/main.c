
/* The main module for the booking system database manager */
#define _AM_MAIN_
#include	<sys/types.h>
#include	<unistd.h>
#include	<time.h>
#include	"db_header.h"
#include	<stdio.h>
#include	<signal.h>
#include	<sys/wait.h>
#ifdef apollo
#include	<sys/statfs.h>
#elif defined (USE_STATVFS)
#include	<sys/statvfs.h>
#else
#include	<sys/param.h>
#include	<sys/mount.h>
#endif
#include	<ctype.h>
#include	<arpa/inet.h>
#include	<rpc/pmap_clnt.h>
#include	<getopt.h>
#ifdef SOLARIS2
#include <sys/time.h>
#include <sys/resource.h>
#endif

#ifdef NOTDEF
#define svc_run my_svc_run
#endif

#define CHECK_TIME (60 * 10) /* amount of time between checks on free */
			     /* disc space */ 
int helper_pid; /* the process id of the helper */
char * server;
update_status update_st;
time_t last_status_set = 0;
bool_t perform_stats = FALSE;

char *Usage[] = {
    "Usage: db_manager [-I] [-n] [-M database_maintainer] [-t]",
    "where:",
    "	-I	Initialise database",
    "	-n	Do not fork/exec database helper/maintainer",
    "	-M db	The database helper/maintainer to fork/exec",
    "	-t	enable tracing",
    NULL };

#ifdef NEED_TO_CHECK_DISK
/* CHECK_DISK: returns is enough disc space ( > 4000) blocks, else */
 /* returns false */
bool_t check_disk()
{
	static time_t last =0;
#ifdef apollo    
	struct statfs buf;
#elif defined(USE_STATVFS)
	struct statvfs buf;
#else    
	struct fs_data buf;
#endif    
	if (last + CHECK_TIME > time(0) )
		return TRUE;
	time(&last);

#ifdef apollo
	statfs(BookBase, &buf, sizeof(struct statfs), 0);
	printf("Blocks free %d, size %d\n", buf.f_bfree, buf.f_bsize);
    
	return ((buf.f_bfree * buf.f_bsize / 1024) < 4000) ? FALSE : TRUE;

#elif defined(USE_STATVFS)
	statvfs("/var", &buf);
/*    printf("Blocks free %d\n", buf.f_bfree); */
	return(buf.f_bfree <4000) ? FALSE : TRUE;
#else
	/* on decs check /var */
	statfs("/var", &buf);
	if (tracing)
		printf("Blocks free %d\n", buf.fd_req.bfree);
    
	return (buf.fd_req.bfree < 4000) ? FALSE : TRUE;
    
    
#endif
    
}
#endif

bool_t open_dbs(int gdbm_stat)
{
	int i;
	char buf[128];
	sprintf(buf,"Open stat %d pid %d\n", gdbm_stat, getpid());
	shout(buf);

	if (tracing) printf("OPEN\n");

	for (i =0; i <= MAXDB; i++)
		if (!db_open(i, gdbm_stat))
			return 0;

	init_attr_types();
	return 1;
}

void close_dbs(void)
{
	int i;
	char buf[128];
	sprintf(buf,"Close  pid %d\n",  getpid());
	shout(buf);

	if (tracing) printf("CLOSE\n");
    
	for (i=0;i <= MAXDB; i++)
		close_db(i);
}

void init_db(void)
{
	server = get_myhostname();
	if (server == NULL)
		bailout("cant get my hostname\n",7);
    
	/* set up the change database */
	if (change_init()
	    && open_dbs(GDBM_WRITER))
		update_st = COPY;
	else
		update_st = NONE;
	last_status_set = time(0);
}

void close_all(void)
{
	update_st = NONE;
	close_dbs();

	/* close the change database */
	close_change();
    
}

/* forks a helper process */
static char *helper_path = "/usr/local/etc/book_maint";
void start_helper(void)
{
	int i;
	static char *args[2];
	if ((helper_pid = fork()) == 0) {
		/* close all sockets opened by parent except standard ones */
		for (i = 3 ; i <20;i ++)
			close(i);
		/* have to do an exec.  can't just call the helper as another */
		/* procedure as both the client and server stubs would have to */
		/* be linked in.  both client and server stubs contain */
		/* different definitions for remote procedures. */
		args[0] = helper_path;
		args[1] = NULL;
		i = execv(helper_path, args);
		if (i != 0) {
			printf("exec failed!, status %d\n",i);
			exit(4);
		}
	}
}

/* send term to helper pid and wait for it to die */
void kill_helper(void)
{
    int pid;
    int st;
    
    if (helper_pid > 0) {
	kill(helper_pid, SIGTERM);
	while ((pid=wait(&st)) > 0 && pid != helper_pid)
	    ;
	helper_pid = 0;
    }
}

/* called when sigalrm is received.  ensures that database closed and */
 /* server dies.  the terminate routine sets timer to send sigalrm and */
 /* call this routine */
SIGRTN die()
{
    kill_helper();
    close_all();

    /* remove portmap's knowledge of this manager, so attempts to */
    /* access it by other manager's timeout quickly */
    svc_unregister(BOOK_DATABASE, BOOKVERS);
    
    exit(0);
}

void *SVC(terminate_2)(int *dummy, struct svc_req *rqstp)
{
    static char  res;
    if (ntohs(svc_getcaller(rqstp->rq_xprt)->sin_port) > 1024) {
	/* FIXME check host */
	return (&res);
    }
    update_st = NONE;

    svc_sendreply(rqstp->rq_xprt, (xdrproc_t)xdr_char, (void*)&res);
    die();
    return NULL;
}

void *SVC(set_updatest_2)(update_status * st, struct svc_req *rq)
{
    extern GDBM_FILE log_file;
    static char c;
    int i;
    int any=0;
    time_t now = time(0);

    last_status_set = now;

    /* possibly reorganise some databases */
    for (i=0 ; i<= MAXDB ; i++)
    {
	time_t chng, reorg;
	get_status_key(i, &chng, &reorg);
	if (chng > reorg
	    && reorg + 23*60*60 < now
	    && chng + 2*60*60 < now)
	{
	    any=1;
	    gdbm_reorganize(db_file[i]);
	    update_status_key(i, 0, time(0));
	}
    }
    if (any) gdbm_reorganize(log_file);

    if (*st == FULL && replica_grow(server, -1) < 0)
	*st = COPY; /* only replicas on list can be FULL */
    if (update_st == *st)
	return &c;
    
    /* close the db handles */
    close_dbs();
    update_st = *st;

    /* open them again with the right mode */
    if (update_st == NONE)
	open_dbs(GDBM_READER);
    else
	open_dbs(GDBM_WRCREAT);
    return (&c);
}

void check_update_status(void)
{
    time_t now;
    now = time(0);
    if (last_status_set + 5*60 < now && update_st == FULL)
	update_st = COPY;
}

update_status * SVC(get_updatest_2)(void *dummy, struct svc_req *rq)
{
    static update_status st;

    check_update_status();
    collect_xfer();
    st = update_st;
    return (&st);
    
}

/* calls the update routines for various databases */
book_changeres change_db (book_changeent *ch)
{
    static book_changeres result = C_OK;
    
    extern book_changeres replica_add();
    extern book_changeres replica_del();
    extern book_changeres add_booking();
    extern book_changeres add_user_booking();
    extern book_changeres change_booking();
    extern book_changeres claim_booking();
    extern book_changeres change_user_booking();
    extern book_changeres add_token();
    extern book_changeres add_class();
    extern book_changeres remove_booking();
    extern book_changeres change_implication();
    extern book_changeres change_attr_type();
    extern book_changeres set_namenum();
    extern book_changeres change_ws();
    extern book_changeres change_host();
    extern book_changeres change_lab();
    extern book_changeres change_configdata();
    extern book_changeres commit();


    int *refund;
    int *tokens;
    int *who_by;
    int l;
    
    /* check the change number from the server */
    if (ch->changenum != 0)
    {
	/* copied from elsewhere,
	 * make sure the right change number
	 * and not this machine
	 */
	int lastch;
	if (strcmp(ch->machine, server)==0)
	    return C_BAD;

	lastch = replica_grow(ch->machine, -1);
	if (lastch < 0 || lastch+1 != ch->changenum)
	    return C_BAD;
    }
    switch (ch ->thechange.chng)  {

    case ADD_SERVER:
        result = replica_add(ch->thechange.book_change_u.server);
	break;
    case DEL_SERVER:
        result = replica_del(ch->thechange.book_change_u.delserver);
	if (result == C_OK && strcmp(ch->thechange.book_change_u.delserver, server)==0)
	    update_st = COPY;
	break;
    case ADD_BOOKING:
	result = add_user_booking(&ch->thechange.book_change_u.booking,
				   ch->changenum==0);
	/* This should only fail with C_MISMATCH
	   if it fails for other reason...?? should clean up.  FIXME */
	if (result != C_OK)  
	    break; 
	result = add_booking(&ch->thechange.book_change_u.booking);
	break;
    case CHANGE_BOOKING:
	refund = (int*)malloc(l=sizeof(int)*
		    ch->thechange.book_change_u.changes.chlist.book_ch_list_len);
	memset(refund, 0, l);
	tokens = (int*)malloc(l);
	who_by = (int*)malloc(l);

	result = change_booking(&ch->thechange.book_change_u.changes,
				 refund, tokens, who_by);
	/* If this fails, then it is probably just out of order changes */
	if (result != C_OK)
	{
	    free(refund);
	    free(tokens);
	    free(who_by);
	    break;
	}
	if (tracing) printf("before change user booking\n");
	
	result = change_user_booking(&ch->thechange.book_change_u.changes,
				      refund, tokens, who_by);
	free(refund);
	free(tokens);
	free(who_by);
	break;
    case CLAIM_BOOKING:
	result = claim_booking(&ch->thechange.book_change_u.aclaim);
	break;
    case REMOVE_BOOKING:
	result = remove_booking(&ch->thechange.book_change_u.removal); 
	break;
    case REMOVE_USER:
	result = del_user(ch->thechange.book_change_u.user_togo);
	break;
    case ADD_TOKEN:
	result = add_token(&ch->thechange.book_change_u.addtok);
	break;
    case ADD_CLASS:
	result = add_class(&ch->thechange.book_change_u.addcl);
	break;
    case IMPLICATIONS:
	result = change_implication(&ch->thechange.book_change_u.implch);
	break;
    case ATTR_TYPE:
	result = change_attr_type(&ch->thechange.book_change_u.attrs);
	break;
    case CONFIGDATA:
	result = change_configdata(&ch->thechange.book_change_u.configent);
	break;
    case SET_NAMENUM:
	result = set_namenum(&ch->thechange.book_change_u.map);
	break;
    case CHANGE_WS:
	result = change_ws(&ch->thechange.book_change_u.newws);
	break;
    case CHANGE_HOST:
	result = change_host(&ch->thechange.book_change_u.newhost);
	break;
    case CHANGE_LAB:
	result = change_lab(&ch->thechange.book_change_u.newlab);
	break;
    default:
	if (tracing)
	    printf("server operation (num %d) not inplemented yet\n", ch->thechange.chng);
	return C_NOP;
    }
    
    /* now check the result to make sure that the operation was successful */
    if (result != C_OK) {
	if (tracing)
	    printf("result of change operation = %d\n",result);
	return result;
    }

    /* if the change has come from a client then the changenum field */
    /* will be 0.  in this case the update should be written out to */
    /* the changes file */
    if (1/*ch-> changenum == 0*/) {
/*	printf("about to commit\n"); */
	result = commit(ch);
    }
    /* update the changes recieved in the replicas database.  even do */
    /* it for our own changes */
    replica_grow(ch->machine, ch->changenum);

    
    /* free the request */
    /* NO this happens automatically!! */
/*     xdr_free(xdr_book_changeent, (char*) ch); */
    
    return result;
}

/* this is the external interface.  Checks the the request comes from */
 /* a privileged port and that the database is accepting updates */
book_changeres *SVC(change_db_2)(book_changeent *ch, struct svc_req *rqstp)
{
    
	struct sockaddr_in *clxprt;
	static book_changeres result;
	static int am_priv = -1;

	if (am_priv == -1) {
		am_priv = (geteuid() == 0);
	}
	clxprt = svc_getcaller(rqstp->rq_xprt);
    
	/* make sure from priviliged port */
	if (am_priv && ntohs( clxprt->sin_port) > 1024) {
		/* FIXME check host */
		result = C_NOPERM;
		return(&result);
	}

	collect_xfer();
	check_update_status();
    

	/* check the database is accepting updates */
	if (update_st == NONE) {
		result = C_NOUP;
		shout("no updates acepted\n");
		return(&result);
	}

	if (ch->changenum == 0)
	{
		if (ch->machine) free(ch->machine);
		ch->machine = strdup(server);
	}
	if (ch->machine == NULL)
	{
		result = C_NOUP;
		shout("change with number but no host\n");
		return &result;;
	}
	if (update_st == COPY && (strcmp(ch->machine,server) == 0)) {
		result = C_NOUP;
		shout("no fresh updates acepted\n");
		return(&result);

	}
	if (!islower(ch->machine[0])) {
		char buf[128];
		sprintf(buf, "No host set in change, source = %s, type = %d, number =%d\n",
			inet_ntoa(clxprt->sin_addr), ch->thechange.chng,
			ch->changenum) ;
		shout(buf);
		result = C_UNKNOWNHOST;
		return(&result);
	}

#ifdef NEED_TO_CHECK_DISK
	/* if no disc space... die */
	/* eventually this will just be a warning */
	if (check_disk() == FALSE)
	{
		update_st = NONE;
		shout("MANAGER: out of disc\n");
		alarm(2);
	}
#endif
    
	result = change_db(ch);
	return &result;
}

query_reply * SVC(query_db_2)(query_req * request, struct svc_req *rq)
{
    extern query_reply *generic_query();

    return (generic_query(request));
}

usermap_res * SVC(map_bookings_2)(usermap_req *req, struct svc_req *rq)
{
    /* lookup each booking in the blist for user and
     * return a list of bookings
     */
    static usermap_res res;
    booklist bl = map_bookings(req->user, req->blist);

    res.blist = bl;
    if (bl == NULL)
	res.res = C_NOMATCH;
    else
	res.res = C_OK;
    return &res;
}

utilizationlist * SVC(free_slots_2)(slotlist * req, struct svc_req *rq)
{
    
#ifdef NEED_TO_CHECK_DISK
    if (check_disk() == FALSE)
    {
	update_st = NONE;
	shout("MANAGER: out of disk");
	alarm(2);
    }
#endif

    return free_slots(*req);
}



/* this main routine is the same as the one generated by rpcgen, */
 /* except that we need to initialize the book database as well */
int main (int argc, char *argv[])
{
    SVCXPRT *transp;
    extern void book_database_2();
    bool_t willstart_helper = TRUE;
    extern char * optarg;
    int createnew = 0;

    update_status updtst = COPY; 
	
    int c;

    
#ifdef SOLARIS2
	struct rlimit rl;
	rl.rlim_max=1024;
	rl.rlim_cur=1024;
	setrlimit(RLIMIT_NOFILE, &rl);
#endif


    while ((c = getopt(argc, argv, "M:H:nrIt")) != EOF) {
	switch (c) {
	case 'I':
	    createnew = 1;
	    break;
	case 'M': /* Maintainer */
	case 'H': /* Helper */
	    helper_path = optarg;
	    break;
	case 'n':
	    willstart_helper =  FALSE;
	    break;
	case 'r':
	    perform_stats = TRUE;
	    break;
	case 't':
	    tracing = 1;
	    break;
	default:
	    usage();
	    exit(1);
	}
    }

    /* check if manager already running */
    if (callrpc("localhost", BOOK_DATABASE, BOOKVERS, NULLPROC,
		(xdrproc_t)xdr_void, (char *)NULL,
		(xdrproc_t)xdr_void, (char *)NULL) == 0)
	bailout("Manager already running", 3);
	
    /* set the trap for finishing */
    signal(SIGALRM, die);
    signal(SIGTERM, die);


    /* open local database */
    server = get_myhostname();
    if (server == NULL)
	bailout("cant get my hostname\n",7);
    
    /* set up the change database */
    change_init();
    update_st = COPY;
    last_status_set = time(0);
	
    if (createnew)
    {
	if (open_dbs(GDBM_NEWDB) && replica_init())
	{
	    printf("database created\n");
	}
	else
	{
	    printf("cannot initialise database, check for existing database\n");
	    exit(1);
	}
    }
    else
	if (!open_dbs(GDBM_WRITER))
	{
	    printf("cannot open database\n");
	    exit(1);
	}


    update_st = updtst;
    last_status_set = time(0);
	
    /* the rest of this routine is standard from rpcgen */
    (void)pmap_unset(BOOK_DATABASE, BOOKVERS);

    transp = svcudp_create(RPC_ANYSOCK);
    if (transp == NULL) {
	(void)fprintf(stderr, "cannot create udp service.\n");
	exit(1);
    }
    if (!svc_register(transp, BOOK_DATABASE, BOOKVERS, book_database_2, IPPROTO_UDP)) {
	(void)fprintf(stderr, "unable to register (BOOK_DATABASE, BOOKVERS, udp).\n");
	exit(1);
    }

    transp = svctcp_create(RPC_ANYSOCK, 0, 0);
    if (transp == NULL) {
	(void)fprintf(stderr, "cannot create tcp service.\n");
	exit(1);
    }
    if (!svc_register(transp, BOOK_DATABASE, BOOKVERS, book_database_2, IPPROTO_TCP)) {
	(void)fprintf(stderr, "unable to register (BOOK_DATABASE, BOOKVERS, tcp).\n");
	exit(1);
    }

    if (willstart_helper == TRUE)
	start_helper();

    svc_run();
    (void)fprintf(stderr, "svc_run returned\n");
    exit(1);
}


#ifdef NOTDEF
/*
 * The heart of the server.  A crib from libc for the most part...
 */
void
my_svc_run(void)
{
	fd_set		readfds;
	int             selret;

	for (;;) {

		readfds = svc_fdset;
		cache_set_fds(&readfds);

		selret = select(FD_SETSIZE, &readfds,
				(void *) 0, (void *) 0, (struct timeval *) 0);


		switch (selret) {
		case -1:
			if (errno == EINTR || errno == ECONNREFUSED
			 || errno == ENETUNREACH || errno == EHOSTUNREACH)
				continue;
			xlog(L_ERROR, "my_svc_run() - select: %m");
			return;

		default:
			if (selret)
				svc_getreqset(&readfds);
		}
	}
}
#endif
