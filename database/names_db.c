#include	"db_header.h"

    
/* this database records the mappings from names to numbers
 * and numbers to names
 */
book_changeres set_namenum(name_mapping * mapping)
{
    static book_changeres result;
    char key[20];
    item k;
    static map_value oldvalue;
    static int oldkey;
    extern char * value_key();

    /* look for map(keyint) => old_string */
    result = read_db(k = key_int(key, C_NAMES + mapping->type, mapping->key),
		     &oldvalue, sizeof(oldvalue),
		     (xdrproc_t)xdr_map_value, CONFIG);

    if (result == C_OK)
    {
	/* found  map(keyint) => old_string
	 * remove map(old_string) => keyint
	 */
	k = key_string(C_NUMBERS + mapping->type, oldvalue);
	result = delete_from_db(k, CONFIG);
	free(k.item_val);
    }

    if (mapping->value && mapping->value[0])
    {
	/* look for map(new_string) => otherkey */
	result = read_db(k= key_string(C_NUMBERS + mapping->type, mapping->value),
			 &oldkey, sizeof(oldkey),
			 (xdrproc_t)xdr_int, CONFIG);
	free(k.item_val);
	if (result == C_OK)
	{
	    /* found  map(new_string) => otherkey
	     * remove map(otherkey) => new_string
	     */
	    result = delete_from_db(key_int(key, C_NAMES + mapping->type, oldkey),
			   CONFIG);
	}

	/* now write map(keyint) => newstring */
	result = write_db(key_int(key, C_NAMES + mapping->type, mapping->key),
			  &mapping->value, 
			  (xdrproc_t)xdr_map_value, CONFIG);

	if (result == C_OK)
	{
	    /* write map(newstring) => keyint  */
	    result = write_db(k = key_string(C_NUMBERS + mapping->type, mapping->value),
			      &mapping->key, (xdrproc_t)xdr_int, CONFIG);
	    free(k.item_val);
	}
    }
    else
    {
	/* remove map(keyint) => oldstr */
	result = delete_from_db(key_int(key, C_NAMES + mapping->type, mapping->key),
				CONFIG);
    }
    return result;
}
