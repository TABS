
/* from intpairlist.c */
void add_to_intpairlist(intpairlist * ch, int new, int num);
int del_from_intpairlist(intpairlist * ch, int name);
intpairlist findintkey(intpairlist ptr, int key);
void incintkey(intpairlist *ch, int  key, int diff);
void decintkey(intpairlist *ch, int  key, int diff);
void decintkey_nodel(intpairlist *ch, int  key, int diff);
void print_intpairlist(intpairlist *ch);
void merge_intlists(intpairlist * tot, intpairlist * others);
void del_intlist(intpairlist * tot, intpairlist * subs);
void del_intlist_nodel(intpairlist * tot, intpairlist * subs);


/* from utils.c */
item slotbits2key(int doy, int pod, description *desc);
item slot2key(slotgroup *slot);
item bookkey2key(char *ky, book_key k);
void book_key_bits(book_key *bkey, int *doy, int *pod, int *lab);
book_key make_bookkey(int doy, int pod, int lab);

/* from desc_utils.c */
void free_slot(description * slot);
description new_desc();
void set_a_bit(description * desc, int i);
bool_t del_a_bit(description * desc, int i);
bool_t query_bit(description * desc, int num);
void full_desc(description * desc);
void zero_desc(description * desc);
void desc_add(description d1, description d2);
void desc_and(description d1, description d2);
void desc_sub(description d1, description d2);
bool_t null_desc(description * desc);
description invert_desc(description * desc);
bool_t required_bits(description * template, description * testcase);
bool_t desc_member(description * master, description * slave);
void desc_cpy(description *dest, description *src);
bool_t mandatory_attrs(item *mand, item *d1, item *d2);
bool_t desc_eql(item d1, item d2);
description * desc_dup(description * src);
int desc_2_labind(description *d);
void slot2bookkey(slotgroup *slot, book_key *key);

/* from make_keys.c */
item user_key(char *key, int value);
item key_int(char *key, config_type type, int num);
item key_string(config_type type, char *str);
item str_key(char * str);

/* from db_decode.c */
bool_t datum_decode (datum * content, void * data, xdrproc_t xdr_fn );
bool_t item_decode (item * val, void *data, xdrproc_t xdr_fn);
bool_t item_encode(item * val, void * data, xdrproc_t xdr_fn);
bool_t datum_encode(datum * val, void * data, xdrproc_t xdr_fn);
