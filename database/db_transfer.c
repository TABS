/* this module contains routines to set up the transfer of the */
 /* database (from this manager).  The db_receive program will handle */
 /* the other end */

#include	"db_header.h"

#include	<sys/socket.h>
#include	<unistd.h>
#include	<netdb.h>
#include	<errno.h>
#include	<sys/wait.h>
#include	<signal.h>
#include	<arpa/inet.h>
extern update_status update_st;

static int xferers[10];
static int xfercnt = 0;

void collect_xfer(void)
{
	int pid;

	while ((pid =
#ifdef ultrix
		wait3(NULL, WNOHANG, (char*)NULL)
#else
		waitpid((pid_t)-1,(int*)NULL ,WNOHANG)
#endif
		       ) > 0)
	{
		int i;
		for (i=0 ; i<xfercnt ; i++)
			if (xferers[i] == pid)
				xferers[i] = xferers[--xfercnt];
	}
	if (xfercnt == 0)
	{
		update_status st = COPY;
		if (update_st == NONE)
			SVC(set_updatest_2)(&st, NULL);
	}
}

/* given  a socket, sends the contents of the database down */
void send_database(int sock, db_number db)
{
        
	GDBM_FILE database;
	char * db_path;
	datum key, value;
	int datasize;
	int sent;

	database = db_file[db];
	db_path = DatabaseName[db];
    
	if (database == NULL) {
		printf("No file descriptor\n");
		return;
	}

	/* send off id of database file, so the other end knows which one */
	/* it is.  Send off - the length of the database name. The path is */
	/* setup at the other end */
	datasize = htonl(-strlen(db_path));
	sent = write(sock, &datasize, sizeof(datasize));
	if (sent != sizeof(datasize)) {
		bailout("TRANSFER error sending... (len) bye\n",6);
	}
	sent = write(sock, db_path, strlen(db_path));
	if (sent != strlen(db_path)) {
		shout("TRANSFER: error sending... (data) bye\n");
	}
    
	/* now the contents */
	key = gdbm_firstkey(database);

	while (key.dptr != NULL)
	{
		datum newkey;
		value = gdbm_fetch(database, key);

		/* first send the key (preceeded by it's length) */
		datasize = htonl(key.dsize);
		write(sock, &datasize, sizeof(datasize));
		write(sock, key.dptr, key.dsize);

		/* now send the value */
		datasize = htonl(value.dsize);
		write(sock, &datasize, sizeof(datasize));
		write(sock, value.dptr, value.dsize);

	
		/* get the next key in the database */
		/* free the data, and possibly the old key?? */
		free(value.dptr);
	
		newkey = gdbm_nextkey(database, key);
		free(key.dptr);
		key = newkey;
	}
}

/* Returns the port on which a child is listening for a request to */
 /* connect.  The child then transfers the contents of the database */
 /* through this stream.  */

/* Can't just wait for child, as have to return something.   Best we */
 /* can do is wait for the last child */
/* change to priv sockets!!! */
transfer_reply * SVC(transfer_db_2)(void *arg, struct svc_req *q)
{
	static transfer_reply result;
	int pid;
	int sock;
	int sk;  /* for the child */
	int port = 1023;  /* a privileged one */
	struct sockaddr_in from;
	unsigned int fromlen;
	struct sockaddr_in sin;
	unsigned int sinlen;
	int datasize;
	extern book_changeres * change_db();
	extern char * get_myhostname();
	char buf[256];
	int dbnum;

	collect_xfer();

	if (xfercnt >5)
	{
		result.error = R_SYS;
		return &result;
	}
    
	
	/* pick up the last child */
	sock = socket(AF_INET, SOCK_STREAM,0);
	/* now bind */
	memset(&sin, 0, sizeof(sin));

	sin.sin_family = AF_INET;
	if (bind (sock, (struct sockaddr *) &sin,sizeof(sin)) != 0)
	{
		perror("Bind failed\n");
		shout("Bind failed");
		result.error = R_SYS;
		return(&result);
	}

	/* now find out what happened with bind!!! */
	sinlen = sizeof(sin);
	getsockname(sock, (struct sockaddr *)&sin, &sinlen);

    
	/* set up port */
	port = sin.sin_port;
    
	if (sock == -1) {
		shout("Sock == -1");
		result.error = R_SYS;
		return(&result);
	}
    
	result.port = ntohs(port);
	close_dbs();
    
	switch (pid=fork()) {
	case -1:
		shout("Attempting copy: Fork failed");
		result.error = R_SYS;
		close(sock);
		open_dbs(GDBM_WRCREAT);
		return(&result);
		break;
	case 0:   /* child */
		break;
	default:  /* parent */
		xferers[xfercnt++] = pid;
		close(sock);
		result.error = R_RESOK;
		/* put the database in no update mode */
		update_st = NONE;
		open_dbs(GDBM_READER);

		return(&result);
	}

	/* if no reply in 10 mins then die */
	/* This should be fixed in other places... why does the connection */
	/* fail sometimes.... #ifdef apollo perhaps */
	signal(SIGALRM, SIG_DFL);
	alarm(600);

	open_dbs(GDBM_READER);
	/* child waits for connection then sends info down the wire */
	/* watch for attempt to connect */
	listen (sock,2);

	/* accept first connection, from any address*/
	/* FIXME... check this the address of the requesting server!! */
    
	fromlen = sizeof(from);
	sk = accept(sock,(struct sockaddr *)&from,&fromlen);
	close(sock);
	alarm(0);
	/* check it */
	if (sk == -1)
		exit(5);
	sprintf(buf,"connection accpted from %s\n",inet_ntoa(from.sin_addr));
	shout(buf);

	/* now send stuff down the wire */
	for (dbnum =0 ; dbnum <= MAXDB ;dbnum++) 
		send_database(sk, dbnum);
    
	/* send end of trasmition signal after all transfered*/
	datasize = htonl(0);
	write(sk, &datasize, sizeof(datasize));
    
	close_dbs();
	close(sk);
	exit(0);
}
