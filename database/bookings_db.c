/* these routines handle the bookings database */
/* the bookings database is of the following form:

   key					value
   doy.pod.lab				[userid,count,token,timemake,status,request]
   doy.pod.desc				total-count

   */

#include	"db_header.h"
#include	<stdio.h>

book_changeres add_booking_to_list(bookhead * blist, book_req *req)
{
    booklist new;
    
    new = (booklist)malloc(sizeof(bookslot));
    new -> who_for = req->who_for;
    new -> who_by  = req->who_by;
    desc_cpy(&new->mreq,&req->mreq);
    desc_cpy(&new->charged, &req->slot.what);
    new -> number = req->number;
    new -> tnum = req->tnum;
    new -> time = req->when;
    new -> status = B_PENDING;
    new -> claimed = 0;
    new -> allocations.entitylist_len = 0;
    new -> allocations.entitylist_val = NULL;
    new -> next = blist->data;
    blist -> data = new;

    return C_OK;
}

static booklist * finduser(bookhead * bh, bookuid_t user, int when)
{
	booklist *ptr = &bh->data;

	while ((*ptr != NULL) &&
	       ((user != (*ptr)->who_for)
		|| when != (*ptr)->time
		       )
		)
		ptr = &(*ptr)-> next;

	return(ptr);
    
}

static booklist finduser_alloc(bookhead * bh, bookuid_t user)
{
	booklist ptr;
	booklist found = NULL;

	for (ptr = bh->data ; ptr ; ptr=ptr->next)
		if (user == ptr->who_for
		    && (ptr->status == B_ALLOCATED || ptr->status == B_RETURNED || ptr->status == B_FREEBIE))
		{
			if (found)
				return NULL;
			found = ptr;
		}

	/* HACK alert - if not found, try for cancelled or refunded.. */
	if (! found)
		for (ptr = bh->data ; ptr ; ptr=ptr->next)
			if (user == ptr->who_for
			    && (ptr->status == B_CANCELLED || ptr->status == B_REFUNDED))
			{
/*	    if (found)
	    return NULL;*/
				found = ptr;
			}

	/* OK, go for a pending one ... */
	if (! found)
		for (ptr = bh->data ; ptr ; ptr=ptr->next)
			if (user == ptr->who_for
			    && (ptr->status == B_PENDING))
			{
				if (found)
					return NULL;
				found = ptr;
			}
       
	return found;
}

book_changeres incuser(bookhead *bh, book_req *req)
{
	booklist val;

	if ((val = *finduser(bh, req->who_for, req->when)) == NULL)
		return add_booking_to_list(bh, req);

	/* Do We really want the following. Infact, should it  return an error FIXME */
	/* or should we just update all the fields, assuming this was created by
	 * a change request
	 *.. We do want this, for class booking that need to be made in several passes.
	 */
	val->number += req->number;
	val->tnum = req->tnum;
	/* FIXME should I free val->mreq.???? */
	desc_cpy(&val->mreq, &req->mreq);
	return(C_OK);
}

book_changeres read_bookhead(bookhead *bh, book_key *key)
{
	char ky[20];
	book_changeres rv;
	rv = read_db(bookkey2key(ky,*key), bh, sizeof(bookhead),
		     (xdrproc_t)xdr_bookhead, BOOKINGS);
	return rv;
}

book_changeres write_bookhead(bookhead *bh, book_key *key)
{
	char ky[20];
	book_changeres rv = write_db(bookkey2key(ky, *key), bh,
				     (xdrproc_t)xdr_bookhead, BOOKINGS);
	return rv;
}

book_changeres add_booking(book_req * booking)
{
	book_key keyv;
	static bookhead blist;
	static book_changeres result;
    
	/* read the book slot */
	slot2bookkey(&booking->slot, &keyv);
    
	result = read_bookhead(&blist, &keyv);

	if ((result != C_OK) && (result != C_NOMATCH))
		return result;

	/*  now add new user to this list */
	result = incuser(&blist,booking);
	if (result != C_OK)
		return result;

	/* now write result out */
	result = write_bookhead(&blist, &keyv);

	inc_usage_count(&booking->slot, booking->number);
    
	return result;
}

int inc_usage_count(slotgroup *slot, int number)
{
	item key;
	int cnt;
	book_changeres result;

	key = slot2key(slot);
	cnt = 0;
	result = read_db(key, &cnt, sizeof(cnt), (xdrproc_t)xdr_int, BOOKINGS);
	if (result != C_OK && result != C_NOMATCH)
	{
		free(key.item_val);
		return -1;
	}
	cnt += number;
	if (cnt)
		result = write_db(key, &cnt, (xdrproc_t)xdr_int, BOOKINGS);
	else    /* remove the key.  This should save heaps of space in the */
		/* database adam 03/11/93 */
		delete_from_db(key,BOOKINGS);
    
	free(key.item_val);
	return cnt;
}

book_changeres claim_booking(book_set_claim *claim)
{
	book_changeres result;
	static bookhead blist;
	booklist thebooking;
    
	result = read_bookhead(&blist, &claim->slot);
	if (result != C_OK)
		return result;

	if (claim->when)
		thebooking = *finduser(&blist, claim->user, claim->when);
	else
		thebooking = finduser_alloc(&blist, claim->user);
	if (thebooking == NULL)
		result = C_NOMATCH;
	else
	{
		/* update thebooking->claimed */
		int  bits = claim->claimtime & 0xf;
		int when = claim->claimtime >> 4;
		int oldwhen = thebooking->claimed >>4;

		/* make sure propagated claims have the when field set. */
		claim->when = thebooking->time;

		if ((bits&DID_LOGIN) &&
		    (!(thebooking->claimed&DID_LOGIN)|| (oldwhen > when)))
			oldwhen = when;
		thebooking->claimed = ((thebooking->claimed | bits)&0xf) | (oldwhen<<4);
		if (claim->where >= 0 && thebooking->allocations.entitylist_len>0
		    && thebooking->allocations.entitylist_val[0] == claim->where)
			thebooking->claimed |= LOGIN_ON_ALLOC;
		result = write_bookhead(&blist, &claim->slot);
	}
	return result;
}
    
book_changeres change_booking(book_chng *bchanges, int *refund, int *tokens, int *who_by)
{
	static bookhead blist;
	static book_changeres result;
	int i;

	/* collect mods to usage count in an array and process all at end */
	int *usage_mods;

	/* break the key open and use the description to */
	/* form a slotgroup */
	slotgroup theslot;
	int doy,pod,lab;
      
	/* read the book slot */
	result = read_bookhead(&blist, &bchanges->slot);
    
	if (result != C_OK)
		return result;

	usage_mods = (int*)malloc(sizeof(int)*bchanges->chlist.book_ch_list_len);

	for (i=0;i < bchanges->chlist.book_ch_list_len;i++)
	{
		booklist thebooking;
		book_ch_ent *usr;

		usage_mods[i]=0;
		usr = &bchanges->chlist.book_ch_list_val[i];
	
		thebooking = *finduser(&blist, usr->user, usr->when);

		if (thebooking == NULL)
		{
			char errbuf[128];
	    
			/* changing a booking that has not yet been made!!
			 * we should add the booking here
			 */
			sprintf(errbuf, "Couldn't find booking for user %ld in the list, time %d\n",
				(long)usr->user, usr->when);
			shout(errbuf);
			/* looks like out of sequence changes. Could make booking now, anyway, or
			 * fail and let the create come first before we proceed.
			 * the latter is easier
			 */
			free(usage_mods);
			return C_NOMATCH;
		}
		else
		{
			/* if the change has already been done, then ignore */
			if (usr->status <= thebooking->status)
			{
				/* already been done, just accept */
			}
			else
			{
				/* change the status and possibly the lab usage */
				if (thebooking->status < B_ALLOCATED &&
				    usr->status >= B_ALLOCATED)
				{
					usage_mods[i] -= thebooking->number;
				}
		
				if (thebooking->status <= B_ALLOCATED
				    && usr->status > B_ALLOCATED)
				{
					refund[i]+= thebooking->number;
					tokens[i] = thebooking->tnum;
					if (usr->priv_refund)
						tokens[i] |= PRIV_REFUND_FLAG;
					who_by[i] = thebooking->who_by;
				}
				thebooking->status = usr->status;
			}
			if (thebooking->allocations.entitylist_len < usr->allocations.entitylist_len)
			{
				int l = usr->allocations.entitylist_len;
				if (thebooking->allocations.entitylist_val)
					free(thebooking->allocations.entitylist_val);
				thebooking->allocations.entitylist_val = memdup(usr->allocations.entitylist_val, l*sizeof(entityid));
				thebooking->allocations.entitylist_len = l; 
			}
			if (usr->tentative_alloc & 1)
				thebooking->claimed |= WAS_TENTATIVE;
			if (usr->tentative_alloc & 2)
			{
				thebooking->claimed = (thebooking->claimed&0xf)|
					(((30-10)*60)<<4)|DID_LOGIN|LOGIN_ON_ALLOC;
			}
		}
	}
	/* now apply the usage changes */
	book_key_bits(&bchanges->slot,&doy,&pod,&lab);
	theslot.pod = pod;
	theslot.doy = doy;
	for (i=0;i < bchanges->chlist.book_ch_list_len;i++)
		if (usage_mods[i])
		{
			booklist thebooking;
			book_ch_ent *usr;

			usr = &bchanges->chlist.book_ch_list_val[i];
	
			thebooking = *finduser(&blist, usr->user, usr->when);
			theslot.what = thebooking->charged;
			inc_usage_count(&theslot, usage_mods[i]);
		}
	free(usage_mods);
	/* and write result out */
	result = write_bookhead(&blist, &bchanges->slot);
	return result;
}

booklist map_bookings(bookuid_t user, userbookinglist list)
{
	static booklist result = NULL;
	booklist *lp = &result;

	if (result)
		xdr_free((xdrproc_t)xdr_booklist, (char*) &result);

	while (list)
	{
		static bookhead bh;
		booklist *ent;
		book_changeres st;

		st = read_bookhead(&bh, &list->slot);
	
		if (st != C_OK)
			return NULL;
		ent = finduser(&bh, user, list->when);
		if (ent == NULL&& *ent == NULL)
			return NULL;
		*lp = *ent;
		*ent = (*lp)->next;
		lp = & (*lp)->next;
		*lp = NULL;
		list = list->next;
	}
	return result;
}

book_changeres remove_booking(book_key * removal)
{
	static bookhead blist;
	booklist bptr;
	static book_changeres result;
	extern book_changeres del_user_booking();
	char ky[20];

    
	/* get the list of bookings */
	result = read_bookhead(&blist, removal);
	if (result != C_OK)
		return result;

	/* remove the booking from the user lists */
	bptr = blist.data;
	while (bptr != NULL) {
#if 0
		slotgroup slot;
		int lab;
		book_key_bits(&removal, &slot.doy, &slot.pod, &lab);
		slot.what = bptr->charged;
		inc_usage_count(&slot, -bptr->number);
#endif
		result = del_user_booking(bptr->who_for, bptr->time, removal);
		if (result != C_OK && result != C_NOMATCH)
			return result;
		bptr = bptr->next;
	}

	
	/* delete the booking slot from the database */
	/* CHECK THIS FIRST */
	result = delete_from_db(bookkey2key(ky, *removal), BOOKINGS);
	return result;
}


