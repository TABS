/* some routines for adding and deleting from lists of strings and numbers */

#include	"db_header.h"


void add_to_intpairlist(intpairlist * ch, int new, int num)
{
    intpairlist new_el;

    /* maybe check if present */

    new_el = (intpairlist) malloc (sizeof(intpairnode));

    new_el -> data = new;
    new_el -> num = num;
    new_el -> next = *ch;
    *ch  = new_el;
}

int del_from_intpairlist(intpairlist * ch, int name)
{
    intpairlist ptr = *ch;
    intpairlist trailer = ptr;  /* this points to the prev entry in the */
			     /* list to do deletions, easier than */
			     /* doubly linked list */
    int result = 0;


    while (ptr != NULL) {
	if (ptr->data == name) {

	    /* delete !! */
	    if (ptr == trailer) {  /* first node */
		if (ptr -> next == NULL) /* only one node in list*/
		    *ch = NULL;
		else
		    *ch = ptr -> next;
	    }
	    else
		trailer->next = ptr-> next;
	    /* cut the node off */
	    ptr -> next = NULL;
	    xdr_free((xdrproc_t)xdr_intpairlist, (char*) &ptr);  	    
	    result = 1;
	}
	else { /* go to the next element in the list */
	    trailer = ptr;
	    ptr = ptr->next;
	}
    }
    return result;
}

intpairlist findintkey(intpairlist ptr, int key)
{
    while ((ptr != NULL) && (ptr->data != key) ) {
	ptr = ptr->next;
    }
    return(ptr);
}

/* if the key is present in the list inc it's value, else add it to */
 /* the list with a value of 1 operation should always suceed*/
void incintkey(intpairlist *ch, int  key, int diff)
{
    intpairlist val;
    if ((val = findintkey(*ch,key)) == NULL) /* add new element to list */
	add_to_intpairlist(ch,key,diff);
    else
    {
	val->num += diff;
	if (val->num == 0)	/* if this key is empty remove it */
	    del_from_intpairlist(ch,key);
    }
}

/* if the key is in the list decrement it.  If it's value is 0, then */
 /* delete it from the list */
/* what should happen with values that are not found... should we just */
 /* add the negative amount!!!! */
void decintkey(intpairlist *ch, int  key, int diff)
{
    intpairlist val;
    
    if ((val = findintkey(*ch,key)) == NULL) /*look for element in list */
	add_to_intpairlist(ch, key, -diff);
    else
    {
	val->num -= diff;

	if (val->num == 0)	/* if this key is empty remove it */
	    del_from_intpairlist(ch,key);
    }
    
}
void decintkey_nodel(intpairlist *ch, int  key, int diff)
{
    intpairlist val;
    
    if ((val = findintkey(*ch,key)) == NULL) /*look for element in list */
	add_to_intpairlist(ch, key, -diff);
    else
    {
	val->num -= diff;
    }
    
}

void print_intpairlist(intpairlist *ch)
{
    int i = 0;
    intpairlist ptr;
    if (ch == NULL) {
	printf("EMPTY INTPAIRLIST\n");
	return;
    }
    
    ptr = *ch;
    while (ptr != NULL) {
	printf("Element %d: (%d, %d)\n",i,ptr->data, ptr->num);
	i++;
	ptr = ptr->next;
    }
    
}



void merge_intlists(intpairlist * tot, intpairlist * others)
{
    intpairlist ptr = *others;

    while (ptr != NULL) {
	incintkey(tot, ptr->data, ptr->num);
	ptr = ptr ->next;
    }

}

void del_intlist(intpairlist * tot, intpairlist * subs)
{
    intpairlist ptr = *subs;
    while (ptr != NULL) {
	decintkey(tot, ptr->data, ptr->num);
	ptr = ptr->next;
    }
}

void del_intlist_nodel(intpairlist * tot, intpairlist * subs)
{
    intpairlist ptr = *subs;
    while (ptr != NULL) {
	decintkey_nodel(tot, ptr->data, ptr->num);
	ptr = ptr->next;
    }
}
