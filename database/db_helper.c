/* db_helper: a program to fetch updates from other servers in the network and */
 /* apply them to this manager */
/* This program is usually  forked (and execed) when the manager starts up. */
 /* There is  a good reason for it not being a routine inside the manager that */
 /* is just forked.  See the comment inside the manager that forks this process */

#include	"db_header.h"
#include	<stdio.h>
#include	<unistd.h>
#include	<stdlib.h>
#include	<string.h>
#ifndef apollo
#include	<sys/types.h>
#include	<netinet/in.h>
#endif
#include	<string.h>
#include	<rpc/pmap_clnt.h>

#include	<netdb.h>
#include	<sys/socket.h>
#include	"../lib/skip.h"

GDBM_FILE server_file = NULL; 
static char *ServerName;
CLIENT * manager_cl;
char errbuff[1024];

int tracing = 0;

/* open_hdb opens the server database, and establishes a */
 /* connection to the manager. */
void open_hdb()
{

	/* now for some initialization */
	/* set up connection to manager */
	ServerName = get_myhostname();
    
	manager_cl = clnt_create("localhost",BOOK_DATABASE,BOOKVERS,"udp");
	if (manager_cl == NULL) {
		/* no connection to server */
		clnt_pcreateerror("localhost");
		exit(1);
	}
}

/* maintain a list of down servers in a skip list... */

typedef struct downserv
{
	char *server;
	int change_num;
	int up_to_date;
} * downserv;

int ds_cmp(downserv a, downserv b, char *c)
{
	if (b) c = b->server;
	return strcmp(a->server, c);
}

void ds_free(downserv a)
{
	free(a->server);
	free(a);
}

void *down_list;
void down_init()
{
	down_list = skip_new(ds_cmp, ds_free, NULL);
}

void add_down(char *server, int chnum)
{
	downserv * dsp, ds;
	dsp = skip_search(down_list, server);
	if (dsp)
	{
		(*dsp)->change_num = chnum;
	}
	else
	{
		ds = (downserv)malloc(sizeof(struct downserv));
		ds->server = strdup(server);
		ds->change_num = chnum;
		ds->up_to_date = 0;
		skip_insert(down_list, ds);
	}
}

void rem_down(char *server)
{
	skip_delete(down_list, server);
}
downserv *first_down()
{
	return skip_first(down_list);
}
downserv *next_down(downserv *d)
{
	return skip_next(d);
}

void print_slot(FILE *f, book_key slot)
{
	int pod, doy, lab;
	book_key_bits(&slot, &doy, &pod, &lab);
	fprintf(f, "d%d p%d l%d", doy, pod, lab);
}

static int get_latestchanges(char * changesource, char *changeowner, int *changes)
{
	CLIENT * replica_cl;
	change_reply * result;
	book_changeres *  ch_res;
	bool_t failed = FALSE;
	book_changeent * chptr;
	int index;
	int rv = 0;
	struct timeval tv;

	/* set up connection */
	replica_cl = clnt_create(changesource,BOOK_DATABASE,BOOKVERS,"udp");
	if (replica_cl == NULL) {
		/* no connection to server */
		/* clnt_pcreateerror(changesource);*/
		return -1;
	}
	tv.tv_sec = 5;
	tv.tv_usec = 0;
	clnt_control(replica_cl, CLSET_TIMEOUT, (char*)&tv);
	tv.tv_sec = 2;
	clnt_control(replica_cl, CLSET_RETRY_TIMEOUT, (char*)&tv);
    
	while (failed == FALSE)
	{
		update_request ur;

		ur.minchange = *changes;
		ur.replica = changeowner;
		result = last_replica_updates_2(&ur, replica_cl);

		if (result == NULL)  /* no contact */
		{
			if (tracing) printf("Call %s for %d of %s got NULL\n", changesource, *changes, changeowner);
			rv = -1;
			break;
		}
		if (tracing) printf("Call %s for %d of %s got %d\n", changesource, *changes, changeowner, result->error);
		if (result->error == CR_NONE)
		{
			rv = 1;
			break;
		}
	
		if (result->error != CR_RESOK) /* no changes */
			break;

		/* now process results */
		chptr = (book_changeent *)result->change_reply_u.data.changelist_val;
		for (index = 0;!failed && index < result ->change_reply_u.data.changelist_len;index++, chptr++)
		{

			if (chptr->changenum == 0)
			{
				bailout("HELPER, got change with 0 changenumber\n", 9);
				break;
			}
			ch_res = change_db_2(chptr,manager_cl);
			/* only bailout if no contact AND parent is 0*/
			/* It is possible that manager could have been busy */
			if ((ch_res == NULL) && (getppid() == 1))
				bailout("HELPER could not contact manager\n",9);

			/* this is a temporary aberation */
			if (ch_res == NULL)  
			{
				if (tracing) printf("NULL returned for change %d\n", chptr->changenum);
				failed = TRUE;
				break;
			}
	    
			/* update the server table */
			if (*ch_res == C_OK)
				*changes = chptr->changenum;
			else
			{
				if (tracing)
				{
					printf("got status %d for change %d\n", *ch_res, chptr->changenum);
					printf(" change type is %d\n", chptr->thechange.chng);
					if (chptr->thechange.chng == CHANGE_BOOKING) {
						printf(" change_booking\n  slot");
						print_slot(stdout, chptr->thechange.book_change_u.changes.slot);
						printf("\n");
					}
					if (chptr->thechange.chng ==  CLAIM_BOOKING) {
						printf(" claim booking\n  slot");
						print_slot(stdout, chptr->thechange.book_change_u.aclaim.slot);
						printf("\n  user %d\n  time %d\n  where %d\n  claim %d\n",
						       chptr->thechange.book_change_u.aclaim.user,
						       chptr->thechange.book_change_u.aclaim.when,
						       chptr->thechange.book_change_u.aclaim.where,
						       chptr->thechange.book_change_u.aclaim.claimtime);
						break;
					}
				}
				if (*ch_res != C_NOUP)
				{ 
					update_status stat = COPY;

					/* tell the manager to only accept copied updates */
					/* stop it from becoming further out of date */
					set_updatest_2(&stat, manager_cl);
				}
				failed = TRUE;
			}
		}
		/* free the result */
		xdr_free((xdrproc_t)xdr_change_reply, (char*) result);
	}
    
	/* free handle */
	clnt_destroy(replica_cl);

	/* and the remaining result */
	if (result != NULL)
		xdr_free((xdrproc_t)xdr_change_reply, (char*) result);
    
	return rv;
}


/* get_updates: poll the other servers in the network for any changes */
 /* they may have recieved */
void get_updates()
{
    
	static char dummy[] = "first";
	static query_req request;
	query_reply * result;
	char * replicaserver = NULL;
	downserv *one_down;

	bool_t up_to_date = TRUE;
	static char next_server[100] = "" ; /* next server to consider discarding changes from */
	char this_server[100]; /* server we are considering discarding changes from */
	int minupdate_sent = -1; /* the minimum update number received by all servers */
	int anydown = 0; /* if any replicas are down, we cannot discard changes */
	static int am_a_server = 0;	/* true if my name is found in list of servers */
	int new_am_a_server = 0; /* true if we find out selves in list of servers */
    
    
	request.key.item_val = dummy;
	request.key.item_len = 4;
	request.type = M_FIRST;
	request.database = REPLICAS;

	/* get the first replica key from the manager (and result!) */
	result = query_db_2(&request, manager_cl);
	if (result == NULL) {
		if (tracing) printf("Could not contact manager \n");
		if (getppid() == 1)
			exit(1);
		return;
	}
	request.type = M_NEXT;

	for (one_down = first_down() ; one_down ; one_down = next_down(one_down))
		(*one_down)->up_to_date = 1;

	strcpy(this_server, next_server);
	next_server[0] = '\0';
	while (result-> error == R_RESOK)
	{
		int change_num;
		/* for each replica:
		   - If it is me, skip it
		   - ask for more changes from where we are up to
		   apply them
		   - if it is not talking, remember it is broken
		   else ask for changes from all non-talking hosts
		   and find out where it is upto wrt me
		*/

		change_num = *(int*)result->query_reply_u.data.value.item_val;
		change_num = ntohl(change_num);
	
		/* set up the next possible request here as the pointer may change if */
		/* other RPC calls are made in the body of the loop */
		/* copy the result, so it can be freed */
		request.key.item_len = result-> query_reply_u.data.key.item_len;
		request.key.item_val =
			memdup(result->query_reply_u.data.key.item_val,
			       request.key.item_len);  


		replicaserver = strndup(result ->query_reply_u.data.key.item_val, result->query_reply_u.data.key.item_len);
		if (next_server[0] == '\0')
			strcpy(next_server, replicaserver);
		if (this_server[0] == '\0') strcpy(this_server, next_server);
		if (strcmp(ServerName, replicaserver)==0)
		{
			new_am_a_server = 1;
		}
		else
		{
			switch(get_latestchanges(replicaserver, replicaserver, &change_num))
			{
			case -1:		/* could not contact */
				add_down(replicaserver, change_num); anydown = 1;
				break;
			case 0:		/* got some changes, but not all */
				up_to_date = 0;
				rem_down(replicaserver);
				break;
			case 1:		/* got all changes, see what else we can discover */
				rem_down(replicaserver);
				for (one_down = first_down(); one_down ; one_down = next_down(one_down))
				{
					switch(get_latestchanges(replicaserver, (*one_down)->server, &(*one_down)->change_num))
					{
					case -1:	/* golly, now this one is down too */
						break;
					case 0:
						(*one_down)->up_to_date = 0; /* should only set this if replicaserver is in full service */
						break;
					case 1:	/* happiness, atleast wrt replicaserver */
						break;
					}
				}
				/* replica is up, maybe see where they are wrt me */
				if (am_a_server && ! anydown)
				{
					/* ask for me in their replica list */
					CLIENT *repl_cl = clnt_create(replicaserver, BOOK_DATABASE, BOOKVERS, "udp");
					if (repl_cl == NULL)
						anydown = 1;
					else
					{
						static query_req rq;
						query_reply *rs;
						rq.key.item_val = this_server;
						rq.key.item_len = strlen(rq.key.item_val);
						rq.type = M_MATCH;
						rq.database = REPLICAS;
						rs = query_db_2(&rq, repl_cl);
						if (rs == NULL) anydown = 1;
						else if (rs->error == R_RESOK &&
							 (minupdate_sent == -1 || minupdate_sent > ntohl(*(int*)rs->query_reply_u.data.value.item_val)))
							minupdate_sent = ntohl(*(int*)rs->query_reply_u.data.value.item_val);
						clnt_destroy(repl_cl);
					}
				}
				break;
			}
		}
	
	    
		/*  free previous result */
		xdr_free((xdrproc_t)xdr_query_reply, (char*) result);
	
		/* get next replica server from our manager */
		result = query_db_2(&request, manager_cl);
		if ((result == NULL) && (getppid() == 1))
			/* our manager could not be  contacted and has disappeared */
			/* i.e. our parent is init */
			bailout("HELPER: could not contact my manager",4);

		/* this is a temporary aboration the manager is busy doing */
		/* other things*/
		if (result == NULL)
			return;
		if (result->error == R_RESOK && strcmp(this_server, replicaserver)==0)
			next_server[0] = 0;
		free(replicaserver);
	}
	/* if up to date, move server into update mode */
	/* only if not in no update mode */
	for (one_down = first_down() ; up_to_date && one_down ; one_down = next_down(one_down))
	{
		if (!(*one_down)->up_to_date)
		{
			if (tracing)
				printf("down host %s is not up-to-date(%d)\n", (*one_down)->server, (*one_down)->change_num);
			up_to_date = 0;
		}
	}

	if (tracing)
		printf("uptodate = %d am_a_server = %d\n", up_to_date, new_am_a_server);
	if (up_to_date && new_am_a_server)
	{
		update_status * oldst;
		static update_status stat = FULL;

		oldst = get_updatest_2((void*)NULL, manager_cl);
		if ((oldst != NULL) && (*oldst != NONE))
			set_updatest_2(&stat, manager_cl);
	}
	if (minupdate_sent > 1 && up_to_date && this_server[0] && ! anydown)
	{
		/* DISCARD changes < minupdate_sent */
		update_request rq;
		if (tracing)
			printf("can discard changes up to %d from %s\n", minupdate_sent, this_server);
		rq.minchange = minupdate_sent;
		rq.replica = this_server;
		delete_changes_2(&rq, manager_cl);
	}
	am_a_server = new_am_a_server;
}


int main (int argc, char *argv[])
{
	static struct timeval tv = {5,0};
	static struct timeval total = {20,0};
        
	/* open our database of servers, and get connection to our */
	/* manager */
	if (argc == 2 && strcmp(argv[1], "trace")==0)
		tracing = 1;
    
	open_hdb();
	down_init();

	/* set faster timeouts for talking to portmaps */
	pmap_settimeouts(tv, total);

	/* now just look for updates from other servers in the */
	/* network */
	while (1) {
		get_updates();
		sleep(60);
	}
}




