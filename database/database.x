/* This is the protocol for client database server interation */

% /* register in arglist may be broken with solaris/pc */
%#ifndef register
%#define register
%#endif
typedef string machine_name<>;
/* typedef string tokenname<>;*/
typedef int tokenid;
typedef int btime_t;
typedef int bookuid_t;
typedef string bitstringname<>;
typedef string map_value<>;
typedef int entityid;
typedef entityid entitylist<>;
typedef int hostid;
typedef int hostgroup;
typedef string reason<>;
typedef string cfgstr<>;

typedef opaque item<>;

typedef item description;

struct db_updaterec {
	int update_time;
	int reorganise_time;
};

struct slotgroup {
	int		doy;
	int		pod;
	description	what;
};

typedef int book_key; /* (8bits pod, 9bits doy, 8bits lab) */


/* there are two components to a token.  The first is the definition
of bookings that can be made in the future, the second is bookings
that can be made within four days */
struct token {
	description advance;
	description late;
};

/* a request for a booking will contain a description of the slot */
/* (pod.dow.desc) and the actual request that was made. */
/* The request  will be stored in the user database  */
/* the description is a request without wildcards! */
struct book_req {
	btime_t		when;		/* when booking was made */
	slotgroup	slot;		/* machine group to charge */
	description	mreq;		/* actual request */
	tokenid		tnum;		/* token used	*/
	int		tok_cnt;	/* how many we think the user has, to avoid races */
        int		number;		/* how many hosts to book */
	bookuid_t	who_for;	/* who gets allocated */
	bookuid_t	who_by;		/* who gets charged */
};

/* for adding tokens */
struct tok_req {
	tokenid tnum;
	token tok;
};

struct class_req{
	bookuid_t user;
	tokenid tnum;
	int count;
};

enum booking_status {
	B_PENDING   = 1,
	B_TENTATIVE = 2,
	B_ALLOCATED = 3,  /* allocated, token kept */
	B_RETURNED  = 4,  /* allocated, reuable token returned */
	B_FREEBIE   = 5,  /* allocated, free period, token returned */
	B_REFUNDED  = 6,  /* not allocated, token returned */
	B_CANCELLED = 7   /* cancelled before allocation */
};

struct book_ch_ent {
	bookuid_t	user;
	int		when;
	booking_status	status;
	entitylist	allocations;
	int		priv_refund;
	int		tentative_alloc;
};
/* tentative alloc is a bit or of
 *  1 tentative alloc
 *  2 already logged on
 */

typedef book_ch_ent book_ch_list<>;

struct book_chng {
	book_key slot;
	book_ch_list chlist;
};

struct book_set_claim {
    book_key	slot;
    bookuid_t	user;
    int		when;	/* if zero, find a booking which is pending or allocated */
    int		where;	/* if != -1, set bit3 if wsid matches */
    int		claimtime;
};

enum machine_status {
	BOOKINSERVICE = 1,
	BOOKOUTOFSERVICE = 2
};
struct mach_chng {
	machine_name machine;
	machine_status newst;
};

struct workstation_state
{
	description state;
	reason why;
	btime_t time;
	hostid host;
};


struct workstation_ch {
	entityid ws;
	workstation_state state;
};

struct host_ch {
	hostid entity;
	hostgroup clump;
};
/* stuff for the implications file */
struct implication {
	int startday;
	int endday;
	int startdow;
	int enddow;
	int starttime;
	int endtime;
	description included;
	description excluded;
	int attribute;
};

struct impls {
    int first;
    int total;
    implication exlist<>;
};

enum bittype {
	BIT_LAB = 1,
	BIT_MAND = 2,
	BIT_WEEK = 4,
	BIT_OPTIONAL = 8,
	BIT_BOOKABLE = 16,
	BIT_OPEN = 32,
	BIT_CLOSED = 64,
	BIT_NORMALLAB = 128,
	BIT_REUSABLE = 256,
	BIT_FORGET = 512
};


struct bitentry {
	bitstringname name;
	int number;
	bittype type;
};

enum nmapping_type {
	M_HOST = 1,
	M_WORKSTATION =2,
	M_HOSTGROUP = 3,
	M_TOKEN = 4,
	M_ATTRIBUTE = 5, /* this includes labs */
	M_CLASS = 6
};

enum config_type {
    C_NUMBERS = 100,		/* string + int*/
    C_NAMES = 200,		/* int + int*/
    C_WORKSTATIONS = 3,		/* int */
    C_WS_COUNT = 4,		/* desc */
    C_HOSTGROUPS = 5,		/* int */
    C_HOSTS = 6,		/* int */
    C_LABS = 7,			/* int */
    C_TOKENS = 8,		/* int */
    C_ALLOTMENTS = 9,		/* int */
    C_GENERAL = 10,		/* string */
    C_ATTR_TYPE = 11,		/* int */
    C_ATTR_DEF = 12		/* int */
};

struct configdata {
	cfgstr key;
	cfgstr value;
};
struct name_mapping {
	nmapping_type type;
	int key;
	map_value value;
};


/* stuff for change requests */
enum book_changetype {
	NO_CHANGE = 0,
	CHANGE_WS = 1,
	CHANGE_HOST = 2,
	CHANGE_LAB = 3,
	ADD_SERVER = 10,
	DEL_SERVER = 12,
	ADD_BOOKING = 20,
/*	DEL_BOOKING = 22, */
	CHANGE_BOOKING = 23,
	CLAIM_BOOKING = 24,	/* record when a booking was claimed */
	REMOVE_BOOKING = 25,
	REMOVE_USER = 26,
	ADD_TOKEN = 30,
	ADD_CLASS = 40,
/*	DEL_CLASS = 42, */
	IMPLICATIONS = 50,  /* for the implications */		
	ATTR_TYPE = 52,  /* for the bittable entries */
	CONFIGDATA = 54,
	SET_NAMENUM = 60
};
/* a header for the bitstrings and exclusions in the config file */
struct attr_type_desc
{
	description mandatory;
	description lab;
	description all;
	int open;
	int closed;
	int available;
	int bookable;
	int reusable;
	int firmalloc;
};

enum db_number {
	REPLICAS = 0,
	BOOKINGS = 1,
	CONFIG = 2,
	RESERVATIONS = 3
};

const MAXDB = RESERVATIONS;

union book_change switch (book_changetype chng)
    {
    case NO_CHANGE:
	void;
    case CHANGE_WS:
	workstation_ch newws;
    case CHANGE_HOST:
	host_ch newhost;
    case CHANGE_LAB:
	host_ch newlab;
    case ADD_SERVER:
	machine_name server;
    case DEL_SERVER:
	machine_name delserver;
    case ADD_BOOKING:
	book_req booking;
    case REMOVE_BOOKING:
	book_key removal;
    case REMOVE_USER:
	bookuid_t user_togo;
    case CHANGE_BOOKING:
	book_chng changes;
    case CLAIM_BOOKING:
	book_set_claim aclaim;
    case ADD_TOKEN:
	tok_req addtok;
    case ADD_CLASS:
	class_req addcl;
    case IMPLICATIONS:
	impls implch;
    case ATTR_TYPE:
	attr_type_desc attrs;
    case CONFIGDATA:
	configdata configent;
    case SET_NAMENUM:
	name_mapping map;
    };

struct book_changeent {
	btime_t time;  /*  the time the change was made to the db*/
	machine_name machine; /* the machine the change came from */
	int changenum; /* used when the change is stored in */
		/* the log file so it can be given to another machine */ 
	book_change thechange;
};

enum book_changeres {
	C_OK 		= 1,
	C_NOMATCH	= 2,
	C_SYSERR	= 3,
	C_XDR		= 4,
        C_LOG		= 5,
	C_MISMATCH	= 6,
	C_BAD		= 7,
	C_FILE		= 8,
	C_DUP		= 10,
	C_NOPERM	= 12,
	C_UNKNOWNHOST	= 14,
	C_NOP		= 20,
	C_NOUP 		= 24,
	C_NOMEM		= 25,
	C_GDBM		= 30,
	C_NOSERVER	= 35,
	C_RPCERR	= 40
};

/* data structures to store in gdbm files. */
/* machines */

typedef struct descnode * desclist;
struct descnode {
	description data;
	desclist next;
};

typedef struct slotnode *slotlist;
struct slotnode {
	slotgroup *data;
	slotlist next;
};


typedef struct  intpairnode * intpairlist;
struct intpairnode {
	int data;
	int num;
	struct intpairnode * next;
};

struct deschead {
	desclist data;
	int num_els;
};

typedef struct  descpairnode * descpairlist;
struct descpairnode {
	description data;
	int num;
	descpairlist next;
};
typedef struct utilizationnode * utilizationlist;
struct utilizationnode{
 	   description machine_set;
 	   int free;
 	   int total;
 	   utilizationlist next;
};


struct hostinfo
{
	entitylist workstations;
	hostgroup clump;
};


struct hostgroup_info
{
    entitylist hosts;
    entitylist labs;
};

/* bookings */

/* Note on "claimed".
 * "claimed" records when, and whether, booking was claimed.
 * The least significant four bits determine whether they logged on.
 * The remainder determine the number of seconds after allocation time
 * that they logged on -  plus 30*60
 * The whether field is 
 *   bit 0 - have logged on (time is set)
 *   bit 1 - were not logged on when grace expired
 *   bit 2 - alloc was tentative
 *   bit 3 - logged on to allocated ws
 * 
 * bits can be set but not cleared.
 * once set (when bit 0 set) number can only be made smaller.
 */

enum claimflag {
	DID_LOGIN = 1,
	DID_DEFAULT= 2,
	WAS_TENTATIVE = 4,
	LOGIN_ON_ALLOC = 8
};
typedef struct bookslot *booklist;
struct bookslot {
	bookuid_t	who_for;
	bookuid_t	who_by;
	description	mreq;
	description	charged;	/* which group was "charged" for this booking */
	int		number;
	tokenid		tnum;
	btime_t		time;
	booking_status	status;
	entitylist	allocations;
	int		claimed;	/* when, and whether, booking was claimed */
	booklist	next;
};

struct bookhead {
	booklist data;
};
	
/* class db */

/* user db */
typedef struct  userbookingnode * userbookinglist;
struct userbookingnode {
	book_key	slot;
	int		when;	/* when booking was made */
	struct userbookingnode * next;
};

struct users
{
	userbookinglist bookings;	/* the bookings the user has made			*/
	userbookinglist pastbookings;	/* bookings that are no longer pending			*/
	intpairlist tokens;		/* the tokens the user has consumed			*/
	intpairlist priv_tokens;	/* the tokens which have been refunded with privilege	*/
};

struct usermap_req {
	bookuid_t			user;
	userbookinglist		blist;
};
struct usermap_res {
	book_changeres		res;	
	booklist		blist;
};
	

/* stuff for query requests */

enum query_type {
	M_FIRST = 0,
	M_NEXT = 1,
	M_MATCH = 2,
	M_NEXT_PREFIX = 3 /* Next entry with same prefix (upto '_') */

/* should domain be an optional query????? */
/* this would be useful for the machines and replica */
/* databases which are quite small */
};

struct query_req{
	query_type type;
	db_number database;
	item key;
};
enum reply_res {
	R_RESOK = 0,
	R_INTERN  =1,
	R_BADCASE = 3,
	R_NOMATCH = 5,
	R_NOMEM = 6,
	R_NOFIRST = 10,
	R_NONEXT = 12,
	R_NODB = 14,
	R_SYS = 16,
	R_SERVADD = 18,
	R_NOSERVER = 20
};

/* a generic reply */
struct query_reply_data {
	item key;
	item value;
};
union query_reply switch ( reply_res error) {
	case R_RESOK:
		query_reply_data data;
	default:
		void;
};


/* stuff for the changes database */

struct log_header {
    int lastchangenum;
    int lowestchangenum;
    btime_t createstamp;
    btime_t lastchangestamp;
    
};

/* to send requests for changes to other places */
enum change_reqres {
	CR_RESOK = 0,
	CR_NONE = 1
};
typedef	book_changeent changelist<>;

union change_reply switch( change_reqres error)
{
case CR_RESOK:
	changelist data;
default:
	void;
};
typedef  int replica_no;

struct update_request {
	replica_no minchange;
	string replica<>;
};

/* 
struct replica_ent {
	replica_no heighest;
	machine_id server;
};
*/
struct transfer_reply{
	reply_res error;
	short port;
};

enum update_status {
	NONE = 1,
	COPY = 2,
	FULL = 3 };

program BOOK_DATABASE{
	version BOOKVERS {
		/* adds a machine to the description set */
		book_changeres
			CHANGE_DB(book_changeent) = 1;
		query_reply
			QUERY_DB(query_req) = 15;
		utilizationlist
			FREE_SLOTS(slotlist) = 16;
		usermap_res
			MAP_BOOKINGS(usermap_req) = 17;
		void 
			SET_UPDATEST(update_status) = 40;
		update_status
			GET_UPDATEST(void) = 42;

		change_reply
			LAST_REPLICA_UPDATES(update_request) = 51;

		transfer_reply
			TRANSFER_DB(void) = 60;
		void
			TERMINATE(int) = 100;
		bool
			DELETE_CHANGES(update_request) = 110;
		void
			DB_REORGANIZE(void) = 112;
	} =2;
} = 0x20304052;

