#include	 "db_header.h"

/* config database, parameters for the booking system */
/* these are of three categories
   * General
   * Bitspec
   * Exclusion

*/

#if 0
/* UPDATE_HEADER: fixes up the header for the bitspec and exclusions */
 /* stuff */
book_changeres *update_header(int bit, bittype type, int maxexclusion, int exclusions_per_block)
{
    static book_changeres result;
    item key;
    static bitstring_exclusion_header header;


    key = str_key(BIT_EXCL_HEADER);

    
    result = read_db(key, &header,
		     sizeof(bitstring_exclusion_header),
		     xdr_bitstring_exclusion_header, CONFIG);

    if (result == C_NOMATCH) {
	header.lab = new_desc();
	header.mandatory = new_desc();
	header.all = new_desc();
	header.weeks = new_desc();
	header.normallab = header.closed = header.open = header.bookable = 0;
    }
    /* if not present.. no matter */

    /* if it was a max exclusion */
    if (maxexclusion)
    {
	header.maxexclusion = maxexclusion;
	header.exclusions_per_block = exclusions_per_block;
    }
    else /* this means it was a bittype */
    {
        if (type & BIT_LAB)
	    set_a_bit(&header.lab, bit);
	else
	    clr_a_bit(&header.lab, bit);

	if (type & BIT_MAND)
	    set_a_bit(&header.mandatory, bit);
	else
	    clr_a_bit(&header.mandatory, bit);

	if (type & BIT_WEEK)
	    set_a_bit(&header.weeks, bit);
	else
	    clr_a_bit(&header.weeks, bit);

	if (bit == header.normallab) header.normallab = 0;
	if (bit == header.closed) header.closed = 0;
	if (bit == header.open) header.open = 0;
	if (bit == header.bookable) header.bookable = 0;
	if (type & BIT_NORMALLAB)
	    header.normallab = bit;
	if (type & BIT_CLOSED)
	    header.closed = bit;
	if (type & BIT_OPEN)
	    header.open = bit;
	if (type & BIT_BOOKABLE)
	    header.bookable = bit;
	/* set all no matter what */
	if (type & BIT_FORGET)
	    clr_a_bit(&header.all, bit);
	else
	    set_a_bit(&header.all, bit);
	
    }
    /* write out the result */
    result = write_db(key, &header,
		      xdr_bitstring_exclusion_header, CONFIG);
    
    return(&result);
    
}
#endif


book_changeres change_implication(impls * impl)
{
    char key[20];
    item k;
    book_changeres result;
    
    /*    impl->ex, impl->number are the two fields */
    /* how to remove them???? */

    /* make a unique key */
    k = key_int(key, C_ATTR_DEF, impl->first);

    if (impl->first >= impl->total
	|| impl->exlist.exlist_len == 0)
	result = delete_from_db(k, CONFIG);
    else
	result = write_db(k, impl, (xdrproc_t)xdr_impls, CONFIG);

    return result;

}


void init_attr_types()
{
    read_db(str_key(ATTR_TYPE_KEY), &attr_types, sizeof(attr_types),
	    (xdrproc_t)xdr_attr_type_desc, CONFIG);
}

book_changeres change_attr_type(attr_type_desc *attrs)
{
    book_changeres result;
    
    result = write_db(str_key(ATTR_TYPE_KEY), attrs,
		      (xdrproc_t)xdr_attr_type_desc, CONFIG);
    init_attr_types();
    return result;
}

book_changeres change_configdata(configdata * configent)
{
    book_changeres result;
    item key;

    key = key_string(C_GENERAL, configent->key);

    
    result = write_db(key, &configent->value,
		      (xdrproc_t)xdr_cfgstr, CONFIG);

    free(key.item_val);
    return result;
}
