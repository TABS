#include	"db_header.h"

/* classes database
   key: class_id, value: [tokens, count] 
   */
book_changeres add_class(class_req * cl_req)
{
    static intpairlist list_of_tokens;
    book_changeres result;
    char key[20];
    item k;

    /* read in record for this user */
    result = read_db(k=key_int(key, C_ALLOTMENTS, cl_req->user),
		     &list_of_tokens,
		     sizeof(intpairlist), (xdrproc_t)xdr_intpairlist, CONFIG);

    if ((result != C_OK) && (result != C_NOMATCH))
	return result;


    /* add token to list */
    incintkey(&list_of_tokens, cl_req->tnum, cl_req->count);
    
    /* now save the resultant record */
    result = write_db(k, &list_of_tokens, (xdrproc_t)xdr_intpairlist, CONFIG);
    return result;
}

book_changeres set_allotment(int uid, int tok, int cnt)
{
    char key[20];
    item k;
    book_changeres result;
    static intpairlist toks;
    intpairlist t;
    result = read_db(k=key_int(key, C_ALLOTMENTS, uid),
		     &toks,
		     sizeof(intpairlist), (xdrproc_t)xdr_intpairlist, CONFIG);

    if ((result != C_OK) && (result != C_NOMATCH))
	return result;

    t = findintkey(toks, tok);
    if (t) cnt -= t->num;
    if (cnt)
    {
	incintkey(&toks, tok, cnt);
	result = write_db(k, &toks, (xdrproc_t)xdr_intpairlist, CONFIG);
    }
    else
	result = C_OK;
    return C_OK;
}

			  
