/* a module for maintaining the replicas database.
 * servers can be added and deleted
 * changes to server records are done implicitly
 * when changes are received from the relevant server.
 */
/* replicas database is
   key	       value
   hostname  replicaheight
   */
#include	"db_header.h"
#include	<stdio.h>


/* a new change has arrived from this replica */
int replica_grow(machine_name machine, replica_no height)
{
    int result;
    replica_no watermark;
    char buf[256];
    item key;

    key = str_key(machine);

    watermark = 0;
    result = read_db(key, &watermark, sizeof(watermark), (xdrproc_t)xdr_replica_no, REPLICAS);
    if (result != C_OK && (result != C_NOMATCH || height != 0))
	return -1;
    if (height < 0)
	return watermark;
    
    if (height < watermark)
    {
	sprintf(buf,"MANAGER PROBLEM: changes must be recieved in increasing order (%ld > %ld)\n",
		(long)height, (long)watermark); 
	shout(buf);
    }
	
    watermark = height;
    result = write_db(key,&watermark, (xdrproc_t)xdr_replica_no, REPLICAS);
    if (result != C_OK)
	return -1;
    return watermark;
}

book_changeres replica_add(machine_name server)
{
    book_changeres result;
    int rep_num;
    item key;
    
    key = str_key(server);
    
    result = read_db(key, &rep_num, sizeof(rep_num), (xdrproc_t)xdr_replica_no, REPLICAS);
    if (result == C_NOMATCH) {
	result = replica_grow(server,0)>= 0 ? C_OK:C_SYSERR;
	change_del_changes(server, -1); /* delete all old changes from this replica */
    }
    /* if the read was ok, then try not to worry, I think it is probably safer */
    if (result == C_SYSERR) shout("replica grow failed\n");
    return result;
}

/* this change should always (appear to) be made to the exiting server
 * This discourages confusion if a server is removed and
 * recreated soon after
 */
book_changeres replica_del(machine_name server)
{
    book_changeres result;
    static replica_no rep_num = 0;
    item key;

    key = str_key(server);
    
    result = read_db(key, &rep_num, sizeof(rep_num), (xdrproc_t)xdr_replica_no, REPLICAS);
    if (result != C_OK)
	return result;
    change_del_changes(server, 1); /* make sure change one is gone so that when server gets
				    * re-added, these changes wont go
				    */
    return delete_from_db(key, REPLICAS);

}

/* only called when creating a brand new database */
int replica_init()
{
    static char * ServerName;
    book_changeres res;
    extern char * get_myhostname();

    ServerName = get_myhostname();
    res = replica_add(ServerName);
    return res == C_OK;
}
