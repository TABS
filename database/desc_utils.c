
#include	"db_header.h"


void free_slot(description * slot)
{
    /* this will occur if there is no space allocated for the */
    /* bitstring..    This should not occur too often!! */
    if ((void *) slot->item_val == (void *) slot)
	bailout("Memory allocation problem in freeing slot!!!\n",9);
    
    free(slot->item_val);
    free (slot);
}

description new_desc()
{
    description result;
    result.item_len = BITSTRLEN;
    result.item_val = (char *) calloc(BITSTRLEN,
				      sizeof(char));
    
    return(result);
}

void set_a_bit(description * desc, int i)
{
    ((signed char *)desc->item_val)[i / 8] |= (signed char)(0x80 >> (i % 8));
}

/* DEL_A_BIT: delete a single bit, return status that indicates if was */
 /* already set */
bool_t del_a_bit(description * desc, int i)
{
    bool_t status = query_bit(desc, i);
    ((signed char *)desc->item_val)[i / 8] &= ~(signed char)(0x80 >> (i % 8));
    return(status);
}

bool_t query_bit(description * desc, int num)
{
    return(((signed char)(desc->item_val[num / 8] &
	    (signed char)(0x80 >>( num %8))) !=  (signed char)0)?
	    TRUE  :	FALSE); 
}

/* FULL_DESC: sets all the bits in the set */
void full_desc(description * desc)
{
    int i;
    for (i=0;i <desc->item_len;i++)
	desc->item_val[i] = (signed char)0xff;
}    

void zero_desc(description * desc)
{
    int i;
    for (i=0;i <desc->item_len;i++)
	desc->item_val[i] = (signed char)0x00;
}    

void desc_add(description d1, description d2)
{
    int i;
    for (i=0 ; i<d1.item_len ; i++)
	d1.item_val[i] |= d2.item_val[i];
}

void desc_and(description d1, description d2)
{
    int i;
    for (i=0 ; i<d1.item_len ; i++)
	d1.item_val[i] &= d2.item_val[i];
}

void desc_sub(description d1, description d2)
{
    int i;
    for (i=0 ; i<d1.item_len ; i++)
	d1.item_val[i] &= ~d2.item_val[i];
}

/* NULL_DESC: returns true if no bits in the description are set */
bool_t null_desc(description * desc)
{
    int i;
    for (i=0;i < desc->item_len;i++)
	if ((signed char)desc->item_val[i] != (signed char)0x00)
	    return(FALSE);
    
    return(TRUE);
}

description invert_desc(description * desc)
{
    description result = new_desc();
    int i;
    for (i=0;i < desc->item_len;i++)
	((signed char *)result.item_val)[i] = (signed char)
	    ~ desc->item_val[i];
    return result;
    
}


/*  REQUIRED_BITS: returns TRUE if only the bits set in the template */
 /*  are set in the testcase */
bool_t required_bits(description * template, description * testcase)
{
    int i;
    for (i =0 ; i < BITINDMAX; i++)
	if (query_bit(template, i) && ! query_bit(testcase, i))
	    return FALSE;
    return TRUE;
    
}

/* DESC_MEMBER: returns TRUE if all the bits in the master appear in */
 /* the slavlabe*/
bool_t desc_member(description * master, description * slave)
{
    int i;
    
    for (i = 0;i < master->item_len;i++)
    {
	 /* 
	printf("%d = %x\n",i,(int)((char)~master->item_val[i] &
	       (char) slave->item_val[i]));
	*/
	if ((char)((char)~master->item_val[i] & (char)slave->item_val[i]) != (char)0)
	    return FALSE;
    }
    
    return TRUE;
    
}

void desc_cpy(description *dest, description *src)
{

    dest->item_len = src->item_len;
    dest->item_val = memdup(src->item_val, src->item_len);
    
}

bool_t mandatory_attrs(item *mand, item *d1, item *d2)
{
    /* true if all manadatory bit in d1 are also in d2 */
    int i;
    for (i=0; i<BITINDMAX ; i++)
	if (query_bit(mand,i))
	    if (query_bit(d1,i) && ! query_bit(d2, i))
		return FALSE;
    return TRUE;
}

bool_t desc_eql(item d1, item d2)
{
    int i;
    for (i=0 ; i < BITSTRLEN ; i++)
	if (d1.item_val[i] != d2.item_val[i])
	    return FALSE;
    return TRUE;
}

description * desc_dup(description * src)
{
    description * result;
    
    result = (description *) malloc(sizeof(description));
    /* result->item_val = (char *) malloc(src->item_len); */
    
    result->item_len = src->item_len;
    result->item_val = memdup(src->item_val, src->item_len);

    return(result);
}


int desc_2_labind(description *d)
{
    int i;
    for (i=0; i< BITINDMAX ; i++)
    {
	if (query_bit(&attr_types.lab, i)
	    && query_bit(d, i))
	    return i;
    }
    return -1;
}


void slot2bookkey(slotgroup *slot, book_key *key)
{
    /* key is 3 bytes/24 bits.
     * these are used
     *  7:pod, 9:doy, 8:lab
     */
    int pod, doy, lab;
    int k;
    pod = slot->pod;
    doy = slot->doy;
    lab = desc_2_labind(&slot->what);
    
    k = (pod<<17) | (doy <<8) | lab;
    *key = k;
}
