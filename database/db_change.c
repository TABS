/* this module is for the changes that are made to the database */

#include	<sys/types.h>
#include	"db_header.h"
#include	<time.h>
#include	<stdio.h>

char log_head_key[] = "__CHANGE_HEADER__";

log_header replica_state;
char *replica_machine = NULL;

GDBM_FILE log_file = NULL;
extern update_status update_st;

static datum mk_host_chkey(char *host, int num)
{
	datum key;
	key.dptr = (char*)malloc(strlen(host)+ 20);
	sprintf(key.dptr, "%s_%d_", host, num);
	key.dsize = strlen(key.dptr);
	return key;
}

static datum mk_log_header_key(char *host)
{
	datum key;
	key.dptr = (char*)malloc(strlen(host)+30);
	strcpy(key.dptr, host);
	strcat(key.dptr, log_head_key);
	key.dsize = strlen(key.dptr);
	return key;
}

void get_replica_header(char *host)
{
	datum key, value;
	if (replica_machine && strcmp(replica_machine, host)==0)
		return;
	free(replica_machine); replica_machine = NULL;
	key = mk_log_header_key(host);
	replica_machine = strdup(host);
    
	value  = gdbm_fetch(log_file, key);
	if (value.dptr == NULL)
	{
		replica_state.lastchangenum = 0;
		replica_state.lowestchangenum = 1;
	}
	else
	{
		bcopy(value.dptr, &replica_state, sizeof(replica_state));
		free(value.dptr);
	}
	free(key.dptr);
}

void sync_replica_header()
{
	datum key, content;
    
	if (replica_machine)
	{
		key = mk_log_header_key(replica_machine);
		content.dptr = (char*)&replica_state;
		content.dsize = sizeof(replica_state);
		gdbm_store(log_file, key, content, GDBM_REPLACE);
		free(key.dptr);
	}
}

/* open the change database.  We have to read the header to ensure */
 /* that it is there.. Perhaps this should be CREATE rather than INIT */
int change_init(void)
{
	char buf[256];
    
	log_file = gdbm_open(make_pathname(LogFile), 1024, GDBM_WRCREAT, 0600, 0);

	if (log_file == NULL) {
		sprintf(buf, "Cannot open Log database: %s, error number %d\n",LogFile,gdbm_errno);
		bailout(buf, 4);
		return 0;
	}
	return 1;
}

void close_change(void)
{
	if (log_file  == NULL) return;
	sync_replica_header();
	gdbm_close(log_file);
	log_file = NULL;
}


/* commit takes a change and writes it to the change file, updating */
 /* the header so that the update number and lastchangestamp are */
 /* correct.  It then writes the change, and finally the header.  The */
 /* key is the change number preceded by some string so we will never */
 /* guess it*/
book_changeres commit(book_changeent *ch)
{
	datum key, content;


	get_replica_header(ch->machine);

	if (ch->changenum == 0)
		ch->changenum = replica_state.lastchangenum+1;

	if (replica_state.lastchangenum < replica_state.lowestchangenum)
		replica_state.lowestchangenum = ch->changenum;
	else
		if (ch->changenum != replica_state.lastchangenum+1)
		{
			char line[512];
			sprintf(line,"change number out of synch: low=%d last=%d this=%d\n", replica_state.lowestchangenum, replica_state.lastchangenum, ch->changenum);
			shout(line);
			/* failed e.g.
			   Jun 12 11:45:48 change number out of synch: low=27488 last=34906 this=35031
			*/
/*	    return C_SYSERR;*/
		}
    
	replica_state.lastchangenum = ch->changenum;

	key = mk_host_chkey(ch->machine, ch->changenum);

	if (datum_encode(&content, ch, (xdrproc_t)xdr_book_changeent)== FALSE)
		return C_XDR;

	if (gdbm_store(log_file, key, content,GDBM_REPLACE) != 0 )
	{
		update_st = NONE;
		/* should restore previous change state FIXME */
		return C_DUP ;
	}

	free(key.dptr);
    
	/* now write out the change header */
	sync_replica_header();
	return C_OK;
}

void free_changes(change_reply reply)
{
	int i;

    
	if (reply.error != CR_RESOK)
		return;
	if (reply.change_reply_u.data.changelist_len == 0)
		return;
	for (i=0;i < reply.change_reply_u.data.changelist_len;i++)
		xdr_free((xdrproc_t)xdr_book_changeent,
			 (char*) &reply.change_reply_u.data.changelist_val[i]);
	free(reply.change_reply_u.data.changelist_val);
}


/* returns the changes starting from number LASTNUM */
/* the changes are unXDR'd as they have to be passed over the network */
change_reply *SVC(last_replica_updates_2)(update_request *upr, struct svc_req *rq)
{
    static change_reply result;
    int count;
    int NumberOfChanges;

    get_replica_header(upr->replica);
    NumberOfChanges = replica_state.lastchangenum - upr->minchange;
    
    /* free the last result */
    free_changes(result);

    /* have gotten rid of this change number!!*/
    if (upr->minchange+1 < replica_state.lowestchangenum)
    {
	result.error = CR_NONE;
	return(&result);
    }

    /* make sure getting SOME changes */
    if (NumberOfChanges <= 0)
    {
	result.error = CR_NONE;
	return(&result);
    }
    
    /* this is the most changes we transfer at a time */
    if (NumberOfChanges > MAXCHANGES )
	NumberOfChanges = MAXCHANGES;
if (NumberOfChanges > 10) NumberOfChanges = 10;
    result.error = CR_RESOK;
    
    /* allocate space for the result */    
    result.change_reply_u.data.changelist_val = (book_changeent *) calloc(NumberOfChanges, sizeof(book_changeent));
    result.change_reply_u.data.changelist_len = 0;


    /* remember LASTNUM is the  last change received, want the next!*/
    for (count = (upr->minchange) + 1; count <= (upr->minchange)+ NumberOfChanges; count++)
    {
	datum key, contents;
	key = mk_host_chkey(upr->replica, count);
	
	contents = gdbm_fetch(log_file,key);
	if (contents.dptr == NULL)
	{
	    result.error = CR_NONE;
	    return &result;
	}

	datum_decode(&contents,
		     &result.change_reply_u.data.changelist_val
		      [result.change_reply_u.data.changelist_len++ ],
		     (xdrproc_t)xdr_book_changeent);
	
	/* free the last lot of data fetched */
	free(contents.dptr);
	free(key.dptr);
    }
    
    return(&result);
}


/* this is useless without a reorg at the end to free up the space */
void change_del_changes(char *replica, int highest)
{
    /* delete all changes from replica < highest
     * if highest == -1, delete all
     * if highest > 1, delete at most 100 changes and none younger than 1 day...
     */

    int chnum;
    int last = highest;
    get_replica_header(replica);

    if (tracing)
	printf("database: del changes %d for %s - have %d-%d\n",
	       highest, replica, replica_state.lowestchangenum, replica_state.lastchangenum);
    if (highest == -1)
	last = replica_state.lastchangenum+1;

    chnum = replica_state.lowestchangenum;
    if (chnum <= 0)
	chnum = 1;

    if (highest > 1)
    {
	time_t now, then;
	int nextlast;
	now = then = time(0);
	if (chnum + 100 < last)
	    last = chnum+100;
	if (last > replica_state.lastchangenum+1)
	    last = replica_state.lastchangenum+1;
	nextlast = last;
	while (last > chnum && then > now-24*60*60)
	{
	    datum key, contents;
	    last = nextlast;
	    nextlast = (chnum + last)/2;
	    key = mk_host_chkey(replica, last);
	    contents = gdbm_fetch(log_file, key);
	    if (contents.dptr) {
		book_changeent thech;
		memset(&thech, 0, sizeof(thech));
		if (datum_decode(&contents,
			     &thech, (xdrproc_t)xdr_book_changeent)) {
		    then = thech.time;
		    xdr_free((xdrproc_t)xdr_book_changeent, (char*) &thech);
		}
	    }
	}
    }
    if (tracing)
	printf("database:   will remove %d-%d\n", chnum, last-1);
    for (; chnum < last ; chnum++)
    {
	datum key;
	key = mk_host_chkey(replica, chnum);
	gdbm_delete(log_file, key);
	free(key.dptr);
    }
    if (highest == -1)
    {
	replica_state.lowestchangenum = 1;
	replica_state.lastchangenum = 0;
    }
    else
	replica_state.lowestchangenum = chnum;
    sync_replica_header();
}


bool_t *SVC(delete_changes_2)(update_request *req, struct svc_req *rpstp)
{
    static  bool_t result;
    result = TRUE;

    svc_sendreply(rpstp->rq_xprt, (xdrproc_t) xdr_bool, (caddr_t)&result);

    change_del_changes(req->replica, req->minchange);
    
    return(NULL);
    
}

